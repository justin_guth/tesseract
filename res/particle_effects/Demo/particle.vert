#version 450 core

layout(location = 0) in float pBirthTime;
layout(location = 1) in vec3 pDirection;

uniform mat4 uModel;
uniform vec3 uViewPosition;
uniform mat4 uProjectionView;

uniform vec3 uOrigin;
uniform float uTime;
uniform float uMaxLifeTime;
uniform float uInitialSize;
uniform float uInitialSpeed;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
    vec3 debugColour;
} vertOut;

void main() {
    float age = uTime - pBirthTime;
    float agePercentage = min(age / uMaxLifeTime, 1);

    vec4 worldPos = uModel * vec4(uOrigin + age * uInitialSpeed * pDirection, 1);
    vertOut.position = worldPos.xyz / worldPos.w;
    vec4 homogenizedPosition = vec4(vertOut.position, 1.0);
    gl_Position = uProjectionView * homogenizedPosition;

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    vertOut.size = uInitialSize * (1.0 - agePercentage);

    vertOut.debugColour = abs(pDirection);//vec3(0,uTime,0);// * currentTime;//(1-agePercentage) * abs(pDirection);//vec3(uTimePassed / (float(uMaxLifeTime) / 1000.0));
    if(uTime < 0) vertOut.debugColour = vec3(1,0,0);
    //if(uTimePassed > float(uMaxLifeTime) / 1000.0) vertOut.debugColour = vec3(1,1,0);
}
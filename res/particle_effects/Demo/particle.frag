#version 450 core

in vec3 debugColour;

out vec4 FragColour;

void main() {
    FragColour = vec4(1.5 * debugColour,0.6);
}
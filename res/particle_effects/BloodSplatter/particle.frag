#version 450 core

uniform vec3 uBloodColour;
in vec3 colour;

layout(location = 0) out vec4 FragColour;

void main() {
    FragColour = vec4(uBloodColour,1);
    //FragColour = vec4(colour, 1);
}
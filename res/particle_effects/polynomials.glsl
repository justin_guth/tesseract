#ifndef POLYNOMIALS
#define POLYNOMIALS

struct QuadraticFunction
{
    float a, b, c;
};

float eval(QuadraticFunction p, float t)
{
    return (p.a * t + p.b) * t + p.c;
} 

void zeroCrossings(QuadraticFunction p, out float small, out float large)
{
    float d = sqrt(p.b * p.b - 4 * p.a * p.c);
    float sub = (-p.b - d) / (2 * p.a);
    float add = (-p.b + d) / (2 * p.a);
    small = min(sub, add);
    large = max(sub, add);
}

#endif
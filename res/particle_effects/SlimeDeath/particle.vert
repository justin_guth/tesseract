#version 450 core

#include "../../res/particle_effects/polynomials.glsl"

layout(location = 0) in float pBirthTime;
layout(location = 1) in vec3 pSpawn;

uniform mat4 uModel;
uniform vec3 uViewPosition;
uniform mat4 uProjectionView;

uniform float uTime;
uniform float uMaxLifeTime;
uniform float uInitialSize;
uniform float uGravityConstant;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
    vec3 colour;
} vertOut;

void main() {
    float age = uTime - pBirthTime - 0.3 - 0.5 * pSpawn.z;
    float agePercentage = min(age / uMaxLifeTime, 1);

    QuadraticFunction height = { -0.5 * uGravityConstant, 0, pSpawn.z };

    float timeOfGroundContact;
    float irrelevant; // REVIEW: Find out if anonymous parameters are somehow possible in GLSL
    zeroCrossings(height, irrelevant, timeOfGroundContact);

    age = min(max(age, 0), timeOfGroundContact);

    float zPos = eval(height, age);

    vec4 worldPos = vec4(vec3(pSpawn.xy, zPos), 1);
    vertOut.position = worldPos.xyz / worldPos.w;

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    
    float sizeFactor = (1.0 - agePercentage * agePercentage * agePercentage * agePercentage);
    vertOut.size = uInitialSize * sizeFactor;
    vertOut.colour = vec3(vertOut.position.z);
}
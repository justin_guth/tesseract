#version 450 core

#include "../../res/shaders/turbulence_functions.glsl"

layout(location = 0) in float pPathProgress;

uniform vec3 uViewPosition;
uniform mat4 uProjectionView;
uniform mat4 uModel;

uniform vec3 uSource;
uniform vec3 uTarget;
uniform float uSize;
uniform float uTime;
uniform float uNoiseSeed;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
} vertOut;

void main() {

    vec3 globalSourcePos = (uModel * vec4(uSource, 1)).xyz;
    vec3 dir = uTarget - globalSourcePos;

    vertOut.position = globalSourcePos + pPathProgress * dir;

    vec3 disturbanceNormal = normalize(cross(vec3(0, 0, 1), dir));
    float disturbance = turbulence_2D(vertOut.position.xy + uTime * 2 + uNoiseSeed, 0.3, 0.5, 2, 3);

    vertOut.position += disturbance * disturbanceNormal;

    vec4 homogenizedPosition = vec4(vertOut.position, 1.0);
    gl_Position = uProjectionView * homogenizedPosition;

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    vertOut.size = uSize;
}
#version 450 core

in vec4 colour;

layout(location = 0) out vec4 FragColour;

void main() {
    FragColour = colour;
}
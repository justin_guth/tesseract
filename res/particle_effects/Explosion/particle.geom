#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform mat4 uProjectionView;

in VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
    vec4 colour;
} geomIn[];

flat out vec4 colour;

void main()
{
    vec3 up = normalize(cross(geomIn[0].cameraDirection, vec3(1,0,0)));
    vec3 right = cross(geomIn[0].cameraDirection, up);
    vec3 pos;

    colour = geomIn[0].colour;
    pos = geomIn[0].position - geomIn[0].size * up;
    gl_Position = uProjectionView * vec4(pos, 1.0);
    EmitVertex();
    pos = geomIn[0].position + geomIn[0].size * right;
    gl_Position = uProjectionView * vec4(pos, 1.0);
    EmitVertex();
    pos = geomIn[0].position - geomIn[0].size * right;
    gl_Position = uProjectionView * vec4(pos, 1.0);
    EmitVertex();
    pos = geomIn[0].position + geomIn[0].size * up;
    gl_Position = uProjectionView * vec4(pos, 1.0);
    EmitVertex();
    EndPrimitive();
}
#version 450 core

layout(location = 0) in float pBirthTime;
layout(location = 1) in vec3 pSpawn;
layout(location = 2) in vec3 pDirection;
layout(location = 3) in float pMaxSize;

uniform mat4 uModel;
uniform vec3 uViewPosition;
uniform mat4 uProjectionView;

uniform float uTime;
uniform float uMaxLifeTime;
uniform float uMaxSize;
uniform float uSpeed;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
    vec4 colour;
} vertOut;

void main() {
    float age = uTime - pBirthTime;
    float agePercentage = min(age / uMaxLifeTime, 1);

    vec4 worldPos = uModel * vec4(pSpawn + age * uSpeed * pDirection, 1);
    vertOut.position = worldPos.xyz / worldPos.w;
    vec4 homogenizedPosition = vec4(vertOut.position, 1.0);
    gl_Position = uProjectionView * homogenizedPosition;

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    
    // Up to the apex, the size should grow linearly
    float sizeLine = max(0.3 + 3.5 * agePercentage, 0);
    // Past the apex, size should follow the parabola -1.5625(x-0.2)^2 + 1
    float apex = agePercentage - 0.2;
    float sizeParabola = max(-1.5625 * apex * apex + 1, 0);
    float sizeFactor = min(sizeLine, sizeParabola);
    vertOut.size = pMaxSize * uMaxSize * sizeFactor;

    vec4 fireColour = vec4(3, 1.5, 1.2, 0.2);
    vec4 smokeColour = vec4(0.1, 0.1, 0.12, 0.3);
    float colourFactor = min(agePercentage * 5, 1);

    vertOut.colour = colourFactor * smokeColour + (1 - colourFactor) * fireColour;
}
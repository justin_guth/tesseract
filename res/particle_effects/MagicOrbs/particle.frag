#version 450 core

in vec3 ParticleCentre;
in vec3 FragPos;
in float OrbSize;

layout(location = 0) out vec4 FragColour;

void main() {
    vec3 offsetFromCentre = FragPos - ParticleCentre;
    float distanceFromCentre = length(offsetFromCentre);
    float normalizedDistance = distanceFromCentre / OrbSize;

    vec3 baseColour = 1.2 * vec3(0.9, 0.95, 1);

    vec3 circularizedColour = baseColour * (1.05 - pow(normalizedDistance, 2));
    float opacity = 0.9 - 10 * pow(normalizedDistance, 6);
    if(opacity < 0.05) discard;
    FragColour = vec4(circularizedColour, opacity);
}
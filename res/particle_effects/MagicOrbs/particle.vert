#version 450 core

layout(location = 0) in float pBirthTime;
layout(location = 1) in vec3 pSpawn;
layout(location = 2) in vec3 pDirection;

uniform vec3 uViewPosition;
uniform mat4 uProjectionView;

uniform float uTime;
uniform float uMaxLifeTime;
uniform float uInitialSize;
uniform float uSpeed;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
} vertOut;

void main() {
    float age = uTime - pBirthTime;
    float agePercentage = min(age / uMaxLifeTime, 1);

    vertOut.position = pSpawn + age * uSpeed * pDirection;
    vec4 homogenizedPosition = vec4(vertOut.position, 1.0);
    gl_Position = uProjectionView * homogenizedPosition;

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    vertOut.size = uInitialSize * (1.0 - agePercentage * agePercentage * agePercentage * agePercentage);
}
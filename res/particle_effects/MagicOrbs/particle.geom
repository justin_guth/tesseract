#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform mat4 uProjectionView;

in VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
} geomIn[];

flat out vec3 ParticleCentre;
flat out float OrbSize;
out vec3 FragPos;

void main()
{
    vec3 up = normalize(cross(geomIn[0].cameraDirection, vec3(1,0,0)));
    vec3 right = cross(geomIn[0].cameraDirection, up);

    ParticleCentre = geomIn[0].position;
    OrbSize = geomIn[0].size;
    FragPos = geomIn[0].position - geomIn[0].size * up;
    gl_Position = uProjectionView * vec4(FragPos, 1.0);
    EmitVertex();
    FragPos = geomIn[0].position + geomIn[0].size * right;
    gl_Position = uProjectionView * vec4(FragPos, 1.0);
    EmitVertex();
    FragPos = geomIn[0].position - geomIn[0].size * right;
    gl_Position = uProjectionView * vec4(FragPos, 1.0);
    EmitVertex();
    FragPos = geomIn[0].position + geomIn[0].size * up;
    gl_Position = uProjectionView * vec4(FragPos, 1.0);
    EmitVertex();
    EndPrimitive();
}
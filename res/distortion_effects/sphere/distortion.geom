

#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform mat4 uProjectionView;

in VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
} geomIn[];

out vec2 FragPos;

void main()
{
    vec3 up = normalize(cross(geomIn[0].cameraDirection, vec3(1,0,0)));
    vec3 right = cross(geomIn[0].cameraDirection, up);

    FragPos = vec2(0,-1);
    gl_Position = uProjectionView * vec4(geomIn[0].position - geomIn[0].size * up, 1.0);
    EmitVertex();
    FragPos = vec2(1,0);
    gl_Position = uProjectionView * vec4(geomIn[0].position + geomIn[0].size * right, 1.0);
    EmitVertex();
    FragPos = vec2(-1,0);
    gl_Position = uProjectionView * vec4(geomIn[0].position - geomIn[0].size * right, 1.0);
    EmitVertex();
    FragPos = vec2(0,1);
    gl_Position = uProjectionView * vec4(geomIn[0].position + geomIn[0].size * up, 1.0);
    EmitVertex();
    EndPrimitive();
}
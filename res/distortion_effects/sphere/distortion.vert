#version 450 core

layout(location = 0) in float dSize;

uniform vec3 uViewPosition;
uniform mat4 uModel;

out VertOut {
    float size;
    vec3 position;
    vec3 cameraDirection;
} vertOut;

void main() {

    vertOut.position = uModel[3].xyz;
    vec4 homogenizedPosition = vec4(vertOut.position, 1.0);

    vertOut.cameraDirection = normalize(uViewPosition - vertOut.position);
    vertOut.size = dSize;
}
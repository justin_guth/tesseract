#version 450 core

in vec2 FragPos;

layout(location = 0) out vec2 FragColour;

void main() {
    vec2 extremized_pos = 2 * FragPos;
    float l = length(extremized_pos);
    float dl = pow(l, 3);
    FragColour = step(l,1) * 0.5 * dl * extremized_pos;
}


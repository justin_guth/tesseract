import os
from PIL import Image
import json




# texture = "game/spritesheets/player2"
# path = "vendor/Adventurer-1.5/Individual Sprites"
# target = f"textures/{texture}.png"
# configTarget = "atlases/game/player2.json"
# cols = 7
# rows = 16
# spriteWidth = 50
# spriteHeight = 37

texture = "game/spritesheets/slime"
path = "vendor/Slime/Individual Sprites"
target = f"textures/{texture}.png"
configTarget = "atlases/game/slime.json"
cols = 8
rows = 3
spriteWidth = 32
spriteHeight = 25



files = os.walk(path)

resultImage = Image.new(
    mode = "RGBA",
    size = (cols * spriteWidth, rows * spriteHeight),
    color = (0, 0, 0, 0)
)

spritesheetJson = {
    "Texture": texture,
    "Type": "grid",
    "Columns": cols,
    "Rows": rows,
    "Textures": {}
}

count = 0

for dirpath, dirnames, filenames in files:

    for file in filenames:

        name, ext = os.path.splitext(file)
        print(name)

        col = count % cols
        row = count // cols

        spritesheetJson["Textures"][name] = {

            "Column": col,
            "Row": row,
            "Width": 1,
            "Height": 1
        }

        sprite = Image.open(os.path.join(dirpath, file), "r")

        resultImage.paste(sprite, (col * spriteWidth, row * spriteHeight))

        count += 1

    break

resultImage.save(target)

with open(configTarget, "w") as t:

    t.write(json.dumps(spritesheetJson))
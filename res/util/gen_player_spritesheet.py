from PIL import Image

resultPath = "textures/game/spritesheets/playre.png"
quadWidth = 128
quadHeight = 64
files = [
    ("idle", "vendor/Knight/noBKG_KnightIdle_strip.png", 15),
    ("run", "vendor/Knight/noBKG_KnightAttack_strip.png", 22),
    ("run", "vendor/Knight/noBKG_KnightDeath_strip.png", 15),
   # ("run", "vendor/Knight/noBKG_KnightJumpAndFall_strip.png", 14),
   # ("run", "vendor/Knight/noBKG_KnightRoll_strip.png", 15),
    ("run", "vendor/Knight/noBKG_KnightRun_strip.png", 8),
    ("run", "vendor/Knight/noBKG_KnightShield_strip.png", 7),
]

maxWidth = 0

for _1, _2, count in files:

    maxWidth = count if count > maxWidth else maxWidth

result = Image.new(
    mode = "RGBA",
    size = (maxWidth * quadWidth, quadHeight * len(files)),
    color = (0, 0, 0, 0))

row = 0

for type, source, count in files:

    strip = Image.open(source, "r")
    stride = strip.size[0] // count
    

    for i in range(count):

        base = i * stride + ((stride - quadWidth)// 2)
        sprite = strip.crop((base, 0, base + quadWidth, 0 + strip.size[1]))
        result.paste(sprite, (i * quadWidth , row * quadHeight) )

    row += 1

result.save(resultPath)
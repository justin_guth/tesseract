#ifndef PBR
#define PBR

#include "../../res/shaders/constants.glsl"

in vec3 WorldPosition;
uniform vec3 uViewPosition;

struct PointLight {
    vec3 Position;
    vec3 Color;
};

#define MAX_POINT_LIGHTS 64
uniform int uPointLightCount;
uniform PointLight uPointLights[MAX_POINT_LIGHTS];

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}  

float DistributionGGX(vec3 normal, vec3 halfway, float roughness)
{
    float a = roughness * roughness;
    float aSquared = a * a;
    float normalDotHalfway = max(dot(normal, halfway), 0.0);
    float normalDotHalfwaySquared = normalDotHalfway * normalDotHalfway;
	
    float denominator = (normalDotHalfwaySquared * (aSquared - 1.0) + 1.0);
    denominator = PI * denominator * denominator;
	
    return aSquared / denominator;
}

float GeometrySchlickGGX(float normalDotView, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float denominator = normalDotView * (1.0 - k) + k;
	
    return normalDotView / denominator;
}

float GeometrySmith(vec3 normal, vec3 view, vec3 light, float roughness)
{
    float normalDotView = max(dot(normal, view), 0.0);
    float normalDotLight = max(dot(normal, light), 0.0);
    float ggx2  = GeometrySchlickGGX(normalDotView, roughness);
    float ggx1  = GeometrySchlickGGX(normalDotLight, roughness);
	
    return ggx1 * ggx2;
}

vec3 GetPointLightContribution(PointLight p, vec3 viewDirection, vec3 normal, float roughness, float metallic, vec3 albedo, vec3 F0)
{
        // calculate per-light radiance
        vec3 light = normalize(p.Position - WorldPosition);
        vec3 halfway = normalize(viewDirection + light);
        float dist    = length(p.Position - WorldPosition);
        float attenuation = 1.0 / (dist * dist);
        vec3 radianceIncrement     = p.Color * attenuation;        
        
        
        // cook-torrance brdf
        float normalDistributionFactor = DistributionGGX(normal, halfway, roughness);        
        float geometry   = GeometrySmith(normal, viewDirection, light, roughness);      
        vec3 fresnel    = fresnelSchlick(max(dot(halfway, viewDirection), 0.0), F0);       
        
        

        vec3 kS = fresnel;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;	  
        

        vec3 numerator    = normalDistributionFactor * geometry * fresnel;
        float denominator = 4.0 * max(dot(normal, viewDirection), 0.0) * max(dot(normal, light), 0.0) + 0.0001;
        vec3 specular     = numerator / denominator;  
            
        // add to outgoing radiance
        float normalDotLight = max(dot(normal, light), 0.0);                
        return (kD * albedo / PI + specular) * radianceIncrement * normalDotLight; 
}

vec3 GetColour(vec3 albedo, vec3 normal, float metallic, float roughness, float ao)
{
    vec3 view = normalize(uViewPosition - WorldPosition);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);
	           
    // reflectance equation
    vec3 radiance = vec3(0.0);
    
    for(int i = 0; i < uPointLightCount; ++i)
    {
        radiance += GetPointLightContribution(
            uPointLights[i],
            view, 
            normalize(normal),
            roughness,
            metallic,
            albedo,
            F0
        );
    }
    
    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + radiance;
	
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));  
    
    return color;
}

#endif
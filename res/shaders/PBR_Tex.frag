#version 450 core

// PBR_Tex.frag

#include "../../res/shaders/textured_PBR.glsl"

uniform vec4 uMaterialDiffuseTint; //TODO: Use this
layout(location = 0) out vec4 FragColour;

void main()
{
    vec3 colour = GetColourTextured();
    FragColour = vec4(colour, 1.0);
}
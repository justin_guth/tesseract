#ifndef RANDOM
#define RANDOM

uint hash(uint s)
{
    s ^= 2747636419u;
    s *= 2654435769u;
    s ^= s >> 16;
    s *= 2654435769u;
    s ^= s >> 16;
    s *= 2654435769u;
    return s;
}

uint hash(int s) 
{
    return hash(uint(s));
}

float random(int seed)
{
    return float(hash(seed)) / 4294967295.0; // 2^32-1
}

float random_from_2D(ivec2 seed)
{
    return float(hash(seed.y + hash(seed.x))) / 4294967295.0; // 2^32-1
}

float random_from_3D(ivec3 seed)
{
    return float(hash(seed.z + hash(seed.y + hash(seed.x)))) / 4294967295.0; // 2^32-1
}

#endif
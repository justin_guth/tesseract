#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 uModel;
uniform mat4 uNormal;

out vec3 Normal; // The vertex normal
out vec3 FragPos; // The position of the vertexin world space coordinates

out VertOut
{
    vec3 Normal; // The vertex normal
    vec3 FragPos; // The position of the vertexin world space coordinates
} vertOut;

void main()
{
    vertOut.Normal = normalize(vec3(uNormal * vec4(vNormal, 0.0)));
    vertOut.FragPos = (uModel * vec4(vPosition, 1)).xyz;
}

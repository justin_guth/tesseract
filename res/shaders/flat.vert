#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 uModel;
uniform mat4 uNormal;
uniform mat4 uProjectionViewModel;

void main() {

    vec4 homogenizedPosition = vec4(vPosition, 1.0);
    gl_Position = uProjectionViewModel * homogenizedPosition;
}
#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 uModel;
uniform mat4 uNormal;
uniform mat4 uProjectionViewModel;

out vec3 Normal; // The vertex normal
out vec3 FragPos; // The position of the vertexin world space coordinates
out vec2 TextureCoordinate;
out mat3 TBN;

void main() {

    vec3 T = normalize(vec3(uModel * vec4(vTangent, 0.0)));
    vec3 B = normalize(vec3(uModel * vec4(vBitangent, 0.0)));
    vec3 N = normalize(vec3(uModel * vec4(vNormal, 0.0)));
    TBN = mat3(T, B, N);

    vec4 homogenizedPosition = vec4(vPosition, 1.0);
    gl_Position = uProjectionViewModel * homogenizedPosition;
    Normal = vec3(uNormal * vec4(vNormal, 0.0f));
    FragPos = vec3(uModel * homogenizedPosition);
    TextureCoordinate = vTextureCoordinate;
}
#ifndef DISPLACEMENT
#define DISPLACEMENT

#include "../../res/shaders/heightmaps.glsl"

// Dummy offset
#ifndef OFFSET_TURBULENCE
#define OFFSET_TURBULENCE(p) 0
#endif

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 uModel;
uniform mat4 uNormal;
uniform mat4 uProjectionViewModel;

out vec3 WorldPosition; // The position of the vertexin world space coordinates
out vec2 TextureCoordinates;
out mat3 TBN;

void set_tangent_space_and_position()
{
    vec3 n = normalize(vNormal), t = normalize(vTangent), b = normalize(vBitangent);
    tangent_space(vPosition, HEIGHTMAP_EPS, n, t, b);

    vec3 T = normalize(vec3(uModel * vec4(t, 0.0)));
    vec3 B = normalize(vec3(uModel * vec4(b, 0.0)));
    vec3 N = normalize(vec3(uNormal * vec4(n, 0.0)));
    TBN = mat3(T, B, vNormal);

    vec3 position = vPosition + normalize(vNormal) * OFFSET_TURBULENCE(vPosition);

    vec4 homogenizedPosition = vec4(position, 1.0);
    gl_Position = uProjectionViewModel * homogenizedPosition;
    WorldPosition = vec3(uModel * homogenizedPosition);
    TextureCoordinates = vTextureCoordinate;
}

#endif
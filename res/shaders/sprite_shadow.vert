#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;
layout(location = 5) in int vTextureIndex;
layout(location = 6) in vec4 vFrameData;
layout(location = 7) in vec4 vTint;

out vec2 inTextureCoordinates;
out int inTextureIndex;
out vec4 inFrameData;

void main()
{
    inTextureCoordinates = vTextureCoordinate;
    inTextureIndex = vTextureIndex;
    inFrameData = vFrameData;
    gl_Position = vec4(vPosition, 1.0);
}  
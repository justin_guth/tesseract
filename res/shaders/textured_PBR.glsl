#ifndef TEXTURED_PBR
#define TEXTURED_PBR

#include "../../res/shaders/PBR.glsl"

in mat3 TBN;
in vec2 TextureCoordinates;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uMetallicTexture;
uniform sampler2D uRoughnessTexture;
uniform sampler2D uAOTexture;

uniform float uTextureScale;

vec3 getNormalFromMap()
{
    //vec3 tangentNormal = texture(uNormalTexture, TextureCoordinates).xyz; //FIXME Some normal issue in lighting
    vec3 tangentNormal = texture(uNormalTexture, TextureCoordinates).xyz * 2.0 - 1.0;

    // vec3 Q1  = dFdx(WorldPosition);
    // vec3 Q2  = dFdy(WorldPosition);
    // vec2 scaledTextureCoordinates = TextureCoordinates * uTextureScale;
    // vec2 st1 = dFdx(scaledTextureCoordinates);
    // vec2 st2 = dFdy(scaledTextureCoordinates);

    // vec3 N   = normalize(Normal);
    // vec3 T  = normalize(Q1 * st2.t - Q2 * st1.t);
    // vec3 B  = -normalize(cross(N, T));
    // mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

vec3 GetColourTextured()
{
    
    vec2 scaledTextureCoordinates = TextureCoordinates * uTextureScale;
    vec3 albedo     = pow(texture(uDiffuseTexture, scaledTextureCoordinates).rgb, vec3(2.2));
    vec3 normal     = getNormalFromMap().rgb;
    float metallic  = texture(uMetallicTexture, scaledTextureCoordinates).r;
    metallic = 0; //TODO Metallic isnt uploaded
    float roughness = texture(uRoughnessTexture, scaledTextureCoordinates).r;
    float ao        = texture(uAOTexture, scaledTextureCoordinates).r;

    return GetColour(
        albedo,
        normal,
        metallic,
        roughness,
        ao
    );
}

#endif
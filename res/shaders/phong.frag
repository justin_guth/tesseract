#version 450 core

#include "../../res/shaders/textured_phong.glsl"

layout(location = 0) out vec4 FragColor;

uniform vec4 uMaterialDiffuseTint;


void main() {

    vec3 result = GetColourTextured();

    //FragColor = vec4(specularStrength);
    FragColor = vec4(result, 1.0);
    //FragColor = vec4(TextureCoordinate* uTextureScale, 0.0f, 1.0f);
    //FragColor = vec4(1.0, 0.2, 0.1, 1.0);
}
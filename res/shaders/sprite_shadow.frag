#version 450 core
in vec4 FragPos;

uniform vec3 uLightPosition;
uniform float uFarPlane;

uniform sampler2D uSpriteTexture0;
uniform sampler2D uSpriteTexture1;
uniform sampler2D uSpriteTexture2;
uniform sampler2D uSpriteTexture3;
uniform sampler2D uSpriteTexture4;
uniform sampler2D uSpriteTexture5;
uniform sampler2D uSpriteTexture6;
uniform sampler2D uSpriteTexture7;
uniform sampler2D uSpriteTexture8;
uniform sampler2D uSpriteTexture9;
uniform sampler2D uSpriteTexture10;
uniform sampler2D uSpriteTexture11;
uniform sampler2D uSpriteTexture12;
uniform sampler2D uSpriteTexture13;
uniform sampler2D uSpriteTexture14;
uniform sampler2D uSpriteTexture15;
uniform sampler2D uSpriteTexture16;
uniform sampler2D uSpriteTexture17;
uniform sampler2D uSpriteTexture18;
uniform sampler2D uSpriteTexture19;
uniform sampler2D uSpriteTexture20;
uniform sampler2D uSpriteTexture21;
uniform sampler2D uSpriteTexture22;
uniform sampler2D uSpriteTexture23;
uniform sampler2D uSpriteTexture24;
uniform sampler2D uSpriteTexture25;
uniform sampler2D uSpriteTexture26;
uniform sampler2D uSpriteTexture27;
uniform sampler2D uSpriteTexture28;
uniform sampler2D uSpriteTexture29;
uniform sampler2D uSpriteTexture30;
uniform sampler2D uSpriteTexture31;

vec4 SampleWithIndex(int index, vec2 textureCoordinates)
{
    switch (index)
    {
        case 0:
         return texture(uSpriteTexture0, textureCoordinates);
        case 1:
         return texture(uSpriteTexture1, textureCoordinates);
        case 2:
         return texture(uSpriteTexture2, textureCoordinates);
        case 3:
         return texture(uSpriteTexture3, textureCoordinates);
        case 4:
         return texture(uSpriteTexture4, textureCoordinates);
        case 5:
         return texture(uSpriteTexture5, textureCoordinates);
        case 6:
         return texture(uSpriteTexture6, textureCoordinates);
        case 7:
         return texture(uSpriteTexture7, textureCoordinates);
        case 8:
         return texture(uSpriteTexture8, textureCoordinates);
        case 9:
         return texture(uSpriteTexture9, textureCoordinates);
        case 10:
         return texture(uSpriteTexture10, textureCoordinates);
        case 11:
         return texture(uSpriteTexture11, textureCoordinates);
        case 12:
         return texture(uSpriteTexture12, textureCoordinates);
        case 13:
         return texture(uSpriteTexture13, textureCoordinates);
        case 14:
         return texture(uSpriteTexture14, textureCoordinates);
        case 15:
         return texture(uSpriteTexture15, textureCoordinates);
        case 16:
         return texture(uSpriteTexture16, textureCoordinates);
        case 17:
         return texture(uSpriteTexture17, textureCoordinates);
        case 18:
         return texture(uSpriteTexture18, textureCoordinates);
        case 19:
         return texture(uSpriteTexture19, textureCoordinates);
        case 20:
         return texture(uSpriteTexture20, textureCoordinates);
        case 21:
         return texture(uSpriteTexture21, textureCoordinates);
        case 22:
         return texture(uSpriteTexture22, textureCoordinates);
        case 23:
         return texture(uSpriteTexture23, textureCoordinates);
        case 24:
         return texture(uSpriteTexture24, textureCoordinates);
        case 25:
         return texture(uSpriteTexture25, textureCoordinates);
        case 26:
         return texture(uSpriteTexture26, textureCoordinates);
        case 27:
         return texture(uSpriteTexture27, textureCoordinates);
        case 28:
         return texture(uSpriteTexture28, textureCoordinates);
        case 29:
         return texture(uSpriteTexture29, textureCoordinates);
        case 30:
         return texture(uSpriteTexture30, textureCoordinates);
        case 31:
         return texture(uSpriteTexture31, textureCoordinates);
         default:
         return vec4(0);
    }
}

in vec2 TextureCoordinates;
flat in int TextureIndex;
in vec4 FrameData;

void main()
{
    float subtextureX = FrameData.x;
    float subtextureY = FrameData.y;
    float subtextureWidth = FrameData.z;
    float subtextureHeight = FrameData.w;

    vec2 subTextureCoordinate;

    subTextureCoordinate.x = subtextureX + (TextureCoordinates.x * subtextureWidth);
    subTextureCoordinate.y = subtextureY + (TextureCoordinates.y * subtextureHeight);

    vec4 diffuseColor = SampleWithIndex(TextureIndex, subTextureCoordinate);

    if (diffuseColor.a < 0.5f) 
    {
        discard;
    }

    // get distance between fragment and light source
    float lightDistance = length(FragPos.xyz - uLightPosition);
    
    // map to [0;1] range by dividing by uFarPlane
    lightDistance = lightDistance / uFarPlane;
    
    // write this as modified depth
    gl_FragDepth = lightDistance;
}  
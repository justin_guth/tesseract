#version 450 core

layout(points) in;
layout(line_strip, max_vertices = 2) out;

uniform mat4 uProjectionView;

in VertOut
{
    vec3 Normal; // The vertex normal
    vec3 FragPos; // The position of the vertexin world space coordinates
} geomIn[];

void main()
{
    gl_Position = uProjectionView * vec4(geomIn[0].FragPos, 1);
    EmitVertex();
    gl_Position = uProjectionView * vec4(geomIn[0].FragPos + geomIn[0].Normal * 0.5, 1);
    EmitVertex();
    EndPrimitive();
}

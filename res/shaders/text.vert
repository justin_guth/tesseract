#version 450 core
layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec4 vColor;
layout(location = 3) in int vTextureIndex;

out vec4 Color;
out vec2 TexCoords;
out int text;

uniform mat4 uProjectionView;

void main()
{
    gl_Position = uProjectionView * vec4(vPosition, 1.0);
    text = vTextureIndex;
    TexCoords = vTextureCoordinate;
    Color = vColor;
}  
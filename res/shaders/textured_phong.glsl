#ifndef TEXTURED_PHONG
#define TEXTURED_PHONG

#include "../../res/shaders/phong.glsl"

in vec3 Normal;
in vec2 TextureCoordinate;
in mat3 TBN;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uRoughnessTexture;

uniform float uTextureScale;

vec3 GetColourTextured()
{
    // TODO: also tangent space stuff https://learnopengl.com/Advanced-Lighting/Normal-Mapping
    vec3 normal = texture(uNormalTexture, TextureCoordinate * uTextureScale).rgb;
    normal = normal * 2.0 - 1.0;
    normal = normalize(TBN * normal);

    return GetColour(
        normal,
        1.0f - texture(uRoughnessTexture, TextureCoordinate * uTextureScale).r,
        vec3(texture(uDiffuseTexture, TextureCoordinate * uTextureScale)));
}

#endif
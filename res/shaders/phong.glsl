#ifndef PHONG
#define PHONG

in vec3 FragPos;

struct PointLight {
    vec3 Position;

    float Constant;
    float Linear;
    float Quadratic;

    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;
};

#define MAX_POINT_LIGHTS 64
uniform int uPointLightCount;
uniform PointLight uPointLights[MAX_POINT_LIGHTS];

uniform vec3 uViewPosition;

// Taken from https://learnopengl.com/Lighting/Multiple-lights
vec3 ComputePointLightContribution(PointLight light, vec3 normal, vec3 fragPosition, vec3 viewDirection, float specularity, vec3 diffuseColour) {
    vec3 lightDir = normalize(light.Position - fragPosition);

    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);

    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDirection, reflectDir), 0.0), specularity);

    // attenuation
    float distance = length(light.Position - fragPosition);
    float attenuation = 1.0 / (light.Constant + light.Linear * distance +
        light.Quadratic * (distance * distance));    

    // combine results
    vec3 ambient = light.Ambient * diffuseColour;
    vec3 diffuse = light.Diffuse * diff * diffuseColour;
    vec3 specular = light.Specular * spec; // vec3(texture(material.specular, TexCoords));

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    //return vec3(distance / 10.0);
    return attenuation * light.Diffuse * diff * diffuseColour;
    //return (ambient + diffuse + specular);
}

vec3 GetColour(vec3 normal, float specularity, vec3 diffuseColour)
{
    vec3 viewDirection = normalize(uViewPosition - FragPos);

    vec3 result = vec3(0);
    for(int i = 0; i < uPointLightCount; i++) {

        result += ComputePointLightContribution(
            uPointLights[i], 
            normal, 
            FragPos, 
            viewDirection, 
            specularity,
            diffuseColour
        );
    }
    return result;
}

#endif
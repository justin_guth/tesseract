#version 450 core

#include "../../res/shaders/turbulence_functions.glsl"

// Must be defined before including displacement.glsl
#define OFFSET_TURBULENCE(p) turbulence_3D(p, 0.05, 2, 2, 6)

#include "../../res/shaders/displacement.glsl"

void main() {
    set_tangent_space_and_position();
}
#version 450 core

/*
 *    Haskell for providing all these copies:
 *

cases n = mapM_ putStrLn (map singleCase [0..(n-1)]) where
  singleCase i = "case " ++ (show i) ++ ":\n return texture(uSpriteTexture" ++ (show i) ++ ", textureCoordinates);"

definitions n = mapM_ putStrLn (map singleDefinition [0..(n-1)]) where
  singleDefinition i = "uniform sampler2D uSpriteTexture" ++ show i ++";"

 *
 */

// REVIEW: I'm not sure this is too sensible, but it's the only way I see to use a large number of samplers.
uniform sampler2D uSpriteTexture0;
uniform sampler2D uSpriteTexture1;
uniform sampler2D uSpriteTexture2;
uniform sampler2D uSpriteTexture3;
uniform sampler2D uSpriteTexture4;
uniform sampler2D uSpriteTexture5;
uniform sampler2D uSpriteTexture6;
uniform sampler2D uSpriteTexture7;
uniform sampler2D uSpriteTexture8;
uniform sampler2D uSpriteTexture9;
uniform sampler2D uSpriteTexture10;
uniform sampler2D uSpriteTexture11;
uniform sampler2D uSpriteTexture12;
uniform sampler2D uSpriteTexture13;
uniform sampler2D uSpriteTexture14;
uniform sampler2D uSpriteTexture15;
uniform sampler2D uSpriteTexture16;
uniform sampler2D uSpriteTexture17;
uniform sampler2D uSpriteTexture18;
uniform sampler2D uSpriteTexture19;
uniform sampler2D uSpriteTexture20;
uniform sampler2D uSpriteTexture21;
uniform sampler2D uSpriteTexture22;
uniform sampler2D uSpriteTexture23;
uniform sampler2D uSpriteTexture24;
uniform sampler2D uSpriteTexture25;
uniform sampler2D uSpriteTexture26;
uniform sampler2D uSpriteTexture27;
uniform sampler2D uSpriteTexture28;
uniform sampler2D uSpriteTexture29;
uniform sampler2D uSpriteTexture30;
uniform sampler2D uSpriteTexture31;
// uniform sampler2D uSpriteTexture32;
// uniform sampler2D uSpriteTexture33;
// uniform sampler2D uSpriteTexture34;
// uniform sampler2D uSpriteTexture35;
// uniform sampler2D uSpriteTexture36;
// uniform sampler2D uSpriteTexture37;
// uniform sampler2D uSpriteTexture38;
// uniform sampler2D uSpriteTexture39;
// uniform sampler2D uSpriteTexture40;
// uniform sampler2D uSpriteTexture41;
// uniform sampler2D uSpriteTexture42;
// uniform sampler2D uSpriteTexture43;
// uniform sampler2D uSpriteTexture44;
// uniform sampler2D uSpriteTexture45;
// uniform sampler2D uSpriteTexture46;
// uniform sampler2D uSpriteTexture47;
// uniform sampler2D uSpriteTexture48;
// uniform sampler2D uSpriteTexture49;
// uniform sampler2D uSpriteTexture50;
// uniform sampler2D uSpriteTexture51;
// uniform sampler2D uSpriteTexture52;
// uniform sampler2D uSpriteTexture53;
// uniform sampler2D uSpriteTexture54;
// uniform sampler2D uSpriteTexture55;
// uniform sampler2D uSpriteTexture56;
// uniform sampler2D uSpriteTexture57;
// uniform sampler2D uSpriteTexture58;
// uniform sampler2D uSpriteTexture59;
// uniform sampler2D uSpriteTexture60;
// uniform sampler2D uSpriteTexture61;
// uniform sampler2D uSpriteTexture62;
// uniform sampler2D uSpriteTexture63;
// uniform sampler2D uSpriteTexture64;
// uniform sampler2D uSpriteTexture65;
// uniform sampler2D uSpriteTexture66;
// uniform sampler2D uSpriteTexture67;
// uniform sampler2D uSpriteTexture68;
// uniform sampler2D uSpriteTexture69;
// uniform sampler2D uSpriteTexture70;
// uniform sampler2D uSpriteTexture71;
// uniform sampler2D uSpriteTexture72;
// uniform sampler2D uSpriteTexture73;
// uniform sampler2D uSpriteTexture74;
// uniform sampler2D uSpriteTexture75;
// uniform sampler2D uSpriteTexture76;
// uniform sampler2D uSpriteTexture77;
// uniform sampler2D uSpriteTexture78;
// uniform sampler2D uSpriteTexture79;
// uniform sampler2D uSpriteTexture80;
// uniform sampler2D uSpriteTexture81;
// uniform sampler2D uSpriteTexture82;
// uniform sampler2D uSpriteTexture83;
// uniform sampler2D uSpriteTexture84;
// uniform sampler2D uSpriteTexture85;
// uniform sampler2D uSpriteTexture86;
// uniform sampler2D uSpriteTexture87;
// uniform sampler2D uSpriteTexture88;
// uniform sampler2D uSpriteTexture89;
// uniform sampler2D uSpriteTexture90;
// uniform sampler2D uSpriteTexture91;
// uniform sampler2D uSpriteTexture92;
// uniform sampler2D uSpriteTexture93;
// uniform sampler2D uSpriteTexture94;
// uniform sampler2D uSpriteTexture95;
// uniform sampler2D uSpriteTexture96;
// uniform sampler2D uSpriteTexture97;
// uniform sampler2D uSpriteTexture98;
// uniform sampler2D uSpriteTexture99;
// uniform sampler2D uSpriteTexture100;
// uniform sampler2D uSpriteTexture101;
// uniform sampler2D uSpriteTexture102;
// uniform sampler2D uSpriteTexture103;
// uniform sampler2D uSpriteTexture104;
// uniform sampler2D uSpriteTexture105;
// uniform sampler2D uSpriteTexture106;
// uniform sampler2D uSpriteTexture107;
// uniform sampler2D uSpriteTexture108;
// uniform sampler2D uSpriteTexture109;
// uniform sampler2D uSpriteTexture110;
// uniform sampler2D uSpriteTexture111;
// uniform sampler2D uSpriteTexture112;
// uniform sampler2D uSpriteTexture113;
// uniform sampler2D uSpriteTexture114;
// uniform sampler2D uSpriteTexture115;
// uniform sampler2D uSpriteTexture116;
// uniform sampler2D uSpriteTexture117;
// uniform sampler2D uSpriteTexture118;
// uniform sampler2D uSpriteTexture119;
// uniform sampler2D uSpriteTexture120;
// uniform sampler2D uSpriteTexture121;
// uniform sampler2D uSpriteTexture122;
// uniform sampler2D uSpriteTexture123;
// uniform sampler2D uSpriteTexture124;
// uniform sampler2D uSpriteTexture125;
// uniform sampler2D uSpriteTexture126;
// uniform sampler2D uSpriteTexture127;

layout(location = 0) out vec4 FragColor;

uniform vec4 uMaterialDiffuseTint;

in vec3 Normal;
in vec3 FragPos;
in vec2 TextureCoordinates;
in mat3 TBN;
flat in int TextureIndex;
in vec4 FrameData;
in vec4 Tint;
in vec3 WorldPosition;

uniform float uFogginess;


uniform vec3 uViewPosition;

#define MAX_POINT_LIGHTS 32

uniform int uPointLightCount;

struct PointLight {
    vec3 Position;
    vec3 Color;
};

uniform PointLight uPointLights[MAX_POINT_LIGHTS];

vec4 SampleWithIndex(int index, vec2 textureCoordinates)
{
    switch (index)
    {
        case 0:
         return texture(uSpriteTexture0, textureCoordinates);
        case 1:
         return texture(uSpriteTexture1, textureCoordinates);
        case 2:
         return texture(uSpriteTexture2, textureCoordinates);
        case 3:
         return texture(uSpriteTexture3, textureCoordinates);
        case 4:
         return texture(uSpriteTexture4, textureCoordinates);
        case 5:
         return texture(uSpriteTexture5, textureCoordinates);
        case 6:
         return texture(uSpriteTexture6, textureCoordinates);
        case 7:
         return texture(uSpriteTexture7, textureCoordinates);
        case 8:
         return texture(uSpriteTexture8, textureCoordinates);
        case 9:
         return texture(uSpriteTexture9, textureCoordinates);
        case 10:
         return texture(uSpriteTexture10, textureCoordinates);
        case 11:
         return texture(uSpriteTexture11, textureCoordinates);
        case 12:
         return texture(uSpriteTexture12, textureCoordinates);
        case 13:
         return texture(uSpriteTexture13, textureCoordinates);
        case 14:
         return texture(uSpriteTexture14, textureCoordinates);
        case 15:
         return texture(uSpriteTexture15, textureCoordinates);
        case 16:
         return texture(uSpriteTexture16, textureCoordinates);
        case 17:
         return texture(uSpriteTexture17, textureCoordinates);
        case 18:
         return texture(uSpriteTexture18, textureCoordinates);
        case 19:
         return texture(uSpriteTexture19, textureCoordinates);
        case 20:
         return texture(uSpriteTexture20, textureCoordinates);
        case 21:
         return texture(uSpriteTexture21, textureCoordinates);
        case 22:
         return texture(uSpriteTexture22, textureCoordinates);
        case 23:
         return texture(uSpriteTexture23, textureCoordinates);
        case 24:
         return texture(uSpriteTexture24, textureCoordinates);
        case 25:
         return texture(uSpriteTexture25, textureCoordinates);
        case 26:
         return texture(uSpriteTexture26, textureCoordinates);
        case 27:
         return texture(uSpriteTexture27, textureCoordinates);
        case 28:
         return texture(uSpriteTexture28, textureCoordinates);
        case 29:
         return texture(uSpriteTexture29, textureCoordinates);
        case 30:
         return texture(uSpriteTexture30, textureCoordinates);
        case 31:
         return texture(uSpriteTexture31, textureCoordinates);
        //case 32:
        // return texture(uSpriteTexture32, textureCoordinates);
        //case 33:
        // return texture(uSpriteTexture33, textureCoordinates);
        //case 34:
        // return texture(uSpriteTexture34, textureCoordinates);
        //case 35:
        // return texture(uSpriteTexture35, textureCoordinates);
        //case 36:
        // return texture(uSpriteTexture36, textureCoordinates);
        //case 37:
        // return texture(uSpriteTexture37, textureCoordinates);
        //case 38:
        // return texture(uSpriteTexture38, textureCoordinates);
        //case 39:
        // return texture(uSpriteTexture39, textureCoordinates);
        //case 40:
        // return texture(uSpriteTexture40, textureCoordinates);
        //case 41:
        // return texture(uSpriteTexture41, textureCoordinates);
        //case 42:
        // return texture(uSpriteTexture42, textureCoordinates);
        //case 43:
        // return texture(uSpriteTexture43, textureCoordinates);
        //case 44:
        // return texture(uSpriteTexture44, textureCoordinates);
        //case 45:
        // return texture(uSpriteTexture45, textureCoordinates);
        //case 46:
        // return texture(uSpriteTexture46, textureCoordinates);
        //case 47:
        // return texture(uSpriteTexture47, textureCoordinates);
        //case 48:
        // return texture(uSpriteTexture48, textureCoordinates);
        //case 49:
        // return texture(uSpriteTexture49, textureCoordinates);
        //case 50:
        // return texture(uSpriteTexture50, textureCoordinates);
        //case 51:
        // return texture(uSpriteTexture51, textureCoordinates);
        //case 52:
        // return texture(uSpriteTexture52, textureCoordinates);
        //case 53:
        // return texture(uSpriteTexture53, textureCoordinates);
        //case 54:
        // return texture(uSpriteTexture54, textureCoordinates);
        //case 55:
        // return texture(uSpriteTexture55, textureCoordinates);
        //case 56:
        // return texture(uSpriteTexture56, textureCoordinates);
        //case 57:
        // return texture(uSpriteTexture57, textureCoordinates);
        //case 58:
        // return texture(uSpriteTexture58, textureCoordinates);
        //case 59:
        // return texture(uSpriteTexture59, textureCoordinates);
        //case 60:
        // return texture(uSpriteTexture60, textureCoordinates);
        //case 61:
        // return texture(uSpriteTexture61, textureCoordinates);
        //case 62:
        // return texture(uSpriteTexture62, textureCoordinates);
        //case 63:
        // return texture(uSpriteTexture63, textureCoordinates);
        //case 64:
        // return texture(uSpriteTexture64, textureCoordinates);
        //case 65:
        // return texture(uSpriteTexture65, textureCoordinates);
        //case 66:
        // return texture(uSpriteTexture66, textureCoordinates);
        //case 67:
        // return texture(uSpriteTexture67, textureCoordinates);
        //case 68:
        // return texture(uSpriteTexture68, textureCoordinates);
        //case 69:
        // return texture(uSpriteTexture69, textureCoordinates);
        //case 70:
        // return texture(uSpriteTexture70, textureCoordinates);
        //case 71:
        // return texture(uSpriteTexture71, textureCoordinates);
        //case 72:
        // return texture(uSpriteTexture72, textureCoordinates);
        //case 73:
        // return texture(uSpriteTexture73, textureCoordinates);
        //case 74:
        // return texture(uSpriteTexture74, textureCoordinates);
        //case 75:
        // return texture(uSpriteTexture75, textureCoordinates);
        //case 76:
        // return texture(uSpriteTexture76, textureCoordinates);
        //case 77:
        // return texture(uSpriteTexture77, textureCoordinates);
        //case 78:
        // return texture(uSpriteTexture78, textureCoordinates);
        //case 79:
        // return texture(uSpriteTexture79, textureCoordinates);
        //case 80:
        // return texture(uSpriteTexture80, textureCoordinates);
        //case 81:
        // return texture(uSpriteTexture81, textureCoordinates);
        //case 82:
        // return texture(uSpriteTexture82, textureCoordinates);
        //case 83:
        // return texture(uSpriteTexture83, textureCoordinates);
        //case 84:
        // return texture(uSpriteTexture84, textureCoordinates);
        //case 85:
        // return texture(uSpriteTexture85, textureCoordinates);
        //case 86:
        // return texture(uSpriteTexture86, textureCoordinates);
        //case 87:
        // return texture(uSpriteTexture87, textureCoordinates);
        //case 88:
        // return texture(uSpriteTexture88, textureCoordinates);
        //case 89:
        // return texture(uSpriteTexture89, textureCoordinates);
        //case 90:
        // return texture(uSpriteTexture90, textureCoordinates);
        //case 91:
        // return texture(uSpriteTexture91, textureCoordinates);
        //case 92:
        // return texture(uSpriteTexture92, textureCoordinates);
        //case 93:
        // return texture(uSpriteTexture93, textureCoordinates);
        //case 94:
        // return texture(uSpriteTexture94, textureCoordinates);
        //case 95:
        // return texture(uSpriteTexture95, textureCoordinates);
        //case 96:
        // return texture(uSpriteTexture96, textureCoordinates);
        //case 97:
        // return texture(uSpriteTexture97, textureCoordinates);
        //case 98:
        // return texture(uSpriteTexture98, textureCoordinates);
        //case 99:
        // return texture(uSpriteTexture99, textureCoordinates);
        //case 100:
        // return texture(uSpriteTexture100, textureCoordinates);
        //case 101:
        // return texture(uSpriteTexture101, textureCoordinates);
        //case 102:
        // return texture(uSpriteTexture102, textureCoordinates);
        //case 103:
        // return texture(uSpriteTexture103, textureCoordinates);
        //case 104:
        // return texture(uSpriteTexture104, textureCoordinates);
        //case 105:
        // return texture(uSpriteTexture105, textureCoordinates);
        //case 106:
        // return texture(uSpriteTexture106, textureCoordinates);
        //case 107:
        // return texture(uSpriteTexture107, textureCoordinates);
        //case 108:
        // return texture(uSpriteTexture108, textureCoordinates);
        //case 109:
        // return texture(uSpriteTexture109, textureCoordinates);
        //case 110:
        // return texture(uSpriteTexture110, textureCoordinates);
        //case 111:
        // return texture(uSpriteTexture111, textureCoordinates);
        //case 112:
        // return texture(uSpriteTexture112, textureCoordinates);
        //case 113:
        // return texture(uSpriteTexture113, textureCoordinates);
        //case 114:
        // return texture(uSpriteTexture114, textureCoordinates);
        //case 115:
        // return texture(uSpriteTexture115, textureCoordinates);
        //case 116:
        // return texture(uSpriteTexture116, textureCoordinates);
        //case 117:
        // return texture(uSpriteTexture117, textureCoordinates);
        //case 118:
        // return texture(uSpriteTexture118, textureCoordinates);
        //case 119:
        // return texture(uSpriteTexture119, textureCoordinates);
        //case 120:
        // return texture(uSpriteTexture120, textureCoordinates);
        //case 121:
        // return texture(uSpriteTexture121, textureCoordinates);
        //case 122:
        // return texture(uSpriteTexture122, textureCoordinates);
        //case 123:
        // return texture(uSpriteTexture123, textureCoordinates);
        //case 124:
        // return texture(uSpriteTexture124, textureCoordinates);
        //case 125:
        // return texture(uSpriteTexture125, textureCoordinates);
        //case 126:
        // return texture(uSpriteTexture126, textureCoordinates);
        //case 127:
        // return texture(uSpriteTexture127, textureCoordinates);
        default: 
         return vec4(0);
    }
}



const float PI = 3.14159265359;

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}  

float DistributionGGX(vec3 normal, vec3 halfway, float roughness)
{
    float a      = roughness*roughness;
    float aSquared     = a*a;
    float normalDotHalfway  = max(dot(normal, halfway), 0.0);
    float normalDotHalfwaySquared = normalDotHalfway*normalDotHalfway;
	
    float denominator = (normalDotHalfwaySquared * (aSquared - 1.0) + 1.0);
    denominator = PI * denominator * denominator;
	
    return aSquared / denominator;
}

float GeometrySchlickGGX(float normalDotView, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float denominator = normalDotView * (1.0 - k) + k;
	
    return normalDotView / denominator;
}

float GeometrySmith(vec3 normal, vec3 view, vec3 light, float roughness)
{
    float normalDotView = max(dot(normal, view), 0.0);
    float normalDotLight = max(dot(normal, light), 0.0);
    float ggx2  = GeometrySchlickGGX(normalDotView, roughness);
    float ggx1  = GeometrySchlickGGX(normalDotLight, roughness);
	
    return ggx1 * ggx2;
}

vec3 getNormalFromMap()
{
    //vec3 tangentNormal = texture(uNormalTexture, TextureCoordinates).xyz; //FIXME Some normal issue in lighting
    vec3 tangentNormal = vec3(0.0, 0.0, 1.0);

    // vec3 Q1  = dFdx(WorldPosition);
    // vec3 Q2  = dFdy(WorldPosition);
    // vec2 TextureCoordinates = TextureCoordinates * uTextureScale;
    // vec2 st1 = dFdx(TextureCoordinates);
    // vec2 st2 = dFdy(TextureCoordinates);

    // vec3 N   = normalize(Normal);
    // vec3 T  = normalize(Q1 * st2.t - Q2 * st1.t);
    // vec3 B  = -normalize(cross(N, T));
    // mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

void main()
{	
    float subtextureX = FrameData.x;
    float subtextureY = FrameData.y;
    float subtextureWidth = FrameData.z;
    float subtextureHeight = FrameData.w;

    vec2 subTextureCoordinate;

    subTextureCoordinate.x = subtextureX + (TextureCoordinates.x * subtextureWidth);
    subTextureCoordinate.y = subtextureY + (TextureCoordinates.y * subtextureHeight);

    vec4 diffuseColor = SampleWithIndex(TextureIndex, subTextureCoordinate) * Tint;

    if(diffuseColor.a < 0.05) discard;

    vec3 albedo     = pow(diffuseColor.rgb, vec3(2.2));
    vec3 normal     = getNormalFromMap().rgb;
    float metallic  = 0.0f; // TODO, if wanted
    float roughness = 1.0f; // TODO, if wanted
    float ao        = 1.0f; // TODO, if wanted

    vec3 view = normalize(uViewPosition - WorldPosition);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);
	           
    // reflectance equation
    vec3 radiance = vec3(0.0);
    for(int i = 0; i < uPointLightCount; ++i)
    {
        // calculate per-light radiance
        vec3 light = normalize(uPointLights[i].Position - WorldPosition);
        vec3 halfway = normalize(view + light);
        float dist    = length(uPointLights[i].Position - WorldPosition);
        float attenuation = 1.0 / (dist * dist);
        vec3 radianceIncrement     = uPointLights[i].Color * attenuation;        
        
        
        // cook-torrance brdf
        float normalDistributionFactor = DistributionGGX(normal, halfway, roughness);        
        float geometry   = GeometrySmith(normal, view, light, roughness);      
        vec3 fresnel    = fresnelSchlick(max(dot(halfway, view), 0.0), F0);       
        
        

        vec3 kS = fresnel;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;	  
        

        vec3 numerator    = normalDistributionFactor * geometry * fresnel;
        float denominator = 4.0 * max(dot(normal, view), 0.0) * max(dot(normal, light), 0.0) + 0.0001;
        vec3 specular     = numerator / denominator;  
            
        // add to outgoing radiance
        float normalDotLight = max(dot(normal, light), 0.0);                
        radiance += (kD * albedo / PI + specular) * radianceIncrement * normalDotLight; 
        
    }   
  
    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + radiance;
	
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));  

    float z = gl_FragCoord.z / gl_FragCoord.w;


    
   
    FragColor = vec4(mix(color, vec3(1.0f), z * uFogginess), diffuseColor.a);
    //FragColor = vec4(vec3(roughness), 1.0);
    //FragColor = vec4(albedo, 1.0);
    //FragColor = vec4(vec3(ao), 1.0);
    //FragColor = vec4(normal, 1.0);
    //FragColor = vec4(ambient, 1.0);
    //FragColor = vec4(radiance, 1.0);
    

}  
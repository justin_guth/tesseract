#version 450 core

layout(location = 0) out vec4 FragColor;

uniform vec4 uColorFill;
uniform vec4 uColorWire;
uniform bool uFilled;


void main() {

    if (uFilled) 
    {
        FragColor = uColorFill;
    }
    else 
    {
        FragColor = uColorWire;
    }
}
#ifndef HEIGHTMAPPING
#define HEIGHTMAPPING

// Define dummy turbulence if none is given
#ifndef OFFSET_TURBULENCE
#define OFFSET_TURBULENCE(p) 0
#endif

#define HEIGHTMAP_EPS 0.00001

vec3 estimate_normal(vec3 p, float step_size)
{
    vec3 samples[4] = 
    {
        p - step_size * vec3(1,0,0),
        p + step_size * vec3(1,0,0),
        p - step_size * vec3(0,1,0),
        p + step_size * vec3(0,1,0)
    };

    for(int s = 0; s < 4; s++) { samples[s] += vec3(0,0,1) * OFFSET_TURBULENCE(samples[s]); }

    vec3 t1 = samples[1] - samples[0];
    vec3 t2 = samples[3] - samples[2];

    return normalize(cross(t1, t2));
}

void tangent_space(vec3 p, float step_size, out vec3 n, out vec3 t, out vec3 co_t)
{
    n = estimate_normal(p, step_size);
    co_t = normalize(cross(n, vec3(0,0,1)));
    t = normalize(cross(n, co_t));
}

#endif
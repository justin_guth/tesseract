#version 450 core

// Cave fragment shader

in mat3 TBN;

#include "../../res/shaders/turbulence_functions.glsl"

#define OFFSET_TURBULENCE(p) cave(p)

#include "../../res/shaders/heightmaps.glsl"

#define ALBEDO_SAMPLING(p) cave_albedo(p) + vec3(0.2,0.25,0.2) * cave_metal(p)

#define NORMAL_SAMPLING(p) estimate_normal(p, HEIGHTMAP_EPS)

#define METAL_SAMPLE(p) cave_metal(p)

#define ROUGHNESS_SAMPLE(p) 1.2 - cave_metal(p)

#include "../../res/shaders/procedural_PBR.glsl"

layout(location = 0) out vec4 FragColor;

uniform vec4 uMaterialDiffuseTint;


void main() {

    vec3 result = GetColourProcedural();

    FragColor = vec4(result, 1.0);
}
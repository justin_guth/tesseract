#ifndef NOISE_INCLUDED
#define NOISE_INCLUDED
#include "../../res/shaders/random.glsl"
#include "../../res/shaders/constants.glsl"

#define TEXTBOOK_IMPLEMENTATION 0

vec3 gradient_at_lattice_point_3D(ivec3 lattice_coord)
{
    float theta = 2.0 * PI * random_from_2D(lattice_coord.xy);
    float phi = 2.0 * PI * random_from_2D(lattice_coord.xz);;

    float x = sin(theta) * cos(phi);
    float y = sin(theta) * sin(phi);
    float z = cos(theta);

    return vec3(x, y, z);
}

vec2 gradient_at_lattice_point_2D(ivec2 lattice_coord)
{
    float phi = 2.0 * PI * random_from_2D(lattice_coord);

    float x = cos(phi);
    float y = sin(phi);

    return vec2(x, y);
}

float lerp(float a, float b, float t)
{
    return t * b + (1 - t) * a;
}

float perlin_interpolate(float a, float b, float t)
{
#if TEXTBOOK_IMPLEMENTATION
    return t * t * (3.0 - 2.0 * t) * (b - a) + a;
#else
    return lerp(a, b, smoothstep(0, 1, t));
#endif
}

float perlin_noise_3D(vec3 coord)
{
    ivec3 lattice_coord = ivec3(floor(coord));
    vec3 offsets[8] = {
        coord - lattice_coord - vec3(0, 0, 0),
        coord - lattice_coord - vec3(0, 0, 1),
        coord - lattice_coord - vec3(0, 1, 0),
        coord - lattice_coord - vec3(0, 1, 1),
        coord - lattice_coord - vec3(1, 0, 0),
        coord - lattice_coord - vec3(1, 0, 1),
        coord - lattice_coord - vec3(1, 1, 0),
        coord - lattice_coord - vec3(1, 1, 1)
    };

    float intermediate[8] = {
        dot(offsets[0], gradient_at_lattice_point_3D(lattice_coord + ivec3(0, 0, 0))),
        dot(offsets[1], gradient_at_lattice_point_3D(lattice_coord + ivec3(0, 0, 1))),
        dot(offsets[2], gradient_at_lattice_point_3D(lattice_coord + ivec3(0, 1, 0))),
        dot(offsets[3], gradient_at_lattice_point_3D(lattice_coord + ivec3(0, 1, 1))),
        dot(offsets[4], gradient_at_lattice_point_3D(lattice_coord + ivec3(1, 0, 0))),
        dot(offsets[5], gradient_at_lattice_point_3D(lattice_coord + ivec3(1, 0, 1))),
        dot(offsets[6], gradient_at_lattice_point_3D(lattice_coord + ivec3(1, 1, 0))),
        dot(offsets[7], gradient_at_lattice_point_3D(lattice_coord + ivec3(1, 1, 1)))
    };

    float x_perp[4] = {
        perlin_interpolate(intermediate[0], intermediate[4], coord.x - lattice_coord.x),
        perlin_interpolate(intermediate[1], intermediate[5], coord.x - lattice_coord.x),
        perlin_interpolate(intermediate[2], intermediate[6], coord.x - lattice_coord.x),
        perlin_interpolate(intermediate[3], intermediate[7], coord.x - lattice_coord.x)
    };

    float y_perp[2] = {
        perlin_interpolate(x_perp[0], x_perp[2], coord.y - lattice_coord.y),
        perlin_interpolate(x_perp[1], x_perp[3], coord.y - lattice_coord.y)
    };

    return perlin_interpolate(y_perp[0], y_perp[1], coord.z - lattice_coord.z);
}

float perlin_noise_2D(vec2 coord)
{
    ivec2 lattice_coord = ivec2(floor(coord));

    vec2 offsets[4] = {
        coord - lattice_coord - vec2(0, 0),
        coord - lattice_coord - vec2(0, 1),
        coord - lattice_coord - vec2(1, 0),
        coord - lattice_coord - vec2(1, 1)
    };

    float intermediate[4] = {
        dot(offsets[0], gradient_at_lattice_point_2D(lattice_coord + ivec2(0, 0))),
        dot(offsets[1], gradient_at_lattice_point_2D(lattice_coord + ivec2(0, 1))),
        dot(offsets[2], gradient_at_lattice_point_2D(lattice_coord + ivec2(1, 0))),
        dot(offsets[3], gradient_at_lattice_point_2D(lattice_coord + ivec2(1, 1)))
    };

    float y_perp[2] = {
        perlin_interpolate(intermediate[0], intermediate[1], coord.y - lattice_coord.y),
        perlin_interpolate(intermediate[2], intermediate[3], coord.y - lattice_coord.y)
    };

    return perlin_interpolate(y_perp[0], y_perp[1], coord.x - lattice_coord.x);
}

// Simplex noise (hopefully runs faster)

#define FOREIGN_CODE 1
#if FOREIGN_CODE

//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
// 

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x * 34.0) + 10.0) * x);
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+10.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float simplex_noise_2D(vec2 v)
{
    const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                        0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                       -0.577350269189626,  // -1.0 + 2.0 * C.x
                        0.024390243902439); // 1.0 / 41.0
    // First corner
    vec2 i  = floor(v + dot(v, C.yy) );
    vec2 x0 = v -   i + dot(i, C.xx);

    // Other corners
    vec2 i1;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;

    // Permutations
    i = mod289(i); // Avoid truncation effects in permutation
    vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
    		+ i.x + vec3(0.0, i1.x, 1.0 ));

    vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
    m = m*m ;
    m = m*m ;

     // Gradients: 41 points uniformly over a line, mapped onto a diamond.
     // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt( a0*a0 + h*h );
    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

    // Compute final noise value at P
    vec3 g;
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 130.0 * dot(m, g);
}

float simplex_noise_3D(vec3 v)
{ 
    const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
    const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

    // First corner
    vec3 i  = floor(v + dot(v, C.yyy) );
    vec3 x0 =   v - i + dot(i, C.xxx) ;

    // Other corners
    vec3 g = step(x0.yzx, x0.xyz);
    vec3 l = 1.0 - g;
    vec3 i1 = min( g.xyz, l.zxy );
    vec3 i2 = max( g.xyz, l.zxy );

    //   x0 = x0 - 0.0 + 0.0 * C.xxx;
    //   x1 = x0 - i1  + 1.0 * C.xxx;
    //   x2 = x0 - i2  + 2.0 * C.xxx;
    //   x3 = x0 - 1.0 + 3.0 * C.xxx;
    vec3 x1 = x0 - i1 + C.xxx;
    vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
    vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

    // Permutations
    i = mod289(i); 
    vec4 p = permute( permute( permute( 
               i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
             + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
             + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

    // Gradients: 7x7 points over a square, mapped onto an octahedron.
    // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
    float n_ = 0.142857142857; // 1.0/7.0
    vec3  ns = n_ * D.wyz - D.xzx;

    vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

    vec4 x_ = floor(j * ns.z);
    vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

    vec4 x = x_ *ns.x + ns.yyyy;
    vec4 y = y_ *ns.x + ns.yyyy;
    vec4 h = 1.0 - abs(x) - abs(y);

    vec4 b0 = vec4( x.xy, y.xy );
    vec4 b1 = vec4( x.zw, y.zw );

    vec4 s0 = floor(b0)*2.0 + 1.0;
    vec4 s1 = floor(b1)*2.0 + 1.0;
    vec4 sh = -step(h, vec4(0.0));

    vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
    vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

    vec3 p0 = vec3(a0.xy,h.x);
    vec3 p1 = vec3(a0.zw,h.y);
    vec3 p2 = vec3(a1.xy,h.z);
    vec3 p3 = vec3(a1.zw,h.w);

    //Normalise gradients
    vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
    p0 *= norm.x;
    p1 *= norm.y;
    p2 *= norm.z;
    p3 *= norm.w;

    // Mix final noise value
    vec4 m = max(0.5 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
    m = m * m;
    return 105.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                  dot(p2,x2), dot(p3,x3) ) );
}

#else

/* Matrix columns have the shape
 *
 *           / (r + d - 1) / d, if i = j
 * m_{ij} = <|
 *           \ (r - 1) / d      otherwise
 * 
 * where d is the dimension of the vectors
 */

// FIXME: Broken. Other implementation's probably faster anyways, though.

const float r_2D = sqrt(2);
const mat2 isotri_to_square = mat2(
    r_2D + 1, r_2D - 1,
    r_2D - 1, r_2D + 1    
) / 2;
const mat2 square_to_isotri = mat2(
    r_2D + 1, -r_2D + 1, 
    -r_2D + 1, r_2D + 1
) / (2 * r_2D);

const float r_3D = sqrt(3); 
const mat3 regtet_to_cube = mat3(
    r_3D + 2, r_3D - 1, r_3D - 1,
    r_3D - 1, r_3D + 2, r_3D - 1,
    r_3D - 1, r_3D - 1, r_3D + 2
) / 3;
const mat3 cube_to_regtet = mat3(
    2 * r_3D + 1, -r_3D + 1, -r_3D + 1,
    -r_3D + 1, 2 * r_3D + 1,  -r_3D + 1,
    -r_3D + 1, -r_3D + 1, 2 * r_3D + 1
) / (3 * r_3D);

const float r_4D = sqrt(4);
const mat4 simplex_to_cuboid_4D = mat4(
    r_4D + 3, r_4D - 1, r_4D - 1, r_4D - 1,
    r_4D - 1, r_4D + 3, r_4D - 1, r_4D - 1,
    r_4D - 1, r_4D - 1, r_4D + 3, r_4D - 1,
    r_4D - 1, r_4D - 1, r_4D - 1, r_4D + 3
)  / 4;
const mat4 cuboid_to_simplex_4D = mat4(
    3 * r_4D + 1, -r_4D + 1, -r_4D + 1, -r_4D + 1, 
    -r_4D + 1, 3 * r_4D + 1, -r_4D + 1, -r_4D + 1,
    -r_4D + 1, -r_4D + 1, 3 * r_4D + 1, -r_4D + 1, 
    -r_4D + 1, -r_4D + 1, -r_4D + 1, 3 * r_4D + 1
) / (4 * r_4D);

float simplex_noise_2D(vec2 coord)
{
    vec2 transformed = isotri_to_square * coord;
    ivec2 c1 = ivec2(floor(transformed));
    ivec2 c2 = c1 + 1;
    ivec2 c3 = c1 + ivec2(step(transformed.yx, transformed.xy));

    vec2 gradients[3] = {
        gradient_at_lattice_point_2D(c1),
        gradient_at_lattice_point_2D(c2),
        gradient_at_lattice_point_2D(c3)
    };

    vec2 offsets[3] = {
        coord - c1,
        coord - c2,
        coord - c3
    };

    vec3 gradient_products = vec3(
        dot(gradients[0], square_to_isotri * c1),
        dot(gradients[1], square_to_isotri * c2),
        dot(gradients[2], square_to_isotri * c3)
    );

    vec3 contribution = vec3(
        pow(max(0.6 - dot(offsets[0], offsets[0]), 0), 4),
        pow(max(0.6 - dot(offsets[1], offsets[1]), 0), 4),
        pow(max(0.6 - dot(offsets[2], offsets[2]), 0), 4)
    );

    return dot(contribution, gradient_products);
}

float simplex_noise_3D(vec3 coord)
{
    vec3 transformed = regtet_to_cube * coord;
    ivec3 c1 = ivec3(floor(transformed));
    ivec3 g = ivec3(step(transformed.yzx, transformed.xyz));   // Adapted from https://github.com/ashima/webgl-noise/blob/master/src/noise3D.glsl
    ivec3 l = 1 - g;
    ivec3 c2 = c1 + 1;
    ivec3 c3 = c1 + min(g.xyz, l.zxy);
    ivec3 c4 = c1 + max(g.xyz, l.zxy);

    vec3 gradients[4] = {
        gradient_at_lattice_point_3D(c1),
        gradient_at_lattice_point_3D(c2),
        gradient_at_lattice_point_3D(c3),
        gradient_at_lattice_point_3D(c4),
    };

    vec3 offsets[4] = {
        coord - c1,
        coord - c2,
        coord - c3,
        coord - c4
    };

    vec4 gradient_products = vec4(
        dot(gradients[0], cube_to_regtet * c1),
        dot(gradients[1], cube_to_regtet * c2),
        dot(gradients[2], cube_to_regtet * c3),
        dot(gradients[3], cube_to_regtet * c4)
    );

    vec4 contribution = vec4(
        pow(max(0.6 - dot(offsets[0], offsets[0]), 0), 4),
        pow(max(0.6 - dot(offsets[1], offsets[1]), 0), 4),
        pow(max(0.6 - dot(offsets[2], offsets[2]), 0), 4),
        pow(max(0.6 - dot(offsets[3], offsets[3]), 0), 4)
    );

    return dot(contribution, gradient_products);
}

#endif

#endif
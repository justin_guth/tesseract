#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vDimensions;
layout(location = 2) in vec4 vColor;

uniform mat4 uProjectionView;

out VertexOut {
    vec3 position;
    vec2 dimensions;
    vec4 color;
} vertexOut;

void main() {

    gl_Position = uProjectionView * vec4(vPosition, 1.0);

    vertexOut.position = (uProjectionView * vec4(vPosition, 1.0)).xyz;
    vertexOut.dimensions = (uProjectionView * vec4(vDimensions, 0.0, 0.0)).xy;
    vertexOut.color = vColor;
}
#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform mat4 uProjectionView;

in VertexOut {
    vec3 position;
    vec2 dimensions;
    vec4 color;
} vertexIn[];

flat out vec4 color;

void main()
{
    vec3 position;

    color = vertexIn[0].color;
    position = vertexIn[0].position;
    gl_Position = vec4(position, 1.0);
    EmitVertex();

    position = vertexIn[0].position + vec3(vertexIn[0].dimensions.x, 0.0, 0.0);
    gl_Position = vec4(position, 1.0);
    EmitVertex();

    position = vertexIn[0].position + vec3(0.0, vertexIn[0].dimensions.y, 0.0);
    gl_Position = vec4(position, 1.0);
    EmitVertex();

    position = vertexIn[0].position + vec3(vertexIn[0].dimensions.x, vertexIn[0].dimensions.y, 0.0);
    gl_Position = vec4(position, 1.0);
    EmitVertex();

    EndPrimitive();
}
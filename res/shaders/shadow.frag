#version 450 core
in vec4 FragPos;

uniform vec3 uLightPosition;
uniform float uFarPlane;




void main()
{
    // get distance between fragment and light source
    float lightDistance = length(FragPos.xyz - uLightPosition);
    
    // map to [0;1] range by dividing by uFarPlane
    lightDistance = lightDistance / uFarPlane;
    
    // write this as modified depth
    gl_FragDepth = lightDistance;
}  
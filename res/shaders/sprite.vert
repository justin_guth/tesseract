#version 450 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;
layout(location = 5) in int vTextureIndex;
layout(location = 6) in vec4 vFrameData;
layout(location = 7) in vec4 vTint;

uniform mat4 uProjectionView;

out vec3 Normal; // The vertex normal
out vec3 WorldPosition; // The position of the vertex in world space coordinates
out vec2 TextureCoordinates;
out mat3 TBN;
out int TextureIndex;
out vec4 FrameData;
out vec4 Tint;
void main() {
    TBN = mat3(vTangent, vBitangent, vNormal);

    vec4 homogenizedPosition = vec4(vPosition, 1.0);
    gl_Position = uProjectionView * homogenizedPosition;
    Normal = vNormal;
    WorldPosition = vPosition;
    TextureCoordinates = vTextureCoordinate;

    FrameData = vFrameData;
    TextureIndex = vTextureIndex;
    Tint = vTint;
}
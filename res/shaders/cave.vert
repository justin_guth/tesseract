#version 450 core

#include "../../res/shaders/turbulence_functions.glsl"

// Must be defined before including displacement.glsl
#define OFFSET_TURBULENCE(p) cave(p)

#include "../../res/shaders/displacement.glsl"

void main() {
    set_tangent_space_and_position();
}
#version 450 core
layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoordinate;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent; //TODO: Can be removed from Vertex Layout.
layout(location = 4) in vec3 vBitangent;

uniform mat4 uModel;

void main()
{
    gl_Position = uModel * vec4(vPosition, 1.0);
}  
#version 450 core
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 uShadowMatrices[6];

out vec4 FragPos; // FragPos from GS (output per emitvertex)

in vec2 inTextureCoordinates[];
in int inTextureIndex[];
in vec4 inFrameData[];

out vec2 TextureCoordinates;
out int TextureIndex;
out vec4 FrameData;

void main()
{
    for(int face = 0; face < 6; ++face)
    {
        gl_Layer = face; // built-in variable that specifies to which face we render.
        for(int i = 0; i < 3; ++i) // for each triangle vertex
        {
            TextureCoordinates = inTextureCoordinates[i];
            TextureIndex = inTextureIndex[i];
            FrameData = inFrameData[i];
                       
            FragPos = gl_in[i].gl_Position;
            gl_Position = uShadowMatrices[face] * FragPos;
            EmitVertex();
        }    
        EndPrimitive();
    }
}  
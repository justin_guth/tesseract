#ifndef TEXTURED_PHONG
#define TEXTURED_PHONG

// Dummy colour
#ifndef ALBEDO_SAMPLING
#define ALBEDO_SAMPLING(p) vec3(1)
#endif

// Dummy normal
#ifndef NORMAL_SAMPLING
#define NORMAL_SAMPLING(p) vec3(0,0,1)
#endif

// Dummy metallicness?
#ifndef METALLIC_SAMPLING
#define METALLIC_SAMPLING(p) 0
#endif

// Dummy roughness
#ifndef ROUGHNESS_SAMPLING
#define ROUGHNESS_SAMPLING(p) 0
#endif

// Dummy roughness
#ifndef AO_SAMPLING
#define AO_SAMPLING(p) 0
#endif

#include "../../res/shaders/PBR.glsl"

vec3 GetColourProcedural()
{
    vec3 albedo     = pow(ALBEDO_SAMPLING(WorldPosition), vec3(2.2));
    vec3 normal     = normalize(TBN * NORMAL_SAMPLING(WorldPosition));
    float metallic  = METALLIC_SAMPLING(WorldPosition);
    float roughness = ROUGHNESS_SAMPLING(WorldPosition);
    float ao        = AO_SAMPLING(WorldPosition);

    return GetColour(
        albedo,
        normal,
        metallic,
        roughness,
        ao
    );
}

#endif
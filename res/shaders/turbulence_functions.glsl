#ifndef NOISE_FUNC_INCLUDED
#define NOISE_FUNC_INCLUDED

#include "../../res/shaders/noise.glsl"

float turbulence_2D(vec2 pos, float amplitude, float frequency, float falloff, int octaves)
{
    float noise = 0;
    for(; octaves > 0; octaves--)
    {
        noise += amplitude * simplex_noise_2D(pos * frequency);
        amplitude /= falloff;
        frequency *= falloff;
    }
    return noise;
}

float turbulence_2D_split_falloff(vec2 pos, float amplitude, float frequency, float falloff_a, float inc_f, int octaves)
{
    float noise = 0;
    for(; octaves > 0; octaves--)
    {
        noise += amplitude * simplex_noise_2D(pos * frequency);
        amplitude /= falloff_a;
        frequency *= inc_f;
    }
    return noise;
}

float turbulence_3D(vec3 pos, float amplitude, float frequency, float falloff, int octaves)
{
    float noise = 0;
    for(; octaves > 0; octaves--)
    {
        noise += amplitude * simplex_noise_3D(pos * frequency);
        amplitude /= falloff;
        frequency *= falloff;
    }
    return noise;
}

float shifted_noise(vec3 pos, float amplitude, float frequency, float base_value, float base_factor, float abs_factor)
{
    return base_factor * amplitude * (base_value - abs_factor * abs(simplex_noise_3D(pos * frequency)));
}

float ridge_turbulence(vec3 pos, float amplitude, float frequency, float falloff, int octaves)
{
    float noise = 0;
    for(; octaves > 0; octaves--)
    {
        noise += amplitude * 2.0 * abs(simplex_noise_3D(pos * frequency) - 1);
        amplitude /= falloff;
        frequency *= falloff;
    }
    return noise;
}

float smooth_valley_turbulence(vec3 pos, float base_amplitude, float base_frequency, float falloff, int octaves, float base_value, float base_factor, float abs_factor)
{
    float noise = shifted_noise(pos, base_amplitude, base_frequency, base_value, base_factor, abs_factor);
    for(; octaves > 0; --octaves)
    {
        base_amplitude /= falloff;
        base_frequency *= falloff;

        float a = base_amplitude * noise;
        noise += shifted_noise(pos, a, base_frequency, base_value, base_factor, abs_factor);
    }
    return noise;
}


float con_cave(vec3 pos, float amplitude, float frequency, float falloff, int octaves, float wall_bend)
{
    float d = 2 * pos.z - 0.5;
    float wall_inset = -4 * wall_bend * (d * d - 0.25);
    return smooth_valley_turbulence(pos, amplitude, frequency, falloff, octaves, 0.5, 0.2, 32.0) - wall_inset;//smooth_valley_turbulence(pos, amplitude, frequency, falloff, octaves, -0.1, 0.75, 4.0) - wall_inset;
}

// I've been unable to find out if loops where every thread loops the same number of times are a performance issue, but taking them apart also allows for greater flexibility, so here we are.

float cave(vec3 pos)
{
    float a = 0.12;
    float f = 0.125;
    float noise = a * simplex_noise_3D(pos * f);

    a = 0.05;
    f = 1;
    noise += a * simplex_noise_3D(pos * f);

    a = 0.0075;
    f = 8;
    noise += a * simplex_noise_3D(pos * f);

    float wall_bend = 0.2;
    float d = 2 * pos.z - 0.5;
    float wall_inset = -4 * wall_bend * (d * d - 0.25);

    //return con_cave(pos, 0.06, 2, 1.2, 3, 0.2);
    return noise - wall_inset;
}

vec3 cave_albedo(vec3 pos)
{
    float grey_offset = 0.5;
    float red_offset = 35;
    float blue_offset = -3;

    float grey_noise = 0;
    float red_noise = 0;
    float blue_noise = 0;


    // Brightness

    float a = 0.1;
    float f = 0.25;
    grey_noise = a * simplex_noise_2D(f * (pos.xy + pos.z + grey_offset));

    a = 0.015;
    f = 4;
    grey_noise += a * simplex_noise_3D(f * (pos + grey_offset));

    // Red tint

    a = 0.2;
    f = 0.125;
    red_noise = a * simplex_noise_2D(f * (pos.xy - pos.z + red_offset));
    red_noise *= red_noise * red_noise;

    // Blue tint

    a = 0.35;
    f = 0.25;
    blue_noise = a * simplex_noise_2D(f * (pos.xy + blue_offset));

    /*
    return (vec3(0.25) + turbulence_3D(pos + 0.5, 0.3, 0.5, 2, 4)) * vec3(
        0.8 + turbulence_3D(pos + 35, 0.2, 0.25, 2, 2),
        0.7, 
        1 + turbulence_3D(pos - 3, 0.3, 0.25, 2, 2));
    */
    return (vec3(0.25) + grey_noise * vec3(
        0.8 + red_noise,
        0.7, 
        1 + blue_noise));
}

float cave_metal(vec3 pos)
{
    float a = 1;
    float f = 0.1;
    float noise = a * simplex_noise_2D((pos.xy + 1806 + 0.1 * pos.z) * f);
    
    a = 0.25;
    f = 0.4;
    noise += a * simplex_noise_2D((pos.xy + 1806 + 0.1 * pos.z) * f);
    
    a = 0.03125;
    f = 3.2;
    noise += a * simplex_noise_2D((pos.xy + 1806 + 0.1 * pos.z) * f);

    float m = 1.00 - abs(noise);

    m = max(m, 0);

    return 1.25 * (simplex_noise_2D(0.1 * pos.xy) + 0.5) * pow(m, 64);
}

#endif
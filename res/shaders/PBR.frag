#version 450 core

layout(location = 0) out vec4 FragColor;
in vec2 TextureCoordinates;
in vec3 WorldPosition;
in vec3 Normal;


uniform vec4 uMaterialDiffuseTint; //TODO: Use this
uniform vec3 uViewPosition;
uniform vec3 uLightPosition;
uniform float uFarPlane;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uMetallicTexture;
uniform sampler2D uRoughnessTexture;
uniform sampler2D uAOTexture;
uniform samplerCube uDepthMap;
uniform float uFogginess;

uniform float uTextureScale;

#define MAX_POINT_LIGHTS 64

uniform int uPointLightCount;

struct PointLight {
    vec3 Position;
    vec3 Color;
};

in mat3 TBN;

uniform PointLight uPointLights[MAX_POINT_LIGHTS];

const float PI = 3.14159265359;

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}  

float DistributionGGX(vec3 normal, vec3 halfway, float roughness)
{
    float a      = roughness*roughness;
    float aSquared     = a*a;
    float normalDotHalfway  = max(dot(normal, halfway), 0.0);
    float normalDotHalfwaySquared = normalDotHalfway*normalDotHalfway;
	
    float denominator = (normalDotHalfwaySquared * (aSquared - 1.0) + 1.0);
    denominator = PI * denominator * denominator;
	
    return aSquared / denominator;
}

float GeometrySchlickGGX(float normalDotView, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float denominator = normalDotView * (1.0 - k) + k;
	
    return normalDotView / denominator;
}

float GeometrySmith(vec3 normal, vec3 view, vec3 light, float roughness)
{
    float normalDotView = max(dot(normal, view), 0.0);
    float normalDotLight = max(dot(normal, light), 0.0);
    float ggx2  = GeometrySchlickGGX(normalDotView, roughness);
    float ggx1  = GeometrySchlickGGX(normalDotLight, roughness);
	
    return ggx1 * ggx2;
}

vec3 getNormalFromMap()
{
    //vec3 tangentNormal = texture(uNormalTexture, TextureCoordinates).xyz; //FIXME Some normal issue in lighting
    vec3 tangentNormal = texture(uNormalTexture, TextureCoordinates).xyz * 2.0 - 1.0;

    // vec3 Q1  = dFdx(WorldPosition);
    // vec3 Q2  = dFdy(WorldPosition);
    // vec2 scaledTextureCoordinates = TextureCoordinates * uTextureScale;
    // vec2 st1 = dFdx(scaledTextureCoordinates);
    // vec2 st2 = dFdy(scaledTextureCoordinates);

    // vec3 N   = normalize(Normal);
    // vec3 T  = normalize(Q1 * st2.t - Q2 * st1.t);
    // vec3 B  = -normalize(cross(N, T));
    // mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

float ShadowCalculation(vec3 fragPosition)
{
    vec3 fragToLight = fragPosition - uLightPosition;
    float currentDepth = length(fragToLight);

    vec3 sampleOffsetDirections[20] = vec3[]
    (
    vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1), 
    vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
    vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
    vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
    vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
    );   

    float shadow = 0.0;
    float epsilon   = 0.2;
    int samples  = 20;
    float viewDistance = length(uViewPosition - fragPosition);

    float lightToShadowCasterDistance = texture(uDepthMap, fragToLight).r;
    float distanceToSurface = abs(currentDepth - lightToShadowCasterDistance);

    //float diskRadius = 0.001; 
    float diskRadius = (1.0 + (viewDistance / uFarPlane)) / 25.0; 
    //float diskRadius = ((distanceToSurface / uFarPlane)); 

    for(int i = 0; i < samples; ++i)
    {
        float closestDepth = texture(uDepthMap, fragToLight + sampleOffsetDirections[i] * diskRadius).r;
        closestDepth *= uFarPlane;
        if(currentDepth - epsilon > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);
    return shadow;
}   

void main()
{	

    vec2 scaledTextureCoordinates = TextureCoordinates * uTextureScale;
    vec3 albedo     = pow(texture(uDiffuseTexture, scaledTextureCoordinates).rgb, vec3(2.2));
    vec3 normal     = getNormalFromMap().rgb;
    float metallic  = texture(uMetallicTexture, scaledTextureCoordinates).r;
    metallic = 0; //TODO Metallic isnt uploaded
    float roughness = texture(uRoughnessTexture, scaledTextureCoordinates).r;
    float ao        = texture(uAOTexture, scaledTextureCoordinates).r;

    vec3 view = normalize(uViewPosition - WorldPosition);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);
	           
    // reflectance equation
    vec3 radiance = vec3(0.0);
    for(int i = 0; i < uPointLightCount; ++i)
    {
        // calculate per-light radiance
        vec3 light = normalize(uPointLights[i].Position - WorldPosition);
        vec3 halfway = normalize(view + light);
        float dist    = length(uPointLights[i].Position - WorldPosition);
        float attenuation = 1.0 / (dist * dist);
        vec3 radianceIncrement     = uPointLights[i].Color * attenuation;        
        
        
        // cook-torrance brdf
        float normalDistributionFactor = DistributionGGX(normal, halfway, roughness);        
        float geometry   = GeometrySmith(normal, view, light, roughness);      
        vec3 fresnel    = fresnelSchlick(max(dot(halfway, view), 0.0), F0);       
        
        

        vec3 kS = fresnel;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;	  
        

        vec3 numerator    = normalDistributionFactor * geometry * fresnel;
        float denominator = 4.0 * max(dot(normal, view), 0.0) * max(dot(normal, light), 0.0) + 0.0001;
        vec3 specular     = numerator / denominator;  
            
        // add to outgoing radiance
        float normalDotLight = max(dot(normal, light), 0.0);                
        radiance += (kD * albedo / PI + specular) * radianceIncrement * normalDotLight; 
        
    }   
  
    vec3 ambient = vec3(0.03) * albedo * ao;

    float shadow = ShadowCalculation(WorldPosition);                      
    vec3 color = ambient + (1.0f - shadow) * radiance;
	
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));  

    float z = gl_FragCoord.z / gl_FragCoord.w;

   
    FragColor = vec4(mix(color, vec3(1.0f), z * uFogginess), 1.0);
    //FragColor = vec4(vec3(roughness), 1.0);
    //FragColor = vec4(albedo, 1.0);
    //FragColor = vec4(vec3(ao), 1.0);
    //FragColor = vec4(normal, 1.0);
    //FragColor = vec4(ambient, 1.0);
    //FragColor = vec4(radiance, 1.0);
    

}  
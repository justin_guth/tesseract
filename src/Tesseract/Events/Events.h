#pragma once

#include <stdint.h>
#include <string>
#include <functional>
#include <sstream>

#include "Tesseract/Reference.h"
#include "Tesseract/Core/Scene.h"

namespace Tesseract
{
    namespace Events
    {
        enum class EventType
        {
            None = 0,

            WindowClose,
            WindowResize,
            WindowFocus,
            WindowLostFocus,
            WindowMoved,

            AppTick,
            AppUpdate,
            AppRender,

            KeyPressed,
            KeyReleased,

            MouseButtonPressed,
            MouseButtonReleased,
            MouseMoved,
            MouseScrolled,

            SceneLoaded
        };


        class Event
        {
            friend class EventHandler;

        public:

            virtual EventType GetEventType() const = 0;
            virtual const char* GetName() const = 0;

            virtual std::string ToString() const { return GetName(); }

            inline bool Handled() { return m_IsHandled; }

        protected:

            bool m_IsHandled = false;

        protected:

            inline void MarkHandled(bool handled = true) { m_IsHandled = handled; }
        };

        class EventHandler
        {
        public:

            EventHandler(Event& event) : m_Event(event)
            {

            }

            template<typename T_Type>
            bool Handle(std::function<bool(T_Type&)> function)
            {
                if (m_Event.GetEventType() == T_Type::GetStaticType())
                {
                    m_Event.MarkHandled(function(*((T_Type*)&m_Event)));
                    return true;
                }

                return false;
            }

        private:

            Event& m_Event;

        };

        inline std::ostream& operator<<(std::ostream& outputStream, const Event& event)
        {
            return outputStream << event.ToString();
        }


        // Events:

        class KeyEvent : public Event
        {
        public:

            inline int GetKeyCode() const { return m_KeyCode; }

        protected:

            KeyEvent(int keyCode) : m_KeyCode(keyCode)
            {

            }

        protected:

            int m_KeyCode;

        };


        class KeyPressedEvent : public KeyEvent
        {
        public:

            KeyPressedEvent(int keyCode, int repeatCount) :
                KeyEvent(keyCode), m_RepeatCount(repeatCount)
            {
            }

            inline static EventType GetStaticType() { return EventType::KeyPressed; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "KeyPressed"; }

            inline int getRepeatCount() const { return m_RepeatCount; }

            inline std::string ToString() const override
            {
                std::stringstream stringStream;
                stringStream << "KeyPressedEvent: " << m_KeyCode << " (" << m_RepeatCount << " times repeated)";
                return stringStream.str();
            }

        private:

            int m_RepeatCount;
        };


        class KeyReleasedEvent : public KeyEvent
        {
        public:

            KeyReleasedEvent(int keyCode)
                : KeyEvent(keyCode)
            {
            }

            inline static EventType GetStaticType() { return EventType::KeyReleased; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "KeyReleased"; }

            inline std::string ToString() const override
            {
                std::stringstream stringStream;
                stringStream << "KeyReleasedEvent: " << m_KeyCode;
                return stringStream.str();
            }
        };


        class MouseButtonEvent : public Event
        {
        public:

            inline int GetMouseButtonCode() const { return m_ButtonCode; }

        protected:

            int m_ButtonCode;

            MouseButtonEvent(int buttonCode) : m_ButtonCode(buttonCode)
            {
            }

        };


        class MouseButtonPressedEvent : public MouseButtonEvent
        {
        public:

            MouseButtonPressedEvent(int buttonCode) : MouseButtonEvent(buttonCode)
            {
            }

            inline static EventType GetStaticType() { return EventType::MouseButtonPressed; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "MouseButtonPressed"; }

            inline std::string ToString() const override
            {
                std::stringstream stringStream;
                stringStream << "MouseButtonPressedEvent: " << m_ButtonCode;
                return stringStream.str();
            }
        };


        class MouseButtonReleasedEvent : public MouseButtonEvent
        {
        public:

            MouseButtonReleasedEvent(int buttonCode) : MouseButtonEvent(buttonCode)
            {
            }

            inline static EventType GetStaticType() { return EventType::MouseButtonReleased; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "MouseButtonReleased"; }

            inline std::string ToString() const override
            {
                std::stringstream stringStream;
                stringStream << "MouseButtonReleasedEvent: " << m_ButtonCode;
                return stringStream.str();
            }
        };


        class MouseMovedEvent : public Event
        {
        public:

            MouseMovedEvent(float positionX, float positionY) :
                m_PositionX(positionX),
                m_PositionY(positionY)
            {
            }

            inline static EventType GetStaticType() { return EventType::MouseMoved; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "MouseMoved"; }

            inline float GetMousePositionX() const { return m_PositionX; }
            inline float GetMousePositionY() const { return m_PositionY; }

        private:

            float m_PositionX;
            float m_PositionY;
        };


        class MouseScrolledEvent : public Event
        {
        public:

            MouseScrolledEvent(float offsetX, float offsetY) :
                m_OffsetX(offsetX),
                m_OffsetY(offsetY)
            {
            }

            inline static EventType GetStaticType() { return EventType::MouseScrolled; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "MouseScrolled"; }

            inline float GetOffsetX() const { return m_OffsetX; }
            inline float GetOffsetY() const { return m_OffsetY; }

        private:

            float m_OffsetX;
            float m_OffsetY;
        };


        class WindowCloseEvent : public Event
        {
        public:
            WindowCloseEvent()
            {
            }

            inline static EventType GetStaticType() { return EventType::WindowClose; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "WindowClose"; }
        };

        class WindowResizeEvent : public Event
        {
        public:
            WindowResizeEvent(uint32_t width, uint32_t height) :
                m_Width(width),
                m_Height(height)
            {
            }

            inline static EventType GetStaticType() { return EventType::WindowResize; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "WindowResize"; }

            inline uint32_t GetWidth() const { return m_Width; }
            inline uint32_t GetHeight() const { return m_Height; }

        private:

            uint32_t m_Width;
            uint32_t m_Height;

        };

        class SceneLoadedEvent : public Event
        {
        public:
            SceneLoadedEvent(const Ref<Core::Scene>& scene) :
                m_Scene(scene)
            {
            }

            inline static EventType GetStaticType() { return EventType::SceneLoaded; }
            inline virtual EventType GetEventType() const override { return GetStaticType(); }
            inline virtual const char* GetName() const override { return "SceneLoadedEvent"; }

            inline const Ref<Core::Scene>& GetScene() const { return m_Scene; }

        private:

            const Ref<Core::Scene> m_Scene;

        };

    } // namespace Events

} // namespace Tesseract

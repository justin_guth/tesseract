#pragma once

namespace Tesseract
{
	namespace IO
	{

		enum class KeyCode
		{

			_Unknown = 0,   // 0  GLFW_KEY_UNKNOWN            -1
								  
			_Space,         // 1  GLFW_KEY_SPACE              32
								  
			_Apostrophe,    // 2  GLFW_KEY_APOSTROPHE         39  /* ' */
								  
			_Comma,         // 3  GLFW_KEY_COMMA              44  /* , */
			_Minus,         // 4  GLFW_KEY_MINUS              45  /* - */
			_Period,        // 5  GLFW_KEY_PERIOD             46  /* . */
			_Slash,         // 6  GLFW_KEY_SLASH              47  /* / */
			_0,				// 7  GLFW_KEY_0                  48
			_1,				// 8  GLFW_KEY_1                  49
			_2,				// 9  GLFW_KEY_2                  50
			_3,				// 10 GLFW_KEY_3                  51
			_4,				// 11 GLFW_KEY_4                  52
			_5,				// 12 GLFW_KEY_5                  53
			_6,				// 13 GLFW_KEY_6                  54
			_7,				// 14 GLFW_KEY_7                  55
			_8,				// 15 GLFW_KEY_8                  56
			_9,				// 16 GLFW_KEY_9                  57
			
			_Semicolon,		// 17 GLFW_KEY_SEMICOLON          59  /* ; */
			
			_Equal,			// 18 GLFW_KEY_EQUAL              61  /* = */
			
			_A,				// 19 GLFW_KEY_A                  65
			_B,				// 20 GLFW_KEY_B                  66
			_C,				// 21 GLFW_KEY_C                  67
			_D,				// 22 GLFW_KEY_D                  68
			_E,				// 23 GLFW_KEY_E                  69
			_F,				// 24 GLFW_KEY_F                  70
			_G,				// 25 GLFW_KEY_G                  71
			_H,				// 26 GLFW_KEY_H                  72
			_I,				// 27 GLFW_KEY_I                  73
			_J,				// 28 GLFW_KEY_J                  74
			_K,				// 29 GLFW_KEY_K                  75
			_L,				// 30 GLFW_KEY_L                  76
			_M,				// 31 GLFW_KEY_M                  77
			_N,				// 32 GLFW_KEY_N                  78
			_O,				// 33 GLFW_KEY_O                  79
			_P,				// 34 GLFW_KEY_P                  80
			_Q,				// 35 GLFW_KEY_Q                  81
			_R,				// 36 GLFW_KEY_R                  82
			_S,				// 37 GLFW_KEY_S                  83
			_T,				// 38 GLFW_KEY_T                  84
			_U,				// 39 GLFW_KEY_U                  85
			_V,				// 40 GLFW_KEY_V                  86
			_W,				// 41 GLFW_KEY_W                  87
			_X,				// 42 GLFW_KEY_X                  88
			_Y,				// 43 GLFW_KEY_Y                  89
			_Z,				// 44 GLFW_KEY_Z                  90

			_LeftBracket,	// 45 GLFW_KEY_LEFT_BRACKET       91  /* [ */
			_Backslash,		// 46 GLFW_KEY_BACKSLASH          92  /* \ */
			_RightBrackt,	// 47 GLFW_KEY_RIGHT_BRACKET      93  /* ] */
			_GraveAccent,	// 48 GLFW_KEY_GRAVE_ACCENT       96  /* ` */

			_World1,		// 49 GLFW_KEY_WORLD_1            161 /* non-US #1 */
			_World2,		// 50 GLFW_KEY_WORLD_2            162 /* non-US #2 */
			
			_Escape,		// 51 GLFW_KEY_ESCAPE             256
			_Enter,			// 52 GLFW_KEY_ENTER              257
			_Tab,			// 53 GLFW_KEY_TAB                258
			_Backspace,		// 54 GLFW_KEY_BACKSPACE          259
			_Insert,		// 55 GLFW_KEY_INSERT             260
			_Delete,		// 56 GLFW_KEY_DELETE             261
			_Right,			// 57 GLFW_KEY_RIGHT              262
			_Left,			// 58 GLFW_KEY_LEFT               263
			_Down,			// 59 GLFW_KEY_DOWN               264
			_Up,			// 61 GLFW_KEY_UP                 265
			_PageUp,		// 62 GLFW_KEY_PAGE_UP            266
			_PageDown,		// 63 GLFW_KEY_PAGE_DOWN          267
			_Home,			// 64 GLFW_KEY_HOME               268
			_End,			// 65 GLFW_KEY_END                269

			_CapsLock,		// 66 GLFW_KEY_CAPS_LOCK          280
			_ScrollLock,	// 67 GLFW_KEY_SCROLL_LOCK        281
			_NumLock,		// 68 GLFW_KEY_NUM_LOCK           282
			_PrintScreen,	// 69 GLFW_KEY_PRINT_SCREEN       283
			_Pause,			// 70 GLFW_KEY_PAUSE              284

			_F1,			// 71 GLFW_KEY_F1                 290
			_F2,			// 72 GLFW_KEY_F2                 291
			_F3,			// 73 GLFW_KEY_F3                 292
			_F4,			// 74 GLFW_KEY_F4                 293
			_F5,			// 75 GLFW_KEY_F5                 294
			_F6,			// 76 GLFW_KEY_F6                 295
			_F7,			// 77 GLFW_KEY_F7                 296
			_F8,			// 78 GLFW_KEY_F8                 297
			_F9,			// 79 GLFW_KEY_F9                 298
			_F10,			// 80 GLFW_KEY_F10                299
			_F11,			// 81 GLFW_KEY_F11                300
			_F12,			// 82 GLFW_KEY_F12                301
			_F13,			// 83 GLFW_KEY_F13                302
			_F14,			// 84 GLFW_KEY_F14                303
			_F15,			// 85 GLFW_KEY_F15                304
			_F16,			// 86 GLFW_KEY_F16                305
			_F17,			// 87 GLFW_KEY_F17                306
			_F18,			// 88 GLFW_KEY_F18                307
			_F19,			// 89 GLFW_KEY_F19                308
			_F20,			// 90 GLFW_KEY_F20                309
			_F21,			// 91 GLFW_KEY_F21                310
			_F22,			// 92 GLFW_KEY_F22                311
			_F23,			// 93 GLFW_KEY_F23                312
			_F24,			// 94 GLFW_KEY_F24                313
			_F25,			// 95 GLFW_KEY_F25                314

			_Numpad0,		// 96 GLFW_KEY_KP_0               320
			_Numpad1,		// 97 GLFW_KEY_KP_1               321
			_Numpad2,		// 98 GLFW_KEY_KP_2               322
			_Numpad3,		// 99 GLFW_KEY_KP_3               323
			_Numpad4,		// 100 GLFW_KEY_KP_4               324
			_Numpad5,		// 101 GLFW_KEY_KP_5               325
			_Numpad6,		// 102 GLFW_KEY_KP_6               326
			_Numpad7,		// 103 GLFW_KEY_KP_7               327
			_Numpad8,		// 104 GLFW_KEY_KP_8               328
			_Numpad9,		// 105 GLFW_KEY_KP_9               329
			_NumpadDecimal,	// 106 GLFW_KEY_KP_DECIMAL         330
			_NumpadDivide,  // 107 GLFW_KEY_KP_DIVIDE          331
			_NumpadMultiply,// 108 GLFW_KEY_KP_MULTIPLY        332
			_NumpadSubtract,// 109 GLFW_KEY_KP_SUBTRACT        333
			_NumpadAdd,		// 110 GLFW_KEY_KP_ADD             334
			_NumpadEnter,	// 111 GLFW_KEY_KP_ENTER           335
			_NumpadEqual,	// 112 GLFW_KEY_KP_EQUAL           336

			_LeftShift,		// 113 GLFW_KEY_LEFT_SHIFT         340
			_LeftControl,	// 114 GLFW_KEY_LEFT_CONTROL       341
			_LeftAlt,		// 115 GLFW_KEY_LEFT_ALT           342
			_LeftSuper,		// 116 GLFW_KEY_LEFT_SUPER         343
			_RightShift,	// 117 GLFW_KEY_RIGHT_SHIFT        344
			_RightControl,	// 118 GLFW_KEY_RIGHT_CONTROL      345
			_RightAlt,		// 119 GLFW_KEY_RIGHT_ALT          346
			_RightSuper,	// 120 GLFW_KEY_RIGHT_SUPER        347
			_Menu,			// 121 GLFW_KEY_MENU               348

			Count
		};
	}
}
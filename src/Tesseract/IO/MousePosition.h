#pragma once


#include "Tesseract/Math/Vector2.h"

namespace Tesseract
{
    namespace IO
    {
        class MousePosition
        {
        public:

            float X;
            float Y;

            MousePosition(float x, float y);

            inline Math::Vector2 ToVector2() const;
        };

        inline Math::Vector2 MousePosition::ToVector2() const
        {
            return {X, Y};
        }

    }
}

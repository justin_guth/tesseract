#include "Input.h"
#include "Tesseract/Events/Events.h"

#include "Tesseract/IO/KeyCode.h"

#include <memory>
#include <functional>

#include "GLFW/glfw3.h"

namespace Tesseract
{
	namespace IO
	{
		static bool KeyPressedEvent(Events::KeyPressedEvent& event);
		static bool KeyReleasedEvent(Events::KeyReleasedEvent& event);

		static bool MouseButtonPressedEvent(Events::MouseButtonPressedEvent& event);
		static bool MouseButtonReleasedEvent(Events::MouseButtonReleasedEvent& event);

		static bool MouseMovedEvent(Events::MouseMovedEvent& event);

		static float s_MousePositionX;
		static float s_MousePositionY;

		static bool* s_KeysDown;
		static bool* s_KeysUp;
		static bool* s_KeysPressed;
		static bool* s_KeysReleased;

		static bool* s_MouseButtonsDown;
		static bool* s_MouseButtonsUp;
		static bool* s_MouseButtonsPressed;
		static bool* s_MouseButtonsReleased;

		static KeyCode MapGlfwKey(int glfwKeyCode);
		static MouseButton MapGlfwMouseButton(int button);

		static KeyCode MapGlfwKey(int glfwKeyCode)
		{

			if (glfwKeyCode >= 65 && glfwKeyCode <= 90)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_A) + glfwKeyCode - 65);
			}
			else if (glfwKeyCode >= 44 && glfwKeyCode <= 57)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_Comma) + glfwKeyCode - 44);
			}
			else if (glfwKeyCode >= 340 && glfwKeyCode <= 348)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_LeftShift) + glfwKeyCode - 340);
			}
			else if (glfwKeyCode == GLFW_KEY_SPACE)
			{
				return KeyCode::_Space;
			}
			else if (glfwKeyCode == GLFW_KEY_UNKNOWN)
			{
				return KeyCode::_Unknown;
			}
			else if (glfwKeyCode == GLFW_KEY_APOSTROPHE)
			{
				return KeyCode::_Apostrophe;
			}
			else if (glfwKeyCode == GLFW_KEY_SEMICOLON)
			{
				return KeyCode::_Semicolon;
			}
			else if (glfwKeyCode == GLFW_KEY_EQUAL)
			{
				return KeyCode::_Equal;
			}
			else if (glfwKeyCode >= 91 && glfwKeyCode <= 96)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_LeftBracket) + glfwKeyCode - 91);
			}
			else if (glfwKeyCode >= 161 && glfwKeyCode <= 162)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_World1) + glfwKeyCode - 161);
			}
			else if (glfwKeyCode >= 256 && glfwKeyCode <= 269)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_Escape) + glfwKeyCode - 256);
			}
			else if (glfwKeyCode >= 280 && glfwKeyCode <= 284)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_CapsLock) + glfwKeyCode - 280);
			}
			else if (glfwKeyCode >= 290 && glfwKeyCode <= 314)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_F1) + glfwKeyCode - 290);
			}
			else if (glfwKeyCode >= 320 && glfwKeyCode <= 336)
			{
				return static_cast<KeyCode>(static_cast<int>(KeyCode::_Numpad0) + glfwKeyCode - 320);
			}

			return KeyCode::_Unknown;
		}

		static MouseButton MapGlfwMouseButton(int button)
		{
			if (button >= GLFW_MOUSE_BUTTON_1 && button <= GLFW_MOUSE_BUTTON_8)
			{
				return static_cast<MouseButton>(static_cast<int>(MouseButton::_1) + button);
			}

			return MouseButton::_Unknown;
		}

		bool Input::KeyDown(KeyCode code)
		{
			return s_KeysDown[static_cast<int>(code)];
		}

		bool Input::KeyUp(KeyCode code)
		{
			return s_KeysUp[static_cast<int>(code)];
		}

		bool Input::KeyPressed(KeyCode code)
		{
			return s_KeysPressed[static_cast<int>(code)];
		}

		bool Input::KeyReleased(KeyCode code)
		{
			return s_KeysReleased[static_cast<int>(code)];
		}

		bool Input::MouseButtonDown(MouseButton button)
		{
			return s_MouseButtonsDown[static_cast<int>(button)];
		}

		bool Input::MouseButtonUp(MouseButton button)
		{
			return s_MouseButtonsUp[static_cast<int>(button)];
		}

		bool Input::MouseButtonPressed(MouseButton button)
		{
			return s_MouseButtonsPressed[static_cast<int>(button)];
		}

		bool Input::MouseButtonReleased(MouseButton button)
		{
			return s_MouseButtonsReleased[static_cast<int>(button)];
		}

		float Input::GetMouseX()
		{
			return s_MousePositionX;
		}

		float Input::GetMouseY()
		{
			return s_MousePositionY;
		}

		MousePosition Input::GetMousePosition()
		{
			return { s_MousePositionX, s_MousePositionY };
		}


		bool Input::OnEvent(Events::Event& event)
		{
			Events::EventHandler dispatcher(event);
			dispatcher.Handle<Events::KeyPressedEvent>(&KeyPressedEvent);
			dispatcher.Handle<Events::KeyReleasedEvent>(&KeyReleasedEvent);

			dispatcher.Handle<Events::MouseButtonPressedEvent>(&MouseButtonPressedEvent);
			dispatcher.Handle<Events::MouseButtonReleasedEvent>(&MouseButtonReleasedEvent);

			dispatcher.Handle<Events::MouseMovedEvent>(&MouseMovedEvent);

			return false;
		}

		void Input::Update()
		{
			for (size_t i = 0; i < static_cast<int>(KeyCode::Count); i++)
			{
				s_KeysPressed[i] = false;
				s_KeysReleased[i] = false;
			}

			for (size_t i = 0; i < static_cast<int>(MouseButton::Count); i++)
			{
				s_MouseButtonsPressed[i] = false;
				s_MouseButtonsReleased[i] = false;
			}
		}

		static bool KeyPressedEvent(Events::KeyPressedEvent& event)
		{

			int keyCode = event.GetKeyCode();
			KeyCode code = MapGlfwKey(keyCode);
			int arrayPosition = static_cast<int>(code);

			s_KeysPressed[arrayPosition] = true;
			s_KeysDown[arrayPosition] = true;
			s_KeysUp[arrayPosition] = false;

			return false;
		}

		static bool KeyReleasedEvent(Events::KeyReleasedEvent& event)
		{

			int keyCode = event.GetKeyCode();
			KeyCode code = MapGlfwKey(keyCode);
			int arrayPosition = static_cast<int>(code);

			s_KeysReleased[arrayPosition] = true;
			s_KeysUp[arrayPosition] = true;
			s_KeysDown[arrayPosition] = false;

			return false;
		}

		static bool MouseButtonPressedEvent(Events::MouseButtonPressedEvent& event)
		{

			int buttonCode = event.GetMouseButtonCode();
			MouseButton button = MapGlfwMouseButton(buttonCode);
			int arrayPosition = static_cast<int>(button);

			s_MouseButtonsPressed[arrayPosition] = true;
			s_MouseButtonsDown[arrayPosition] = true;
			s_MouseButtonsUp[arrayPosition] = false;

			if (buttonCode == GLFW_MOUSE_BUTTON_LEFT)
			{
				arrayPosition = static_cast<int>(MouseButton::_Left);

				s_MouseButtonsPressed[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = false;
			}

			else if (buttonCode == GLFW_MOUSE_BUTTON_RIGHT)
			{
				arrayPosition = static_cast<int>(MouseButton::_Right);

				s_MouseButtonsPressed[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = false;
			}

			else if (buttonCode == GLFW_MOUSE_BUTTON_MIDDLE)
			{
				arrayPosition = static_cast<int>(MouseButton::_Middle);

				s_MouseButtonsPressed[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = false;
			}

			return false;
		}

		static bool MouseButtonReleasedEvent(Events::MouseButtonReleasedEvent& event)
		{

			int buttonCode = event.GetMouseButtonCode();
			MouseButton button = MapGlfwMouseButton(buttonCode);
			int arrayPosition = static_cast<int>(button);

			s_MouseButtonsReleased[arrayPosition] = true;
			s_MouseButtonsUp[arrayPosition] = true;
			s_MouseButtonsDown[arrayPosition] = false;

			if (buttonCode == GLFW_MOUSE_BUTTON_LEFT)
			{
				arrayPosition = static_cast<int>(MouseButton::_Left);

				s_MouseButtonsReleased[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = false;
			}

			else if (buttonCode == GLFW_MOUSE_BUTTON_RIGHT)
			{
				arrayPosition = static_cast<int>(MouseButton::_Right);

				s_MouseButtonsReleased[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = false;
			}

			else if (buttonCode == GLFW_MOUSE_BUTTON_MIDDLE)
			{
				arrayPosition = static_cast<int>(MouseButton::_Middle);

				s_MouseButtonsReleased[arrayPosition] = true;
				s_MouseButtonsUp[arrayPosition] = true;
				s_MouseButtonsDown[arrayPosition] = false;
			}

			return false;
		}

		static bool MouseMovedEvent(Events::MouseMovedEvent& event)
		{
			s_MousePositionX = event.GetMousePositionX();
			s_MousePositionY = event.GetMousePositionY();
			return false;
		}

		void Input::Init()
		{

			s_MousePositionX = 0.0f;
			s_MousePositionY = 0.0f;

			s_KeysDown = (bool*)std::malloc(sizeof(bool) * static_cast<int>(KeyCode::Count));
			s_KeysUp = (bool*)std::malloc(sizeof(bool) * static_cast<int>(KeyCode::Count));
			s_KeysPressed = (bool*)std::malloc(sizeof(bool) * static_cast<int>(KeyCode::Count));
			s_KeysReleased = (bool*)std::malloc(sizeof(bool) * static_cast<int>(KeyCode::Count));

			s_MouseButtonsDown = (bool*)std::malloc(sizeof(bool) * static_cast<int>(MouseButton::Count));
			s_MouseButtonsUp = (bool*)std::malloc(sizeof(bool) * static_cast<int>(MouseButton::Count));
			s_MouseButtonsPressed = (bool*)std::malloc(sizeof(bool) * static_cast<int>(MouseButton::Count));
			s_MouseButtonsReleased = (bool*)std::malloc(sizeof(bool) * static_cast<int>(MouseButton::Count));

			for (size_t i = 0; i < static_cast<int>(KeyCode::Count); i++)
			{
				s_KeysDown[i] = false;
				s_KeysUp[i] = true;
				s_KeysPressed[i] = false;
				s_KeysReleased[i] = false;
			}

			for (size_t i = 0; i < static_cast<int>(MouseButton::Count); i++)
			{
				s_MouseButtonsDown[i] = false;
				s_MouseButtonsUp[i] = true;
				s_MouseButtonsPressed[i] = false;
				s_MouseButtonsReleased[i] = false;
			}
		}
		void Input::Shutdown()
		{

			std::free(s_KeysDown);
			std::free(s_KeysUp);
			std::free(s_KeysPressed);
			std::free(s_KeysReleased);

			std::free(s_MouseButtonsDown);
			std::free(s_MouseButtonsUp);
			std::free(s_MouseButtonsPressed);
			std::free(s_MouseButtonsReleased);
		}
	}

}


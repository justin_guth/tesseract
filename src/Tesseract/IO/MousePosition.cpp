#include "MousePosition.h"

namespace Tesseract
{
    namespace IO
    {
        MousePosition::MousePosition(float x, float y) :
            X(x),
            Y(y)
        {
        }
    }
}
#pragma once
#include "KeyCode.h"
#include "MouseButton.h"
#include "MousePosition.h"
#include "Tesseract/Events/Events.h"

namespace Tesseract
{
	namespace IO
	{
		class Input
		{
		public:

			static bool KeyPressed(KeyCode code);
			static bool KeyReleased(KeyCode code);
			static bool KeyUp(KeyCode code);
			static bool KeyDown(KeyCode code);

			static bool MouseButtonPressed(MouseButton button);
			static bool MouseButtonReleased(MouseButton button);
			static bool MouseButtonUp(MouseButton button);
			static bool MouseButtonDown(MouseButton button);

			static float GetMouseX();
			static float GetMouseY();
			static MousePosition GetMousePosition();

			static void Init();
			static void Update();
			static void Shutdown();

			static bool OnEvent(Events::Event& event);
		};

	} 

} 


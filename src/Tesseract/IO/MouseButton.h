#pragma once

namespace Tesseract
{
	namespace IO
	{

		enum class MouseButton
		{

			_Unknown = 0,

			_1,
			_2,
			_3,
			_4,
			_5,
			_6,
			_7,
			_8,
			_Left,
			_Right,
			_Middle,

			Count
		};
	}
}
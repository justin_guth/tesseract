#pragma once
#include <memory>

namespace Tesseract
{
	namespace Memory
	{
		/**
		* SlotAllocator provides simplified pool allocation for a given type.
		* 
		* \tparam T_Type the type for which slots should be allocated
		* \tparam T_MaxCount the number of T_Type elements that may be stored
		*/
		template <typename T_Type, int64_t T_MaxCount>
		class SlotAllocator
		{
		private:

			/**
			* Points to the base memory location.
			*/
			T_Type* m_MemoryBasePointer;

			/**
			* Points to the next available node. If all slots are occupied it points to nullptr.
			*/
			T_Type** m_NextFreeNode;

		public:

			/**
			* Constructor. Allocates and initialises memory through std::malloc.
			* 
			* The initialisation runtime complexity lies in O(T_MaxCount)
			*/
			SlotAllocator();

			/**
			* Destructor. Frees the allocated memory using std::free.
			*/
			~SlotAllocator();

			/**
			* Check if memory is full.
			* 
			* \return Whether memory is full
			*/
			bool Full();

			/**
			* Allocate a slot and assign the object's value to it.
			* 
			* \param object the object whose value should be assigned to the allocated slot
			* \return A pointer to the allocated slot
			*/
			T_Type* Allocate(const T_Type& object);

			/**
			* Allocate a slot.
			*
			* \return A pointer to the allocated slot
			*/
			T_Type* AllocateSlot();

			/**
			* Free a slot.
			*
			* \param address pointer to the slot that should be freed
			*/
			void FreeSlot(T_Type* address);
		};

		template<typename T_Type, int64_t T_MaxCount>
		inline SlotAllocator<T_Type, T_MaxCount>::SlotAllocator()
		{
			// Make sure each slot has enough space to accomodate a pointer to the next slot
			static_assert(sizeof(T_Type) > sizeof(T_Type*), "The allocated type must be at least as wide as a pointer!");

			// Allocate the required space on the heap and store the address in m_MemoryBasePointer
			m_MemoryBasePointer = (T_Type*)std::malloc(T_MaxCount * sizeof(T_Type));

			// Throw an exception if an invalid memory address is returned
			if (m_MemoryBasePointer == nullptr) {

				throw - 1;
			}

			// Assign the first slot's address to m_MemoryBasePointer
			// (Note: since the linked list within the free slots works by pointing to a pointer the base pointer is cast to a T_Type**)
			m_NextFreeNode = (T_Type**)m_MemoryBasePointer;

			// Begin construction of the linked list by going backwards through the allocated space
			// nextPointer stores the address of the next slot
			T_Type** nextPointer = nullptr;

			// Iterate from back to front through all slots
			for (int64_t i = T_MaxCount - 1; i >= 0; i--) {

				// Store slot address in position
				T_Type* position = m_MemoryBasePointer + i;

				// store current position as T_Type**
				T_Type** newNext = (T_Type**)position;

				// Store the address of the next slot at the current address.
				*(newNext) = (T_Type*)nextPointer;

				// Set nextPointer to point to current slot
				nextPointer = newNext;
			}
		}

		template<typename T_Type, int64_t T_MaxCount>
		inline SlotAllocator<T_Type, T_MaxCount>::~SlotAllocator()
		{
			// Deallocate all memory
			std::free(m_MemoryBasePointer);
		}

		template<typename T_Type, int64_t T_MaxCount>
		inline bool SlotAllocator<T_Type, T_MaxCount>::Full()
		{
			return m_NextFreeNode == nullptr;
		}

		template<typename T_Type, int64_t T_MaxCount>
		inline T_Type* SlotAllocator<T_Type, T_MaxCount>::AllocateSlot()
		{
			// Throw an exception if no free node exists
			// (The last node always contains nullptr as next)
			if (Full()) {

				throw - 1;
			}

			// Get the pointer to the allocated slot
			T_Type* result = (T_Type*)m_NextFreeNode;

			// Set the next free node to the next slot stored in the current slot
			m_NextFreeNode = (T_Type**)*m_NextFreeNode;

			return result;
		}

		template<typename T_Type, int64_t T_MaxCount>
		inline T_Type* SlotAllocator<T_Type, T_MaxCount>::Allocate(const T_Type& object)
		{
			// Allocate slot for object
			T_Type* result = AllocateSlot();

			// Assign the object value to the slot object
			*result = object;

			return result;
		}

		
		template<typename T_Type, int64_t T_MaxCount>
		inline void SlotAllocator<T_Type, T_MaxCount>::FreeSlot(T_Type* address)
		{
			// Write the pointer to the current next free node to the deallocated slot
			*((T_Type**)address) = (T_Type*)m_NextFreeNode;

			// Set the next free node to the now deallocated slot
			m_NextFreeNode = (T_Type**)address;
		}

	}

} 

#pragma once
#include <memory>
#include <vector>

namespace Tesseract
{
	namespace Memory
	{
		/**
		* DynamicSlotAllocator provides simplified pool allocation for a given type.
		*
		* \tparam T_Type the type for which slots should be allocated
		* \tparam T_MaxCount the number of T_Type elements that may be stored
		*/
		template <typename T_Type>
		class DynamicSlotAllocator
		{
		private:

			/**
			* Points to the base memory location.
			*/
			T_Type* m_MemoryBasePointer;

			/**
			* Points to the next available node. If all slots are occupied it points to nullptr.
			*/
			T_Type** m_NextFreeNode;

			uint32_t m_Capacity;
			uint32_t m_LastBlockCapacity;

			std::vector<void*> m_Blocks;

		public:

			/**
			* Constructor. Allocates and initialises memory through std::malloc.
			*
			* The initialisation runtime complexity lies in O(initialCapacity)
			*/
			DynamicSlotAllocator(uint32_t initialCapacity = 16);

			/**
			* Destructor. Frees the allocated memory using std::free.
			*/
			~DynamicSlotAllocator();

			/**
			* Allocate a slot and call the default constructor of T_Type.
			*
			* \return A pointer to the allocated slot
			*/
			T_Type* Allocate();

			/**
			* Allocate a slot and assign the object's value to it.
			*
			* \param object the object whose value should be assigned to the allocated slot
			* \return A pointer to the allocated slot
			*/
			T_Type* Allocate(const T_Type& object);

			/**
			* Allocate a slot.
			*
			* \return A pointer to the allocated slot
			*/
			T_Type* AllocateSlot();

			/**
			* Free a slot.
			*
			* \param address pointer to the slot that should be freed
			*/
			void FreeSlot(T_Type* address);


			inline uint32_t GetCapacity() const;

		private:

			void SetUp(T_Type* baseAddress, uint32_t count);
		};

		template<typename T_Type>
		inline DynamicSlotAllocator<T_Type>::DynamicSlotAllocator(uint32_t initialCapacity) :
			m_Capacity(initialCapacity),
			m_LastBlockCapacity(initialCapacity),
			m_Blocks()
		{
			// Make sure each slot has enough space to accomodate a pointer to the next slot
			static_assert(sizeof(T_Type) >= sizeof(T_Type*), "The allocated type must be at least as wide as a pointer!");

			// Allocate the required space on the heap and store the address in m_MemoryBasePointer
			m_MemoryBasePointer = (T_Type*) std::malloc(m_Capacity * sizeof(T_Type));
			m_Blocks.push_back(m_MemoryBasePointer);

			// Throw an exception if an invalid memory address is returned
			if (m_MemoryBasePointer == nullptr)
			{

				throw - 1;
			}

			SetUp(m_MemoryBasePointer, m_LastBlockCapacity);
		}

		template<typename T_Type>
		inline DynamicSlotAllocator<T_Type>::~DynamicSlotAllocator()
		{
			// Deallocate all memory
			for (std::vector<void*>::iterator it = m_Blocks.begin(); it != m_Blocks.end(); it++)
			{
				std::free(*it);
			}
		}

		template<typename T_Type>
		inline T_Type* DynamicSlotAllocator<T_Type>::Allocate()
		{
			// Allocate slot for object
			T_Type* result = AllocateSlot();

			// Call default constructor
			*result = T_Type();

			return result;
		}


		template<typename T_Type>
		inline T_Type* DynamicSlotAllocator<T_Type>::Allocate(const T_Type& object)
		{
			// Allocate slot for object
			T_Type* result = AllocateSlot();

			// Assign the object value to the slot object
			*result = object;

			return result;
		}

		template<typename T_Type>
		inline T_Type* DynamicSlotAllocator<T_Type>::AllocateSlot()
		{
			// Throw an exception if no free node exists
			// (The last node always contains nullptr as next)
			if (m_NextFreeNode == nullptr)
			{
				uint32_t newBlockCapacity = m_LastBlockCapacity * 2;

				void* newBlock = std::malloc(newBlockCapacity * sizeof(T_Type));

				m_LastBlockCapacity = newBlockCapacity;
				m_Capacity += newBlockCapacity;

				m_Blocks.push_back(newBlock);

				SetUp((T_Type*) newBlock, newBlockCapacity);

				m_NextFreeNode = (T_Type**) newBlock;
			}

			// Get the pointer to the allocated slot
			T_Type* result = (T_Type*) m_NextFreeNode;

			// Set the next free node to the next slot stored in the current slot
			m_NextFreeNode = (T_Type**) *m_NextFreeNode;

			return result;
		}

		template<typename T_Type>
		inline void DynamicSlotAllocator<T_Type>::FreeSlot(T_Type* address)
		{
			// Write the pointer to the current next free node to the deallocated slot
			*((T_Type**) address) = (T_Type*) m_NextFreeNode;

			// Set the next free node to the now deallocated slot
			m_NextFreeNode = (T_Type**) address;
		}

		template<typename T_Type>
		inline uint32_t DynamicSlotAllocator<T_Type>::GetCapacity() const
		{
			return m_Capacity;
		}

		template<typename T_Type>
		inline void DynamicSlotAllocator<T_Type>::SetUp(T_Type* baseAddress, uint32_t count)
		{
			// Assign the first slot's address to m_MemoryBasePointer
			// (Note: since the linked list within the free slots works by pointing to a pointer the base pointer is cast to a T_Type**)
			m_NextFreeNode = (T_Type**) baseAddress;

			// Begin construction of the linked list by going backwards through the allocated space
			// nextPointer stores the address of the next slot
			T_Type** nextPointer = nullptr;

			// Iterate from back to front through all slots
			for (int64_t i = m_LastBlockCapacity - 1; i >= 0; i--)
			{

				// Store slot address in position
				T_Type* position = baseAddress + i;

				// store current position as T_Type**
				T_Type** newNext = (T_Type**) position;

				// Store the address of the next slot at the current address.
				*(newNext) = (T_Type*) nextPointer;

				// Set nextPointer to point to current slot
				nextPointer = newNext;
			}
		}
	}
}

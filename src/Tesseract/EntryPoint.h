#include "Tesseract/Core/Application.h"
#include "Tesseract/Logging/Logger.h"
#include "Tesseract/IO/Input.h"
#include "Tesseract/Core/Time.h"
#include "Tesseract/Math/Functions.h"
#include "Tesseract/Util/File.h"
#include "Tesseract/Graphics/Renderer.h"
#include "Tesseract/Audio/Audio.h"
#include "Tesseract/Profiling/Profiler.h"
#include "Tesseract/Physics/Physics.h"

#include <thread>

#ifdef TESSERACT_ENTRY_POINT

extern Tesseract::Core::Application* GetApplication();

int main()
{
    using namespace Tesseract;

    Tesseract::Profiling::Profiler::Init();

    TESSERACT_PROFILER_BEGIN_SESSION("Initialization");

    Logging::Logger::Init();

    TESSERACT_CORE_TRACE("Current woking directory: {}", Util::GetCWD());

    TESSERACT_CORE_INFO("Initializing IO");
    Math::Functions::Init();
    IO::Input::Init();
    Core::Time::Init();

    TESSERACT_CORE_INFO("Initializing Audio");
    Audio::Audio::Init();

    Core::Application* app = GetApplication();
    Core::Application::SetApplication(app);


    app->BeforeInit();
    app->Init();
    Graphics::Renderer::Init();
    app->OnInit();

    TESSERACT_CORE_INFO("Launching Physics Thread");

    std::thread physicsThread;
    physicsThread = std::thread(Physics::Physics::Run);


    app->BeforeRun();
    TESSERACT_PROFILER_END_SESSION();


    TESSERACT_PROFILER_BEGIN_SESSION("Run");
    app->Run();
    TESSERACT_PROFILER_END_SESSION();

    TESSERACT_PROFILER_BEGIN_SESSION("Shutdown");
    app->OnShutdown();

    TESSERACT_CORE_INFO("Stopping Physics Thread");

    Physics::Physics::Stop();
    physicsThread.join();

    app->Shutdown();
    delete app;

    TESSERACT_CORE_INFO("Shutting down IO");

    Graphics::Renderer::Shutdown();
    Audio::Audio::Shutdown();

    IO::Input::Shutdown();
    Logging::Logger::Shutdown();
    TESSERACT_PROFILER_END_SESSION();


    Tesseract::Profiling::Profiler::Shutdown();
}

#endif
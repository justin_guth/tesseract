#pragma once   

#include "al.h"

#include "Tesseract/Math/Vector3.h"
#include "AudioBuffer.h"

namespace Tesseract
{
    namespace Audio
    {

        class AudioSource
        {
        public:

            inline AudioSource():
                m_SourceID(0)
            {
                alGenSources(1, &m_SourceID);

                SetPitch(1.0f);
                SetGain(1.0f);
                SetPosition(0.0f);
                SetVelocity(0.0f);
                SetLooping(false);
            }

            inline ~AudioSource()
            {
                alDeleteSources(1, &m_SourceID);
            }

            inline void SetPitch(float pitch)
            {
                alSourcef(m_SourceID, AL_PITCH, pitch);
            }

            inline void SetGain(float gain)
            {
                alSourcef(m_SourceID, AL_GAIN, gain);
            }

            inline void SetPosition(const Math::Vector3& position)
            {
                alSource3f(m_SourceID, AL_POSITION, position.X, position.Y, position.Z);
            }

            inline void SetVelocity(const Math::Vector3& velocity)
            {
                alSource3f(m_SourceID, AL_VELOCITY, velocity.X, velocity.Y, velocity.Z);
            }

            inline void SetLooping(bool looping = true)
            {
                alSourcei(m_SourceID, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
            }

            inline void SetBuffer(const Ref<AudioBuffer>& buffer)
            {
                m_HeldBuffer = buffer;
                alSourcei(m_SourceID, AL_BUFFER, buffer->GetBufferID());
            }

            inline void Play()
            {
                alSourcePlay(m_SourceID);
            }

            inline void Pause()
            {
                alSourcePause(m_SourceID);
            }

            inline void Stop()
            {
                alSourceStop(m_SourceID);
            }

            inline bool IsPlaying() { ALint state; alGetSourcei(m_SourceID, AL_SOURCE_STATE, &state); return state == AL_PLAYING; }
            inline bool IsPaused() { ALint state; alGetSourcei(m_SourceID, AL_SOURCE_STATE, &state); return state == AL_PAUSED; }

            inline static Ref<AudioSource> Create()
            {
                return MakeRef<AudioSource>();
            }

        private:

            ALuint m_SourceID;
            Ref<AudioBuffer> m_HeldBuffer;

        };
    } // namespace Audio

} // namespace Tesseract

#pragma once

#include "al.h"
#include "alc.h"

#include "Tesseract/Reference.h"
#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
    namespace Audio
    {
        class AudioDevice
        {
        public:

            inline AudioDevice():
                m_Device(nullptr),
                m_Context(nullptr)
            {
            }

            inline ~AudioDevice()
            {
                alcDestroyContext(m_Context);
                alcCloseDevice(m_Device);
            }

            inline void MakeContextCurrent()
            {
                alcMakeContextCurrent(m_Context);
            }

            inline static Ref< AudioDevice> GetDefaultDevice()
            {
                Ref<AudioDevice> result = MakeRef<AudioDevice>();
                result->m_Device = alcOpenDevice(nullptr);
                int attributes[] = { 0 };
                result->m_Context = alcCreateContext(result->m_Device, attributes);

                TESSERACT_CORE_ASSERT(result->m_Context != nullptr, "Failed to create OpenAL context!");

                return result;
            }

        private:

            ALCdevice* m_Device;
            ALCcontext* m_Context;

        };
    } // namespace Audio

} // namespace Tesseract


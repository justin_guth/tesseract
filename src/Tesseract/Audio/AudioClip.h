#pragma once

#include "Tesseract/Reference.h"
#include "Tesseract/Core/Resource.h"

#include "AudioFile.h"
#include "al.h"

#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
    namespace Audio
    {
        class AudioClip: public Core::Resource
        {
        public:

            inline AudioClip(const std::string& path):
                m_AudioFile(),
                m_Data(nullptr)
            {
                m_AudioFile.load(path);

                TESSERACT_CORE_ASSERT(
                    m_AudioFile.getBitDepth() == 8 || m_AudioFile.getBitDepth() == 16,
                    "Invalid bit depth for audio file!"
                );

                m_Data = std::malloc(
                    GetSize()
                );

                int32_t nChannels = m_AudioFile.getNumChannels();

                for (int32_t sample = 0; sample < m_AudioFile.getNumSamplesPerChannel(); sample++)
                {
                    for (int32_t channel = 0; channel < nChannels; channel++)
                    {
                        double sampleValue = m_AudioFile.samples[channel][sample];

                        if (m_AudioFile.getBitDepth() == 8)
                        {
                            ((uint8_t*) m_Data)[sample * nChannels + channel] = (uint8_t) (128 + (sampleValue * 127));
                        }
                        else
                        {
                            ((int16_t*) m_Data)[sample * nChannels + channel] = (int16_t) (sampleValue * 32767);
                        }
                    }
                }
            }

            inline ~AudioClip() { 
                std::free(m_Data); 
            }

            static Ref<AudioClip> LoadFromFile(const std::string& path);

            inline ALenum GetFormat()
            {
                int channels = m_AudioFile.getNumChannels();
                // TODO @Justin check if this is correct: 
                int bitsPerSample = m_AudioFile.getBitDepth();

                if (channels == 1 && bitsPerSample == 8)
                    return AL_FORMAT_MONO8;
                else if (channels == 1 && bitsPerSample == 16)
                    return AL_FORMAT_MONO16;
                else if (channels == 2 && bitsPerSample == 8)
                    return AL_FORMAT_STEREO8;
                else if (channels == 2 && bitsPerSample == 16)
                    return AL_FORMAT_STEREO16;
                else
                {
                    return 0;
                }
            }

            inline void* GetData()
            {
                return (void*) m_Data;
            }

            inline uint32_t GetSize()
            {
                return m_AudioFile.getNumChannels()
                    * m_AudioFile.getNumSamplesPerChannel()
                    * (m_AudioFile.getBitDepth() / 8);
            }

            inline uint32_t GetFrequency()
            {
                return m_AudioFile.getSampleRate();
            }


        private:

            AudioFile<double> m_AudioFile;
            void* m_Data;

        };
    } // namespace Audio

} // namespace Tesseract


#pragma once

#include "al.h"
#include "AudioDevice.h"

#include "AudioSource.h"

#include <vector>

namespace Tesseract
{
    namespace Audio
    {
        class Audio
        {
        public:

            inline static void Init();
            static void Update();
            inline static void Shutdown();

            inline const Ref<AudioDevice>& GetDefaultDevice();

            static void MakePersistent(const Ref<AudioSource>& source);

        private:


            static std::vector<Ref<AudioSource>> s_PersistentSources;

            static Ref<AudioDevice> s_DefaultDevice;

        };


        void Audio::Init()
        {
            s_DefaultDevice = AudioDevice::GetDefaultDevice();
            s_DefaultDevice->MakeContextCurrent();
        }

        void Audio::Shutdown()
        {

        }


        const Ref<AudioDevice>& Audio::GetDefaultDevice()
        {
            return s_DefaultDevice;
        }

    } // namespace Audio

} // namespace Tesseract

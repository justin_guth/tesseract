#pragma once

#include "Tesseract/Reference.h"
#include "AudioClip.h"
#include "al.h"

namespace Tesseract
{
    namespace Audio
    {
        class AudioBuffer
        {
        public:

            inline AudioBuffer():
                m_BufferID(0)
            {
                alGenBuffers(1, &m_BufferID);
            }

            inline ~AudioBuffer()
            {
                alDeleteBuffers(1, &m_BufferID);
            }

            inline void SetAudioClip(const Ref<AudioClip>& clip)
            {
                // TODO: check when to delete audio clip data from RAM
                alBufferData(
                    m_BufferID,
                    clip->GetFormat(),
                    clip->GetData(),
                    clip->GetSize(),
                    clip->GetFrequency()
                );
            }

            inline static Ref<AudioBuffer> Create()
            {
                return MakeRef<AudioBuffer>();
            }

            inline ALuint GetBufferID() const { return m_BufferID; }


        private:

            ALuint m_BufferID;

        };
    } // namespace Audio

} // namespace Tesseract

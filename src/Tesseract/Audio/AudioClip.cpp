#include "AudioClip.h"

#include "AudioFile.h"

namespace Tesseract
{
    namespace Audio
    {

        Ref<AudioClip> AudioClip::LoadFromFile(const std::string& path)
        {
            Ref<AudioClip> result = MakeRef<AudioClip>(path);
            return result;
        }
    } // namespace Audio

} // namespace Tesseract


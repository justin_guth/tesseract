#include "Audio.h"

namespace Tesseract
{
    namespace Audio
    {
        Ref<AudioDevice> Audio::s_DefaultDevice;
        std::vector<Ref<AudioSource>> Audio::s_PersistentSources;


        void Audio::Update()
        {
            std::vector<Ref<AudioSource>> newSources;

            for (const Ref<AudioSource>& source : s_PersistentSources)
            {
                if (source->IsPlaying())
                {
                    newSources.push_back(source);
                }
            }

            s_PersistentSources = newSources;
        }


        void Audio::MakePersistent(const Ref<AudioSource>& source)
        {
            s_PersistentSources.push_back(source);
        }

    } // namespace Audio

} // namespace Enine

#pragma once

#include <memory>

namespace Tesseract
{   
    template<typename T_Type>
    using Ref = std::shared_ptr<T_Type>;

    template<typename T_Type>
    using WeakRef = std::weak_ptr<T_Type>;

    template<typename T_Type, typename ... T_Args>
    inline Ref<T_Type> MakeRef(T_Args&& ... arguments) {
        return std::make_shared<T_Type>(std::forward<T_Args>(arguments) ...);
    }

} // namespace Tesseract

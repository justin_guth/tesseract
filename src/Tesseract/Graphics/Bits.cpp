#include "Bits.h"

namespace Tesseract
{
    namespace Graphics
    {

        const uint32_t Bits::DepthBufferBit = 1 << 0;
        const uint32_t Bits::ColorBufferBit = 1 << 1;

    } // namespace Graphics

} // namespace Tesseract

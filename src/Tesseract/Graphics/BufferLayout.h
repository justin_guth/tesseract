#pragma once

#include <string>
#include <vector>
#include <initializer_list>

#include "Tesseract/DataType.h"

namespace Tesseract
{
    namespace Graphics
    {

        uint32_t GetDataTypeComponentCount(DataType dataType);

        class BufferLayout
        {

        public:

            struct Element
            {
                std::string Name;
                DataType AssignedDataType;
                uint32_t Offset;
                uint32_t Size;
                bool Normalized;

                Element();
                Element(const std::string& name, Tesseract::DataType dataType, bool normalized);
            };

        public:

            BufferLayout() = default;
            BufferLayout(const std::initializer_list<Element>& initializerList);
            BufferLayout(const std::vector<Element>& elements);
            const std::vector<Element>& GetElements() const { return m_Elements; }

        private:

            std::vector<Element>  m_Elements;

        };

    } // namespace Graphics
} // namespace Tesseract

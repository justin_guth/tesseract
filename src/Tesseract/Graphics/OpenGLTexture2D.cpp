#include "OpenGLTexture2D.h"

#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
    namespace Graphics
    {
        OpenGLTexture2D::OpenGLTexture2D(const std::string& filePath):
            Texture2D(filePath),
            m_TextureID(0)
        {
            glGenTextures(1, &m_TextureID);
        }

        OpenGLTexture2D::~OpenGLTexture2D()
        {
            glDeleteTextures(1, &m_TextureID);
        }

        void OpenGLTexture2D::Bind()
        {
            TESSERACT_CORE_ASSERT(m_TextureID != 0, "Texture ID was not set!");

            glBindTexture(GL_TEXTURE_2D, m_TextureID);
        }


        void OpenGLTexture2D::UploadData()
        {
            Bind();

            glTexImage2D(
                GL_TEXTURE_2D, 
                0, 
                GL_RGBA, 
                m_Width, 
                m_Height, 
                0, 
                GL_RGBA, 
                GL_UNSIGNED_BYTE, 
                m_Data
            );
            
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        void OpenGLTexture2D::AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer)
        {
            TESSERACT_CORE_ASSERT(false, "Not implemented!");
        }




    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "ElementBuffer.h"

#include "glad/gl.h"


namespace Tesseract
{
    namespace Graphics
    {

        class OpenGLElementBuffer : public ElementBuffer
        {

        public: 

            OpenGLElementBuffer();
            ~OpenGLElementBuffer();

            virtual void Bind() override;
            virtual void UploadData(void* data, size_t size) override;

        private: 

            GLuint m_BufferID;

        };

    } // namespace Graphics
} // namespace Tesseract

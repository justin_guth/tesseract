#include "FrameBuffer.h"
#include "OpenGLFrameBuffer.h"

namespace Tesseract
{
    namespace Graphics
    {

        Ref<FrameBuffer> FrameBuffer::Create()
        {
            return MakeRef<OpenGLFrameBuffer>();
        }

    } // namespace Graphics

} // namespace Tesseract
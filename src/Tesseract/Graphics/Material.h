#pragma once

#include "ShaderProgram.h"
#include <string>

#include "Tesseract/IHasParameters.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Core/Resource.h"

namespace Tesseract
{
    namespace Graphics
    {
        class Material: public Core::Resource, public IHasParameters
        {
        public:

            Material() = default;
            Material(const Ref<ShaderProgram>& shaderProgram);
            Material(const char* vertexSourcePath, const char* fragmentSourcePath);

            virtual void UploadData();
            virtual Ref<ShaderProgram> GetShaderProgram() = 0;
            void SetShaderProgram(const Ref<ShaderProgram>& shaderProgram);
            inline const Ref<ShaderProgram>& Program() { return m_ShaderProgram; }
            void Use();

        protected:

            Ref<ShaderProgram> m_ShaderProgram;
        };
    } // namespace Graphics

} // namespace Tesseract

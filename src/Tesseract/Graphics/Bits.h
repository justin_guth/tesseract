#pragma once

#include <stdint.h>

namespace Tesseract
{
    namespace Graphics
    {
        class Bits
        {

        public:


            const static uint32_t DepthBufferBit;
            const static uint32_t ColorBufferBit;
        };

    } // namespace Graphics

} // namespace Tesseract

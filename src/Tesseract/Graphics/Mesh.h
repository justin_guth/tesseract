#pragma once

#include "Tesseract/Core/Geometry.h"
#include "Material.h"
#include "SubTexture2D.h"
#include "Texture2D.h"

#include "Tesseract/Reference.h"

#include "Tesseract/ISerializable.h"

#include "Tesseract/Core/Resource.h"

#include <unordered_map>


namespace Tesseract
{
    namespace Graphics
    {
        class Mesh: public Core::Resource, public ISerializable
        {
        public:

            Mesh();
            inline void SetGeometry(const Ref<Core::Geometry>& geometry, const bool recomputeTangentSpace = true) { m_Geometry = geometry; if (recomputeTangentSpace) { m_Geometry->RecomputeTangentSpace(); } }
            inline const Ref<Core::Geometry>& GetGeometry() { return m_Geometry; }
            inline void SetMaterial(const Ref<Material>& material) { m_Material = material; }
            inline const Ref<Material>& GetMaterial() { return m_Material; }
            inline void AddTexture(const std::string& key, const Ref<SubTexture2D>& texture) { m_Textures[key] = texture; }
            inline void AddTexture(const std::string& key, const Ref<Texture2D>& texture) { m_Textures[key] = SubTexture2D::Create(texture, 0.0, 0.0, 1.0f, 1.0f); }
            inline const std::unordered_map<std::string, Ref<SubTexture2D>>& GetTextures() const { return m_Textures; }
            inline float GetTextureScale() const { return m_TextureScale; }
            inline void SetTextureScale(float scale) { m_TextureScale = scale; }
            inline void DrawDebugNormals(bool value = true) { m_DrawDebugNormals = value; }
            inline bool DebugNormalsActive() { return m_DrawDebugNormals; }

            Util::DynamicData GetSerializationData() const override;

        private:
            bool m_DrawDebugNormals;
            Ref<Core::Geometry> m_Geometry;
            Ref<Material> m_Material;
            std::unordered_map<std::string, Ref<SubTexture2D>> m_Textures;
            float m_TextureScale = 1.0f;

        };
    } // namespace Graphics
} // namespace Tesseract

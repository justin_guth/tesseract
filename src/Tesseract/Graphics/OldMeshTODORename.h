#pragma once

#include <stdint.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "../Util/File.h"
#include "../Math/Vector3.h"
#include "../Math/Vector2.h"
#include "../Logging/Logger.h"
#include "BufferLayout.h"

#define TESSERACT_VERTEX_POSITION 0
#define TESSERACT_VERTEX_TEXTURE 1
#define TESSERACT_VERTEX_NORMAL 2

#define TESSERACT_VERTEX_STRIDE 16

namespace Tesseract
{
    namespace Graphics
    {

        struct Vertex
        {
            Math::Vector3 m_Position;
            Math::Vector2 m_TextureCoord;
            Math::Vector3 m_Normal;

            void AppendToBitList(std::vector<uint8_t>& bitList, uint8_t flags);
        };

        struct OldMeshTODORename
        {
            std::vector<Vertex> Verices;
            std::vector<uint16_t> Triangles;

            static OldMeshTODORename FromObj(std::string src);

            inline void AddAttribute(uint8_t flag) { m_VertexAttributes |= flag; }
            BufferLayout GetBufferLayout();

        private:
            uint8_t m_VertexAttributes;
        };

    } // namespace Graphics
} // namespace Tesseract

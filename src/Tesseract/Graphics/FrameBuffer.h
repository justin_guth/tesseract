#pragma once

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {

        class FrameBuffer
        {

        public:

            virtual void Bind() = 0;
            virtual void Unbind() = 0;
            static Ref<FrameBuffer> Create();


            inline virtual ~FrameBuffer()
            {
            };
        };

    } // namespace Graphics

} // namespace Tesseract
#include "ShaderProgram.h"

#include "OpenGLShaderProgram.h"
#include "Tesseract/Util/File.h"

namespace Tesseract
{
    namespace Graphics
    {
        Ref<ShaderProgram> ShaderProgram::Create()
        {
            // TODO: Allocate through / register with resource manager
            return MakeRef<OpenGLShaderProgram>();
        }

        Ref<ShaderProgram> ShaderProgram::CreateAndCompileFromSourceFiles(const std::string& vertexSourcePath, const std::string& fragmentSourcePath, const std::string& geometrySourcePath)
        {
            Ref<ShaderProgram> shaderProgram = Create();

            std::string vertexShaderSource = Util::File::ReadFileContents(vertexSourcePath);
            std::string geometryShaderSource = "";
            if (!geometrySourcePath.empty()) { geometryShaderSource = Util::File::ReadFileContents(geometrySourcePath); }
            std::string fragmentShaderSource = Util::File::ReadFileContents(fragmentSourcePath);

            shaderProgram->SetVertexSource(vertexShaderSource);
            shaderProgram->SetGeometrySource(geometryShaderSource);
            shaderProgram->SetFragmentSource(fragmentShaderSource);

            shaderProgram->Compile();

            return shaderProgram;
        }


    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "Texture2D.h"
#include "glad/gl.h"

#include <string>

namespace Tesseract
{
    namespace Graphics
    {
        class OpenGLTexture2D: public Texture2D
        {
        public:

            OpenGLTexture2D(const std::string& filePath);
            ~OpenGLTexture2D();

            virtual void Bind() override;
            virtual void UploadData() override;


            virtual void AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer) override;

        private:

            GLuint m_TextureID;
        };

    } // namespace Graphics

} // namespace Tesseract

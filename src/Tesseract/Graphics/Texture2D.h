#pragma once

#include "Tesseract/Core/Resource.h"
#include "Tesseract/Reference.h"
#include "Texture.h"

#include <stdint.h>

namespace Tesseract
{
    namespace Graphics
    {

        enum class TextureWrapMode
        {
            Repeat,
            MirroredRepeat,
            ClampToEdge,
            ClampToBorder
        };

        enum class TextureFilteringMode
        {
            Nearest,
            Linear,


            // TODO: move somewhere else, mipmap filtering modes do not apply to magnification for example
            // Also mipmap must be used for this to work
            NearestMipmapNearest,
            LinearMipmapNearest,
            NearestMipmapLinear,
            LinearMipmapLinear

            // REVIEW: Add bilinear filtering? this would need to be included into shaders
        };

        class Texture2D: public Core::Resource, public Texture
        {
        public:

            virtual ~Texture2D();

            inline void SetHorizontalWrapMode(TextureWrapMode wrapMode) { m_HorizontalWrapMode = wrapMode; }
            inline void SetVerticalWrapMode(TextureWrapMode wrapMode) { m_VerticalWrapMode = wrapMode; }
            inline TextureWrapMode GetHorizontalWrapMode() const { return m_HorizontalWrapMode; }
            inline TextureWrapMode GetVerticalWrapMode() const { return m_VerticalWrapMode; }

            inline void SetMinificationFilteringMode(TextureFilteringMode filteringMode) { m_MinificationFilterMode = filteringMode; }
            inline void SetMagnificationFilteringMode(TextureFilteringMode filteringMode) { m_MagnificationFilterMode = filteringMode; }
            inline TextureFilteringMode GetMinificationFilteringMode() const { return m_MinificationFilterMode; }
            inline TextureFilteringMode GetMagnificationFilteringMode() const { return m_MagnificationFilterMode; }

            static Ref<Texture2D> LoadFromFile(const std::string& filePath);

            virtual void UploadData() = 0;

        protected:

            Texture2D(const std::string& filePath);

        protected:

            TextureWrapMode m_VerticalWrapMode = TextureWrapMode::Repeat;
            TextureWrapMode m_HorizontalWrapMode = TextureWrapMode::Repeat;
            TextureFilteringMode m_MagnificationFilterMode = TextureFilteringMode::Linear;
            TextureFilteringMode m_MinificationFilterMode = TextureFilteringMode::Nearest;
       

            int32_t m_Width = 0;
            int32_t m_Height = 0;
            int32_t m_Channels = 0;

            uint8_t* m_Data = nullptr;
        };

    } // namespace Graphics

} // namespace Tesseract

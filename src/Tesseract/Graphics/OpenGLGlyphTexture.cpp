#include "OpenGLGlyphTexture.h"

#include "Tesseract/Core/Macros.h"

#include "Tesseract/Core/Application.h"

namespace Tesseract
{
    namespace Graphics
    {
        OpenGLGlyphTexture::OpenGLGlyphTexture(uint32_t width, uint32_t height, uint8_t* data) :
            GlyphTexture(),
            m_Width(width),
            m_Height(height),
            m_Data(data),
            m_TextureID(0)
        {

            glGenTextures(1, &m_TextureID);


        }

        OpenGLGlyphTexture::~OpenGLGlyphTexture()
        {
            if (Core::Application::GetApplication()->GetWindow()->ContextInvalid())
            {
                return;
            }
            glDeleteTextures(1, &m_TextureID);
        }

        void OpenGLGlyphTexture::Bind()
        {
            TESSERACT_CORE_ASSERT(m_TextureID != 0, "Texture ID was not set!");

            glBindTexture(GL_TEXTURE_2D, m_TextureID);
        }


        void OpenGLGlyphTexture::UploadData()
        {
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            Bind();
            glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                m_Width,
                m_Height,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                m_Data
            );

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


            glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        }

        void OpenGLGlyphTexture::AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer)
        {
            TESSERACT_CORE_ASSERT(false, "Not implemented!");
        }




    } // namespace Graphics

} // namespace Tesseract

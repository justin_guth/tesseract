#pragma once

#include "CubeMap.h"
#include "glad/gl.h"

namespace Tesseract
{
    namespace Graphics
    {

        class OpenGLCubeMap: public CubeMap
        {

        public:

            OpenGLCubeMap(uint32_t width, uint32_t height);
            virtual void AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer);

            inline virtual void Bind() override
            {
                glBindTexture(GL_TEXTURE_CUBE_MAP, m_CubeMapID);
            }


            inline ~OpenGLCubeMap() override
            {
                if (m_CubeMapID != 0)
                {
                    glDeleteTextures(1, &m_CubeMapID);
                }
            }



        private:

            GLuint m_CubeMapID;
            uint32_t m_Width;
            uint32_t m_Height;

        };

    } // namespace Graphics

} // namespace Tesseract

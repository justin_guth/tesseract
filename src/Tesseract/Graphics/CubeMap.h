#pragma once

#include "Texture.h"

namespace Tesseract
{
    namespace Graphics
    {

        class CubeMap: public Texture
        {
        public:

            static Ref<CubeMap> Create(uint32_t width, uint32_t height);

            virtual ~CubeMap()
            {

            }

        };

    } // namespace Graphics

} // namespace Tesseract

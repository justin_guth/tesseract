#pragma once


#include "ShaderProgram.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {

        class ParticleSystem
        {
        public:

            virtual void Update() = 0;
            virtual WeakRef<ShaderProgram> GetShaderProgram() = 0;
             
        };

    } // namespace Graphics

} // namespace Tesseract

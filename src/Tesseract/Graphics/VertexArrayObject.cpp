#include "VertexArrayObject.h"

#include "OpenGLVertexArrayObject.h"

namespace Tesseract
{
    namespace Graphics
    {

        VertexArrayObject::VertexArrayObject() :
            m_VertexBuffer(nullptr),
            m_ElementBuffer(nullptr)
        {

        }

        void VertexArrayObject::AssignBuffers(const Ref<VertexBuffer>& vertexBuffer, const Ref<ElementBuffer>& elementBuffer)
        {
            m_VertexBuffer = vertexBuffer;
            m_ElementBuffer = elementBuffer;

            LinkBuffers(vertexBuffer, elementBuffer);
        }

        Ref<VertexArrayObject> VertexArrayObject::Create()
        {
            return MakeRef<OpenGLVertexArrayObject>();
        }

    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "Tesseract/Core/Resource.h"
#include "Tesseract/Reference.h"
#include "Texture2D.h"

#include <tuple>

namespace Tesseract
{
    namespace Graphics
    {
        class SubTexture2D: public Core::Resource
        {
        public:

            inline SubTexture2D(Ref<Texture2D> texture, float x, float y, float width, float height):
                m_Texture(texture),
                m_X(x),
                m_Y(y),
                m_Width(width),
                m_Height(height)
            {

            }

            inline float GetX() const { return m_X; }
            inline float GetY() const { return m_Y; }
            inline float GetWidth() const { return m_Width; }
            inline float GetHeight() const { return m_Height; }

            inline std::tuple<float, float, float, float> GetDimensions() const { return { m_X, m_Y, m_Width, m_Height }; }
            inline const Ref<Texture2D>& GetTexture() const { return m_Texture; }

            inline void Bind() { m_Texture->Bind(); }

            inline static Ref<SubTexture2D> Create(Ref<Texture2D> texture, float x, float y, float width, float height)
            {
                return MakeRef<SubTexture2D>(texture, x, y, width, height);
            }

        private:

            float m_X = 0.0f;
            float m_Y = 0.0f;
            float m_Width = 0.0f;
            float m_Height = 0.0f;

            Ref<Texture2D> m_Texture = nullptr;
        };

    } // namespace Graphics

} // namespace Tesseract

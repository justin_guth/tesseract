#pragma once

#include <string>
#include <vector>

#include "Tesseract/Math/Vector4.h"
#include "Tesseract/Math/Matrix4.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {
        class ShaderProgram
        {
        public:

            virtual void SetVertexSource(const std::string& vertexSource) = 0;
            virtual void SetGeometrySource(const std::string& geometrySource) = 0;
            virtual void SetFragmentSource(const std::string& fragmentSource) = 0;
            virtual void Compile() = 0;
            virtual void Use() = 0;

            virtual void SetUniformFloat(const char* name, float value) = 0;
            inline void SetUniformFloat(const std::string& name, float value) { SetUniformFloat(name.c_str(), value); }
            virtual void SetUniformVec2(const char* name, const Math::Vector2& vector) = 0;
            inline void SetUniformVec2(const std::string& name, const Math::Vector2& vector) { SetUniformVec2(name.c_str(), vector); };
            virtual void SetUniformVec3(const char* name, const Math::Vector3& vector) = 0;
            inline void SetUniformVec3(const std::string& name, const Math::Vector3& vector) { SetUniformVec3(name.c_str(), vector); };
            virtual void SetUniformVec4(const char* name, const Math::Vector4& vector) = 0;
            inline void SetUniformVec4(const std::string& name, const Math::Vector4& vector) { SetUniformVec4(name.c_str(), vector); };

            virtual void SetUniformInt(const char* name, int32_t integer) = 0;
            inline void SetUniformInt(const std::string& name, int32_t integer) { SetUniformInt(name.c_str(), integer); }
            virtual void SetUniformInt2(const char* name, const Math::Vector2T<int32_t>& vector) = 0;
            inline void SetUniformInt2(const std::string& name, const Math::Vector2T<int32_t>& vector) { SetUniformInt2(name.c_str(), vector); };
            virtual void SetUniformInt3(const char* name, const Math::Vector3T<int32_t>& vector) = 0;
            inline void SetUniformInt3(const std::string& name, const Math::Vector3T<int32_t>& vector) { SetUniformInt3(name.c_str(), vector); };
            virtual void SetUniformInt4(const char* name, const Math::Vector4T<int32_t>& vector) = 0;
            inline void SetUniformInt4(const std::string& name, const Math::Vector4T<int32_t>& vector) { SetUniformInt4(name.c_str(), vector); };

            virtual void SetUniformUint(const char* name, uint32_t integer) = 0;
            inline void SetUniformUint(const std::string& name, uint32_t integer) { SetUniformUint(name.c_str(), integer); }
            virtual void SetUniformUint2(const char* name, const Math::Vector2T<uint32_t>& vector) = 0;
            inline void SetUniformUint2(const std::string& name, const Math::Vector2T<uint32_t>& vector) { SetUniformUint2(name.c_str(), vector); };
            virtual void SetUniformUint3(const char* name, const Math::Vector3T<uint32_t>& vector) = 0;
            inline void SetUniformUint3(const std::string& name, const Math::Vector3T<uint32_t>& vector) { SetUniformUint3(name.c_str(), vector); };
            virtual void SetUniformUint4(const char* name, const Math::Vector4T<uint32_t>& vector) = 0;
            inline void SetUniformUint4(const std::string& name, const Math::Vector4T<uint32_t>& vector) { SetUniformUint4(name.c_str(), vector); };

            virtual void SetUniformBool(const char* name, bool value) = 0;
            inline void SetUniformBool(const std::string& name, bool value) { SetUniformBool(name.c_str(), value); };

            virtual void SetUniformMat4(const char* name, const Math::Matrix4& matrix) = 0;
            inline void SetUniformMat4(const std::string name, const Math::Matrix4& matrix) { SetUniformMat4(name.c_str(), matrix); };

            virtual void SetUniformVec3Array(const char* name, const std::vector<Math::Vector3>& vectors) = 0;
            virtual void SetUniformFloatArray(const char* name, const std::vector<float>& values) = 0;
            virtual void SetUniformIntArray(const char* name, const std::vector<int32_t>& values) = 0;

            static Ref<ShaderProgram> Create();
            static Ref<ShaderProgram> CreateAndCompileFromSourceFiles(const std::string& vertexSourcePath, const std::string& fragmentSourcePath, const std::string& geometrySourcePath = "");
        };
    } // namespace Graphics

} // namespace Tesseract

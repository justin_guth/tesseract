#include "OpenGLVertexArrayObject.h"

namespace Tesseract
{
    namespace Graphics
    {

        OpenGLVertexArrayObject::OpenGLVertexArrayObject() :
            VertexArrayObject(),
            m_ObjectID(0)
        {
            glGenVertexArrays(1, &m_ObjectID);
        }

        OpenGLVertexArrayObject::~OpenGLVertexArrayObject()
        {
            glDeleteVertexArrays(1, &m_ObjectID);
        }

        void OpenGLVertexArrayObject::LinkBuffers(const Ref<VertexBuffer>& vertexBuffer, const Ref<ElementBuffer>& elementBuffer)
        {
            Bind();

            m_ElementBuffer->Bind();

            m_VertexBuffer->Bind();
            m_VertexBuffer->ApplyLayout();

        }

        void OpenGLVertexArrayObject::Bind()
        {
            glBindVertexArray(m_ObjectID);
        }

    } // namespace Graphics

} // namespace Tesseract

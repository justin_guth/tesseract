namespace Tesseract
{
    namespace Graphics
    {
        enum class LightSourceType
        {
            Unset = 0,
            Point
            // TODO: Spot, area, sun
        };
    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "Tesseract/Core/Resource.h"
#include "Tesseract/Reference.h"
#include "Texture.h"

#include <stdint.h>

namespace Tesseract
{
    namespace Graphics
    {

        class GlyphTexture: public Core::Resource, public Texture
        {
        public:

            virtual ~GlyphTexture();


            static Ref<GlyphTexture> Create(uint32_t width, uint32_t height, uint8_t* data);

            virtual void UploadData() = 0;

        protected:

            GlyphTexture();

        protected:

            int32_t m_Width = 0;
            int32_t m_Height = 0;
            int32_t m_Channels = 0;

            uint8_t* m_Data = nullptr;
        };

    } // namespace Graphics

} // namespace Tesseract

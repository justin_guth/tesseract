#include "TextureAtlas.h"

namespace Tesseract
{
    namespace Graphics
    {
        Ref<TextureAtlas> TextureAtlas::Create(const Ref<Texture2D>& texture, int32_t columns, int32_t rows)
        {
            Ref<TextureAtlas> result(new TextureAtlas(texture, columns, rows));

            return result;
        }

        void TextureAtlas::DefineSubTexture(const std::string& identifier, int32_t column, int32_t row, int32_t width, int32_t height)
        {
            float widthPerCell = 1.0f / (float)m_Columns;
            float heightPerCell = 1.0f / (float)m_Rows;

            m_Map[identifier] = SubTexture2D::Create(m_Texture, widthPerCell * column, 1.0f - (heightPerCell * (row + 1) ), width * widthPerCell, height * heightPerCell);
        }

        const Ref<SubTexture2D>& TextureAtlas::operator[](const std::string& identifier) const
        {
            return m_Map.at(identifier);
        }

    } // namespace Graphics

} // namespace Tesseract

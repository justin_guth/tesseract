#pragma once

#include "Mesh.h"
#include "SpriteAnimation.h"

#include <functional>
#include "Tesseract/Reference.h"
#include "Tesseract/Math/Matrix4.h"
#include "Tesseract/Math/Vector2.h"
#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Math/Vector4.h"
#include "Tesseract/Typing/Typing.h"

namespace Tesseract
{
    namespace Graphics
    {
        class RendererImplementation
        {
        protected:
            static Ref<ShaderProgram> m_DebugNormalsShader;
        public:

            virtual void Init() = 0;

            virtual void DrawText(
                const std::string& text,
                const Math::Matrix4& globalTransform,
                Ref<Typing::Face> face,
                float fontSize,
                const Math::Vector4& color
            ) = 0;

            virtual void DrawMesh(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            ) = 0;

            virtual void DrawMeshShadow(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            ) = 0;


            virtual void DrawAnimatedQuad(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            ) = 0;

            virtual void DrawAnimatedQuadShadow(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            ) = 0;
            
            virtual void DrawRectangle(
                const Math::Vector3& position,
                const Math::Vector2& dimensions,
                const Math::Vector4& color
            ) = 0;

            virtual void RenderAndFlushSprites() = 0;
            virtual void RenderAndFlushTexts() = 0;
            virtual void RenderAndFlushRectangles() = 0;

            virtual void SetCameraPosition(const Math::Vector3& cameraPosition) = 0;
            virtual void ClearLights() = 0;
            virtual void AddPointLight(const Math::Vector3& position, const Math::Vector3& color, float intensity) = 0;

            virtual void SetBackfaceCullingEnabled(bool enabled) = 0;
            virtual void SetViewport(int32_t x, int32_t y, int32_t width, int32_t height) = 0;
            virtual void Clear(uint32_t bits) = 0;

            virtual void BeginShadowPass() = 0;
            virtual void BeginRenderPass() = 0;
            virtual void ComputeShadows(const Math::Vector3& lightPosition) = 0;

            virtual void EnableDepthTest(bool enabled = true) = 0;
            virtual void SetWireframe(bool enabled = true) = 0;

            inline int32_t GetShadowResolution()
            {
                return m_ShadowResolution;
            }

            inline virtual void SetDefaultViewport(int32_t x, int32_t y, int32_t width, int32_t height)
            {
                m_DefaultViewportX = x;
                m_DefaultViewportY = y;
                m_DefaultViewportWidth = width;
                m_DefaultViewportHeight = height;
            }

            inline virtual void RevertToDefaultViewport()
            {
                SetViewport(m_DefaultViewportX,
                            m_DefaultViewportY,
                            m_DefaultViewportWidth,
                            m_DefaultViewportHeight);
            }

        protected:

            int32_t m_DefaultViewportX;
            int32_t m_DefaultViewportY;
            int32_t m_DefaultViewportWidth;
            int32_t m_DefaultViewportHeight;

            int32_t m_ShadowResolution;

        };
    } // namespace Graphics
} // namespace Tesseract

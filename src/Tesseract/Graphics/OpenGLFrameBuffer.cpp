#include "OpenGLFrameBuffer.h"

namespace Tesseract
{
    namespace Graphics
    {

            OpenGLFrameBuffer::OpenGLFrameBuffer():
                m_FrameBufferID(0)
            {
                glGenFramebuffers(1, &m_FrameBufferID);
            }

            OpenGLFrameBuffer::~OpenGLFrameBuffer() {
                if (m_FrameBufferID != 0)
                {
                    glDeleteFramebuffers(1, &m_FrameBufferID);
                }
            }


    } // namespace Graphics

} // namespace Tesseract
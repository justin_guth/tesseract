#pragma once

#include "Tesseract/Reference.h"
#include "FrameBuffer.h"

namespace Tesseract
{
    namespace Graphics
    {

        class Texture
        {

        public:

            virtual void Bind() = 0;
            virtual void AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer) = 0;

        };

    } // namespace Graphics

} // namespace Tesseract
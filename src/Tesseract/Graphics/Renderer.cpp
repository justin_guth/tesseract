#include "Renderer.h"

#include "Bits.h"
#include "OpenGLRendererImplementation.h"

#include "Tesseract/Typing/Typing.h"

namespace Tesseract
{
    namespace Graphics
    {
        RendererImplementation* Renderer::s_RendererImplementation = nullptr;
        Renderer::SpriteDistanceFunction Renderer::s_SpriteDistanceFunction;
        Math::Matrix4 Renderer::s_ViewMatrix;
        Math::Matrix4 Renderer::s_ProjectionMatrix;
        Math::Matrix4 Renderer::s_ProjectionViewMatrix;
        Math::Vector3 Renderer::s_ViewPosition;


        void Renderer::SetViewMatrix(
            const Math::Matrix4& viewMatrix
        )
        {
            s_ViewMatrix = viewMatrix;
            RecomputeProjectionViewMatrix();
        }

        void Renderer::SetProjectionMatrix(
            const Math::Matrix4& projectionMatrix
        )
        {
            s_ProjectionMatrix = projectionMatrix;
            RecomputeProjectionViewMatrix();
        }

        void Renderer::SetViewPosition(
            const Math::Vector3& viewPosition
        )
        {
            s_ViewPosition = viewPosition;
        }

        


        void Renderer::DrawText(
            const std::string& text,
            const Math::Matrix4& globalTransform,
            Ref<Typing::Face> face,
            float fontSize,
            const Math::Vector4& color
        )
        {
            s_RendererImplementation->DrawText(
                text,
                globalTransform,
                face,
                fontSize,
                color
            );
        }

        void Renderer::DrawMesh(
            const Ref<Graphics::Mesh>& mesh,
            const Math::Matrix4& modelMatrix
        )
        {
            s_RendererImplementation->DrawMesh(
                mesh,
                modelMatrix
            );
        }

        void Renderer::DrawMeshShadow(
            const Ref<Graphics::Mesh>& mesh,
            const Math::Matrix4& modelMatrix
        )
        {
            s_RendererImplementation->DrawMeshShadow(
                mesh,
                modelMatrix
            );
        }

        void Renderer::DrawAnimatedQuad(
            const Math::Matrix4& modelMatrix,
            const Ref<SpriteAnimation>& animation
        )
        {
            s_RendererImplementation->DrawAnimatedQuad(
                modelMatrix,
                animation
            );
        }

        void Renderer::DrawAnimatedQuadShadow(
            const Math::Matrix4& modelMatrix,
            const Ref<SpriteAnimation>& animation
        )
        {
            s_RendererImplementation->DrawAnimatedQuadShadow(modelMatrix, animation);
        }

        void Renderer::DrawRectangle(
            const Math::Vector3& position,
            const Math::Vector2& dimensions,
            const Math::Vector4& color
        )
        {
            s_RendererImplementation->DrawRectangle(position, dimensions, color);
        }


        void Renderer::FlushSpriteBuffer()
        {
            s_RendererImplementation->RenderAndFlushSprites();
        }

        void Renderer::FlushTextBuffer()
        {
            s_RendererImplementation->RenderAndFlushTexts();
        }

        void Renderer::FlushRectangleBuffer()
        {
            s_RendererImplementation->RenderAndFlushRectangles();
        }


        void Renderer::ClearLights()
        {
            s_RendererImplementation->ClearLights();
        }

        void Renderer::AddPointLight(
            const Math::Vector3& position,
            const Math::Vector3& color,
            float intensity
        )
        {
            s_RendererImplementation->AddPointLight(position, color, intensity);
        }

        void Renderer::SetBackfaceCullingEnabled(bool enabled)
        {
            s_RendererImplementation->SetBackfaceCullingEnabled(enabled);
        }

        void Renderer::EnableDepthTest(bool enabled)
        {
            s_RendererImplementation->EnableDepthTest(enabled);
        }

        void Renderer::SetWireframe(bool enabled)
        {
            s_RendererImplementation->SetWireframe(enabled);
        }



        void Renderer::Init()
        {
            s_RendererImplementation = new OpenGLRendererImplementation;
            s_RendererImplementation->Init();
            SetSpriteDistanceFunction(
                [](const Math::Vector3& viewPosition, const Math::Vector3& spritePosition)
                {
                    return (spritePosition - viewPosition).MagnitudeSquared();
                }
            );



            BLA();
        }

        void Renderer::Shutdown()
        {
            delete s_RendererImplementation;
        }

        void Renderer::SetCameraPosition(const Math::Vector3& cameraPosition)
        {
            s_RendererImplementation->SetCameraPosition(cameraPosition);
        }

        void Renderer::SetViewport(int32_t x, int32_t y, int32_t width, int32_t height)
        {
            s_RendererImplementation->SetViewport(x, y, width, height);
        }

        void Renderer::Clear(uint32_t bits)
        {
            s_RendererImplementation->Clear(bits);
        }

        void Renderer::SetDefaultViewport(int32_t x, int32_t y, int32_t width, int32_t height)
        {
            s_RendererImplementation->SetDefaultViewport(x, y, width, height);

        }

        void Renderer::RevertToDefaultViewport()
        {
            s_RendererImplementation->RevertToDefaultViewport();
        }

        void Renderer::BeginShadowPass()
        {
            s_RendererImplementation->BeginShadowPass();
        }

        void Renderer::BeginRenderPass()
        {
            s_RendererImplementation->BeginRenderPass();

        }

        void Renderer::ComputeShadows(const Math::Vector3& lightPosition)
        {
            s_RendererImplementation->ComputeShadows(lightPosition);
        }

        int32_t Renderer::GetShadowResolution()
        {
            return s_RendererImplementation->GetShadowResolution();
        }






    } // namespace Graphics

} // namespace Tesseract

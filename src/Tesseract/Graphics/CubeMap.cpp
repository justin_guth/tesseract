#include "CubeMap.h"
#include "OpenGLCubeMap.h"

namespace Tesseract
{
    namespace Graphics
    {

        Ref<CubeMap> CubeMap::Create(uint32_t width, uint32_t height)
        {
            return MakeRef<OpenGLCubeMap>(width, height);
        }

    } // namespace Graphics

} // namespace Tesseract

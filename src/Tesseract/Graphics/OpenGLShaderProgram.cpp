#include "OpenGLShaderProgram.h"

#include "Tesseract/Logging/Logger.h"
#include "Tesseract/Util/File.h"
#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
    namespace Graphics
    {

        void OpenGLShaderProgram::SetVertexSource(const std::string& vertexSource)
        {
            m_VertexSource = vertexSource;
        }

        void OpenGLShaderProgram::SetGeometrySource(const std::string& geometrySource)
        {
            m_GeometrySource = geometrySource;
        }

        void OpenGLShaderProgram::SetFragmentSource(const std::string& fragmentSource)
        {
            m_FragmentSource = fragmentSource;
        }

        enum class ExpectancyState
        {
            NONE,
            INCLUDE,
            SPACE_BEFORE_PATH,
            START_PATH,
            READ_PATH,
            STOP_PATH
        };

        static const char includeString[] = "#include";
        static uint8_t processingIndex = 0;

#define PREPROCESS_EXPECT(c) if(shaderSource[token] != (c)) { TESSERACT_CORE_CRITICAL("Attempted to load shader with faulty include instruction"); state = ExpectancyState::NONE; break; } token++;

        const std::string OpenGLShaderProgram::Preprocess(const std::string& shaderSource)
        {
            uint32_t token = 0;
            std::string result = "";
            std::string pathBuffer = "";
            ExpectancyState state = ExpectancyState::NONE;

            uint8_t bufferLevel = 0;
            std::string loadedContent = "";

            while (token < shaderSource.length())
            {
                switch (state)
                {
                    case ExpectancyState::NONE:
                        if (shaderSource[token] == '#')
                        {
                            bufferLevel = 0;
                            state = ExpectancyState::INCLUDE;
                        }
                        else
                        {
                            result.push_back(shaderSource[token]);
                            token++;
                        }
                        break;
                    case ExpectancyState::INCLUDE:
                        // Replace #include-instructions with text from other files
                        if (shaderSource[token] == includeString[bufferLevel])
                        {
                            bufferLevel++;
                            token++;
                            if (bufferLevel == 8) { state = ExpectancyState::SPACE_BEFORE_PATH; }
                        }
                        else
                        {
                            for (uint8_t c = 0; c < bufferLevel; c++) { result += includeString[c]; }
                            state = ExpectancyState::NONE;
                        }
                        break;
                    case ExpectancyState::SPACE_BEFORE_PATH:
                        while (shaderSource[token] == ' ') { token++; }
                        state = ExpectancyState::START_PATH;
                        break;
                    case ExpectancyState::START_PATH:
                        PREPROCESS_EXPECT('"');
                        state = ExpectancyState::READ_PATH;
                        pathBuffer.clear();
                        break;
                    case ExpectancyState::READ_PATH:
                        if (shaderSource[token] == '"') { state = ExpectancyState::STOP_PATH; }
                        else
                        {
                            pathBuffer += shaderSource[token];
                            token++;
                        }
                        break;
                    case ExpectancyState::STOP_PATH:
                        PREPROCESS_EXPECT('"');
                        // REVIEW: consider saving all these files in some registry instead of loading from memory every time
                        loadedContent = Util::File::ReadFileContents(pathBuffer);
                        result += Preprocess(loadedContent);
                        state = ExpectancyState::NONE;
                        break;
                    default:
                        break;
                }
            }

            TESSERACT_CORE_ASSERT(state == ExpectancyState::NONE, "Incomplete include instruction in shader");

            Util::File::WriteFile("../../res/preprocessed_shaders/preprocessed (" + std::to_string(processingIndex) + ") .glsl", result);
            processingIndex++;

            return result;
        }

        void OpenGLShaderProgram::Compile()
        {
            if (m_ProgramID != 0)
            {
                glDeleteProgram(m_ProgramID);
            }

            GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
            GLuint geometryShaderID = 0;
            if (!m_GeometrySource.empty()) { geometryShaderID = glCreateShader(GL_GEOMETRY_SHADER); }
            GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

            TESSERACT_CORE_ASSERT(!m_VertexSource.empty(), "No vertex shader source provided for shader!");
            TESSERACT_CORE_ASSERT(!m_FragmentSource.empty(), "No fragment shader source provided for shader!");

            std::string preprocessedVertexSource = Preprocess(m_VertexSource);
            uint8_t vert_id = processingIndex - 1;
            std::string preprocessedGeometrySource = Preprocess(m_GeometrySource);
            uint8_t geom_id = processingIndex - 1;
            std::string preprocessedFragmentSource = Preprocess(m_FragmentSource);
            uint8_t frag_id = processingIndex - 1;
            const char* vertexSource = preprocessedVertexSource.c_str();
            const char* geometrySource = preprocessedGeometrySource.c_str();
            const char* fragmentSource = preprocessedFragmentSource.c_str();

            glShaderSource(vertexShaderID, 1, &vertexSource, NULL);
            if (geometryShaderID) { glShaderSource(geometryShaderID, 1, &geometrySource, NULL); }
            glShaderSource(fragmentShaderID, 1, &fragmentSource, NULL);


            glCompileShader(vertexShaderID);
            {
                int  success;
                char infoLog[512];
                glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &success);

                if (!success)
                {
                    glGetShaderInfoLog(vertexShaderID, 512, NULL, infoLog);
                    TESSERACT_CLIENT_ERROR("Vertex shader compilation failed! Details (see preprocessed ({})):\n{}", vert_id, infoLog);
                }
            }

            if (geometryShaderID)
            {
                glCompileShader(geometryShaderID);

                {
                    int  success;
                    char infoLog[512];
                    glGetShaderiv(geometryShaderID, GL_COMPILE_STATUS, &success);

                    if (!success)
                    {
                        glGetShaderInfoLog(geometryShaderID, 512, NULL, infoLog);
                        TESSERACT_CLIENT_CRITICAL("Geometry shader compilation failed! Details (see preprocessed ({})):\n{}", geom_id, infoLog);
                    }
                }
            }

            glCompileShader(fragmentShaderID);

            {
                int  success;
                char infoLog[512];
                glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &success);

                if (!success)
                {
                    glGetShaderInfoLog(fragmentShaderID, 512, NULL, infoLog);
                    TESSERACT_CLIENT_CRITICAL("Fragment shader compilation failed! Details (see preprocessed ({})):\n{}", frag_id, infoLog);
                }
            }

            m_ProgramID = glCreateProgram();

            glAttachShader(m_ProgramID, vertexShaderID);
            if (geometryShaderID) { glAttachShader(m_ProgramID, geometryShaderID); }
            glAttachShader(m_ProgramID, fragmentShaderID);
            glLinkProgram(m_ProgramID);

            {
                int  success;
                char infoLog[512];
                glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &success);

                if (!success)
                {
                    glGetProgramInfoLog(m_ProgramID, 512, NULL, infoLog);
                    TESSERACT_CLIENT_CRITICAL("Program linking failed! Details:\n{}", infoLog);
                }
            }

            glDeleteShader(vertexShaderID);
            if (geometryShaderID) { glDeleteShader(vertexShaderID); }
            glDeleteShader(fragmentShaderID);
        }

        void OpenGLShaderProgram::Use()
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            glUseProgram(m_ProgramID);
        }


        OpenGLShaderProgram::OpenGLShaderProgram():
            m_VertexSource(),
            m_FragmentSource(),
            m_ProgramID(0)
        {
        }

        OpenGLShaderProgram::~OpenGLShaderProgram()
        {
        }


        void OpenGLShaderProgram::SetUniformFloat(const char* name, float value)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1f(location, value);
        }

        void OpenGLShaderProgram::SetUniformVec2(const char* name, const Math::Vector2& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform2f(location, vector.X, vector.Y);
        }

        void OpenGLShaderProgram::SetUniformVec3(const char* name, const Math::Vector3& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform3f(location, vector.X, vector.Y, vector.Z);
        }

        void OpenGLShaderProgram::SetUniformVec4(const char* name, const Math::Vector4& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform4f(location, vector.X, vector.Y, vector.Z, vector.W);
        }

        void OpenGLShaderProgram::SetUniformMat4(const char* name, const Math::Matrix4& matrix)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniformMatrix4fv(location, 1, GL_FALSE, matrix.GetData());
        }

        void OpenGLShaderProgram::SetUniformInt(const char* name, int32_t integer)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1i(location, integer);
        }

        void OpenGLShaderProgram::SetUniformInt2(const char* name, const Math::Vector2T<int32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform2i(location, vector.X, vector.Y);
        }

        void OpenGLShaderProgram::SetUniformInt3(const char* name, const Math::Vector3T<int32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform3i(location, vector.X, vector.Y, vector.Z);
        }

        void OpenGLShaderProgram::SetUniformInt4(const char* name, const Math::Vector4T<int32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform4i(location, vector.X, vector.Y, vector.Z, vector.W);
        }

        void OpenGLShaderProgram::SetUniformUint(const char* name, uint32_t integer)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1ui(location, integer);
        }

        void OpenGLShaderProgram::SetUniformUint2(const char* name, const Math::Vector2T<uint32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform2ui(location, vector.X, vector.Y);
        }

        void OpenGLShaderProgram::SetUniformUint3(const char* name, const Math::Vector3T<uint32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform3ui(location, vector.X, vector.Y, vector.Z);
        }

        void OpenGLShaderProgram::SetUniformUint4(const char* name, const Math::Vector4T<uint32_t>& vector)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform4ui(location, vector.X, vector.Y, vector.Z, vector.W);
        }

        void OpenGLShaderProgram::SetUniformBool(const char* name, bool value)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1i(location, value);
        }


        void OpenGLShaderProgram::SetUniformVec3Array(const char* name, const std::vector<Math::Vector3>& vectors)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform3fv(location, vectors.size(), (GLfloat*)vectors.data());
        }


        void OpenGLShaderProgram::SetUniformFloatArray(const char* name, const std::vector<float>& values)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1fv(location, values.size(), (GLfloat*)values.data());
        }

        void OpenGLShaderProgram::SetUniformIntArray(const char* name, const std::vector<int32_t>& values)
        {
            TESSERACT_CORE_ASSERT(m_ProgramID != 0, "No program has been compiled for this ShaderProgram!");
            // TODO: Use a hashmap to "remember" already identified uniform names. (needs to be cleared when recompiling)
            GLuint location = glGetUniformLocation(m_ProgramID, name);
            glUniform1iv(location, values.size(), (GLint*)values.data());
        }


    } // namespace Graphics

} // namespace Tesseract

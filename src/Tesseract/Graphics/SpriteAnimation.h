#pragma once

#include "Tesseract/Core/Time.h"
#include "Tesseract/Core/Resource.h"

#include "Tesseract/Reference.h"
#include "TextureAtlas.h"

#include "Tesseract/Math/Functions.h"

#include <string>
#include <functional>
#include <vector>

namespace Tesseract
{
    namespace Graphics
    {
        class SpriteAnimation: public Core::Resource
        {
        public:

            Math::Vector4 Tint;
            bool m_Loop;

        public:

            inline SpriteAnimation():
                SpriteAnimation(1.0f)
            {
            }

            inline SpriteAnimation(float animationSpeed) :
                m_AnimationSpeed(animationSpeed),
                m_Time(0.0f),
                m_Playing(false),
                m_Atlas(nullptr),
                Tint(1.0f),
                m_Loop(false)
            {
            }

            inline void SetAnimationSpeed(float speed) { m_AnimationSpeed = speed; }
            inline float GetAnimationSpeed() const { return m_AnimationSpeed; }

            inline float GetTime() const { return m_Time; }

            inline void LoadAtlas(
                const Ref<TextureAtlas>& atlas,
                const std::vector<std::string>& startFrames,
                const std::vector<std::string>& loopFrames,
                const std::vector<std::string>& endFrames
            )
            {
                m_Atlas = atlas;
                m_StartFrames = startFrames;
                m_LoopFrames = loopFrames;
                m_EndFrames = endFrames;
            }
            inline const Ref<TextureAtlas>& GetAtlas() { return m_Atlas; }

            inline void BindTexture() { m_Atlas->BindTexture(); }

            inline void Update()
            {
                if (m_Playing)
                {
                    m_Time += Core::Time::Delta() * m_AnimationSpeed;

                    if (Done())
                    {
                        if (m_DoneCallback)
                        {
                            m_DoneCallback();
                        }
                    }
                }
            }

            inline void Pause() { m_Playing = false; }
            inline SpriteAnimation& Play() { m_Playing = true; return *this; }
            inline void Then(std::function<void()> callback) { m_DoneCallback = callback; }

            inline void Stop() { m_Playing = false; m_Time = 0.0f; }
            inline bool IsPlaying() const { return m_Playing; }
            inline void Loop(bool loop = true) { m_Loop = loop; }

            inline bool Done()
            {
                return !m_Loop && (m_Time > m_StartFrames.size() + m_LoopFrames.size() - 1);
            }

            inline const Ref<SubTexture2D>& GetCurrentFrame() const
            {
                using namespace Math;

                float startLength = (float)m_StartFrames.size() / m_AnimationSpeed;

                if (m_Time < startLength)
                {
                    int32_t index = Functions::Floor(Functions::Fmod(m_Time, m_StartFrames.size()));
                    return (*m_Atlas)[m_StartFrames[index]];
                }

                if (!m_Loop)
                {
                    int32_t index = Functions::Floor(Functions::Min(m_Time - startLength, m_LoopFrames.size() - 1));
                    return (*m_Atlas)[m_LoopFrames[index]];
                }
                else
                {
                    // TODO: End frames handling

                    int32_t index = Functions::Floor(Functions::Fmod(m_Time - startLength, m_LoopFrames.size()));
                    return (*m_Atlas)[m_LoopFrames[index]];
                }
            }

        private:

            float m_AnimationSpeed;
            float m_Time;
            bool m_Playing;

            Ref<TextureAtlas> m_Atlas;
            std::vector<std::string> m_StartFrames;
            std::vector<std::string> m_LoopFrames;
            std::vector<std::string> m_EndFrames;
            std::function<void()>  m_DoneCallback;
        };
    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "ElementBuffer.h"
#include "VertexBuffer.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {
        class VertexArrayObject
        {
        public:

            VertexArrayObject();
            virtual ~VertexArrayObject() = default;

            void AssignBuffers(const Ref<VertexBuffer>& vertexBuffer, const Ref<ElementBuffer>& elementBuffer);
            virtual void LinkBuffers(const Ref<VertexBuffer>& vertexBuffer, const Ref<ElementBuffer>& elementBuffer) = 0;

            inline const Ref<VertexBuffer>& GetVertexBuffer() { return m_VertexBuffer; }
            inline const Ref<ElementBuffer>& GetElementBuffer() { return m_ElementBuffer; }

            virtual void Bind() = 0;

            static Ref<VertexArrayObject> Create();

            inline uint32_t GetElementCount() { return m_ElementBuffer->GetElementCount(); }
            inline ElementBuffer::ElementType GetElementType() const { return m_ElementBuffer->GetElementType(); }

        protected:

            Ref<VertexBuffer> m_VertexBuffer;
            Ref<ElementBuffer> m_ElementBuffer;
        };
    } // namespace Graphics
} // namespace Tesseract

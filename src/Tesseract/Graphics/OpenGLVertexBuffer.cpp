#include "OpenGLVertexBuffer.h"

#include "Tesseract/Core/Macros.h"
#include "Tesseract/Math/Vector3.h"

namespace Tesseract
{
    namespace Graphics
    {

        OpenGLVertexBuffer::OpenGLVertexBuffer() :
            m_BufferID(0)
        {
            glGenBuffers(1, &m_BufferID);
        }

        OpenGLVertexBuffer::~OpenGLVertexBuffer()
        {
            TESSERACT_ASSERT(m_BufferID != 0, "Buffer id was null when (0) deleting buffer");

            glDeleteBuffers(1, &m_BufferID);
        }

        void OpenGLVertexBuffer::Bind() const
        {
            glBindBuffer(GL_ARRAY_BUFFER, m_BufferID);
        }

        void OpenGLVertexBuffer::SetData(void* baseAddress, size_t size)
        {
            UploadData(baseAddress, size);
        }

        struct PD
        {
            float Size;
            Tesseract::Math::Vector3 Position;
        };

        void OpenGLVertexBuffer::UploadData(void* baseAddress, size_t size)
        {
            Bind(); // REVIEW: Check if binding here is adequate
            glBufferData(GL_ARRAY_BUFFER, size, baseAddress, GL_STATIC_DRAW); // REVIEW: Do we need different draw modes other than GL_STATIC_DRAW
        }

        void OpenGLVertexBuffer::ApplyLayout()
        {
            uint32_t index = 0;
            uint32_t stride = 0;

            for (const BufferLayout::Element& element : m_Layout.GetElements())
            {
                stride += element.Size;
            }            
            
            for (const BufferLayout::Element& element : m_Layout.GetElements())
            {
                glVertexAttribPointer(
                    index,
                    GetDataTypeComponentCount(element.AssignedDataType),
                    GL_FLOAT,
                    element.Normalized ? GL_TRUE : GL_FALSE,
                    stride,
                    (void*) (uint64_t) element.Offset
                );
                glEnableVertexAttribArray(index);

                index++;
            }
        }


    } // namespace Graphics
} // namespace Tesseract

#include "OpenGLElementBuffer.h"

namespace Tesseract
{
    namespace Graphics
    {

        OpenGLElementBuffer::OpenGLElementBuffer() :
            m_BufferID(0)
        {
            glGenBuffers(1, &m_BufferID);
        }

        OpenGLElementBuffer::~OpenGLElementBuffer()
        {
            glDeleteBuffers(1, &m_BufferID);
        }

        void OpenGLElementBuffer::Bind()
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferID);
        }

        void OpenGLElementBuffer::UploadData(void* data, size_t size)
        {
            Bind();
            //REVIEW: Is GL_STATIC_DRAW always correct?
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        }


    } // namespace Graphics
} // namespace Tesseract
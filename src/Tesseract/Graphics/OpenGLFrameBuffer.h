#pragma once

#include "glad/gl.h"

#include "FrameBuffer.h"

namespace Tesseract
{
    namespace Graphics
    {

        class OpenGLFrameBuffer: public FrameBuffer
        {

        public:

            inline virtual void Bind() override
            {
                glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBufferID);
            }
            
            inline virtual void Unbind() override
            {
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
            }

            OpenGLFrameBuffer();
            ~OpenGLFrameBuffer() override;

        private:

            GLuint m_FrameBufferID;
        };

    } // namespace Graphics

} // namespace Tesseract
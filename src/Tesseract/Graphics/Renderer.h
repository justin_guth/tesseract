#pragma once

#include <functional>

#include "RendererImplementation.h"
#include "Mesh.h"

#include "Tesseract/Graphics/SpriteAnimation.h"

#include "Tesseract/Reference.h"
#include "Tesseract/Math/Matrix4.h"

namespace Tesseract
{
    namespace Graphics
    {
        class Renderer
        {
        public:

            using SpriteDistanceFunction = std::function<float(const Math::Vector3&, const Math::Vector3&)>;

            static void Init();
            static void Shutdown();

            static void SetViewMatrix(
                const Math::Matrix4& viewMatrix
            );
            static void SetProjectionMatrix(
                const Math::Matrix4& projectionMatrix
            );          
            static void SetViewPosition(
                const Math::Vector3& viewPosition
            );
            

            inline static const Math::Matrix4& GetViewMatrix() { return s_ViewMatrix; };
            inline static const Math::Matrix4& GetProjectionMatrix() { return s_ProjectionMatrix; };
            inline static const Math::Matrix4& GetProjectionViewMatrix() { return s_ProjectionViewMatrix; };
            inline static const Math::Vector3& GetViewPosition() { return s_ViewPosition; };

            static void DrawText(
                const std::string& text,
                const Math::Matrix4& globalTransform,
                Ref<Typing::Face> face,
                float fontSize,
                const Math::Vector4& color
            );

            // TODO: Move view and projection in some Setter function
            static void DrawMesh(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            );

            static void DrawMeshShadow(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            );

            static void DrawAnimatedQuad(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            );

            static void DrawAnimatedQuadShadow(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            );

            static void DrawRectangle(
                const Math::Vector3& position,
                const Math::Vector2& dimensions,
                const Math::Vector4& color
            );


            static void SetCameraPosition(const Math::Vector3& cameraPosition);

            static void FlushSpriteBuffer();
            static void FlushTextBuffer();
            static void FlushRectangleBuffer();

            inline static void SetSpriteDistanceFunction(const SpriteDistanceFunction& function) { s_SpriteDistanceFunction = function; }
            inline static SpriteDistanceFunction GetSpriteDistanceFunction() { return s_SpriteDistanceFunction; }
            static void ClearLights();
            static void AddPointLight(const Math::Vector3& position, const Math::Vector3& color, float intensity);

            static void SetBackfaceCullingEnabled(bool enabled = true);
            static void EnableDepthTest(bool enabled = true);
            static void SetWireframe(bool enabled = true);
            static void SetViewport(int32_t x, int32_t y, int32_t width, int32_t height);
            static void SetDefaultViewport(int32_t x, int32_t y, int32_t width, int32_t height);
            static void RevertToDefaultViewport();

            static void Clear(uint32_t bits);

            static void BeginShadowPass();
            static void BeginRenderPass();
            static void ComputeShadows(const Math::Vector3& lightPosition);

            static int32_t GetShadowResolution();

        private:

            static RendererImplementation* s_RendererImplementation;
            static SpriteDistanceFunction s_SpriteDistanceFunction;

            static Math::Matrix4 s_ViewMatrix;
            static Math::Matrix4 s_ProjectionMatrix;
            static Math::Matrix4 s_ProjectionViewMatrix;
            static Math::Vector3 s_ViewPosition;

            inline static void RecomputeProjectionViewMatrix() { s_ProjectionViewMatrix = s_ProjectionMatrix * s_ViewMatrix; }


        };
    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include <stdint.h>
#include "BufferLayout.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {
        class VertexBuffer
        {
        public:

            virtual void Bind() const = 0;

            static Ref<VertexBuffer> Create();

            virtual void ApplyLayout() = 0;
            inline void SetLayout(const BufferLayout& layout) { m_Layout = layout; }

            virtual void SetData(void* baseAddress, size_t size) = 0;
            virtual void UploadData(void* baseAddress, size_t size) = 0;

        protected:

            BufferLayout m_Layout;

        };

    } // namespace Graphics
} // namespace Tesseract

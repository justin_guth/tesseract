#pragma once

#include "GlyphTexture.h"
#include "glad/gl.h"

#include <string>

namespace Tesseract
{
    namespace Graphics
    {
        class OpenGLGlyphTexture: public GlyphTexture
        {
        public:

            OpenGLGlyphTexture(uint32_t width, uint32_t height, uint8_t* data);
            ~OpenGLGlyphTexture();

            virtual void Bind() override;
            virtual void UploadData() override;


            virtual void AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer) override;

        private:

            GLuint m_TextureID;

            uint32_t m_Height;
            uint32_t m_Width;
            uint8_t* m_Data;
        };

    } // namespace Graphics

} // namespace Tesseract

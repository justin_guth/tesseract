#include "OpenGLRendererImplementation.h"
#include "Tesseract/Core/Macros.h"
#include "glad/gl.h"
#include "Tesseract/Core/ResourceManager.h"
#include "Tesseract/Graphics/OpenGLShaderProgram.h"
#include "Bits.h"

#include "Tesseract/Profiling/ProfilerScopedTimer.h"
#include "Tesseract/Typing/Typing.h"

#include "Renderer.h"

#include <string>

namespace Tesseract
{
    namespace Graphics
    {
        Ref<ShaderProgram> RendererImplementation::m_DebugNormalsShader = Tesseract::MakeRef<OpenGLShaderProgram>();

        static void GLAPIENTRY
            MessageCallback(GLenum source,
                            GLenum type,
                            GLuint id,
                            GLenum severity,
                            GLsizei length,
                            const GLchar* message,
                            const void* userParam)
        {
            if (type != GL_DEBUG_TYPE_ERROR)
            {
                return;
            }

            TESSERACT_CORE_ERROR(
                "GL CALLBACK: {}\n\ttype = {:#X},\n\tseverity = {:#X},\n\tmessage = {}\n",
                (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
                type,
                severity,
                message
            );
        }


        static GLuint GetGLElementType(ElementBuffer::ElementType elementType)
        {
            switch (elementType)
            {
                case ElementBuffer::ElementType::UInt8: return GL_UNSIGNED_BYTE;
                case ElementBuffer::ElementType::UInt16: return GL_UNSIGNED_SHORT;
                case ElementBuffer::ElementType::UInt32: return GL_UNSIGNED_INT;
                case ElementBuffer::ElementType::None:
                default:
                    TESSERACT_CORE_ASSERT(false, "Invalid element type read from ElementBuffer!");
                    return GL_UNSIGNED_INT;
            }
        }

        void OpenGLRendererImplementation::ApplyDefaultBlendFunc()
        {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        OpenGLRendererImplementation::OpenGLRendererImplementation() :
            m_VertexBuffer(VertexBuffer::Create()),
            m_ElementBuffer(ElementBuffer::Create()),
            m_VertexArrayObject(VertexArrayObject::Create()),
            m_SpriteVertexBuffer(VertexBuffer::Create()),
            m_SpriteElementBuffer(ElementBuffer::Create()),
            m_SpriteVertexArrayObject(VertexArrayObject::Create()),
            m_RectangleVertexBuffer(VertexBuffer::Create()),
            m_TextVertexBuffer(VertexBuffer::Create()),
            m_TextElementBuffer(ElementBuffer::Create()),
            m_TextVertexArrayObject(VertexArrayObject::Create()),
            m_QuadGeometry(Core::ResourceManager::LoadFromFile<Core::Geometry>("tesseract/quad")),
            m_SpriteAnimationMaterial(Core::ResourceManager::LoadFromFile<Material>("tesseract/sprite")),
                        m_ParticleVertexBuffer(VertexBuffer::Create())
        {
            m_VertexArrayObject->AssignBuffers(m_VertexBuffer, m_ElementBuffer);
            m_SpriteVertexArrayObject->AssignBuffers(m_SpriteVertexBuffer, m_SpriteElementBuffer);
            m_TextVertexArrayObject->AssignBuffers(m_TextVertexBuffer, m_TextElementBuffer);
            m_QuadGeometry->RecomputeTangentSpace();

            m_SpriteVertexBuffer->SetLayout(
                {
                        {"position", DataType::Float3, false},
                        {"textureCoordinate", DataType::Float2, false},
                        {"normal", DataType::Float3, false},
                        {"tangent", DataType::Float3, false},
                        {"bitangent", DataType::Float3, false},
                        {"textureIndex", DataType::Int1, false},
                        {"frameData", DataType::Float4, false},
                        {"Tint", DataType::Float4, false}
                }
            );

            m_RectangleVertexBuffer->SetLayout(
                {
                        {"Position", DataType::Float3, false},
                        {"Dimensions", DataType::Float2, false},
                        {"Color", DataType::Float4, false}
                }
            );

            m_TextVertexBuffer->SetLayout(
                {
                        {"position", DataType::Float3, false},
                        {"textureCoordinates", DataType::Float2, false},
                        {"color", DataType::Color, false},
                        {"textureIndex", DataType::Int1, false},
                }
            );
        }

        void OpenGLRendererImplementation::Init()
        {
            // TODO: Move somewhere else
            m_ShadowResolution = 1024;

            m_DebugNormalsShader = ShaderProgram::CreateAndCompileFromSourceFiles(
                "../../res/shaders/normals.vert",
                "../../res/shaders/normals.frag",
                "../../res/shaders/normals.geom"
            );

            m_ShadowShaderProgram = ShaderProgram::CreateAndCompileFromSourceFiles(
                "../../res/shaders/shadow.vert",
                "../../res/shaders/shadow.frag",
                "../../res/shaders/shadow.geom"
            );

            m_SpriteAnimationShadowShaderProgram = ShaderProgram::CreateAndCompileFromSourceFiles(
                "../../res/shaders/sprite_shadow.vert",
                "../../res/shaders/sprite_shadow.frag",
                "../../res/shaders/sprite_shadow.geom"
            );

            m_RectangleShaderProgram = ShaderProgram::CreateAndCompileFromSourceFiles(
                "../../res/shaders/tesseract/rectangle.vert",
                "../../res/shaders/tesseract/rectangle.frag",
                "../../res/shaders/tesseract/rectangle.geom"
            );


            m_TextShaderProgram = ShaderProgram::CreateAndCompileFromSourceFiles(
                "../../res/shaders/text.vert",
                "../../res/shaders/text.frag"
            );

            m_ShadowDepthMapFrameBuffer = Graphics::FrameBuffer::Create();
            m_ShadowDepthMapCubeMap = Graphics::CubeMap::Create(m_ShadowResolution, m_ShadowResolution);
            m_ShadowDepthMapCubeMap->AssignFrameBuffer(m_ShadowDepthMapFrameBuffer);


            // Set counters for batch rendering
            // enable debug output
            glEnable(GL_DEBUG_OUTPUT);
            glDebugMessageCallback(MessageCallback, 0);

            m_NumSprites = 0;
            m_NumTextures = 0;
            m_NumRectangles = 0;
            m_NumTextTextures = 0;
            m_NumTexts = 0;

            EnableDepthTest();
            SetBackfaceCullingEnabled(true);

            ApplyDefaultBlendFunc();
            glEnable(GL_BLEND);

            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


        }

        void OpenGLRendererImplementation::DrawText(
            const std::string& text,
            const Math::Matrix4& globalTransform,
            Ref<Typing::Face> face,
            float fontSize,
            const Math::Vector4& color
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            if (face == nullptr)
            {
                return;
            }

            float scaleFactor = fontSize / 640.0f;

            const Math::Matrix4& projectionViewMatrix = Renderer::GetProjectionViewMatrix();
            const Math::Matrix4 viewModelMatrix = Renderer::GetViewMatrix() * globalTransform;
            //const Math::Matrix4 normalMatrix = viewModelMatrix.Inverse().T();

            

            Math::Vector3 offset;

            float totalWidth = 0.0f;
            float minVerticalPos = 0.0f;
            float maxVerticalPos = 0.0f;

            for (char c : text)
            {
                Typing::Character character = face->GetCharacter(c);
                float high = character.Bearing.Y * scaleFactor;
                float low = (high - character.Size.Y) * scaleFactor;

                if (high > maxVerticalPos) {
                    maxVerticalPos = high;
                }
                if (low < minVerticalPos) {
                    minVerticalPos = low;
                }

                totalWidth += (character.Advance >> 6) * scaleFactor;
            }

            float setbackX = totalWidth / 2;
            //float setbackY = (maxVerticalPos - minVerticalPos) / 2;
            float setbackY = (maxVerticalPos) / 2;

            for (char c : text)
            {
                Typing::Character character = face->GetCharacter(c);
                int textureIndex = GetIndexOfTextTexture(character.Texture);

                float xpos = offset.X + character.Bearing.X * scaleFactor;
                float ypos = offset.Y - (character.Size.Y - character.Bearing.Y) * scaleFactor;

                float w = character.Size.X * scaleFactor / 2;
                float h = character.Size.Y * scaleFactor / 2;
                const Math::Matrix4 scale = Math::Matrix4::Scale({ w, h, 1.0f });

                // Invariant: sprite vertex/index arrays are not full.
                for (int v = 0; v < 4; v++)
                {
                    Core::VertexData quadVertex = m_QuadGeometry->Vertices[v];
                    TextVertexData textVertex;
                    textVertex.Position = (globalTransform * (
                        ((scale * (quadVertex.Position + Math::Vector3(1.0f, 1.0f, 0.0f)).HomogenizedPoint()) + Math::Vector4{ xpos - setbackX, ypos - setbackY, 0.0f, 0.0f })
                        )).XYZ();
                    textVertex.TextureCoordinates = Math::Vector2(quadVertex.TextureCoordinate.X, 1.0f - quadVertex.TextureCoordinate.Y);
                    textVertex.TextureIndex = textureIndex;
                    textVertex.Color = color;

                    m_TextVertices[m_NumTexts * 4 + v] = textVertex;
                }

                offset.X += (character.Advance >> 6) * scaleFactor;

                for (int i = 0; i < 6; i++)
                {
                    m_TextIndices[m_NumTexts * 6 + i] = m_QuadGeometry->Indices[i] + 4 * m_NumTexts;
                }

                m_NumTexts++;

                if (m_NumTexts >= _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTS)
                {
                    RenderAndFlushTexts();
                }
            }
        }

        void OpenGLRendererImplementation::DrawMesh(
            const Ref<Graphics::Mesh>& mesh,
            const Math::Matrix4& modelMatrix
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            const Math::Matrix4& projectionViewMatrix = Renderer::GetProjectionViewMatrix();
            const Math::Matrix4 viewModelMatrix = Renderer::GetViewMatrix() * modelMatrix;

            const Ref<Core::Geometry>& geometry = mesh->GetGeometry();

            const Ref<Material>& material = mesh->GetMaterial();
            const std::unordered_map<std::string, Ref<SubTexture2D>>& textures = mesh->GetTextures();

            if (material == nullptr || geometry == nullptr)
            {
                return; // TODO: Render default material or wireframe or something
            }

            {
                TESSERACT_PROFILE_SCOPE("Uniform Uploads");

                material->Use();
                material->UploadData();

                material->Program()->SetUniformMat4("uModel", modelMatrix);
                material->Program()->SetUniformMat4("uNormal", viewModelMatrix.Inverse().T());
                material->Program()->SetUniformMat4("uProjectionViewModel", projectionViewMatrix * modelMatrix);
                material->Program()->SetUniformVec3("uViewPosition", m_CameraPosition);
                material->Program()->SetUniformFloat("uFarPlane", 60.0f); //TODO farplane variable
                material->Program()->SetUniformVec3("uLightPosition", m_LightPosition);

                material->Program()->SetUniformFloat("uTextureScale", mesh->GetTextureScale());
                material->Program()->SetUniformInt("uDepthMap", 15);

                UploadLightPositions(material->Program());
            }

            {
                TESSERACT_PROFILE_SCOPE("Writing Buffers");

                m_VertexBuffer->SetData(
                    geometry->Vertices.data(),
                    // TODO: Account for dynamic sizes of vertex in the future
                    geometry->Vertices.size() * sizeof(Core::VertexData)
                );

                m_ElementBuffer->SetData(
                    geometry->Indices.data(),
                    geometry->Indices.size(),
                    geometry->ElementType
                );
            }

            {
                TESSERACT_PROFILE_SCOPE("Setting Textures");

                int32_t textureUnit = 0;

                for (const auto& [identifier, texture] : textures)
                {
                    glActiveTexture(GL_TEXTURE0 + textureUnit);

                    texture->Bind();

                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                    // TODO: switch depending on texture parameters (provide getter through subtexture or move to subtexture)
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

                    material->Program()->SetUniformInt(identifier, textureUnit);

                    textureUnit++;
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("Setting VBO Buffer Layout");

                m_VertexBuffer->SetLayout(geometry->BufferLayout);
            }
            {
                TESSERACT_PROFILE_SCOPE("VAO Bind");

                m_VertexArrayObject->Bind();

            }
            {
                TESSERACT_PROFILE_SCOPE("Apply VBO Layout");
                m_VertexBuffer->ApplyLayout();
                // TODO @Justin remove redundant binds
            }
            {
                TESSERACT_PROFILE_SCOPE("Bind VBO");

                m_VertexBuffer->Bind();

            }
            {
                TESSERACT_PROFILE_SCOPE("Bind EBO");

                m_ElementBuffer->Bind();
            }

            {
                TESSERACT_PROFILE_SCOPE("Issue Draw Call");

                // TODO: render later or add to batch
                glDrawElements(
                    GL_TRIANGLES,
                    geometry->Indices.size(),
                    GetGLElementType(geometry->ElementType),
                    0
                );
            }

            if (mesh->DebugNormalsActive())
            {
                m_DebugNormalsShader->Use();
                m_DebugNormalsShader->SetUniformMat4("uModel", modelMatrix);
                m_DebugNormalsShader->SetUniformMat4("uNormal", modelMatrix.Inverse().T());
                m_DebugNormalsShader->SetUniformMat4("uProjectionView", projectionViewMatrix);

                glDrawArrays(
                    GL_POINTS,
                    0,
                    geometry->Indices.size()
                );
            }
        }

        void OpenGLRendererImplementation::DrawMeshShadow(
            const Ref<Graphics::Mesh>& mesh,
            const Math::Matrix4& modelMatrix
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            const Ref<Core::Geometry>& geometry = mesh->GetGeometry();

            if (geometry == nullptr)
            {
                return; // TODO: Render default material or wireframe or something
            }
            m_ShadowShaderProgram->Use();
            m_ShadowShaderProgram->SetUniformMat4("uModel", modelMatrix);

            m_VertexBuffer->SetData(
                geometry->Vertices.data(),
                // TODO: Account for dynamic sizes of vertex in the future
                geometry->Vertices.size() * sizeof(Core::VertexData)
            );

            m_ElementBuffer->SetData(
                geometry->Indices.data(),
                geometry->Indices.size(),
                geometry->ElementType
            );

            m_VertexBuffer->SetLayout(geometry->BufferLayout);
            m_VertexArrayObject->Bind();
            m_VertexBuffer->ApplyLayout();
            // TODO @Justin remove redundant binds

            m_VertexBuffer->Bind();
            m_ElementBuffer->Bind();



            // TODO: render later or add to batch
            glDrawElements(
                GL_TRIANGLES,
                geometry->Indices.size(),
                GetGLElementType(geometry->ElementType),
                0
            );
        }


        void OpenGLRendererImplementation::RenderAndFlushSprites()
        {
            if (m_NumSprites == 0) return;
            // Upload sprite quads
            m_SpriteVertexBuffer->SetData(
                m_SpriteVertices,
                // TODO: Account for dynamic sizes of vertex in the future
                4 * m_NumSprites * sizeof(SpriteVertexData)
            );


            m_SpriteElementBuffer->SetData(
                m_SpriteIndices,
                6 * m_NumSprites,
                ElementBuffer::ElementType::UInt32
            );

            // TODO @Justin remove redundant binds

            m_SpriteVertexArrayObject->Bind();

            m_SpriteVertexBuffer->Bind();
            m_SpriteElementBuffer->Bind();

            m_SpriteVertexBuffer->ApplyLayout();

            if (!m_ShadowPass)
            {
                m_SpriteAnimationMaterial->Use();
                m_SpriteAnimationMaterial->UploadData();

            }
            else
            {
                m_SpriteAnimationShadowShaderProgram->Use();
            }

            // Upload textures
            for (int t = 0; t < m_NumTextures; t++)
            {
                glActiveTexture(GL_TEXTURE0 + t);
                m_Textures[t]->BindTexture();
                if (!m_ShadowPass)
                {
                    m_SpriteAnimationMaterial->Program()->SetUniformInt("uSpriteTexture" + std::to_string(t), t);
                }
                else
                {
                    m_SpriteAnimationShadowShaderProgram->SetUniformInt("uSpriteTexture" + std::to_string(t), t);
                }

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            }

            // Draw sprites
            // TODO: switch depending on texture parameters (provide getter through subtexture or move to subtexture)


            glDrawElements(
                GL_TRIANGLES,
                6 * m_NumSprites,
                GetGLElementType(ElementBuffer::ElementType::UInt32),
                0
            );

            // Reset buffers so new sprites can be drawn
            m_NumSprites = 0;
            m_NumTextures = 0;
        }

        void OpenGLRendererImplementation::RenderAndFlushTexts()
        {
            if (m_NumTexts == 0) return;

            m_TextShaderProgram->Use();

            m_TextShaderProgram->SetUniformMat4("uProjectionView", Renderer::GetProjectionViewMatrix());

            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            // Upload text quads
            m_TextVertexBuffer->SetData(
                m_TextVertices,
                4 * m_NumTexts * sizeof(TextVertexData)
            );


            m_TextElementBuffer->SetData(
                m_TextIndices,
                6 * m_NumTexts,
                ElementBuffer::ElementType::UInt32
            );


            m_TextVertexArrayObject->Bind();

            m_TextVertexBuffer->Bind();
            m_TextElementBuffer->Bind();

            m_TextVertexBuffer->ApplyLayout();

            // Upload textures
            for (int t = 0; t < m_NumTextTextures; t++)
            {
                glActiveTexture(GL_TEXTURE0 + t);
                m_TextTextures[t]->Bind();
                m_TextShaderProgram->SetUniformInt("uSpriteTexture" + std::to_string(t), t);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            }

            // Draw sprites
            // TODO: switch depending on texture parameters (provide getter through subtexture or move to subtexture)


            glDrawElements(
                GL_TRIANGLES,
                6 * m_NumTexts,
                GetGLElementType(ElementBuffer::ElementType::UInt32),
                0
            );

            // Reset buffers so new sprites can be drawn
            m_NumTexts = 0;
            m_NumTextTextures = 0;
        }

        void OpenGLRendererImplementation::RenderAndFlushRectangles()
        {
            if (m_NumRectangles == 0) return;

            // Upload sprite quads
            m_RectangleVertexBuffer->SetData(
                m_RectangleVertices,
                m_NumRectangles * sizeof(RectangleVertexData)
            );


            m_RectangleVertexBuffer->Bind();
            m_RectangleVertexBuffer->ApplyLayout();


            m_RectangleShaderProgram->Use();
            m_RectangleShaderProgram->SetUniformMat4("uProjectionView", Renderer::GetProjectionViewMatrix());

            glDrawArrays(
                GL_POINTS,
                0,
                m_NumRectangles
            );

            m_NumRectangles = 0;
        }


        int OpenGLRendererImplementation::GetIndexOfTexture(const Ref<TextureAtlas>& atlas)
        {
            for (int t = 0; t < m_NumTextures; t++)
            {
                if (m_Textures[t] == atlas) return t;
            }
            if (m_NumTextures >= _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTURES)
            {
                RenderAndFlushSprites();
            }
            m_Textures[m_NumTextures] = atlas;

            int result = m_NumTextures;
            m_NumTextures++;

            return result;
        }

        int OpenGLRendererImplementation::GetIndexOfTextTexture(const Ref<GlyphTexture>& texture)
        {
            for (int t = 0; t < m_NumTextTextures; t++)
            {
                if (m_TextTextures[t] == texture) return t;
            }
            if (m_NumTextTextures >= _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTURES)
            {
                RenderAndFlushTexts();
            }
            m_TextTextures[m_NumTextTextures] = texture;

            int result = m_NumTextTextures;
            m_NumTextTextures++;

            return result;
        }

        void OpenGLRendererImplementation::DrawAnimatedQuad(
            const Math::Matrix4& modelMatrix,
            const Ref<SpriteAnimation>& animation
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            if (animation == nullptr)
            {
                return;
            }

            const Math::Matrix4 projectionViewMatrix = Renderer::GetProjectionViewMatrix();
            const Math::Matrix4 viewModelMatrix = Renderer::GetViewMatrix() * modelMatrix;
            //const Math::Matrix4 normalMatrix = viewModelMatrix.Inverse().T();
            const Math::Matrix4 normalMatrix = modelMatrix.Inverse().T();

            int textureIndex = GetIndexOfTexture(animation->GetAtlas());

            m_SpriteAnimationMaterial->Use();

            m_SpriteAnimationMaterial->Program()->SetUniformMat4("uProjectionView", projectionViewMatrix);
            m_SpriteAnimationMaterial->Program()->SetUniformVec3("uViewPosition", Renderer::GetViewPosition());

            UploadLightPositions(m_SpriteAnimationMaterial->Program());

            const Ref<SubTexture2D>& frame = animation->GetCurrentFrame();

            // Invariant: sprite vertex/index arrays are not full.
            for (int v = 0; v < 4; v++)
            {
                Core::VertexData quadVertex = m_QuadGeometry->Vertices[v];
                SpriteVertexData spriteVertex;
                spriteVertex.Position = (modelMatrix * quadVertex.Position.HomogenizedPoint()).XYZ();
                spriteVertex.Normal = (normalMatrix * quadVertex.Normal.HomogenizedVector()).XYZ();
                spriteVertex.Tangent = (modelMatrix * quadVertex.Tangent.HomogenizedVector()).XYZ();
                spriteVertex.Bitangent = (modelMatrix * quadVertex.Bitangent.HomogenizedVector()).XYZ();
                spriteVertex.TextureCoordinate = quadVertex.TextureCoordinate;
                spriteVertex.TextureIndex = textureIndex;
                spriteVertex.FrameData = Math::Vector4(
                    frame->GetX(),
                    frame->GetY(),
                    frame->GetWidth(),
                    frame->GetHeight()
                );

                spriteVertex.Tint = animation->Tint;

                m_SpriteVertices[m_NumSprites * 4 + v] = spriteVertex;
            }

            for (int i = 0; i < 6; i++)
            {
                m_SpriteIndices[m_NumSprites * 6 + i] = m_QuadGeometry->Indices[i] + 4 * m_NumSprites;
            }
            m_NumSprites++;

            if (m_NumSprites >= _TESSERACT_OPENGLRENDERER_MAX_NUM_SPRITES)
            {
                RenderAndFlushSprites();
            }
        }


        void OpenGLRendererImplementation::DrawAnimatedQuadShadow(
            const Math::Matrix4& modelMatrix,
            const Ref<SpriteAnimation>& animation
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            if (animation == nullptr)
            {
                return;
            }

            //const Math::Matrix4 normalMatrix = viewModelMatrix.Inverse().T();
            const Math::Matrix4 normalMatrix = modelMatrix.Inverse().T();

            int textureIndex = GetIndexOfTexture(animation->GetAtlas());

            m_SpriteAnimationShadowShaderProgram->Use();

            const Ref<SubTexture2D>& frame = animation->GetCurrentFrame();

            // Invariant: sprite vertex/index arrays are not full.
            for (int v = 0; v < 4; v++)
            {
                Core::VertexData quadVertex = m_QuadGeometry->Vertices[v];
                SpriteVertexData spriteVertex;
                spriteVertex.Position = (modelMatrix * quadVertex.Position.HomogenizedPoint()).XYZ();
                spriteVertex.Normal = (normalMatrix * quadVertex.Normal.HomogenizedVector()).XYZ();
                spriteVertex.Tangent = (modelMatrix * quadVertex.Tangent.HomogenizedVector()).XYZ();
                spriteVertex.Bitangent = (modelMatrix * quadVertex.Bitangent.HomogenizedVector()).XYZ();
                spriteVertex.TextureCoordinate = quadVertex.TextureCoordinate;
                spriteVertex.TextureIndex = textureIndex;
                spriteVertex.FrameData = Math::Vector4(
                    frame->GetX(),
                    frame->GetY(),
                    frame->GetWidth(),
                    frame->GetHeight()
                );

                spriteVertex.Tint = animation->Tint;

                m_SpriteVertices[m_NumSprites * 4 + v] = spriteVertex;
            }

            for (int i = 0; i < 6; i++)
            {
                m_SpriteIndices[m_NumSprites * 6 + i] = m_QuadGeometry->Indices[i] + 4 * m_NumSprites;
            }
            m_NumSprites++;

            if (m_NumSprites >= _TESSERACT_OPENGLRENDERER_MAX_NUM_SPRITES)
            {
                RenderAndFlushSprites();
            }
        }

        void OpenGLRendererImplementation::DrawRectangle(
            const Math::Vector3& position,
            const Math::Vector2& dimensions,
            const Math::Vector4& color
        )
        {
            TESSERACT_PROFILE_FUNCTION();

            // Invariant: sprite vertex/index arrays are not full.
            RectangleVertexData rectangleVertex;
            rectangleVertex.Color = color,
            rectangleVertex.Position = position;
            rectangleVertex.Dimensions = dimensions;
            m_RectangleVertices[m_NumRectangles] = rectangleVertex;

            m_NumRectangles++;

            if (m_NumRectangles >= _TESSERACT_OPENGLRENDERER_MAX_NUM_RECTANGLES)
            {
                RenderAndFlushRectangles();
            }
        }

        void OpenGLRendererImplementation::ClearLights()
        {
            m_PointLights.clear();
        }

        void OpenGLRendererImplementation::AddPointLight(const Math::Vector3& position, const Math::Vector3& color, float intensity)
        {
            m_PointLights.push_back(
                {
                    position,
                    color,
                    intensity
                }
            );
        }

        /*
        * Uploads light data to shader program uniforms.
        * Assumes that the shader program is bound
        */
        void OpenGLRendererImplementation::UploadLightPositions(const Ref<ShaderProgram>& material)
        {
            material->SetUniformInt("uPointLightCount", m_PointLights.size());

            int32_t index = 0;

            for (const PointLightData& data : m_PointLights)
            {

                std::string base = "uPointLights[";
                base += std::to_string(index);
                base += "].";

                material->SetUniformVec3(base + "Position", data.Position);
                material->SetUniformVec3(base + "Color", data.Color * data.Intensity); //TODO: Check

                index++;
            }
        }

        void OpenGLRendererImplementation::SetCameraPosition(const Math::Vector3& cameraPosition)
        {
            m_CameraPosition = cameraPosition;
        }



        void OpenGLRendererImplementation::SetBackfaceCullingEnabled(bool enabled)
        {
            if (enabled)
            {
                glEnable(GL_CULL_FACE);

            }
            else
            {
                glDisable(GL_CULL_FACE);

            }
        }

        void OpenGLRendererImplementation::SetViewport(int32_t x, int32_t y, int32_t width, int32_t height)
        {
            glViewport(x, y, width, height);
        }


        void OpenGLRendererImplementation::Clear(uint32_t bits)
        {
            GLbitfield bitField = 0;

            if (bits & Bits::DepthBufferBit)
            {
                bitField |= GL_DEPTH_BUFFER_BIT;
            }

            if (bits & Bits::ColorBufferBit)
            {
                bitField |= GL_COLOR_BUFFER_BIT;
            }

            glClear(bitField);
        }


        void OpenGLRendererImplementation::BeginShadowPass()
        {
            m_ShadowPass = true;
            SetViewport(
                0,
                0,
                m_ShadowResolution,
                m_ShadowResolution
            );

            m_ShadowDepthMapFrameBuffer->Bind();
            Clear(Graphics::Bits::DepthBufferBit);
            m_ShadowShaderProgram->Use();
        }

        void OpenGLRendererImplementation::BeginRenderPass()
        {
            m_ShadowPass = false;

            SetViewport(
                m_DefaultViewportX,
                m_DefaultViewportY,
                m_DefaultViewportWidth,
                m_DefaultViewportHeight
            );

            m_ShadowDepthMapFrameBuffer->Unbind();
            Clear(Graphics::Bits::DepthBufferBit | Graphics::Bits::ColorBufferBit);
            glActiveTexture(GL_TEXTURE15);
            m_ShadowDepthMapCubeMap->Bind();

        }

        void OpenGLRendererImplementation::EnableDepthTest(bool enabled)
        {
            if (enabled)
            {
                glEnable(GL_DEPTH_TEST);
            }
            else
            {
                glDisable(GL_DEPTH_TEST);
            }
        }

        void OpenGLRendererImplementation::SetWireframe(bool enabled)
        {
            if (enabled)
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }
            else
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
        }

        void OpenGLRendererImplementation::ComputeShadows(const Math::Vector3& lightPosition)
        {
            TESSERACT_PROFILE_FUNCTION();

            m_LightPosition = lightPosition;

            float nearPlane = 0.0001f;
            float nearPlaneSprite = 0.01f;
            float farPlane = 60.0f;

            // FIXME: move somewhere else @Justin
            Math::Matrix4 shadowProjection = Math::Matrix4::PerspectiveProjection(
                (float)GetShadowResolution(),
                (float)GetShadowResolution(),
                90.0f,
                nearPlane,
                farPlane
            );

            Math::Matrix4 shadowProjectionSprite = Math::Matrix4::PerspectiveProjection(
                (float)GetShadowResolution(),
                (float)GetShadowResolution(),
                90.0f,
                nearPlaneSprite,
                farPlane
            );

            std::vector<Math::Matrix4> shadowTransforms;
            std::vector<Math::Matrix4> shadowTransformsSprite;

            {
                TESSERACT_PROFILE_SCOPE("Setting up shadow transforms");

                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitX(), -Math::Vector3::UnitY()));
                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitX(), -Math::Vector3::UnitY()));
                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitY(), Math::Vector3::UnitZ()));
                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitY(), -Math::Vector3::UnitZ()));
                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitZ(), -Math::Vector3::UnitY()));
                shadowTransforms.push_back(shadowProjection * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitZ(), -Math::Vector3::UnitY()));

                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitX(), -Math::Vector3::UnitY()));
                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitX(), -Math::Vector3::UnitY()));
                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitY(), Math::Vector3::UnitZ()));
                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitY(), -Math::Vector3::UnitZ()));
                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition + Math::Vector3::UnitZ(), -Math::Vector3::UnitY()));
                shadowTransformsSprite.push_back(shadowProjectionSprite * Math::Matrix4::LookAt(lightPosition, lightPosition - Math::Vector3::UnitZ(), -Math::Vector3::UnitY()));
            }
            {
                TESSERACT_PROFILE_SCOPE("Uploading shadow uniforms");

                {
                    TESSERACT_PROFILE_SCOPE("Using Shadow Shader Program");

                    m_ShadowShaderProgram->Use();
                }

                // - set projection matrix relative to light source
                char idPlaceholder[] = "uShadowMatrices[ ]";

                for (int i = 0; i < 6; i++)
                {
                    idPlaceholder[16] = 48 + i;
                    m_ShadowShaderProgram->SetUniformMat4(idPlaceholder, shadowTransforms[i]);
                }

                m_ShadowShaderProgram->SetUniformFloat("uFarPlane", farPlane);
                m_ShadowShaderProgram->SetUniformVec3("uLightPosition", lightPosition);

                {
                    TESSERACT_PROFILE_SCOPE("Using Sprite Shadow Shader Program");
                    m_SpriteAnimationShadowShaderProgram->Use();
                }

                // - set projection matrix relative to light source
                for (int i = 0; i < 6; i++)
                {
                    idPlaceholder[16] = 48 + i;
                    m_SpriteAnimationShadowShaderProgram->SetUniformMat4(idPlaceholder, shadowTransformsSprite[i]);
                }

                m_SpriteAnimationShadowShaderProgram->SetUniformFloat("uFarPlane", farPlane);
                m_SpriteAnimationShadowShaderProgram->SetUniformVec3("uLightPosition", lightPosition);
            }
        }

    } // namespace Graphics
} // namespace Tesseract

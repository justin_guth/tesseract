#include "OldMeshTODORename.h"

namespace Tesseract::Graphics
{
    void Vertex::AppendToBitList(std::vector<uint8_t>& bitList, uint8_t flags = 7)
    {
        if (flags & (1 << TESSERACT_VERTEX_POSITION))
        {
            // REVIEW: check if c-style cast wouldn't work better
            uint8_t* pos = reinterpret_cast<uint8_t*>(m_Position.GetData());
            for (int i = 0; i < 3 * sizeof(float); i++) bitList.push_back(pos[i]);
        }

        if (flags & (1 << TESSERACT_VERTEX_TEXTURE))
        {
            uint8_t* pos = reinterpret_cast<uint8_t*>(m_TextureCoord.GetData());
            for (int i = 0; i < 2 * sizeof(float); i++) bitList.push_back(pos[i]);
        }

        if (flags & (1 << TESSERACT_VERTEX_NORMAL))
        {
            uint8_t* pos = reinterpret_cast<uint8_t*>(m_Normal.GetData());
            for (int i = 0; i < 3 * sizeof(float); i++) bitList.push_back(pos[i]);
        }
    }

    BufferLayout OldMeshTODORename::GetBufferLayout()
    {
        std::vector<BufferLayout::Element> elems;
        if (m_VertexAttributes & (1 << TESSERACT_VERTEX_POSITION))
        {
            elems.push_back({ "position", DataType::Float3, false });
        }
        if (m_VertexAttributes & (1 << TESSERACT_VERTEX_TEXTURE))
        {
            elems.push_back({ "textureCoordinate", DataType::Float2, false });
        }
        if (m_VertexAttributes & (1 << TESSERACT_VERTEX_NORMAL))
        {
            elems.push_back({ "normal", DataType::Float3, false });
        }

        return BufferLayout(elems);
    }
}
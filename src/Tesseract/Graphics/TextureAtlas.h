#pragma once 

#include "SubTexture2D.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Core/Resource.h"

#include <unordered_map>
#include <string>

namespace Tesseract
{
    namespace Graphics
    {
        class TextureAtlas : public Core::Resource
        {
        public:

            void DefineSubTexture(const std::string& identifier, int32_t column, int32_t row, int32_t width = 1, int32_t height = 1);
            const Ref<SubTexture2D>& operator[](const std::string& identifier) const;

            ~TextureAtlas() = default;
            static Ref<TextureAtlas> Create(const Ref<Texture2D>& texture, int32_t columns, int32_t rows);

            inline void BindTexture() { m_Texture->Bind(); }

        private:

            TextureAtlas(const Ref<Texture2D>& texture, int32_t columns, int32_t rows) :
                Resource(),
                m_Texture(texture),
                m_Columns(columns),
                m_Rows(rows),
                m_Map()
            {
            }

        private:

            std::unordered_map<std::string, Ref<SubTexture2D>> m_Map;
            Ref<Texture2D> m_Texture;
            int32_t m_Columns;
            int32_t m_Rows;
        };

    } // namespace Graphics

} // namespace Tesseract

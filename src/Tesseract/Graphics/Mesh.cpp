#include "Mesh.h"


namespace Tesseract
{
    namespace Graphics
    {
        Mesh::Mesh() :
            Resource(),
            m_Geometry(nullptr),
            m_Material(nullptr),
            m_Textures(),
            m_DrawDebugNormals(false)
        {

        }

        Util::DynamicData Mesh::GetSerializationData() const
        {
            Util::DynamicData result;

            if (m_Geometry == nullptr)
            {
                result["Geometry"] = Util::DynamicData::Null;
            }
            else
            {
                // TODO somehow store geometry identifier
                result["Geometry"] = "some_geometry";
            }

            if (m_Material == nullptr)
            {
                result["Material"] = Util::DynamicData::Null;
            }
            else
            {
                // TODO somehow store material identifier
                result["Material"] = "some_material";
            }

            return result;
        }


    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "ShaderProgram.h"
#include "glad/gl.h"

namespace Tesseract
{
    namespace Graphics
    {
        class OpenGLShaderProgram: public ShaderProgram
        {
        public:
            OpenGLShaderProgram();
            ~OpenGLShaderProgram();

            virtual void SetVertexSource(const std::string& vertexSource) override;
            virtual void SetGeometrySource(const std::string& geometrySource) override;
            virtual void SetFragmentSource(const std::string& fragmentSource) override;
            virtual void Compile() override;
            virtual void Use() override;

            virtual void SetUniformFloat(const char* name, float value) override;
            virtual void SetUniformVec2(const char* name, const Math::Vector2& vector) override;
            virtual void SetUniformVec3(const char* name, const Math::Vector3& vector) override;
            virtual void SetUniformVec4(const char* name, const Math::Vector4& vector) override;

            virtual void SetUniformInt(const char* name, int32_t integer) override;
            virtual void SetUniformInt2(const char* name, const Math::Vector2T<int32_t>& vector) override;
            virtual void SetUniformInt3(const char* name, const Math::Vector3T<int32_t>& vector) override;
            virtual void SetUniformInt4(const char* name, const Math::Vector4T<int32_t>& vector) override;

            virtual void SetUniformUint(const char* name, uint32_t integer) override;
            virtual void SetUniformUint2(const char* name, const Math::Vector2T<uint32_t>& vector) override;
            virtual void SetUniformUint3(const char* name, const Math::Vector3T<uint32_t>& vector) override;
            virtual void SetUniformUint4(const char* name, const Math::Vector4T<uint32_t>& vector) override;

            virtual void SetUniformBool(const char* name, bool value) override;

            virtual void SetUniformMat4(const char* name, const Math::Matrix4& matrix) override;

            virtual void SetUniformVec3Array(const char* name, const std::vector<Math::Vector3>& vectors) override;
            virtual void SetUniformFloatArray(const char* name, const std::vector<float>& values) override;
            virtual void SetUniformIntArray(const char* name, const std::vector<int32_t>& values) override;


            static const std::string Preprocess(const std::string& shaderSource);
            
        private:

            std::string m_VertexSource;
            std::string m_GeometrySource;
            std::string m_FragmentSource;

            GLuint m_ProgramID;


        };


    } // namespace Graphics

} // namespace Tesseract

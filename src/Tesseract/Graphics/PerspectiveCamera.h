#pragma once

#include "Camera.h"

namespace Tesseract
{
    namespace Graphics
    {
        class PerspectiveCamera: public Camera
        {
        protected:
            virtual Physics::Ray GetScreenRay(Math::Vector2T<uint32_t> screenCoord) override;
        public:

            inline PerspectiveCamera(): PerspectiveCamera(1.0f, 90.0f, 0.01f, 100.0f)
            {

            }

            inline PerspectiveCamera(float aspectRatio, float fov, float near, float far) :
                m_FOV(fov),
                m_Near(near),
                m_Far(far)
            {
                m_AspectRatio = aspectRatio;
                UpdateProjection();
            }

            inline virtual void SetWindowSize(uint32_t width, uint32_t height) override { Camera::SetWindowSize(width, height); UpdateProjection(); }
            inline virtual void SetAspectRatio(float ratio) override { Camera::SetAspectRatio(ratio); UpdateProjection(); }
            inline void SetFOV(float fov) { m_FOV = fov; UpdateProjection(); }
            inline void SetNear(float near) { m_Near = near; UpdateProjection(); }
            inline void SetFar(float far) { m_Far = far; UpdateProjection(); }

            inline virtual const std::vector<Parameter> GetParameters() override
            {
                return {
                    { "FOV", "cameraFOVID", DataType::Float1, &m_FOV },
                    { "Near", "cameraNearID", DataType::Float1, &m_Near },
                    { "Far", "cameraFarID", DataType::Float1, &m_Far },
                };
            }

            virtual void OnParameterChange(const Parameter& parameter)
            {
                UpdateProjection();
                // TODO: find out way to check if recompute is necessary (also in scene later when updating view data)
                Recompute();
            }

            virtual Util::DynamicData GetSerializationData() const override;



        private:

            float m_FOV;
            float m_Near;
            float m_Far;

        private:

            inline void UpdateProjection()
            {
                SetProjectionMatrix(
                    Math::Matrix4::PerspectiveProjection(m_AspectRatio, 1.0f, m_FOV, m_Near, m_Far)
                );
            }

        };
    } // namespace Graphics

} // namespace Tesseract

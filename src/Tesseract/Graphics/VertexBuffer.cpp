#include "VertexBuffer.h"

#include "OpenGLVertexBuffer.h"


namespace Tesseract
{
    namespace Graphics
    {

        Ref<VertexBuffer> VertexBuffer::Create()
        {
            return MakeRef<OpenGLVertexBuffer>();
        }


    } // namespace Graphics
} // namespace Tesseract

#pragma once


#include <stdint.h>

#include "Tesseract/Reference.h"

namespace Tesseract
{

    namespace Graphics
    {

        class ElementBuffer
        {
        public:

            enum class ElementType
            {
                None = 0,
                UInt8,
                UInt16,
                UInt32
            };

        public:

            virtual void Bind() = 0;
            virtual void UploadData(void* data, size_t size) = 0;

            void SetData(void* baseAddress, uint32_t count, ElementType elementType);
            inline ElementType GetElementType() const { return m_ElementType; }
            inline uint32_t GetElementCount() const { return m_ElementCount; }

            static Ref<ElementBuffer> Create();

        protected:

            ElementType  m_ElementType = ElementType::None;
            uint32_t m_ElementCount = 0;

        };

    } // namespace Graphics
} // namespace Tesseract



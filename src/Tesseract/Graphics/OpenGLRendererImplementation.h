#pragma once

#include "RendererImplementation.h"

#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "VertexArrayObject.h"
#include "Material.h"

#include "Tesseract/Core/Geometry.h"
#include "Tesseract/Math/Vector3.h"
#include "CubeMap.h"
#include "FrameBuffer.h"
#include "OpenGLShaderProgram.h"

#include <vector>

#define _TESSERACT_OPENGLRENDERER_MAX_NUM_SPRITES 65536
#define _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTS 65536
#define _TESSERACT_OPENGLRENDERER_MAX_NUM_RECTANGLES 8196
#define _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTURES 8

namespace Tesseract
{
    namespace Graphics
    {
        struct SpriteVertexData
        {
            Math::Vector3 Position;
            Math::Vector2 TextureCoordinate;
            Math::Vector3 Normal;
            Math::Vector3 Tangent;
            Math::Vector3 Bitangent;
            int32_t TextureIndex;
            Math::Vector4 FrameData;
            Math::Vector4 Tint;
        };

      

        struct TextVertexData
        {
            Math::Vector3 Position;
            Math::Vector2 TextureCoordinates;
            Math::Vector4 Color;
            int32_t TextureIndex;
        };

        struct RectangleVertexData
        {
            Math::Vector3 Position;
            Math::Vector2 Dimensions;
            Math::Vector4 Color;
        };

        class OpenGLRendererImplementation: public RendererImplementation
        {
        private:
            void ApplyDefaultBlendFunc();
        public:

            OpenGLRendererImplementation();
            virtual void Init() override;

            virtual void DrawText(
                const std::string& text,
                const Math::Matrix4& globalTransform,
                Ref<Typing::Face> face,
                float fontSize,
                const Math::Vector4& color
            ) override;

            virtual void DrawMesh(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            ) override;

            virtual void DrawMeshShadow(
                const Ref<Graphics::Mesh>& mesh,
                const Math::Matrix4& modelMatrix
            ) override;

            virtual void DrawAnimatedQuad(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            ) override;

            virtual void DrawAnimatedQuadShadow(
                const Math::Matrix4& modelMatrix,
                const Ref<SpriteAnimation>& animation
            ) override;

            virtual void DrawRectangle(
                const Math::Vector3& position,
                const Math::Vector2& dimensions,
                const Math::Vector4& color
            ) override;

            virtual void SetCameraPosition(const Math::Vector3& cameraPosition) override;
            virtual void ClearLights() override;
            virtual void AddPointLight(const Math::Vector3& position, const Math::Vector3& color, float intensity) override;

            virtual void SetBackfaceCullingEnabled(bool enabled) override;
            void RenderAndFlushSprites() override;
            void RenderAndFlushTexts() override;
            void RenderAndFlushRectangles() override;

            virtual void SetViewport(int32_t x, int32_t y, int32_t width, int32_t height) override;
            virtual void Clear(uint32_t bits) override;

            virtual void BeginShadowPass() override;
            virtual void BeginRenderPass() override;
            virtual void ComputeShadows(const Math::Vector3& lightPosition) override;

            virtual void EnableDepthTest(bool enabled = true) override;
            virtual void SetWireframe(bool enabled = true) override;


        private:

            void UploadLightPositions(const Ref<ShaderProgram>& material);
            int GetIndexOfTexture(const Ref<TextureAtlas>& atlas);
            int GetIndexOfTextTexture(const Ref<GlyphTexture>& texture);

        private:

            Ref<Graphics::VertexBuffer> m_VertexBuffer;
            Ref<Graphics::ElementBuffer> m_ElementBuffer;
            Ref<Graphics::VertexArrayObject> m_VertexArrayObject;

            Ref<TextureAtlas> m_Textures[_TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTURES];
            Ref<GlyphTexture> m_TextTextures[_TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTURES];
            uint8_t m_NumTextures;
            uint8_t m_NumTextTextures;

            SpriteVertexData m_SpriteVertices[4 * _TESSERACT_OPENGLRENDERER_MAX_NUM_SPRITES];
            uint32_t m_SpriteIndices[6 * _TESSERACT_OPENGLRENDERER_MAX_NUM_SPRITES];
            RectangleVertexData m_RectangleVertices[4 * _TESSERACT_OPENGLRENDERER_MAX_NUM_RECTANGLES];
            uint32_t m_RectangleIndices[6 * _TESSERACT_OPENGLRENDERER_MAX_NUM_RECTANGLES];
            TextVertexData m_TextVertices[4 * _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTS];
            uint32_t m_TextIndices[6 * _TESSERACT_OPENGLRENDERER_MAX_NUM_TEXTS];
            uint32_t m_NumSprites;
            uint32_t m_NumRectangles;
            uint32_t m_NumTexts;

            Ref<Graphics::VertexBuffer> m_SpriteVertexBuffer;
            Ref<Graphics::ElementBuffer> m_SpriteElementBuffer;
            Ref<Graphics::VertexArrayObject> m_SpriteVertexArrayObject;

            Ref<Graphics::VertexBuffer> m_TextVertexBuffer;
            Ref<Graphics::ElementBuffer> m_TextElementBuffer;
            Ref<Graphics::VertexArrayObject> m_TextVertexArrayObject;

            Ref<Graphics::VertexBuffer> m_RectangleVertexBuffer;
            
            Ref<Core::Geometry> m_QuadGeometry;
            Ref<Material> m_SpriteAnimationMaterial;

            Ref<Graphics::VertexBuffer> m_ParticleVertexBuffer;

            struct PointLightData
            {
                Math::Vector3 Position;
                Math::Vector3 Color;
                float Intensity;
            };

            std::vector<PointLightData> m_PointLights;
            Math::Vector3 m_CameraPosition;

            Ref<ShaderProgram> m_ShadowShaderProgram;
            Ref<ShaderProgram> m_SpriteAnimationShadowShaderProgram;
            Ref<ShaderProgram> m_TextShaderProgram;
            Ref<ShaderProgram> m_RectangleShaderProgram;


            Ref<FrameBuffer> m_ShadowDepthMapFrameBuffer;
            Ref<CubeMap> m_ShadowDepthMapCubeMap;
            Math::Vector3 m_LightPosition;

            bool m_ShadowPass = false;

        };
    } // namespace Graphics
} // namespace Tesseract

#include "GlyphTexture.h"
#include "OpenGLGlyphTexture.h"


namespace Tesseract
{
    namespace Graphics
    {

        GlyphTexture::GlyphTexture()
        {

        }


        GlyphTexture::~GlyphTexture()
        {
            
        }


        Ref<GlyphTexture> GlyphTexture::Create(uint32_t width, uint32_t height, uint8_t* data)
        {
            Ref<GlyphTexture> result =  Ref<OpenGLGlyphTexture>(new OpenGLGlyphTexture(width, height, data));
            result->UploadData();
            return result;
        }


    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "VertexArrayObject.h"

#include "glad/gl.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Graphics
    {
        class OpenGLVertexArrayObject : public VertexArrayObject
        {
        public:

            OpenGLVertexArrayObject();
            ~OpenGLVertexArrayObject();

            virtual void LinkBuffers(const Ref<VertexBuffer>& vertexBuffer, const Ref<ElementBuffer>& elementBuffer) override;

            void Bind() override;


        private:

            GLuint m_ObjectID;

        };
    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "Tesseract/Math/Matrix4.h"
#include "Tesseract/Math/Vector3.h"

#include "Tesseract/IHasParameters.h"

#include "Tesseract/Util/DynamicData.h"

#include "Tesseract/ISerializable.h"
#include "Tesseract/Physics/Ray.h"

#include <memory>

namespace Tesseract
{
    namespace Graphics
    {
        class Camera: public IHasParameters, public ISerializable
        {
        public:

            virtual ~Camera()
            {
                if (IsActive())
                {
                    MakeInactive();
                }
            }

            inline const Math::Matrix4& GetViewMatrix() { return m_ViewMatrix; }
            inline const Math::Matrix4& GetProjectionMatrix() { return m_ProjectionMatrix; }
            inline const Math::Matrix4& GetProjectionViewMatrix() { return m_ProjectionViewMatrix; }

            inline void UpdateTransform(const Math::Vector3& position, const Math::Vector3& rotation)
            {
                SetViewMatrix(
                    Math::Matrix4::RotationTranslation(rotation, position).Inverse()
                );
            }

            inline virtual void SetViewParameters(const Math::Vector3& position, const Math::Vector3& rotation)
            {
                SetViewMatrix(
                    Math::Matrix4::RotationTranslation(rotation, position).Inverse()
                );
                Recompute();
            }

            inline virtual void SetWindowSize(uint32_t width, uint32_t height)
            {
                m_AspectRatio = float(width) / height;
                m_WindowSize = { width, height };
            }
            inline virtual void SetAspectRatio(float ratio) { m_AspectRatio = ratio; }  // REVIEW: Can this be a problem concerning m_WindowSize?
            inline virtual float GetAspectRatio() { return m_AspectRatio; }

            void MakeActive();
            void MakeInactive();
            inline bool IsActive() const { return m_Active; }

            inline static Physics::Ray GetRayThroughScreenCoordinate(Math::Vector2T<uint32_t> screenCoordinate) { return s_CurrentActive->GetScreenRay(screenCoordinate); }

        protected:

            inline void SetProjectionMatrix(const Math::Matrix4& matrix) { m_ProjectionMatrix = matrix; }
            inline void SetViewMatrix(const Math::Matrix4& matrix) { m_ViewMatrix = matrix; }

            inline void Recompute() { m_ProjectionViewMatrix = m_ProjectionMatrix * m_ViewMatrix; }

            virtual Physics::Ray GetScreenRay(Math::Vector2T<uint32_t> screenCoordinate) = 0;

        protected:

            Math::Matrix4 m_ViewMatrix;
            Math::Matrix4 m_ProjectionMatrix;
            Math::Matrix4 m_ProjectionViewMatrix;
            Math::Vector2T<uint32_t> m_WindowSize;
            float m_AspectRatio;
            bool m_Active = false;

            // TODO: This breaks when using multiple scenes, find a clean way to solve this
            // maybe storing pointer in scene was the better solution but now maybe use the destructor to clear that reference
            static Camera* s_CurrentActive;
        };
    } // namespace Graphics

} // namespace Tesseract

#include "Texture2D.h"
#include "OpenGLTexture2D.h"

#include "vendor/stb_image/stb_image.h"

namespace Tesseract
{
    namespace Graphics
    {

        Texture2D::Texture2D(const std::string& filePath)
        {
            stbi_set_flip_vertically_on_load(true);
            m_Data = stbi_load(filePath.c_str(), &m_Width, &m_Height, &m_Channels, 0);
        }


        Texture2D::~Texture2D()
        {
            stbi_image_free(m_Data);
        }


        Ref<Texture2D> Texture2D::LoadFromFile(const std::string& filePath)
        {
            Ref<Texture2D> result =  Ref<OpenGLTexture2D>(new OpenGLTexture2D(filePath));
            result->UploadData();
            return result;
        }


    } // namespace Graphics

} // namespace Tesseract

#pragma once

#include "VertexBuffer.h"

#include "glad/gl.h"


namespace Tesseract
{
    namespace Graphics
    {
        class OpenGLVertexBuffer : public VertexBuffer
        {
        public:

            OpenGLVertexBuffer();
            ~OpenGLVertexBuffer();

            virtual void Bind() const override;
            virtual void SetData(void* baseAddress, size_t size) override;
            virtual void UploadData(void* baseAddress, size_t size) override;

            virtual void ApplyLayout() override;

        private:

            GLuint m_BufferID;

        };

    } // namespace Graphics
} // namespace Tesseract

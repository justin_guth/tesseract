#include "PerspectiveCamera.h"

namespace Tesseract
{
    namespace Graphics
    {
        Physics::Ray PerspectiveCamera::GetScreenRay(Math::Vector2T<uint32_t> screenCoord)
        {
            auto inverseView = m_ViewMatrix.Inverse();

            Math::Vector2 normalizedPos = (Math::Vector2(screenCoord.X, screenCoord.Y) / Math::Vector2(m_WindowSize.X, m_WindowSize.Y) - 0.5) * 2;

            Math::Vector3 offsetFromClick = Math::Vector3::Down + Math::Vector3::Right * normalizedPos.X + Math::Vector3::Back * normalizedPos.Y;
            Math::Vector3 globalPos = (inverseView * Math::Vector4(0, 0, 0, 1)).XYZ();
            Math::Vector3 globalOffset = (inverseView * Math::Vector4(offsetFromClick.X, offsetFromClick.Y, offsetFromClick.Z, 1)).XYZ();

            Physics::Ray r =
            {
                globalPos,
                (globalOffset - globalPos).Normalized()
            };
            return r;
        }

        Util::DynamicData PerspectiveCamera::GetSerializationData() const
        {
            Util::DynamicData result;

            result["FOV"] = m_FOV;
            result["Near"] = m_Near;
            result["Far"] = m_Far;
            result["AspectRatio"] = m_AspectRatio;
            result["Type"] = "perspective";
            result["Active"] = IsActive();

            return result;
        }
    } // namespace Graphics

} // namespace Tesseract

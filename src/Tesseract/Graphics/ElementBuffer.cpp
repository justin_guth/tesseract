#include "OpenGLElementBuffer.h"

namespace Tesseract
{

    namespace Graphics
    {


        Ref<ElementBuffer> ElementBuffer::Create()
        {
            return MakeRef<OpenGLElementBuffer>();
        }


        void ElementBuffer::SetData(void* baseAddress, uint32_t elementCount, ElementType elementType)
        {
            m_ElementType = elementType;
            m_ElementCount = elementCount;

            size_t elementSize;

            switch (elementType)
            {
                case ElementType::None: elementSize = 0;  break;
                case ElementType::UInt8: elementSize = 1;  break;
                case ElementType::UInt16: elementSize = 2;  break;
                case ElementType::UInt32: elementSize = 4;  break;
                default: elementSize = 0;  break;
            }

            UploadData(baseAddress, elementCount * elementSize);
        }


    } // namespace Graphics
} // namespace Tesseract



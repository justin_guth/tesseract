#include "OpenGLCubeMap.h"

namespace Tesseract
{
    namespace Graphics
    {
        OpenGLCubeMap::OpenGLCubeMap(uint32_t width, uint32_t height):
            m_CubeMapID(0),
            m_Width(width),
            m_Height(height)
        {
            glGenTextures(1, &m_CubeMapID);
            Bind();

            for (int32_t i = 0; i < 6; i++)
            {
                glTexImage2D(
                    GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                    0,
                    GL_DEPTH_COMPONENT,
                    width,
                    height,
                    0,
                    GL_DEPTH_COMPONENT,
                    GL_FLOAT,
                    NULL
                );
            }


            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }

        void OpenGLCubeMap::AssignFrameBuffer(const Ref<FrameBuffer>& frameBuffer)
        {
            frameBuffer->Bind();
            glFramebufferTexture(
                GL_FRAMEBUFFER,
                GL_DEPTH_ATTACHMENT, //FIXME Make param ;(
                m_CubeMapID,
                0
            );

            glDrawBuffer(GL_NONE); //FIXME yeah, fixme: Only works for Shadowy stuff
            glReadBuffer(GL_NONE);
        }


    } // namespace Graphics

} // namespace Tesseract

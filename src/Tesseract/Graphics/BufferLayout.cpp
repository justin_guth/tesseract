#include "BufferLayout.h"

namespace Tesseract
{
    namespace Graphics
    {

        static uint32_t GetDataTypeSize(DataType dataType)
        {
            switch (dataType)
            {
                case DataType::None: return 0;
                case DataType::Int1: return 1 * 4;
                case DataType::Int2: return 2 * 4;
                case DataType::Int3: return 3 * 4;
                case DataType::Int4: return 4 * 4;
                case DataType::Uint1: return 1 * 4;
                case DataType::Uint2: return 2 * 4;
                case DataType::Uint3: return 3 * 4;
                case DataType::Uint4: return 4 * 4;
                case DataType::Float1: return 1 * 4;
                case DataType::Float2: return 2 * 4;
                case DataType::Float3: return 3 * 4;
                case DataType::Float4: return 4 * 4;
                case DataType::Color: return 4 * 4;
                case DataType::Bool: return 1;
                case DataType::Mat2: return 2 * 2 * 4;
                case DataType::Mat3: return 3 * 3 * 4;
                case DataType::Mat4: return 4 * 4 * 4;
                default: return 0;
            }
        }

        uint32_t GetDataTypeComponentCount(DataType dataType)
        {
            switch (dataType)
            {
                case DataType::None: return 0;
                case DataType::Int1: return 1;
                case DataType::Int2: return 2;
                case DataType::Int3: return 3;
                case DataType::Int4: return 4;
                case DataType::Uint1: return 1;
                case DataType::Uint2: return 2;
                case DataType::Uint3: return 3;
                case DataType::Uint4: return 4;
                case DataType::Float1: return 1;
                case DataType::Float2: return 2;
                case DataType::Float3: return 3;
                case DataType::Float4: return 4;
                case DataType::Color: return 4;
                case DataType::Bool: return 1;
                case DataType::Mat2: return 2 * 2;
                case DataType::Mat3: return 3 * 3;
                case DataType::Mat4: return 4 * 4;
                default: return 0;
            }
        }

        BufferLayout::Element::Element():
            Element("unknown", DataType::None, false)
        {
        }


        BufferLayout::Element::Element(const std::string& name, Tesseract::DataType dataType, bool normalized):
            Name(name),
            AssignedDataType(dataType),
            Normalized(normalized),
            Offset(0),
            Size(GetDataTypeSize(dataType))
        {
        }


        BufferLayout::BufferLayout(const std::initializer_list<Element>& initializerList)
        {

            uint32_t currentOffset = 0;

            for (Element element : initializerList)
            {
                element.Offset = currentOffset;
                m_Elements.push_back(element);
                currentOffset += element.Size;
            }
        }

        BufferLayout::BufferLayout(const std::vector<Element>& elements)
        {

            uint32_t currentOffset = 0;

            for (Element element : elements)
            {
                element.Offset = currentOffset;
                m_Elements.push_back(element);
                currentOffset += element.Size;
            }
        }

    } // namespace Graphics
} // namespace Tesseract
#include "Material.h"

#include "Tesseract/Util/File.h"

namespace Tesseract
{
    namespace Graphics
    {

        Material::Material(const Ref<ShaderProgram>& shaderProgram)
        {
            SetShaderProgram(shaderProgram);
        }


        Material::Material(const char* vertexSourcePath, const char* fragmentSourcePath)
        {
            SetShaderProgram(
                ShaderProgram::CreateAndCompileFromSourceFiles(
                    vertexSourcePath,
                    fragmentSourcePath
                )
            );

        }

        void Material::SetShaderProgram(const Ref<ShaderProgram>& shaderProgram)
        {
            m_ShaderProgram = shaderProgram;
        }



        void Material::UploadData()
        {
            for (const auto [name, identifier, type, address] : GetParameters())
            {
                switch (type)
                {
                    case DataType::Float1:  GetShaderProgram()->SetUniformFloat(identifier.c_str(), *(float*)address);  break; // TODO
                    case DataType::Float2: break; // TODO
                    case DataType::Float3: GetShaderProgram()->SetUniformVec3(identifier.c_str(), *((Math::Vector3*)address));  break;
                    case DataType::Float4: GetShaderProgram()->SetUniformVec4(identifier.c_str(), *((Math::Vector4*)address)); break;
                    case DataType::Color: GetShaderProgram()->SetUniformVec4(identifier.c_str(), *((Math::Vector4*)address)); break;
                    case DataType::Int1: break; // TODO
                    case DataType::Int2: break; // TODO
                    case DataType::Int3: break; // TODO
                    case DataType::Int4: break; // TODO
                    case DataType::Mat2: break; // TODO
                    case DataType::Mat3: break; // TODO
                    case DataType::Mat4: GetShaderProgram()->SetUniformMat4(identifier.c_str(), *((Math::Matrix4*)address)); break;
                    case DataType::Bool: GetShaderProgram()->SetUniformBool(identifier.c_str(), *((bool*)address)); break;
                    case DataType::None: TESSERACT_CORE_ASSERT(false, "None provided as data type!"); break;
                    default: break;
                }
            }
        }



        void Material::Use()
        {
            // TODO: keep track of actively used shader program and only ->Use() the shader program if it is not currently in use
            // TODO: Check if this logic should be part of the ShaderProgram class itself, since all shader switches should go through it

            m_ShaderProgram->Use();
            UploadData();
        }

    } // namespace Graphics

} // namespace Tesseract

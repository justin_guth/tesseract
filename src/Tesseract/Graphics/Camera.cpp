#include "Camera.h"

#include "Tesseract/Core/Application.h"
#include "Tesseract/Core/Scene.h"

namespace Tesseract
{
    namespace Graphics
    {
        Camera* Camera::s_CurrentActive = nullptr;

        void Camera::MakeActive()
        {
            if (m_Active) {
                return;
            }

            m_Active = true;
            if (s_CurrentActive != nullptr)
            {
                s_CurrentActive->MakeInactive();
            }
            s_CurrentActive = this;
        }

        void Camera::MakeInactive()
        {
            m_Active = false;

            if (s_CurrentActive == this)
            {
                s_CurrentActive = nullptr;
            }
        }
    } // namespace Graphics
} // namespace Tesseract

#pragma once


#include "Component.h"

#include "Tesseract/Graphics/Mesh.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
	namespace Components
	{
		struct MeshComponent : public Component
		{
			Ref<Graphics::Mesh> Mesh;

			inline MeshComponent() : Component(), Mesh(nullptr) {}
			inline MeshComponent(const ECS::Entity& entity) : Component(entity), Mesh(nullptr) {}
			inline MeshComponent(const ECS::Entity& entity, const Ref<Graphics::Mesh>& mesh) : Component(entity), Mesh(mesh) {}

			inline MeshComponent operator=(const MeshComponent& other)
			{
				Mesh = other.Mesh;
				Component::operator=(other);

				return *this;
			}
		};
	}
}

#pragma once

#include <string>

#include "Tesseract/Scripting/Script.h"
#include "Tesseract/Reference.h"

#include <vector>

#include "Component.h"

namespace Tesseract
{
	namespace Components
	{
		struct ScriptComponent: public Component
		{
			std::vector<Ref<Scripting::Script>> Scripts;

			inline ScriptComponent(): Component(), Scripts() {}
			inline ScriptComponent(const ECS::Entity& entity) : Component(entity), Scripts() {}

			inline void Attach(const Ref<Scripting::Script>& script, bool callOnAttach = true)
			{
				Scripts.push_back(script);
				script->Entity = Entity;
				if (callOnAttach)
				{
					script->OnAttach();
				}
			}

			inline ScriptComponent operator=(const ScriptComponent& other)
			{
				Scripts = other.Scripts;
				Component::operator=(other);

				return *this;
			}

			template <typename T_Type>
			inline Ref<T_Type> Get()
			{
				for (const Ref<Scripting::Script>& script : Scripts)
				{
					Ref<T_Type> potentialResult = std::dynamic_pointer_cast<T_Type>(script);
					if (potentialResult != nullptr)
					{
						return potentialResult;
					}
				}

				return nullptr;
			}

		};
	}
}

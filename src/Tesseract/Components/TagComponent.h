#pragma once

#include <string>

#include "Component.h"


namespace Tesseract
{
	namespace Components
	{
		struct TagComponent : public Component
		{
			std::string Tag;

			inline TagComponent() : Component(), Tag("Unnamed Entity") {}
			inline TagComponent(const ECS::Entity& entity) : Component(entity), Tag("Unnamed Entity") {}
			inline TagComponent(const ECS::Entity& entity, const std::string& tag) : Component(entity), Tag(tag) {}
			inline TagComponent(const ECS::Entity& entity, const char* tag) : Component(entity), Tag(tag) {}

			inline TagComponent operator=(const TagComponent& other)
			{
				Tag = other.Tag;
				Component::operator=(other);

				return *this;
			}
		};
	}
}

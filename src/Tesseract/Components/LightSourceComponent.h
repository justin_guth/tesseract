#pragma once

#include "Component.h"

#include "Tesseract/Graphics/LightSourceType.h"
#include "Tesseract/Math/Vector3.h"

namespace Tesseract
{
	namespace Components
	{
		struct LightSourceComponent : public Component
		{
			Graphics::LightSourceType Type;
			Math::Vector3 Color;
			float Intensity;
			bool IsPrimary;

			inline LightSourceComponent() : Component(), Type(Graphics::LightSourceType::Unset), Color(1.0f), Intensity(1.0f), IsPrimary(false) {}
			inline LightSourceComponent(const ECS::Entity& entity) : Component(entity), Type(Graphics::LightSourceType::Unset), Color(1.0f), Intensity(1.0f), IsPrimary(false)  {}
			inline LightSourceComponent(const ECS::Entity& entity, const Graphics::LightSourceType type, const Math::Vector3& color, float intensity, bool isPrimary) : Type(type), Color(color), Intensity(intensity), IsPrimary(isPrimary) {}

			inline LightSourceComponent operator=(const LightSourceComponent& other)
			{
				Type = other.Type;
				Color = other.Color;
				Intensity = other.Intensity;
				IsPrimary = other.IsPrimary;
				Component::operator=(other);

				return *this;
			}
		};
	}
}

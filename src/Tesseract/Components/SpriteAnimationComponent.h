#pragma once

#include "Component.h"


#include "Tesseract/Graphics/SpriteAnimation.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
	namespace Components
	{
		struct SpriteAnimationComponent : public Component
		{
			Ref<Graphics::SpriteAnimation> Animation;
			bool CastsShadow;

			inline SpriteAnimationComponent() : Component(), Animation(nullptr), CastsShadow(true) {}
			inline SpriteAnimationComponent(const ECS::Entity& entity) : Component(entity), Animation(nullptr), CastsShadow(true) {}
			inline SpriteAnimationComponent(const ECS::Entity& entity, const Ref<Graphics::SpriteAnimation>& animation, bool castsShadow = true) : Component(entity), Animation(animation), CastsShadow(castsShadow) {}

			inline SpriteAnimationComponent operator=(const SpriteAnimationComponent& other)
			{
				Animation = other.Animation;
				CastsShadow = other.CastsShadow;

				Component::operator=(other);

				return *this;
			}
		};
	}
}

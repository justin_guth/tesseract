#pragma once

#include "Tesseract/ECS/ECS.h"

namespace Tesseract
{
	namespace Components
	{
		class Component
		{
		public:

			ECS::Entity Entity;

			Component() : Entity(ECS::Entity::Null) {}
			Component(const ECS::Entity& entity) : Entity(entity) {}
			Component(const Component& other) : Entity(other.Entity) {}

			Component operator=(const Component& other)
			{
				Entity = other.Entity;
				return *this;
			}
		};
	}
}
#pragma once

#include <vector>

#include "Component.h"
#include "Tesseract/Physics/Collider.h"


namespace Tesseract
{
	namespace Components
	{
		struct ColliderComponent: public Component
		{
			std::vector<Ref<Physics::Collider>> Colliders;

			inline ColliderComponent(): Component(), Colliders() {}
			inline ColliderComponent(const ECS::Entity& entity) : Component(entity), Colliders() {}
			inline ColliderComponent(const ECS::Entity& entity, std::vector<Ref<Physics::Collider>> colliders) : Component(entity), Colliders(colliders) {}

			inline ColliderComponent operator=(const ColliderComponent& other)
			{
				Colliders = other.Colliders;
				Component::operator=(other);

				return *this;
			}

			inline bool Intersects(const Ref<Physics::Collider>& other, const Components::TransformComponent& myTransform, const Components::TransformComponent& otherTransform)
			{

				for (Ref<Physics::Collider>& collider : Colliders)
				{
					if (collider->Intersects(other)) { return true; }
				}

				return false;
			}

		};
	}
}

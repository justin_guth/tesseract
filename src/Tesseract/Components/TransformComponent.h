#pragma once


#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Math/Matrix4.h"
#include "Tesseract/Math/Quaternion.h"

#include "Component.h"

namespace Tesseract
{
	namespace Components
	{
		struct TransformComponent: public Component
		{
			Math::Vector3 Position;
			Math::Vector3 Scale;

			// TODO: Change to quaternion
			Math::Vector3 RotationEulerAngles;

			inline TransformComponent(): Component(), Position(), Scale(1.0f), RotationEulerAngles() {}
			inline TransformComponent(const ECS::Entity& entity) : Component(entity), Position(), Scale(1.0f), RotationEulerAngles() {}

			inline Math::Matrix4 GetLocalTransformMatrix()
			{
				return Math::Matrix4::ScaleRotationTranslation(Scale, RotationEulerAngles, Position);
			}

			Math::Matrix4 GetGlobalTransformMatrix() const;
			Math::Vector3 GetGlobalPosition() const;
			static Math::Matrix4 GetGlobalTransformMatrix(const ECS::Entity& entity);

			inline Math::Vector3 Forward() { return (GetLocalTransformMatrix() * Math::Vector3::Forward.HomogenizedVector()).XYZ(); }
			inline Math::Vector3 Back() { return (GetLocalTransformMatrix() * Math::Vector3::Back.HomogenizedVector()).XYZ(); }
			inline Math::Vector3 Right() { return (GetLocalTransformMatrix() * Math::Vector3::Right.HomogenizedVector()).XYZ(); }
			inline Math::Vector3 Left() { return (GetLocalTransformMatrix() * Math::Vector3::Left.HomogenizedVector()).XYZ(); }
			inline Math::Vector3 Up() { return (GetLocalTransformMatrix() * Math::Vector3::Up.HomogenizedVector()).XYZ(); }
			inline Math::Vector3 Down() { return (GetLocalTransformMatrix() * Math::Vector3::Down.HomogenizedVector()).XYZ(); }

			inline TransformComponent operator=(const TransformComponent& other)
			{
				Position = other.Position;
				Scale = other.Scale;
				RotationEulerAngles = other.RotationEulerAngles;
				Component::operator=(other);

				return *this;
			}
		};
	}
}

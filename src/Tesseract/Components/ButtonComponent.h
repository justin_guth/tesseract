#pragma once

#include <string>

#include "Component.h"


namespace Tesseract
{
	namespace Components
	{
		struct ButtonComponent : public Component
		{
			std::string Text;
			Ref<Typing::Face> TextFace;
			float TextSize;
			Math::Vector4 Color;
			Math::Vector2 Dimensions;
			Math::Vector4 TextColor;
			bool Visible;

			inline ButtonComponent() : Component(), Text("Button"), Dimensions(40, 20), TextFace(nullptr), TextSize(10.0f), TextColor(0.0f), Color(1.0f),  Visible(true) {}
			inline ButtonComponent(const ECS::Entity& entity) : Component(entity), Text("Button"), Dimensions(40, 20), TextFace(nullptr), TextSize(10.0f), TextColor(0.0f), Color(1.0f),  Visible(true) {}
			inline ButtonComponent(const ECS::Entity& entity, const std::string& text) : Component(entity), Text(text), Dimensions(40, 20), TextFace(nullptr), TextSize(10.0f), TextColor(0.0f), Color(1.0f),  Visible(true) {}
			inline ButtonComponent(const ECS::Entity& entity, const char* text) : Component(entity), Text(text), Dimensions(40, 20), TextFace(nullptr), TextSize(10.0f), TextColor(0.0f), Color(1.0f),  Visible(true) {}

			inline ButtonComponent operator=(const ButtonComponent& other)
			{
				Text = other.Text;
				TextFace = other.TextFace;
				TextSize = other.TextSize;
				TextColor = other.TextColor;
				Color = other.Color;
				Visible = other.Visible;
				Dimensions = other.Dimensions;

				Component::operator=(other);

				return *this;
			}
		};
	}
}

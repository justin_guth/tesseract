#pragma once

#include "Component.h"
#include "Tesseract/Physics/Physics.h"
#include "Tesseract/Core/Time.h"

namespace Tesseract
{
	namespace Components
	{
		struct RigidBodyComponent : public Component
		{
			Physics::State State;
			float Mass;
			float Drag;
			bool HasGravity;

			inline RigidBodyComponent() : Component(), State(), Mass(1.0f), Drag(1.0f), HasGravity(true) {}
			inline RigidBodyComponent(const ECS::Entity& entity) : Component(entity), State(), Mass(1.0f), Drag(1.0f), HasGravity(true) {}
			inline RigidBodyComponent(const ECS::Entity& entity, const Physics::State& state, float mass, float drag, bool hasGravity) : Component(entity), State(state), Mass(mass), Drag(drag), HasGravity(hasGravity) {}

			inline RigidBodyComponent operator=(const RigidBodyComponent& other)
			{
				State = other.State;
				Mass = other.Mass;
				Drag = other.Drag;
				HasGravity = other.HasGravity;
				Component::operator=(other);

				return *this;
			}

			inline void AddForce(const Math::Vector3& force)
			{
				Physics::Physics::AddForceInPlace(State, force, Mass);
			}

		};
	}
}

#pragma once

#include <string>

#include "Component.h"
#include "Tesseract/Typing/Typing.h"

namespace Tesseract
{
	namespace Components
	{
		struct TextComponent : public Component
		{
			std::string Text;
			Ref<Typing::Face> Face;
			float Size;
			Math::Vector4 Color;
			bool IsOverlay;
			bool Visible;

			inline TextComponent() : Component(), Text("Text"), Face(nullptr), Size(10.0f), Color(1.0f), IsOverlay(false), Visible(true) {}
			inline TextComponent(const ECS::Entity& entity) : Component(entity), Text("Text"), Face(nullptr) , Size(10.0f), Color(1.0f), IsOverlay(false), Visible(true){}
			inline TextComponent(const ECS::Entity& entity, const std::string& text, Ref<Typing::Face> face, float size, const Math::Vector4& color) : Component(entity), Text(text), Face(face), Size(size), Color(color), IsOverlay(false), Visible(true) {}
			inline TextComponent(const ECS::Entity& entity, const char* text, Ref<Typing::Face> face, float size, const Math::Vector4& color) : Component(entity), Text(text), Face(face), Size(size), Color(color), IsOverlay(false), Visible(true) {}

			inline TextComponent operator=(const TextComponent& other)
			{
				Text = other.Text;
				Face = other.Face;
				Size = other.Size;
				Color = other.Color;
				Visible = other.Visible;
				Component::operator=(other);

				return *this;
			}
		};
	}
}

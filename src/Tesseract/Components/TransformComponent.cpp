#include "TransformComponent.h"

#include "Tesseract/ECS/ECS.h"

namespace Tesseract
{
	namespace Components
	{
		Math::Matrix4 TransformComponent::GetGlobalTransformMatrix() const
		{
			return GetGlobalTransformMatrix(Entity);
		}

		Math::Matrix4 TransformComponent::GetGlobalTransformMatrix(const ECS::Entity& entity)
		{
			Math::Matrix4 transformMatrix = entity.GetComponent<TransformComponent>().GetLocalTransformMatrix();

			Tesseract::ECS::Entity currentParent = entity;

			while (currentParent.HasParent())
			{
				Tesseract::ECS::Entity parent = currentParent.GetParent();

				if (!parent.HasComponent<Tesseract::Components::TransformComponent>())
				{
					currentParent = parent;
					continue;
				}

				Tesseract::Components::TransformComponent& parentTransform = parent.GetComponent<Tesseract::Components::TransformComponent>();

				Math::Matrix4 parentTransformMatrix = parentTransform.GetLocalTransformMatrix();

				transformMatrix = parentTransformMatrix * transformMatrix;
				currentParent = parent;
			}

			return transformMatrix;
		}


		Math::Vector3 TransformComponent::GetGlobalPosition() const
		{
			return GetGlobalTransformMatrix().Column3.XYZ();
		}
	}
}
#pragma once

#include <string>

#include "Tesseract/Reference.h"
#include "Tesseract/Audio/AudioSource.h"

#include "Component.h"

namespace Tesseract
{
	namespace Components
	{
		struct AudioSourceComponent : public Component
		{
			Ref<Audio::AudioSource> AudioSource;

			inline AudioSourceComponent() : Component(), AudioSource(nullptr) {}
			inline AudioSourceComponent(const ECS::Entity& entity) : Component(entity), AudioSource(nullptr) {}
			inline AudioSourceComponent(const ECS::Entity& entity, Audio::AudioSource* audioSource) : Component(entity), AudioSource(audioSource) {}

			inline AudioSourceComponent operator=(const AudioSourceComponent& other)
			{
				AudioSource = other.AudioSource;
				Component::operator=(other);

				return *this;
			}

		};
	}
}

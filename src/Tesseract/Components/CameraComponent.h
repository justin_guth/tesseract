#pragma once

#include <string>

#include "Tesseract/Graphics/Camera.h"
#include "Tesseract/Reference.h"

#include "Component.h"

namespace Tesseract
{
	namespace Components
	{
		struct CameraComponent : public Component
		{
			Ref<Graphics::Camera> Camera;

			inline CameraComponent() : Component(), Camera(nullptr) {}
			inline CameraComponent(const ECS::Entity& entity) : Component(entity), Camera(nullptr) {}
			inline CameraComponent(const ECS::Entity& entity, Graphics::Camera* camera) : Component(entity), Camera(camera) {}

			inline CameraComponent operator=(const CameraComponent& other)
			{
				Camera = other.Camera;
				Component::operator=(other);

				return *this;
			}

		};
	}
}

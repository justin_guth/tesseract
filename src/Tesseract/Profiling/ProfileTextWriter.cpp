#include "ProfileTextWriter.h"

#include "Tesseract/Util/Format.h"

#include <chrono>

namespace Tesseract
{
	namespace Profiling
	{
		const char* ProfileTextWriter::GetExtention() const
		{
			return ".txt";
		}

		void ProfileTextWriter::Write(const Profile& profile) 
		{
			double start = ((double) std::chrono::duration_cast<std::chrono::microseconds>(profile.RelativeStart).count()) / 1000.0;
			double duration = ((double) std::chrono::duration_cast<std::chrono::microseconds>(profile.End - profile.Start).count()) / 1000.0;

			profile.Session->FileStream << Util::Format("Time: {} - '{}' - Duration: {}\n", start, profile.Name, duration);
		}
	}
}
#include "Profiler.h"

#include "ProfilerScopedTimer.h"
#include "Profile.h"

namespace Tesseract
{
	namespace Profiling
	{
		ProfilerScopedTimer::ProfilerScopedTimer(const char* name):
			m_Name(name),
			m_StartTimePoint(std::chrono::high_resolution_clock::now())
		{
		}

		


		ProfilerScopedTimer::~ProfilerScopedTimer()
		{
			std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();

			Profiler::AddProfile(Profile(m_Name, m_StartTimePoint, end));
		}
	}
}
#include "ProfileChromeTraceWriter.h"

#include "Tesseract/Util/Format.h"

#include <fstream>

namespace Tesseract
{
	namespace Profiling
	{
		ProfileChromeTraceWriter::ProfileChromeTraceWriter()
		{
		}
		const char* ProfileChromeTraceWriter::GetExtention() const
		{
			return ".json";
		}
		void ProfileChromeTraceWriter::Write(const Profile& profile) 
		{
			double start = ((double) std::chrono::duration_cast<std::chrono::nanoseconds>(profile.RelativeStart).count()) / 1000.0;
			double duration = ((double) std::chrono::duration_cast<std::chrono::nanoseconds>(profile.End - profile.Start).count()) / 1000.0;

			if (profile.First == false)
			{
				profile.Session->FileStream << ",";
			}

			profile.Session->FileStream << "{";
			profile.Session->FileStream << "\"name\": \"" << profile.Name << "\",";
			profile.Session->FileStream << "\"cat\": \"" << "" << "\",";
			profile.Session->FileStream << "\"ph\": \"" << "X" << "\",";
			profile.Session->FileStream << "\"ts\": " << Util::Format("{}", start) << ",";
			profile.Session->FileStream << "\"dur\": " << Util::Format("{}", duration) << ",";
			profile.Session->FileStream << "\"pid\": " << "0" << ",";
			profile.Session->FileStream << "\"tid\": " << profile.ThreadID << ",";
			profile.Session->FileStream << "\"args\": {" << "" << "}";
			profile.Session->FileStream << "}";
		}

		void ProfileChromeTraceWriter::BeginSession(Profiler::Session& session)
		{
			session.FileStream << "[";
		}

		void ProfileChromeTraceWriter::EndSession(Profiler::Session& session)
		{
			session.FileStream << "]";
		}
	}
}
#include "Profiler.h"

#include "ProfileTextWriter.h"
#include "ProfileChromeTraceWriter.h"
#include "Profile.h"

#include "Tesseract/Logging/Logger.h"
#include "Tesseract/Core/Macros.h"

#include "Tesseract/Reference.h"

#include <fstream>

namespace Tesseract
{
	namespace Profiling
	{
		bool Profiler::s_IsRunning = false;
		bool Profiler::s_IsInitialised = false;
		Ref<Profiler::Session> Profiler::s_Session;

		std::thread* Profiler::s_Thread = nullptr;
		Util::Spinlock Profiler::s_Spinlock;
		Util::SwapQueue<Profile> Profiler::s_Queue;

		ProfileWriter* Profiler::s_Writer = nullptr;
		 bool Profiler::m_First = false;


		std::chrono::high_resolution_clock::time_point Profiler::s_ProfilerStartTime = std::chrono::high_resolution_clock::now();

		Profiler::Session::Session() :
			Name(nullptr),
			FileStream()
		{
		}

		Profiler::Session::Session(const char* name, const char* fileExtention) :
			Name(name),
			FileStream(
				std::fstream(std::string(name) + fileExtention, std::fstream::out)
			)
		{
			Profiler::GetWriter()->BeginSession(*this);
		}

		Profiler::Session::~Session()
		{
			Profiler::GetWriter()->EndSession(*this);

			FileStream.close();
		}

		void Profiler::Init()
		{
			if (s_IsInitialised)
			{
				return;
			}
			
			s_Writer = new ProfileChromeTraceWriter();

			s_IsRunning = true;

			s_Thread = new std::thread(&Profiler::Run);

			s_IsInitialised = true;
		}

		void Profiler::Run()
		{
			using namespace std::chrono_literals;

			while (s_IsRunning || !s_Queue.IsFrontQueueEmpty())
			{
				if (!s_Queue.IsFrontQueueEmpty())
				{
					s_Spinlock.Lock();
					s_Queue.Swap();
					s_Spinlock.Unlock();

					while (s_Queue.BackQueueCount() > 0)
					{
						Profile profile = s_Queue.Pop();

						s_Writer->Write(profile);
					}
				}
				else
				{
					std::this_thread::sleep_for(100ms);
				}
			}
		}

		void Profiler::Shutdown()
		{
			s_IsRunning = false;
			s_Thread->join();
			delete s_Writer;
		}

		void Profiler::AddProfile(Profile&& profile)
		{
			if (s_Session == nullptr)
			{
				return;
			}


			s_Spinlock.Lock();
			profile.SetSession(s_Session);
			profile.SetRelativeStartTime(profile.Start - s_ProfilerStartTime);
			profile.SetFirst(m_First);
			profile.SetThreadID(std::this_thread::get_id());
			s_Queue.Push(profile);
			s_Spinlock.Unlock();

			m_First = false;
		}
	
		void Profiler::BeginSession(const char* sessionName)
		{
			s_Session = Ref<Session>(new Session(sessionName, s_Writer->GetExtention()));
			m_First = true;
		}

		void Profiler::EndSession()
		{
			s_Session.reset();
		}

		ProfileWriter* Profiler::GetWriter()
		{
			return s_Writer;
		}


	}
}
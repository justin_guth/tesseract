#pragma once
#include "ProfileWriter.h"

namespace Tesseract
{
	namespace Profiling
	{
		class ProfileChromeTraceWriter :
			public ProfileWriter
		{
		public:
			
			ProfileChromeTraceWriter();

			virtual const char* GetExtention() const override;
			virtual void Write(const Profile& profile) override;

			virtual void BeginSession(Profiler::Session& session) override;
			virtual void EndSession(Profiler::Session& session) override;

		
		};
	}
}

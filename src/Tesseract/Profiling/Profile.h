#pragma once

#include <chrono>
#include <thread>

#include "Profiler.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
	namespace Profiling
	{
        struct Profile 
        {
            const std::string Name;
            const std::chrono::high_resolution_clock::time_point Start;
            const std::chrono::high_resolution_clock::time_point End;
            std::chrono::high_resolution_clock::duration RelativeStart;
            Ref<Profiler::Session> Session;
            bool First;
            std::thread::id ThreadID;

            Profile(
                const std::string& name,
                const std::chrono::high_resolution_clock::time_point& start,
                const std::chrono::high_resolution_clock::time_point& end
            ) :
                Name(name),
                Start(start),
                RelativeStart(),
                End(end),
                Session(nullptr),
                First(false),
                ThreadID()
            {
            }

            inline void SetSession(Ref<Profiler::Session> session) { Session = session; }
            inline void SetRelativeStartTime(std::chrono::high_resolution_clock::duration relativeStart) { RelativeStart = relativeStart; }
            inline void SetFirst(bool isFirst) { First = isFirst; }
            inline void SetThreadID(std::thread::id&& threadID) { ThreadID = threadID; }

        };
    }
}
#pragma once

#include <chrono>

#include "Tesseract/Util/Format.h"
#include "Tesseract/Core/__VA_OPT__Check.h"

#ifdef __linux__
#define _TESSERACT_FUNCTION_SIGNATURE __PRETTY_FUNCTION__
#else
#define _TESSERACT_FUNCTION_SIGNATURE __FUNCSIG__
#endif


#define _TESSERACT_CONCAT(X,Y) X##Y

#if VA_OPT_SUPPORTED

#define _TESSERACT_PROFILE_SCOPE(name,line, ...) ::Tesseract::Profiling::ProfilerScopedTimer _TESSERACT_CONCAT(timer, line) (name __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_PROFILE_SCOPE(name, ...) _TESSERACT_PROFILE_SCOPE(name, __LINE__ __VA_OPT__(,) __VA_ARGS__) 

#else

#define _TESSERACT_PROFILE_SCOPE(name,line, ...) ::Tesseract::Profiling::ProfilerScopedTimer _TESSERACT_CONCAT(timer, line) (name , __VA_ARGS__)
#define TESSERACT_PROFILE_SCOPE(name, ...) _TESSERACT_PROFILE_SCOPE(name, __LINE__ , __VA_ARGS__) 

#endif


#define TESSERACT_PROFILE_FUNCTION() TESSERACT_PROFILE_SCOPE(_TESSERACT_FUNCTION_SIGNATURE) 

namespace Tesseract
{
	namespace Profiling
	{
		class ProfilerScopedTimer
		{
		public:

			ProfilerScopedTimer(const char* name);

			template <typename ...T_Param>
			ProfilerScopedTimer(const char* format, T_Param&& ... params);

			~ProfilerScopedTimer();

		private:

			const std::string m_Name;
			std::chrono::high_resolution_clock::time_point m_StartTimePoint;
		};

		template <typename ...T_Param>
		ProfilerScopedTimer::ProfilerScopedTimer(const char* format, T_Param&& ... params):
			m_Name(Util::Format(format, std::forward<T_Param>(params)...)),
			m_StartTimePoint(std::chrono::high_resolution_clock::now())
		{
		}
	}
}
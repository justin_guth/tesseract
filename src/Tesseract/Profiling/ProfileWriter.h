#pragma once

#include <fstream>
#include "Profile.h"

namespace Tesseract
{
	namespace Profiling
	{
		class ProfileWriter
		{
		public:

			virtual const char* GetExtention() const = 0;
			virtual void Write(const Profile& profile) = 0;

			virtual void BeginSession(Profiler::Session& session) {}
			virtual void EndSession(Profiler::Session& session) {}
		};
	}
}

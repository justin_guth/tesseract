#pragma once
#include "ProfileWriter.h"

namespace Tesseract
{
	namespace Profiling
	{
		class ProfileTextWriter :
			public ProfileWriter
		{
		public:

			virtual const char* GetExtention() const override;
			virtual void Write(const Profile& profile) override;
		};

	}
}
#pragma once

#include "Tesseract/Util/Spinlock.h"
#include "Tesseract/Util/SwapQueue.h"


#include <thread>
#include <fstream>

#include "Tesseract/Reference.h"

#define TESSERACT_PROFILER_BEGIN_SESSION(name) ::Tesseract::Profiling::Profiler::BeginSession(name)
#define TESSERACT_PROFILER_END_SESSION() ::Tesseract::Profiling::Profiler::EndSession()



namespace Tesseract
{
	namespace Profiling
	{
		struct Profile;
		class ProfileWriter;

		class Profiler
		{
		public:

			struct Session
			{
				const char* Name;
				std::fstream FileStream;

				Session();
				Session(const char* name, const char* fileExtention);

				~Session();
			};

		public:

			static void Init();
			static void Shutdown();
			static void AddProfile(Profile&& profile);

			static void BeginSession(const char* sessionName);
			static void EndSession();

			static ProfileWriter* GetWriter();

		private:

			static void Run();

		private:

			static Util::Spinlock s_Spinlock;
			static std::thread* s_Thread;
			static Util::SwapQueue<Profile> s_Queue;
			static bool s_IsInitialised;
			static bool s_IsRunning;
			static std::chrono::high_resolution_clock::time_point s_ProfilerStartTime;
			static Ref<Session> s_Session;
			static ProfileWriter* s_Writer;

			static bool m_First;
		};
	}
}
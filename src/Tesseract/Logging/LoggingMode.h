#pragma once

/** \file LoggingMode.h
 * Defines the LoggingMode enum class.
 */


namespace Tesseract
{
    namespace Logging
    {
        /**
        * Defines different log severities.
        */
        enum class LoggingMode {

            Trace = 0,
            Info,
            Success,
            Warn,
            Error,
            Critical
        };
    }
} 

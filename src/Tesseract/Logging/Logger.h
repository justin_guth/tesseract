#pragma once

/**
* @file Logger.h
* 
* Defines the Logger class.
*/

#include <string>
#include <thread>

#include "LoggingMode.h"
#include "LogInstruction.h"

#include "Tesseract/Util/SwapQueue.h"
#include "Tesseract/Util/Format.h"
#include "Tesseract/Util/Spinlock.h"

#include "Tesseract/Core/__VA_OPT__Check.h"

/**
* @def TESSERACT_LOGGER_QUEUE_LIMIT
* Defines the maximum number of messages that will be queued at one time.
*/
#define TESSERACT_LOGGER_QUEUE_LIMIT 2000




#if VA_OPT_SUPPORTED

#define TESSERACT_CORE_TRACE(format, ...)      ::Tesseract::Logging::Logger::GetCoreLogger().Trace(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CORE_INFO(format, ...)       ::Tesseract::Logging::Logger::GetCoreLogger().Info(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CORE_SUCCESS(format, ...)    ::Tesseract::Logging::Logger::GetCoreLogger().Success(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CORE_WARN(format, ...)       ::Tesseract::Logging::Logger::GetCoreLogger().Warn(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CORE_ERROR(format, ...)      ::Tesseract::Logging::Logger::GetCoreLogger().Error(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CORE_CRITICAL(format, ...)   ::Tesseract::Logging::Logger::GetCoreLogger().Critical(format __VA_OPT__(,) __VA_ARGS__)

#define TESSERACT_CLIENT_TRACE(format, ...)    ::Tesseract::Logging::Logger::GetClientLogger().Trace(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CLIENT_INFO(format, ...)     ::Tesseract::Logging::Logger::GetClientLogger().Info(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CLIENT_SUCCESS(format, ...)  ::Tesseract::Logging::Logger::GetClientLogger().Success(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CLIENT_WARN(format, ...)     ::Tesseract::Logging::Logger::GetClientLogger().Warn(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CLIENT_ERROR(format, ...)    ::Tesseract::Logging::Logger::GetClientLogger().Error(format __VA_OPT__(,) __VA_ARGS__)
#define TESSERACT_CLIENT_CRITICAL(format, ...) ::Tesseract::Logging::Logger::GetClientLogger().Critical(format __VA_OPT__(,) __VA_ARGS__)

#else

#define TESSERACT_CORE_TRACE(format, ...)      ::Tesseract::Logging::Logger::GetCoreLogger().Trace(format , __VA_ARGS__)
#define TESSERACT_CORE_INFO(format, ...)       ::Tesseract::Logging::Logger::GetCoreLogger().Info(format , __VA_ARGS__)
#define TESSERACT_CORE_SUCCESS(format, ...)    ::Tesseract::Logging::Logger::GetCoreLogger().Success(format , __VA_ARGS__)
#define TESSERACT_CORE_WARN(format, ...)       ::Tesseract::Logging::Logger::GetCoreLogger().Warn(format , __VA_ARGS__)
#define TESSERACT_CORE_ERROR(format, ...)      ::Tesseract::Logging::Logger::GetCoreLogger().Error(format , __VA_ARGS__)
#define TESSERACT_CORE_CRITICAL(format, ...)   ::Tesseract::Logging::Logger::GetCoreLogger().Critical(format , __VA_ARGS__)

#define TESSERACT_CLIENT_TRACE(format, ...)    ::Tesseract::Logging::Logger::GetClientLogger().Trace(format , __VA_ARGS__)
#define TESSERACT_CLIENT_INFO(format, ...)     ::Tesseract::Logging::Logger::GetClientLogger().Info(format , __VA_ARGS__)
#define TESSERACT_CLIENT_SUCCESS(format, ...)  ::Tesseract::Logging::Logger::GetClientLogger().Success(format , __VA_ARGS__)
#define TESSERACT_CLIENT_WARN(format, ...)     ::Tesseract::Logging::Logger::GetClientLogger().Warn(format , __VA_ARGS__)
#define TESSERACT_CLIENT_ERROR(format, ...)    ::Tesseract::Logging::Logger::GetClientLogger().Error(format , __VA_ARGS__)
#define TESSERACT_CLIENT_CRITICAL(format, ...) ::Tesseract::Logging::Logger::GetClientLogger().Critical(format , __VA_ARGS__)

#endif

namespace Tesseract
{
	namespace Logging
	{
		/**
		* Logger is an asynchronous singleton logging system. 
		* Upon initialisation it invokes a worker thread that processes a logging queue.
		* 
		* If the queue contains more than TESSERACT_LOGGER_QUEUE_LIMIT messages any incoming messages are discarded.
		*/
		class Logger
		{
		public:

			/**
			* Default constructor for a Logger.
			* Sets the name to "Logger" and sets the default mode to ::LoggingMode::Info.
			*/
			Logger();
			
			/**
			* Constructor for a Logger.
			* Sets the name to a given @p name and sets the default mode to ::LoggingMode::Info.
			*
			* @param[in] name The name
			*/
			Logger(const char* name);
			
			/**
			* Constructor for a Logger.
			* Sets the name to a given @p name and sets the mode to @p mode.
			*
			* @param[in] name The name
			* @param[in] mode The desired LoggingMode
			*/
			Logger(const char* name, LoggingMode mode);

			/**
			* Queue a log message @p text with a given @p level.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			* 
			* @param[in] text  The message to be queued
			* @param[in] level The level at which the message is to be logged
			*/
			void Log(const std::string text, LoggingMode level) const;

			/**
			* Queue a log message @p text with a the level ::LoggingMode::Trace.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			* 
			* @param[in] text The message text to be queued
			*/
			inline void Trace(const char* text) const { this->Log(text, LoggingMode::Trace); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Info.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Info(const char* text) const { this->Log(text, LoggingMode::Info); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Success.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Success(const char* text) const { this->Log(text, LoggingMode::Success); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Warn.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Warn(const char* text) const { this->Log(text, LoggingMode::Warn); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Error.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Error(const char* text) const { this->Log(text, LoggingMode::Error); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Critical.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Critical(const char* text) const { this->Log(text, LoggingMode::Critical); };

			/**
			* Queue a log message @p text with a the level ::LoggingMode::Trace.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Trace(const std::string& text) const { this->Log(text, LoggingMode::Trace); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Info.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Info(const std::string& text) const { this->Log(text, LoggingMode::Info); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Success.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Success(const std::string& text) const { this->Log(text, LoggingMode::Success); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Warn.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Warn(const std::string& text) const { this->Log(text, LoggingMode::Warn); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Error.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Error(const std::string& text) const { this->Log(text, LoggingMode::Error); };
			
			/**
			* Queue a log message @p text with a the level ::LoggingMode::Critical.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] text The message text to be queued
			*/
			inline void Critical(const std::string& text) const { this->Log(text, LoggingMode::Critical); };

			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Trace.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Trace(const char* format, T_Param&& ... params) const;
			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Info.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Info(const char* format, T_Param&& ... params) const;
			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Success.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Success(const char* format, T_Param&& ... params) const;
			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Warn.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Warn(const char* format, T_Param&& ... params) const;
			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Error.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Error(const char* format, T_Param&& ... params) const;
			
			/**
			* Queue a formatted log message by specifying a @p format and the parameters @p params.
			* The log level is set to ::LoggingMode::Critical.
			* If the logging mode of the Logger is higher than the given level the message is discarded.
			*
			* @param[in] format The format of message text to be queued
			* @param[in] params The parameters to be filled into the @p format
			*/
			template <typename ...T_Param>
			void Critical(const char* format, T_Param&& ... params) const;

		public:

			/**
			* Initialises the logging system.
			* Creates a Logger for the tesseract and one for the client.
			* Starts a worker thread that processes the logging queue.
			*/
			static void Init();
			
			/**
			* Shuts down the logging system.
			* Ends the worker thread that processes the logging queue and blocks until the queue is emptied.
			*/
			static void Shutdown();

			/**
			* Retrieves the default core logger. This logger is reserved for tesseract components.
			* 
			* @return A reference to the core logger
			*/
			static inline const Logger& GetCoreLogger() { return *Logger::s_CoreLogger; };
			
			/**
			* Retrieves the default client logger. This logger is reserved for the client.
			*
			* @return A reference to the client logger
			*/
			static inline const Logger& GetClientLogger() { return *Logger::s_ClientLogger; };

			/**
			* Sets the core logger logging @p mode. Any log messages logged with a lower level are discarded.
			*
			* @param[in] mode The mode to be set
			*/
			static void SetCoreMode(LoggingMode mode);
			
			/**
			* Sets the client logger logging @p mode. Any log messages logged with a lower level are discarded.
			*
			* @param[in] mode The mode to be set
			*/
			static void SetClientMode(LoggingMode mode);

		private:

			/**
			* Starts the queue worker logic.
			*/
			static void Run();

		private:

			const char* m_Name;
			LoggingMode m_Mode;
			
			static Util::Spinlock s_Spinlock;
			static std::thread* s_Thread;
			static Util::SwapQueue<LogInstruction> s_Queue;
			static bool s_IsInitialised;
			static bool s_IsRunning;
			static Logger* s_CoreLogger;
			static Logger* s_ClientLogger;
		};

		//===== Method implementations =====//
		
		template<typename ...T_Param>
		inline void Logger::Trace(const char* format, T_Param && ...params) const
		{
			Trace(Util::Format(format, std::forward<T_Param>(params)...));
		}

		template<typename ...T_Param>
		inline void Logger::Info(const char* format, T_Param && ...params) const
		{
			Info(Util::Format(format, std::forward<T_Param>(params)...));
		}

		template<typename ...T_Param>
		inline void Logger::Success(const char* format, T_Param && ...params) const
		{
			Success(Util::Format(format, std::forward<T_Param>(params)...));
		}

		template<typename ...T_Param>
		inline void Logger::Warn(const char* format, T_Param && ...params) const
		{
			Warn(Util::Format(format, std::forward<T_Param>(params)...));
		}

		template<typename ...T_Param>
		inline void Logger::Error(const char* format, T_Param && ...params) const
		{
			Error(Util::Format(format, std::forward<T_Param>(params)...));
		}

		template<typename ...T_Param>
		inline void Logger::Critical(const char* format, T_Param && ...params) const
		{
			Critical(Util::Format(format, std::forward<T_Param>(params)...));
		}
	}
} 

#pragma once
#include "LoggingMode.h"

#include <string>
#include <ctime>

namespace Tesseract
{
	namespace Logging
	{
		class Logger;

		struct LogInstruction
		{
			std::time_t Time;
			const Logger& IssuingLogger;
			std::string Message;
			LoggingMode Mode;
		};
	}
}

#include "Logger.h"
#include "ColorCodes.h"
#include <iostream>
#include <ctime>
#include <chrono>

namespace Tesseract
{
	namespace Logging
	{
		bool Logger::s_IsInitialised = false;
		bool Logger::s_IsRunning = true;
		Logger* Logger::s_CoreLogger = nullptr;
		Logger* Logger::s_ClientLogger = nullptr;

		Util::SwapQueue<LogInstruction> Logger::s_Queue;
		Util::Spinlock Logger::s_Spinlock;
		std::thread* Logger::s_Thread = nullptr;

		Logger::Logger() : m_Name("Logger"), m_Mode(LoggingMode::Info) {}
		Logger::Logger(const char* name) : m_Name(name), m_Mode(LoggingMode::Info) {}
		Logger::Logger(const char* name, LoggingMode mode) : m_Name(name), m_Mode(mode) {}

		void Logger::Init()
		{
			if (Logger::s_IsInitialised)
			{
				return;
			}

			Logger::s_ClientLogger = new Logger("Client", LoggingMode::Trace);
			Logger::s_CoreLogger = new Logger("Tesseract", LoggingMode::Trace);

			s_IsInitialised = true;

			s_Thread = new std::thread(&Logger::Run);

			GetCoreLogger().Info("Started logging thread.");
		}

		void Logger::Shutdown()
		{
			GetCoreLogger().Info("Shutting down logging thread.");
			s_IsRunning = false;
			s_Thread->join();
			delete s_Thread;

			delete Logger::s_ClientLogger;
			delete Logger::s_CoreLogger;
		}

		void Logger::SetCoreMode(LoggingMode mode)
		{
			Logger::s_CoreLogger->m_Mode = mode;
		}

		void Logger::SetClientMode(LoggingMode mode)
		{
			Logger::s_ClientLogger->m_Mode = mode;
		}

		void Logger::Run()
		{
			using namespace std::chrono_literals;

			while (s_IsRunning || !s_Queue.IsFrontQueueEmpty())
			{
				if (!s_Queue.IsFrontQueueEmpty())
				{
					s_Spinlock.Lock();
					s_Queue.Swap();
					s_Spinlock.Unlock();

					while (s_Queue.BackQueueCount() > 0)
					{
						LogInstruction instruction = s_Queue.Pop();

						char timestamp[9];
						#ifdef __linux__
						// FIXME: use std::strftime
						char* fullTimeStamp;
						fullTimeStamp = ctime(&instruction.Time);
						#else
						char fullTimeStamp[26];
						errno_t error = ctime_s(fullTimeStamp, 26, &instruction.Time);
						#endif

						for (int i = 0; i < 8; i++)
						{
							timestamp[i] = fullTimeStamp[i + 11];
						}
						timestamp[8] = 0x00;

						const char* colorCode = nullptr;
						const char* colorBackgroundCode = nullptr;

						switch (instruction.Mode)
						{

							case LoggingMode::Trace:
								colorCode = TESSERACT_COLOR_WHITE;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_BLACK;
								break;
							case LoggingMode::Info:
								colorCode = TESSERACT_COLOR_WHITE;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_BLACK;
								break;
							case LoggingMode::Success:
								colorCode = TESSERACT_COLOR_GREEN;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_BLACK;
								break;
							case LoggingMode::Warn:
								colorCode = TESSERACT_COLOR_YELLOW;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_BLACK;
								break;
							case LoggingMode::Error:
								colorCode = TESSERACT_COLOR_RED;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_BLACK;
								break;
							case LoggingMode::Critical:
								colorCode = TESSERACT_COLOR_BLACK;
								colorBackgroundCode = TESSERACT_COLOR_BACKGROUND_RED;
								break;
						}

						// Remove backround for anything non critical. Easier on the eyes on some consoles
						if (instruction.Mode == LoggingMode::Critical)
						{
							std::cout << colorCode << colorBackgroundCode << "[" << instruction.IssuingLogger.m_Name << " @ " << timestamp << "] " << instruction.Message << TESSERACT_COLOR_RESET
								<< "\n";
						}
						else
						{
							std::cout << colorCode << "[" << instruction.IssuingLogger.m_Name << " @ " << timestamp << "] " << instruction.Message << TESSERACT_COLOR_RESET
								<< "\n";
						}
					}
				}
				else
				{
					std::this_thread::sleep_for(10ms);
				}
			}
		}

		void Logger::Log(const std::string text, LoggingMode level) const
		{
			if (level < this->m_Mode)
			{
				return;
			}

			std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
			std::time_t now_time = std::chrono::system_clock::to_time_t(now);

			LogInstruction instruction = {
				now_time,
				(*this),
				text,
				level
			};

			s_Spinlock.Lock();

			if (s_Queue.FrontQueueCount() < TESSERACT_LOGGER_QUEUE_LIMIT)
			{
				s_Queue.Push(instruction);
			}

			s_Spinlock.Unlock();
		}
	}
}

#pragma once

#include "DataType.h"

#include <vector>

namespace Tesseract
{
    class IHasParameters
    {
    public:
    
        struct Parameter
        {
            std::string Label;
            std::string Identifier;
            DataType AssignedDataType;
            void* MemoryBaseAddress;
        };

    public:

        virtual const std::vector<Parameter> GetParameters() = 0;
        virtual void OnParameterChange(const Parameter& parameter) {}

    };

} // namespace Tesseract


#include "ECS.h"

#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/TransformComponent.h"

namespace Tesseract
{
	namespace ECS
	{
		const Entity Entity::Null;
		uint64_t Registry::s_EntitiesCreated = 0;




		Entity Registry::CreateEntity()
		{
			entity_id entityNumber = s_EntitiesCreated++;


			for (std::function<void()> resizer : m_PushBehaviours)
			{
				resizer();
			}

			m_IndexToEntityArray.push_back(entityNumber);
			m_EntityToIndexMap.Set(entityNumber, m_EntitesCount);

			m_EntityParents.push_back(Entity::Null.GetID());
			m_EntityChildLists.push_back(Util::LinkedList<entity_id>());

			m_EntitesCount++;

			Entity result(entityNumber, this);

			// Check if this is adequate here. maybe add onentitycreate hooks

			result.AddComponent<Components::TransformComponent>();
			result.AddComponent<Components::TagComponent>();

			return result;
		}

		const Entity Registry::FindEntityChild(const Entity& entity, const std::string& tag)
		{
			uint32_t index = m_EntityToIndexMap.At(entity.GetID());
			for (entity_id id : m_EntityChildLists[index])
			{
				if (!HasEntityComponent<Components::TagComponent>(id))
				{
					continue;
				}

				if (GetComponentFromEntity<Components::TagComponent>(id).Tag == tag)
				{
					Entity result(id, this);
					return result;
				}
			}

			return Entity::Null;
		}

		const Entity Registry::FindEntity(const std::string& tag)
		{
			for (const auto&[entity, tagComponent] : Filter<Components::TagComponent>())
			{
				if (tagComponent.Tag == tag)
				{
					return entity;
				}
			}

			return Entity::Null;
		}
	}
}
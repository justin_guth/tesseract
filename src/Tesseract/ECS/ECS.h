#pragma once

#include "Tesseract/Core/Macros.h"

#include "Tesseract/Util/TypeHash.h"
#include "Tesseract/Util/NSlotHashTable.h"
#include "Tesseract/Util/LinkedList.h"
#include "Tesseract/Memory/DynamicSlotAllocator.h"

#include <vector>
#include <functional>

#include <tuple>


typedef uint64_t entity_id;

namespace Tesseract
{
	namespace ECS
	{
		class Registry;

		class Entity
		{
			friend class Registry;
		public:

			inline Entity(): m_EntityID(0xFFFFFFFFFFFFFFFFu), m_Registry(nullptr) {}
			inline Entity(entity_id id, Registry* registry) : m_EntityID(id), m_Registry(registry) {}
			inline Entity(const Entity& other) : m_EntityID(other.m_EntityID), m_Registry(other.m_Registry) {}

			inline bool HasParent() const;
			inline Entity GetParent() const;
			inline const Util::LinkedList<entity_id>& GetChildrenIDs() const;
			inline const Util::LinkedList<Entity> GetChildren() const;
			inline const bool HasChildren() const;
			inline const Entity FindChild(const std::string& tag) const;
			inline const Entity Find(const std::string& tag) const;


			inline Registry* GetRegistry() const;

			// static Entity Create();

			template<typename T_Type, typename ... T_Params>
			T_Type& AddComponent(T_Params... params);

			template<typename T_Type>
			void RemoveComponent();

			template<typename T_Type>
			bool HasComponent() const;

			template<typename ... T_Types>
			bool HasComponents() const;

			template<typename T_Type>
			T_Type& GetComponent() const;

			template<typename ... T_Types>
			std::tuple<T_Types&...> GetComponents() const;


			inline entity_id GetID() const { return m_EntityID; }

			inline void Destroy(bool recursive = false);
			inline void DestroyLater(bool recursive = false);

			inline Entity CreateChild();

			inline void AddChild(const entity_id id);
			inline void AddChild(const Entity& entity);

			inline bool operator==(const Entity& other);

		public:

			const static Entity Null;

		private:

			entity_id m_EntityID;
			Registry* m_Registry;
		};
	}
}


namespace Tesseract
{
	namespace ECS
	{
		class Registry;

		template<typename T_Type>
		class Comp
		{
		public:

			inline Comp(): m_Entity(Entity::Null) {};
			inline Comp(Entity& entity): m_Entity(entity) {};
			inline Comp(T_Type& component): m_Entity(component.Entity) {};

			inline T_Type* operator->()
			{
				return &(m_Entity.GetComponent<T_Type>());
			}

			inline const T_Type* operator->() const
			{
				return &(m_Entity.GetComponent<T_Type>());
			}

			inline T_Type& operator*()
			{
				return m_Entity.GetComponent<T_Type>();
			}

		private:

			Entity m_Entity;
		};
	}
}

namespace Tesseract
{
	namespace ECS
	{
		class Registry
		{
		public:

			template<typename ... T_Types>
			class RegistryFilter
			{
			public:

				class Iterator
				{
				public:

					Iterator(uint32_t index, uint32_t end, Registry* registry): m_CurrentIndex(index), m_End(end), m_Registry(registry)
					{
						if (m_CurrentIndex < m_End)
						{
							entity_id entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
							if (!m_Registry->HasEntityComponents<T_Types...>(entity))
							{
								operator++();
							}
						}
					}

					std::tuple<Entity, T_Types& ...> operator*() const
					{
						entity_id currentEntity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];

						return std::tuple_cat(
							std::tuple<Entity>(Entity(currentEntity, m_Registry)),
							m_Registry->GetComponentsFromEntity<T_Types ... >(currentEntity)
						);
					}


					Iterator& operator++()
					{
						m_CurrentIndex++;
						entity_id entity;

						if (m_CurrentIndex < m_End)
						{
							entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
						}

						while (m_CurrentIndex < m_End && !m_Registry->HasEntityComponents<T_Types...>(entity))
						{
							m_CurrentIndex++;

							if (m_CurrentIndex == m_End)
							{
								break;
							}

							entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
						}

						return *this;
					}

					Iterator operator++(int)
					{
						Iterator tmp = *this;

						++(*this);

						return tmp;
					}

					friend bool operator== (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex == b.m_CurrentIndex; };
					friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex != b.m_CurrentIndex; };

				private:

					uint32_t m_End;
					uint32_t m_CurrentIndex = 0;
					Registry* m_Registry;
				};

				RegistryFilter(Registry* registry):
					m_Registry(registry)
				{
				}

				Iterator begin() { return Iterator(0, m_Registry->m_EntitesCount, m_Registry); }

				Iterator end() { return Iterator(m_Registry->m_EntitesCount, m_Registry->m_EntitesCount, m_Registry); }

			private:

				Registry* m_Registry;
			};

			class RootFilter
			{
			public:

				class Iterator
				{
				public:

					Iterator(uint32_t index, uint32_t end, Registry* registry): m_CurrentIndex(index), m_End(end), m_Registry(registry)
					{
						if (m_CurrentIndex < m_End)
						{
							entity_id entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
							if (m_Registry->HasEntityParent(entity))
							{
								operator++();
							}
						}
					}

					Entity operator*() const
					{
						entity_id currentEntity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];

						return Entity(currentEntity, m_Registry);
					}

					Iterator& operator++()
					{
						m_CurrentIndex++;
						entity_id entity;

						if (m_CurrentIndex < m_End)
						{
							entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
						}

						while (m_CurrentIndex < m_End && m_Registry->HasEntityParent(entity))
						{
							m_CurrentIndex++;

							if (m_CurrentIndex == m_End)
							{
								break;
							}

							entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
						}

						return *this;
					}

					Iterator operator++(int)
					{
						Iterator tmp = *this;

						++(*this);

						return tmp;
					}

					friend bool operator== (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex == b.m_CurrentIndex; };
					friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex != b.m_CurrentIndex; };

				private:

					uint32_t m_End;
					uint32_t m_CurrentIndex = 0;
					Registry* m_Registry;
				};

				RootFilter(Registry* registry):
					m_Registry(registry)
				{
				}

				Iterator begin() { return Iterator(0, m_Registry->m_EntitesCount, m_Registry); }

				Iterator end() { return Iterator(m_Registry->m_EntitesCount, m_Registry->m_EntitesCount, m_Registry); }

			private:

				Registry* m_Registry;
			};

			class AllFilter
			{
			public:

				class Iterator
				{
				public:

					Iterator(uint32_t index, uint32_t end, Registry* registry): m_CurrentIndex(index), m_End(end), m_Registry(registry)
					{
					}

					Entity operator*() const
					{
						entity_id currentEntity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];

						return Entity(currentEntity, m_Registry);
					}

					Iterator& operator++()
					{
						m_CurrentIndex++;
						entity_id entity;

						if (m_CurrentIndex < m_End)
						{
							entity = m_Registry->m_IndexToEntityArray[m_CurrentIndex];
						}

						return *this;
					}

					Iterator operator++(int)
					{
						Iterator tmp = *this;

						++(*this);

						return tmp;
					}

					friend bool operator== (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex == b.m_CurrentIndex; };
					friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_CurrentIndex != b.m_CurrentIndex; };

				private:

					uint32_t m_End;
					uint32_t m_CurrentIndex = 0;
					Registry* m_Registry;
				};

				AllFilter(Registry* registry):
					m_Registry(registry)
				{
				}

				Iterator begin() { return Iterator(0, m_Registry->m_EntitesCount, m_Registry); }

				Iterator end() { return Iterator(m_Registry->m_EntitesCount, m_Registry->m_EntitesCount, m_Registry); }

			private:

				Registry* m_Registry;
			};

		private:

			template<typename T_Type>
			struct Entry
			{
				bool Valid;
				T_Type Data;

				Entry(): Valid(false) {}
			};

		public:

			inline ~Registry();

			Registry() = default;
			Registry(const Registry& other) = delete; // Temporary solution as copying would break the registry

			Entity CreateEntity();
			Entity CreateChild(const Entity& entity);

			void AddChild(const Entity& entity, const entity_id id);
			void AddChild(const Entity& entity, const Entity& child);


			inline Entity FormEntity(entity_id id);

			void DestroyQueuedEnties();
			void DestroyEntity(const Entity& entity, bool recursive = false);
			void DestroyEntityLater(const Entity& entity, bool recursive = false);

			inline bool HasEntityParent(const Entity& entity) const;
			inline bool HasEntityParent(const entity_id entity) const;
			void SetEntityParent(const Entity& entity, const Entity& parent);
			inline Entity GetEntityParent(const Entity& entity);

			inline const bool HasEntityChildren(const Entity& entity) const;
			inline const Util::LinkedList<Entity> GetEntityChildren(const Entity& entity);
			inline const Util::LinkedList<entity_id>& GetEntityChildrenIDs(const Entity& entity) const;
			const Entity FindEntityChild(const Entity& entity, const std::string& tag);
			const Entity FindEntity(const std::string& tag);

			template<typename T_Type, typename ... T_Params >
			T_Type& AddComponentToEntity(const Entity& entity, T_Params...params);

			template<typename T_Type >
			void RemoveComponentFromEntity(const Entity& entity);

			template<typename T_Type >
			bool HasEntityComponent(const Entity& entity) const;

			template<typename T_Type >
			T_Type& GetComponentFromEntity(const Entity& entity);

			template<typename ... T_Types>
			std::tuple<T_Types&...> GetComponentsFromEntity(const Entity& entity);





			template<typename T_Type, typename ... T_Params >
			T_Type& AddComponentToEntity(const entity_id entity, T_Params...params);

			template<typename T_Type >
			void RemoveComponentFromEntity(const entity_id entity);

			template<typename T_Type >
			bool HasEntityComponent(const entity_id entity) const;

			template<typename T_Type >
			bool HasEntityComponents(const entity_id entity) const;

			template<typename T_Type, typename T_NextType, typename ... T_RestTypes >
			bool HasEntityComponents(const entity_id entity) const;

			template<typename T_Type >
			T_Type& GetComponentFromEntity(const entity_id entity);

			template<typename T_Type, typename T_NextType, typename ... T_RestTypes>
			std::tuple<T_Type&, T_NextType&, T_RestTypes&...> GetComponentsFromEntity(const entity_id entity);

			template<typename T_Type>
			std::tuple<T_Type&> GetComponentsFromEntity(const entity_id entity);


			template<typename ... T_Types>
			RegistryFilter<T_Types...> Filter();

			RootFilter RootEntites();
			AllFilter AllEntites();



		private:

			std::vector<entity_id> m_IndexToEntityArray;
			Util::NSlotHashTable<void*, 4> m_ComponentArrays;
			Util::NSlotHashTable<uint32_t, 4> m_EntityToIndexMap;

			std::vector<std::function<void()>> m_PushBehaviours;
			std::vector<std::function<void()>> m_DeleteBehaviours;
			std::vector<std::function<void(uint32_t)>> m_PopBehaviours;

			std::vector<entity_id> m_EntityParents;
			std::vector<Util::LinkedList<entity_id>> m_EntityChildLists;

			uint32_t m_EntitesCount = 0;
			static uint64_t s_EntitiesCreated;

			std::vector<std::tuple<Entity, bool>> m_DestructionQueue;
		};

	}
}
namespace Tesseract
{
	namespace ECS
	{
		template<typename T_Type, typename ... T_Params>
		inline T_Type& Registry::AddComponentToEntity(const entity_id entity, T_Params... params)
		{
			// Show error if component already exists for entity
			if (HasEntityComponent<T_Type>(entity))
			{
				TESSERACT_CORE_ERROR("Entity already has component of type {}!", typeid(T_Type).name());
				return GetComponentFromEntity<T_Type>(entity);
			}

			type_hash typeHash = Util::TypeHash::Get<T_Type>();

			// Check if component list already exists. If not create one, set resize behaviours
			if (!m_ComponentArrays.Contains(typeHash))
			{
				m_ComponentArrays.Set(typeHash, (void*) new std::vector<Entry<T_Type>>);

				((std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash))->resize(m_EntitesCount);

				m_PushBehaviours.push_back(
					[typeHash, this]()
					{
						std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);
						componentArray->push_back(Entry<T_Type>());
					}
				);

				m_PopBehaviours.push_back(
					[typeHash, this](uint32_t index)
					{
						std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);
						(*componentArray)[index] = (*componentArray)[componentArray->size() - 1];
						componentArray->pop_back();
					}
				);

				m_DeleteBehaviours.push_back(
					[typeHash, this]()
					{
						std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);
						delete componentArray;
					}
				);
			}


			uint32_t index = m_EntityToIndexMap.At(entity);

			std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);

			(*componentArray)[index].Valid = true;
			(*componentArray)[index].Data = T_Type(Entity(entity, this), params...);

			return (*componentArray)[index].Data;
		}

		template<typename T_Type>
		inline void Registry::RemoveComponentFromEntity(const entity_id entity)
		{
			// Show error if component does not exist for entity
			if (!HasEntityComponent<T_Type>(entity))
			{
				TESSERACT_CORE_ERROR("Entity has no component of type {}!", typeid(T_Type).name());
				return;
			}

			// If component exists it mus be allocated and present within a entity component map

			type_hash typeHash = Util::TypeHash::Get<T_Type>();

			uint32_t index = m_EntityToIndexMap.At(entity);

			std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);
			(*componentArray)[index].Valid = false;
		}

		template<typename T_Type>
		inline bool Registry::HasEntityComponent(const entity_id entity) const
		{
			// TODO: Add asserts such that entity is not null


			type_hash typeHash = Util::TypeHash::Get<T_Type>();

			// Check if an entity index for this entity exists. If not return false
			if (!m_EntityToIndexMap.Contains(entity))
			{
				return false;
			}

			// Check if an entity component array for this T_Type exists. If not return false
			if (!m_ComponentArrays.Contains(typeHash))
			{
				return false;
			}

			// Retrieve Entity component array
			std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);

			uint32_t index = m_EntityToIndexMap.At(entity);

			// Return wether the type is present in the entity component array
			return (*componentArray)[index].Valid;
		}

		template<typename T_Type >
		inline bool Registry::HasEntityComponents(const entity_id entity) const
		{
			return HasEntityComponent<T_Type>(entity);
		}

		template<typename T_Type, typename T_NextType, typename ... T_RestTypes >
		inline bool Registry::HasEntityComponents(const entity_id entity) const
		{
			return HasEntityComponent<T_Type>(entity) && HasEntityComponents<T_NextType, T_RestTypes...>(entity);
		}



		template<typename T_Type>
		inline T_Type& Registry::GetComponentFromEntity(const entity_id entity)
		{
			// Show error if component does not exist for entity
			if (!HasEntityComponent<T_Type>(entity))
			{
				TESSERACT_CORE_ERROR("Entity has no component of type {}!", typeid(T_Type).name());
			}

			type_hash typeHash = Util::TypeHash::Get<T_Type>();

			// Retrieve Entity component array
			std::vector<Entry<T_Type>>* componentArray = (std::vector<Entry<T_Type>>*) m_ComponentArrays.At(typeHash);

			uint32_t index = m_EntityToIndexMap.At(entity);

			// Return component for the entity
			return (*componentArray)[index].Data;
		}

		template<typename T_Type, typename T_NextType, typename ... T_RestTypes>
		inline std::tuple<T_Type&, T_NextType&, T_RestTypes&...> Registry::GetComponentsFromEntity(const entity_id entity)
		{
			T_Type& component = GetComponentFromEntity<T_Type>(entity);
			return std::tuple_cat(std::tuple<T_Type&>(component), GetComponentsFromEntity<T_NextType, T_RestTypes...>(entity));
		}

		template<typename T_Type>
		inline std::tuple<T_Type&> Registry::GetComponentsFromEntity(const entity_id entity)
		{
			return std::tuple<T_Type&>(GetComponentFromEntity<T_Type>(entity));
		}


		template<typename T_Type, typename ... T_Params>
		inline T_Type& Registry::AddComponentToEntity(const Entity& entity, T_Params... params)
		{
			return AddComponentToEntity<T_Type>(entity.GetID(), params...);
		}

		template<typename T_Type>
		inline void Registry::RemoveComponentFromEntity(const Entity& entity)
		{
			RemoveComponentFromEntity<T_Type>(entity.GetID());
		}

		template<typename T_Type>
		inline bool Registry::HasEntityComponent(const Entity& entity) const
		{
			return HasEntityComponent<T_Type>(entity.GetID());
		}

		template<typename T_Type>
		inline T_Type& Registry::GetComponentFromEntity(const Entity& entity)
		{
			return GetComponentFromEntity<T_Type>(entity.GetID());
		}

		template<typename ...T_Types>
		inline std::tuple<T_Types&...> Registry::GetComponentsFromEntity(const Entity& entity)
		{
			return GetComponentsFromEntity<T_Types...>(entity.GetID());
		}

		inline Registry::~Registry()
		{
			for (std::function<void()> deleter : m_DeleteBehaviours)
			{
				deleter();
			}
		}



		inline Entity Registry::CreateChild(const Entity& entity)
		{
			Entity result = CreateEntity();
			SetEntityParent(result, entity);
			return result;
		}

		inline void Registry::AddChild(const Entity& entity, const entity_id id)
		{
			Entity child = Entity(id, this);
			AddChild(entity, child);
		}

		inline void Registry::AddChild(const Entity& entity, const Entity& child)
		{
			SetEntityParent(child, entity);
		}

		inline Entity  Registry::FormEntity(entity_id id)
		{
			return Entity(id, this);
		}


		inline void Registry::DestroyQueuedEnties()
		{
			for (auto& [entity, recursive] : m_DestructionQueue)
			{
				DestroyEntity(entity, recursive);
			}

			m_DestructionQueue.clear();
		}


		inline void Registry::DestroyEntityLater(const Entity& entity, bool recursive)
		{
			m_DestructionQueue.push_back(std::make_tuple(entity, recursive));
		}

		inline bool Registry::HasEntityParent(const Entity& entity) const
		{
			uint32_t index = m_EntityToIndexMap.At(entity.GetID());
			return m_EntityParents[index] != Entity::Null.GetID();
		}

		inline bool Registry::HasEntityParent(const entity_id entity) const
		{
			uint32_t index = m_EntityToIndexMap.At(entity);
			return m_EntityParents[index] != Entity::Null.GetID();
		}

		inline void Registry::SetEntityParent(const Entity& entity, const Entity& parent)
		{
			uint32_t entityIndex = m_EntityToIndexMap.At(entity.GetID());

			if (entity.HasParent())
			{
				entity_id currentParent = m_EntityParents[entityIndex];
				uint32_t currentParentIndex = m_EntityToIndexMap.At(currentParent);
				m_EntityChildLists[currentParentIndex].Remove(entity.GetID());
			}

			uint32_t parentIndex = m_EntityToIndexMap.At(parent.GetID());

			m_EntityParents[entityIndex] = parent.GetID();
			m_EntityChildLists[parentIndex].PushBack(entity.GetID());
		}

		inline Entity Registry::GetEntityParent(const Entity& entity)
		{
			uint32_t index = m_EntityToIndexMap.At(entity.GetID());
			entity_id parentID = m_EntityParents[index];
			return Entity(parentID, this);
		}

		inline const Util::LinkedList<Entity> Registry::GetEntityChildren(const Entity& entity)
		{
			Util::LinkedList<Entity> result;

			uint32_t index = m_EntityToIndexMap.At(entity.GetID());
			for (entity_id id : m_EntityChildLists[index])
			{
				result.PushBack(Entity(id, this));
			}

			return result;

		}

		inline const bool Registry::HasEntityChildren(const Entity& entity) const
		{
			uint32_t index = m_EntityToIndexMap.At(entity.GetID());

			return !m_EntityChildLists[index].Empty();
		}

		inline const Util::LinkedList<entity_id>& Registry::GetEntityChildrenIDs(const Entity& entity) const
		{
			uint32_t index = m_EntityToIndexMap.At(entity.GetID());
			return m_EntityChildLists[index];
		}



		inline Registry::RootFilter Registry::RootEntites()
		{
			return RootFilter(this);
		}

		inline Registry::AllFilter Registry::AllEntites()
		{
			return AllFilter(this);
		}

		inline void Registry::DestroyEntity(const Entity& entity, bool recursive)
		{
			if (!m_EntityToIndexMap.Contains(entity.GetID()))
			{
				TESSERACT_CORE_ERROR("Entity not present in registry!");
				return;
			}

			uint32_t index = m_EntityToIndexMap.At(entity.GetID());

			std::vector<Entity> children;

			// Orphan all child entities:
			for (entity_id childId : m_EntityChildLists[index])
			{
				uint32_t childIndex = m_EntityToIndexMap.At(childId);
				m_EntityParents[childIndex] = Entity::Null.GetID();

				children.push_back(Entity(childId, this));
			}

			// Remove entity from parent's child list
			if (HasEntityParent(entity))
			{
				entity_id parentId = m_EntityParents[index];
				uint32_t parentIndex = m_EntityToIndexMap.At(parentId);
				m_EntityChildLists[parentIndex].Remove(entity.GetID());
			}


			// move last entity in registry to newly freed space
			uint32_t lastIndex = m_IndexToEntityArray.size() - 1;

			entity_id relocatedEntity = m_IndexToEntityArray[lastIndex];

			m_IndexToEntityArray[index] = relocatedEntity;
			m_EntityParents[index] = m_EntityParents[lastIndex];
			m_EntityChildLists[index] = m_EntityChildLists[lastIndex];

			m_IndexToEntityArray.pop_back();
			m_EntityParents.pop_back();
			m_EntityChildLists.pop_back();

			m_EntityToIndexMap.Set(relocatedEntity, index);

			for (std::function<void(uint32_t)> popper : m_PopBehaviours)
			{
				popper(index);
			}

			m_EntityToIndexMap.Remove(entity.GetID());
			m_EntitesCount--;

			if (recursive)
			{
				for (Entity child : children)
				{
					DestroyEntity(child, recursive);
				}
			}
		}

		template<typename ... T_Types>
		inline Registry::RegistryFilter<T_Types...> Registry::Filter()
		{
			return RegistryFilter<T_Types...>(this);
		}
	}
}

namespace Tesseract
{
	namespace ECS
	{
		template<typename T_Type, typename ... T_Params>
		inline T_Type& Entity::AddComponent(T_Params ... params)
		{
			return m_Registry->AddComponentToEntity<T_Type>(m_EntityID, params...);
		}

		template<typename T_Type>
		inline void Entity::RemoveComponent()
		{
			m_Registry->RemoveComponentFromEntity<T_Type>(m_EntityID);
		}

		template<typename T_Type>
		inline bool Entity::HasComponent() const
		{
			return m_Registry->HasEntityComponent<T_Type>(m_EntityID);
		}

		template<typename ... T_Types>
		inline bool Entity::HasComponents() const
		{
			return m_Registry->HasEntityComponents<T_Types...>(m_EntityID);
		}

		template<typename T_Type>
		inline T_Type& Entity::GetComponent() const
		{
			return m_Registry->GetComponentFromEntity<T_Type>(m_EntityID);
		}

		template<typename ... T_Types>
		inline std::tuple<T_Types&...> Entity::GetComponents() const
		{
			return m_Registry->GetComponentsFromEntity<T_Types...>(m_EntityID);
		}

		inline bool Entity::HasParent() const
		{
			return m_Registry->HasEntityParent(*this);
		}

		inline Entity Entity::GetParent() const
		{
			return m_Registry->GetEntityParent(*this);
		}

		inline const Util::LinkedList<entity_id>& Entity::GetChildrenIDs() const
		{
			return m_Registry->GetEntityChildrenIDs(*this);
		}

		inline const Util::LinkedList<Entity> Entity::GetChildren() const
		{
			return m_Registry->GetEntityChildren(*this);
		}

		inline const Entity Entity::FindChild(const std::string& tag) const
		{
			ECS::Entity result = m_Registry->FindEntityChild(*this, tag);
			return result;
		}


		inline const Entity Entity::Find(const std::string& tag) const
		{

			return m_Registry->FindEntity(tag);
		}


		inline Registry* Entity::GetRegistry() const
		{
			return m_Registry;
		}



		inline const bool Entity::HasChildren() const
		{
			return m_Registry->HasEntityChildren(*this);
		}



		inline void Entity::Destroy(bool recursive)
		{
			m_Registry->DestroyEntity(*this, recursive);
		}

		inline void Entity::DestroyLater(bool recursive)
		{
			m_Registry->DestroyEntityLater(*this, recursive);
		}

		inline Entity Entity::CreateChild()
		{
			return m_Registry->CreateChild(*this);
		}

		inline void Entity::AddChild(const entity_id id)
		{
			return m_Registry->AddChild(*this, id);
		}
		inline void Entity::AddChild(const Entity& entity)
		{
			return m_Registry->AddChild(*this, entity);
		}


		inline bool Entity::operator==(const Entity& other)
		{
			return m_EntityID == other.m_EntityID && m_Registry == other.m_Registry;
		}

	}
}
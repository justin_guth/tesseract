#pragma once

#include "Vector2.h"
#include "Vector3.h"

namespace Tesseract::Math
{
    class Random
    {
    private:
        Random();
    public:
        inline static uint32_t Hash(uint32_t s)
        {
            s ^= 2747636419u;
            s *= 2654435769u;
            s ^= s >> 16;
            s *= 2654435769u;
            s ^= s >> 16;
            s *= 2654435769u;
            return s;
        };

        inline static float RandomFloat(uint32_t seed) { return float(Hash(seed)) / 4294967295.0; }; // 2^32-1
        inline static float RandomFloat(Vector2T<int32_t> seed) { return float(Hash(seed.Y + Hash(seed.X))) / 4294967295.0; }
        inline static float RandomFloat(Vector3T<int32_t> seed) { return float(Hash(seed.Z + Hash(seed.Y + Hash(seed.X)))) / 4294967295.0; }

        inline static uint32_t Range(uint32_t a, uint32_t b) { return rand() % (b - a) + a; }

        inline static float Range(float a, float b, uint32_t seed)
        {
            float t = RandomFloat(seed);
            return (1.0 - t) * a + t * b;
        }

        inline static float Range(float a, float b) { return Range(a, b, rand()); }

        inline static Vector2 OnUnitCircle(uint32_t seed)
        {
            float phi = 2.0 * Constants::Pi * RandomFloat(seed);
            return { cos(phi), sin(phi) };
        }

        inline static Vector2 OnUnitCircle() { return OnUnitCircle(rand()); }

        inline static Vector3 OnUnitSphere(uint32_t seed)
        {
            float phi = 2.0 * Constants::Pi * RandomFloat(seed);
            float theta = 2.0 * Constants::Pi * RandomFloat(seed + 1);
            return { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };
        }

        inline static Vector3 OnUnitSphere() { return OnUnitSphere(rand()); }

        inline static Vector3 InsideUnitSphere(uint32_t seed)
        {
            float phi = 2.0 * Constants::Pi * RandomFloat(seed);
            float theta = 2.0 * Constants::Pi * RandomFloat(seed + 1);
            float r = RandomFloat(seed + 2);
            return Vector3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta)) * r;
        }

        inline static Vector3 InsideUnitSphere() { return InsideUnitSphere(rand()); }
    };

} // namespace Tesseract::Math

#pragma once

#include "Functions.h"

#include "Tesseract/Core/Macros.h"

#include "Functions.h"

namespace Tesseract
{
	namespace Math
	{
		template<typename T_Type>
		class Vector2T
		{
		public:

			union
			{
				T_Type R;
				T_Type X;
				T_Type S;
			};

			union
			{
				T_Type G;
				T_Type Y;
				T_Type T;
			};

		public:

			inline Vector2T();
			inline Vector2T(T_Type value);
			inline Vector2T(const Vector2T<T_Type>& other);
			inline Vector2T(const Vector2T<T_Type>&& other);
			inline Vector2T(T_Type x, T_Type y);

			inline T_Type Magnitude() const;
			inline T_Type MagnitudeSquared() const;
			inline Vector2T<T_Type> Normalized() const;
			inline Vector2T<T_Type>& Normalize();
			inline static Vector2T<T_Type> RandomOnUnitCircle();

			inline T_Type Dot(const Vector2T<T_Type>& other) const;
			inline T_Type Dot(const Vector2T<T_Type>&& other) const;

			inline T_Type* GetData() { return &X; }
			inline const T_Type* GetData() const { return &X; }

			inline Vector2T<T_Type>& operator=(const Vector2T<T_Type>& other);
			inline Vector2T<T_Type>& operator=(const Vector2T<T_Type>&& other);

			inline bool operator==(const Vector2T<T_Type>& other) const;

			inline Vector2T<T_Type> operator+(const Vector2T<T_Type>& other) const;
			inline Vector2T<T_Type> operator-(const Vector2T<T_Type>& other) const;
			inline Vector2T<T_Type> operator*(const Vector2T<T_Type>& other) const;
			inline Vector2T<T_Type> operator/(const Vector2T<T_Type>& other) const;

			inline Vector2T<T_Type> operator+(T_Type value) const;
			inline Vector2T<T_Type> operator-(T_Type value) const;
			inline Vector2T<T_Type> operator*(T_Type value) const;
			inline Vector2T<T_Type> operator/(T_Type value) const;



			inline Vector2T<T_Type>& operator+=(const Vector2T<T_Type>& other);
			inline Vector2T<T_Type>& operator-=(const Vector2T<T_Type>& other);
			inline Vector2T<T_Type>& operator*=(const Vector2T<T_Type>& other);
			inline Vector2T<T_Type>& operator/=(const Vector2T<T_Type>& other);

			inline Vector2T<T_Type>& operator+=(T_Type value);
			inline Vector2T<T_Type>& operator-=(T_Type value);
			inline Vector2T<T_Type>& operator*=(T_Type value);
			inline Vector2T<T_Type>& operator/=(T_Type value);

			inline T_Type operator[](size_t position) const;
			inline T_Type& operator[](size_t position);

		public:

			inline static T_Type Dot(const Vector2T<T_Type>& vector1, const Vector2T<T_Type>& vector2);
			inline static T_Type Dot(const Vector2T&& vector1, const Vector2T&& vector2);
			inline static T_Type Dot(const Vector2T& vector1, const Vector2T&& vector2);
			inline static T_Type Dot(const Vector2T&& vector1, const Vector2T& vector2);

			inline static Vector2T<T_Type> UnitX();
			inline static Vector2T<T_Type> UnitY();
		};

		template<typename T_Type>
		inline Vector2T<T_Type> operator*(Vector2T<T_Type>&& vector, T_Type value);

		template<typename T_Type>
		inline Vector2T<T_Type> operator-(Vector2T<T_Type>&& vector);

		//== Implementations ========/


		template<typename T_Type>
		inline Vector2T<T_Type>::Vector2T() :
			Vector2T(0.0f)
		{

		}

		template<typename T_Type>
		inline Vector2T<T_Type>::Vector2T(T_Type value) :
			X(value),
			Y(value)
		{

		}

		template<typename T_Type>
		inline Vector2T<T_Type>::Vector2T(const Vector2T<T_Type>& other) :
			X(other.X),
			Y(other.Y)
		{
		}

		template<typename T_Type>
		inline Vector2T<T_Type>::Vector2T(const Vector2T<T_Type>&& other) :
			X(other.X),
			Y(other.Y)
		{
		}

		template<typename T_Type>
		inline Vector2T<T_Type>::Vector2T(T_Type x, T_Type y) :
			X(x),
			Y(y)
		{

		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Magnitude() const
		{
			return Functions::Sqrt((X * X) + (Y * Y));
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::MagnitudeSquared() const
		{
			return (X * X) + (Y * Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::Normalized() const
		{
			T_Type magnitude = Magnitude();
			return Vector2T<T_Type>(X / magnitude, Y / magnitude);
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::Normalize()
		{
			T_Type magnitude = Magnitude();

			X = X / magnitude;
			Y = Y / magnitude;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::RandomOnUnitCircle()
		{
			float angle = Functions::Random() * 2.0f * Constants::Pi;

			return { Functions::QuickSin(angle), Functions::QuickCos(angle) };
		}


		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator=(const Vector2T<T_Type>& other)
		{
			X = other.X;
			Y = other.Y;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator=(const Vector2T<T_Type>&& other)
		{
			X = other.X;
			Y = other.Y;

			return *this;
		}

		template<typename T_Type>
		inline bool Vector2T<T_Type>::operator==(const Vector2T<T_Type>& other) const
		{
			return X == other.X && Y == other.Y;
		}




		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator+(const Vector2T<T_Type>& other) const
		{
			return Vector2T<T_Type>(X + other.X, Y + other.Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator-(const Vector2T<T_Type>& other) const
		{
			return Vector2T<T_Type>(X - other.X, Y - other.Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator*(const Vector2T<T_Type>& other) const
		{
			return Vector2T<T_Type>(X * other.X, Y * other.Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator/(const Vector2T<T_Type>& other) const
		{
			return Vector2T<T_Type>(X / other.X, Y / other.Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator+(T_Type value) const
		{
			return Vector2T<T_Type>(X + value, Y + value);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator-(T_Type value) const
		{
			return Vector2T<T_Type>(X - value, Y - value);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator*(T_Type value) const
		{
			return Vector2T<T_Type>(X * value, Y * value);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::operator/(T_Type value) const
		{
			return Vector2T<T_Type>(X / value, Y / value);
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator+=(const Vector2T<T_Type>& other)
		{
			X += other.X;
			Y += other.Y;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator-=(const Vector2T<T_Type>& other)
		{
			X -= other.X;
			Y -= other.Y;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator*=(const Vector2T<T_Type>& other)
		{
			X *= other.X;
			Y *= other.Y;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator/=(const Vector2T<T_Type>& other)
		{
			X /= other.X;
			Y /= other.Y;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator+=(T_Type value)
		{
			X += value;
			Y += value;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator-=(T_Type value)
		{
			X -= value;
			Y -= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator*=(T_Type value)
		{
			X *= value;
			Y *= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type>& Vector2T<T_Type>::operator/=(T_Type value)
		{
			X /= value;
			Y /= value;

			return *this;
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::operator[](size_t position) const
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 1, "Out of bounds index provided for Vector2T<T_Type>! ({})", position);
			return GetData()[position];
		}

		template<typename T_Type>
		inline T_Type& Vector2T<T_Type>::operator[](size_t position)
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 1, "Out of bounds index provided for Vector2T<T_Type>! ({})", position);
			return GetData()[position];
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>& other) const
		{
			return (X * other.X) + (Y * other.Y);
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>&& other) const
		{
			return (X * other.X) + (Y * other.Y);
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>&& vector1, const Vector2T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>& vector1, const Vector2T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>&& vector1, const Vector2T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector2T<T_Type>::Dot(const Vector2T<T_Type>& vector1, const Vector2T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::UnitX()
		{
			return { 1.0f, 0.0f };
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector2T<T_Type>::UnitY()
		{
			return { 0.0f, 1.0f };
		}

		template<typename T_Type>
		inline Vector2T<T_Type> operator*(Vector2T<T_Type>&& vector, T_Type value)
		{
			return Vector2T<T_Type>(
				vector.X * value,
				vector.Y * value
			);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> operator-(Vector2T<T_Type>&& vector)
		{
			return Vector2T<T_Type>(
				-vector.X,
				-vector.Y
			);
		}

		typedef Vector2T<float> Vector2;
		typedef Vector2T<int> Vector2I;
	}
}


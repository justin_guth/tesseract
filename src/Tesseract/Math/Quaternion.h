#pragma once

#include "Vector3.h"

#include "Tesseract/Core/Macros.h"

#include "Vector4.h"

namespace Tesseract
{
	namespace Math
	{
		template <typename T_Type>
		class QuaternionT
		{
		public:

			T_Type A;
			T_Type B;
			T_Type C;
			T_Type D;

			inline QuaternionT();
			inline QuaternionT(T_Type a);
			inline QuaternionT(Vector4T<T_Type> vector);
			inline QuaternionT(T_Type a, T_Type b, T_Type c, T_Type d);

			inline T_Type Norm() const;
			inline QuaternionT<T_Type> Normalized() const;
			inline QuaternionT<T_Type>& Normalize();
			inline QuaternionT<T_Type> Conjugation() const;
			inline QuaternionT<T_Type> Reciprocal() const;
			inline Vector4T<T_Type> Rotate(const Vector4T<T_Type>& vector) const;

			inline QuaternionT<T_Type> operator+(const QuaternionT<T_Type>& other) const;
			inline QuaternionT<T_Type> operator-(const QuaternionT<T_Type>& other) const;
			inline QuaternionT<T_Type> operator*(const QuaternionT<T_Type>& other) const;

			inline QuaternionT<T_Type>& operator+=(const QuaternionT<T_Type>& other);
			inline QuaternionT<T_Type>& operator-=(const QuaternionT<T_Type>& other);
			inline QuaternionT<T_Type>& operator*=(const QuaternionT<T_Type>& other);

			inline QuaternionT<T_Type> operator*(T_Type value) const;
			inline QuaternionT<T_Type> operator/(T_Type value) const;

			inline QuaternionT<T_Type>& operator*=(T_Type value);
			inline QuaternionT<T_Type>& operator/=(T_Type value);

			inline QuaternionT<T_Type> operator-() const;

			inline Vector4T<T_Type> ToVector4() const;

		public:

			inline static QuaternionT<T_Type> Identity();
			inline static QuaternionT<T_Type> FromAxisAngle(const Vector3T<T_Type>& axis, T_Type angle);
			inline static QuaternionT<T_Type> FromPoint(const Vector4T<T_Type>& point);

			inline static QuaternionT<T_Type> NLerp(const QuaternionT<T_Type>& QuaternionT1, const QuaternionT<T_Type>& QuaternionT2, T_Type alpha);
		};

		//== Implementations ====================================// 




		template<typename T_Type>
		inline QuaternionT<T_Type>::QuaternionT() :
			A(0.0f),
			B(0.0f),
			C(0.0f),
			D(0.0f)
		{
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>::QuaternionT(T_Type a) :
			A(a),
			B(0.0f),
			C(0.0f),
			D(0.0f)
		{
		}
		template<typename T_Type>
		inline QuaternionT<T_Type>::QuaternionT(Vector4T<T_Type> vector) :
			A(vector.X),
			B(vector.Y),
			C(vector.Z),
			D(vector.W)
		{

		}

		template<typename T_Type>
		inline QuaternionT<T_Type>::QuaternionT(T_Type a, T_Type b, T_Type c, T_Type d) :
			A(a),
			B(b),
			C(c),
			D(d)
		{
		}

		template<typename T_Type>
		inline T_Type QuaternionT<T_Type>::Norm() const
		{
			//TODO: FIX!! sqrt(q `dot` q*)

			return Functions::Sqrt((A * A) + (B * B) + (C * C) + (D * D));
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::Normalized() const
		{
			return (*this) / Norm();
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::Normalize()
		{
			(*this) /= Norm();
			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::Conjugation() const
		{
			return QuaternionT<T_Type>(A, -B, -C, -D);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::Reciprocal() const
		{
			return Conjugation() / Norm();
		}

		template<typename T_Type>
		inline Vector4T<T_Type> QuaternionT<T_Type>::Rotate(const Vector4T<T_Type>& vector) const
		{
			QuaternionT<T_Type> resultQuaternion = ((*this) * QuaternionT<T_Type>::FromPoint(vector) * this->Reciprocal());
			return { resultQuaternion.B, resultQuaternion.C, resultQuaternion.D, vector.Z };
		}


		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator+(const QuaternionT<T_Type>& other) const
		{
			return QuaternionT<T_Type>(
				A + other.A,
				B + other.B,
				C + other.C,
				D + other.D
				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator-(const QuaternionT<T_Type>& other) const
		{
			return QuaternionT<T_Type>(
				A - other.A,
				B - other.B,
				C - other.C,
				D - other.D
				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator*(const QuaternionT<T_Type>& other) const
		{
			return QuaternionT<T_Type>(
				(A * other.A) - (B * other.B) - (C * other.C) - (D * other.D),
				(A * other.B) + (B * other.A) + (C * other.D) - (D * other.C),
				(A * other.C) - (B * other.D) + (C * other.A) + (D * other.B),
				(A * other.D) + (B * other.C) - (C * other.B) + (D * other.A)
				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::operator+=(const QuaternionT<T_Type>& other)
		{
			A += other.A;
			B += other.B;
			C += other.C;
			D += other.D;

			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::operator-=(const QuaternionT<T_Type>& other)
		{
			A -= other.A;
			B -= other.B;
			C -= other.C;
			D -= other.D;

			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::operator*=(const QuaternionT<T_Type>& other)
		{

			T_Type oldA = A;
			T_Type oldB = B;
			T_Type oldC = C;
			T_Type oldD = D;

			A = (oldA * other.A) - (oldB * other.B) - (oldC * other.C) - (oldD * other.D);
			B = (oldA * other.B) + (oldB * other.A) + (oldC * other.D) - (oldD * other.C);
			C = (oldA * other.C) - (oldB * other.D) + (oldC * other.A) + (oldD * other.B);
			D = (oldA * other.D) + (oldB * other.C) - (oldC * other.B) + (oldD * other.A);

			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator*(T_Type value) const
		{
			return QuaternionT<T_Type>(
				A * value,
				B * value,
				C * value,
				D * value

				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator/(T_Type value) const
		{
			return QuaternionT<T_Type>(
				A / value,
				B / value,
				C / value,
				D / value
				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::operator*=(T_Type value)
		{
			A *= value;
			B *= value;
			C *= value;
			D *= value;

			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type>& QuaternionT<T_Type>::operator/=(T_Type value)
		{
			A /= value;
			B /= value;
			C /= value;
			D /= value;

			return *this;
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::operator-() const
		{
			return QuaternionT<T_Type>(-A, -B, -C, -D);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> QuaternionT<T_Type>::ToVector4() const
		{
			return Vector4T<T_Type>(A, B, C, D);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::Identity()
		{
			return QuaternionT<T_Type>(1.0f);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::FromAxisAngle(const Vector3T<T_Type>& axis, T_Type angle)
		{
			Vector3T<T_Type> normlizedAxis = axis.Normalized();
			T_Type sinThetaHalved = Functions::QuickSin(angle / 2.0f);

			return QuaternionT<T_Type>(
				Functions::QuickCos(angle / 2),
				normlizedAxis.X * sinThetaHalved,
				normlizedAxis.Y * sinThetaHalved,
				normlizedAxis.Z * sinThetaHalved
				);
		}

		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::FromPoint(const Vector4T<T_Type>& point)
		{
			return QuaternionT<T_Type>(0.0, point.X, point.Y, point.Z);
		}


		template<typename T_Type>
		inline QuaternionT<T_Type> QuaternionT<T_Type>::NLerp(const QuaternionT<T_Type>& QuaternionT1, const QuaternionT<T_Type>& QuaternionT2, T_Type alpha)
		{
			TESSERACT_CORE_ASSERT(alpha >= 0.0f, "Specified alpha is less than 0.0 ({})", alpha);
			TESSERACT_CORE_ASSERT(alpha <= 1.0f, "Specified alpha is grater than 1.0 ({})", alpha);

			return ((QuaternionT1 * (1 - alpha)) + (QuaternionT2 * alpha)).Normalized();
		}


		typedef QuaternionT<float> Quaternion;
	}
}
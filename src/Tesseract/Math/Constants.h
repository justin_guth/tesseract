#pragma once

namespace Tesseract
{
	namespace Math
	{
		class Constants
		{
		public:

			const static float Pi ;
			const static float Epsilon;
		};
	}
}

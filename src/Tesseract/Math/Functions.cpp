#include "Functions.h"

#include "Constants.h"

#include <math.h>

namespace Tesseract
{
	namespace Math
	{
		float Functions::s_SineBuffer[SineCosineBufferResolution];
		float Functions::s_CosineBuffer[SineCosineBufferResolution];
		bool Functions::s_Initialized = false;

		void Functions::Init()
		{
			const double pi2 = Constants::Pi * 2.0;
			double currentSection = 0.0;

			for (uint32_t i = 0; i < SineCosineBufferResolution; i++)
			{
				currentSection = ((double) i * pi2) / ((double) SineCosineBufferResolution);
				s_SineBuffer[i] = Sin(currentSection);
				s_CosineBuffer[i] = Cos(currentSection);
			}

			s_Initialized = true;
		}

		float Functions::QuickSin(float value)
		{
			TESSERACT_CORE_ASSERT(s_Initialized, "Functions module was not initialized!");
			uint32_t index = SineCosineBufferResolution / (Constants::Pi * 2)* (value >= 0.0f ? value : -value);
			index %= SineCosineBufferResolution;
			return (value >= 0.0f ? 1 : -1) * s_SineBuffer[index];
		}

		float Functions::QuickCos(float value)
		{
			TESSERACT_CORE_ASSERT(s_Initialized, "Functions module was not initialized!");
			uint32_t index = SineCosineBufferResolution / (Constants::Pi * 2) * (value >= 0.0f ? value : -value);
			index %= SineCosineBufferResolution;
			return value >= 0.0f ? s_CosineBuffer[index] : s_CosineBuffer[SineCosineBufferResolution - index - 1];
		}
	}
}
#pragma once

#include "Functions.h"
#include "Vector3.h"
#include "Vector2.h"

#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
	namespace Math
	{
		template <typename T_Type>
		class Vector3T;

		template <typename T_Type>
		class Vector4T
		{
		public:

			union
			{
				T_Type R;
				T_Type X;
				T_Type S;
			};

			union
			{
				T_Type G;
				T_Type Y;
				T_Type T;
			};
			union
			{
				T_Type B;
				T_Type Z;
				T_Type P;
			};
			union
			{
				T_Type A;
				T_Type W;
				T_Type Q;
			};

		public:

			inline Vector4T();
			inline Vector4T(T_Type value);
			inline Vector4T(const Vector4T& other) ;
			inline Vector4T(const Vector4T&& other) noexcept;
			inline Vector4T(T_Type x, T_Type y, T_Type z, T_Type w);

			inline T_Type Magnitude() const;
			inline Vector4T Normalized() const;
			inline Vector4T& Normalize();

			inline T_Type Dot(const Vector4T& other) const;
			inline T_Type Dot(const Vector4T&& other) const;

			inline T_Type* GetData() { return &X; }
			inline const T_Type* GetData() const { return &X; }

			inline Vector4T<T_Type>& operator=(const Vector4T& other);
			inline Vector4T<T_Type>& operator=(const Vector4T&& other) noexcept;

			inline Vector4T<T_Type> operator+(const Vector4T& other) const;
			inline Vector4T<T_Type> operator-(const Vector4T& other) const;
			inline Vector4T<T_Type> operator*(const Vector4T& other) const;
			inline Vector4T<T_Type> operator/(const Vector4T& other) const;

			inline Vector4T<T_Type> operator+(T_Type value) const;
			inline Vector4T<T_Type> operator-(T_Type value) const;
			inline Vector4T<T_Type> operator*(T_Type value) const;
			inline Vector4T<T_Type> operator/(T_Type value) const;

			inline Vector4T<T_Type>& operator+=(const Vector4T& other);
			inline Vector4T<T_Type>& operator-=(const Vector4T& other);
			inline Vector4T<T_Type>& operator*=(const Vector4T& other);
			inline Vector4T<T_Type>& operator/=(const Vector4T& other);

			inline Vector4T<T_Type>& operator+=(T_Type value);
			inline Vector4T<T_Type>& operator-=(T_Type value);
			inline Vector4T<T_Type>& operator*=(T_Type value);
			inline Vector4T<T_Type>& operator/=(T_Type value);

			inline Vector2T<T_Type> XY() const;
			inline Vector3T<T_Type> XYZ() const;

			inline T_Type operator[](size_t position) const;
			inline T_Type& operator[](size_t position);

		public:

			inline static T_Type Dot(const Vector4T& vector1, const Vector4T& vector2);
			inline static T_Type Dot(const Vector4T&& vector1, const Vector4T&& vector2);
			inline static T_Type Dot(const Vector4T& vector1, const Vector4T&& vector2);
			inline static T_Type Dot(const Vector4T&& vector1, const Vector4T& vector2);


			inline static Vector4T<T_Type> UnitX();
			inline static Vector4T<T_Type> UnitY();
			inline static Vector4T<T_Type> UnitZ();
			inline static Vector4T<T_Type> UnitW();
		};

		//== Implementations ====================================// 




		template<typename T_Type>
		inline Vector4T<T_Type>::Vector4T() :
			X(0.0f),
			Y(0.0f),
			Z(0.0f),
			W(0.0f)
		{

		}

		template<typename T_Type>
		inline Vector4T<T_Type>::Vector4T(T_Type value) :
			X(value),
			Y(value),
			Z(value),
			W(value)
		{

		}

		template<typename T_Type>
		inline Vector4T<T_Type>::Vector4T(const Vector4T<T_Type>& other) :
			X(other.X),
			Y(other.Y),
			Z(other.Z),
			W(other.W)
		{
		}

		template<typename T_Type>
		inline Vector4T<T_Type>::Vector4T(const Vector4T<T_Type>&& other) noexcept :
			X(other.X),
			Y(other.Y),
			Z(other.Z),
			W(other.W)
		{
		}

		template<typename T_Type>
		inline Vector4T<T_Type>::Vector4T(T_Type x, T_Type y, T_Type z, T_Type w) :
			X(x),
			Y(y),
			Z(z),
			W(w)
		{

		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Magnitude() const
		{
			return Functions::Sqrt((X * X) + (Y * Y) + (Z * Z) + (W * W));
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::Normalized() const
		{
			T_Type magnitude = Magnitude();
			return Vector4T(X / magnitude, Y / magnitude, Z / magnitude, W / magnitude);
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::Normalize()
		{
			T_Type magnitude = Magnitude();

			X = X / magnitude;
			Y = Y / magnitude;
			Z = Z / magnitude;
			W = W / magnitude;

			return *this;
		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>& other) const
		{
			return (X * other.X) + (Y * other.Y) + (Z * other.Z) + (W * other.W);
		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>&& other) const
		{
			return (X * other.X) + (Y * other.Y) + (Z * other.Z) + (W * other.W);
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator=(const Vector4T<T_Type>& other)
		{
			X = other.X;
			Y = other.Y;
			Z = other.Z;
			W = other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator=(const Vector4T<T_Type>&& other) noexcept
		{
 			X = other.X;
			Y = other.Y;
			Z = other.Z;
			W = other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator+(const Vector4T<T_Type>& other) const
		{
			return Vector4T(X + other.X, Y + other.Y, Z + other.Z, W + other.W);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator-(const Vector4T<T_Type>& other) const
		{
			return Vector4T(X - other.X, Y - other.Y, Z - other.Z, W - other.W);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator*(const Vector4T<T_Type>& other) const
		{
			return Vector4T(X * other.X, Y * other.Y, Z * other.Z, W * other.W);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator/(const Vector4T<T_Type>& other) const
		{
			return Vector4T(X / other.X, Y / other.Y, Z / other.Z, W / other.W);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator+(T_Type value) const
		{
			return Vector4T(X + value, Y + value, Z + value, W + value);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator-(T_Type value) const
		{
			return Vector4T(X - value, Y - value, Z - value, W - value);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator*(T_Type value) const
		{
			return Vector4T(X * value, Y * value, Z * value, W * value);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::operator/(T_Type value) const
		{
			return Vector4T(X / value, Y / value, Z / value, W / value);
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator+=(const Vector4T& other)
		{
			X += other.X;
			Y += other.Y;
			Z += other.Z;
			W += other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator-=(const Vector4T<T_Type>& other)
		{
			X -= other.X;
			Y -= other.Y;
			Z -= other.Z;
			W -= other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator*=(const Vector4T<T_Type>& other)
		{
			X *= other.X;
			Y *= other.Y;
			Z *= other.Z;
			W *= other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator/=(const Vector4T<T_Type>& other)
		{
			X /= other.X;
			Y /= other.Y;
			Z /= other.Z;
			W /= other.W;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator+=(T_Type value)
		{
			X += value;
			Y += value;
			Z += value;
			W += value;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator-=(T_Type value)
		{
			X -= value;
			Y -= value;
			Z -= value;
			W -= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator*=(T_Type value)
		{
			X *= value;
			Y *= value;
			Z *= value;
			W *= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector4T<T_Type>& Vector4T<T_Type>::operator/=(T_Type value)
		{
			X /= value;
			Y /= value;
			Z /= value;
			W /= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector4T<T_Type>::XY() const
		{
			return Vector2T<T_Type>(X, Y);
		}


		template<typename T_Type>
		inline Vector3T<T_Type> Vector4T<T_Type>::XYZ() const
		{
			return Vector3T<T_Type>(X, Y, Z);
		}


		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::operator[](size_t position) const
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 3, "Out of bounds index provided for Vector4T! ({})", position);
			return GetData()[position];
		}

		template<typename T_Type>
		inline T_Type& Vector4T<T_Type>::operator[](size_t position)
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 3, "Out of bounds index provided for Vector4T! ({})", position);
			return GetData()[position];
		}
		
		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>&& vector1, const Vector4T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>& vector1, const Vector4T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>&& vector1, const Vector4T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector4T<T_Type>::Dot(const Vector4T<T_Type>& vector1, const Vector4T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::UnitX()
		{
			return {1.0f, 0.0f, 0.0f , 0.0f};
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::UnitY()
		{
			return {0.0f, 1.0f, 0.0f, 0.0f};
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::UnitZ()
		{
			return {0.0f, 0.0f, 1.0f, 0.0f};
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector4T<T_Type>::UnitW()
		{
			return {0.0f, 0.0f, 1.0f, 0.0f};
		}

		typedef Vector4T<float> Vector4;
	}
}


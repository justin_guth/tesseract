#pragma once

#include "Functions.h"
#include "Vector2.h"
#include "Vector4.h"

#include "Tesseract/Core/Macros.h"


namespace Tesseract
{
	namespace Math
	{
		template <typename T_Type>
		class Vector4T;

		template <typename T_Type>
		class Vector3T
		{
		public:

			union
			{
				T_Type R;
				T_Type X;
				T_Type S;
			};

			union
			{
				T_Type G;
				T_Type Y;
				T_Type T;
			};
			union
			{
				T_Type B;
				T_Type Z;
				T_Type P;
			};

		public:

			inline Vector3T();
			inline Vector3T(T_Type value);
			inline Vector3T(const Vector3T& other);
			inline Vector3T(const Vector3T&& other) noexcept;
			inline Vector3T(T_Type x, T_Type y, T_Type z);

			inline T_Type Magnitude() const;
			inline T_Type MagnitudeSquared() const;
			inline Vector3T<T_Type> Normalized() const;
			inline Vector3T<T_Type>& Normalize();

			inline T_Type Dot(const Vector3T<T_Type>& other) const;
			inline T_Type Dot(const Vector3T<T_Type>&& other) const;
			inline Vector3T<T_Type> Cross(const Vector3T<T_Type>& other) const;

			inline T_Type* GetData() { return &X; }
			inline const T_Type* GetData() const { return &X; }

			inline Vector3T<T_Type>& operator=(const Vector3T<T_Type>& other);
			inline Vector3T<T_Type>& operator=(const Vector3T<T_Type>&& other) noexcept;

			inline Vector3T<T_Type> operator-() const;

			inline Vector3T<T_Type> operator+(const Vector3T<T_Type>& other) const;
			inline Vector3T<T_Type> operator-(const Vector3T<T_Type>& other) const;
			inline Vector3T<T_Type> operator*(const Vector3T<T_Type>& other) const;
			inline Vector3T<T_Type> operator/(const Vector3T<T_Type>& other) const;
			inline float operator||(const Vector3T<T_Type>& other) const;
			inline Vector3T<T_Type> operator^(const Vector3T<T_Type>& other) const;

			inline Vector3T<T_Type> operator+(T_Type value) const;
			inline Vector3T<T_Type> operator-(T_Type value) const;
			inline Vector3T<T_Type> operator*(T_Type value) const;
			inline Vector3T<T_Type> operator/(T_Type value) const;

			inline Vector3T<T_Type>& operator+=(const Vector3T<T_Type>& other);
			inline Vector3T<T_Type>& operator-=(const Vector3T<T_Type>& other);
			inline Vector3T<T_Type>& operator*=(const Vector3T<T_Type>& other);
			inline Vector3T<T_Type>& operator/=(const Vector3T<T_Type>& other);

			inline Vector3T<T_Type>& operator+=(T_Type value);
			inline Vector3T<T_Type>& operator-=(T_Type value);
			inline Vector3T<T_Type>& operator*=(T_Type value);
			inline Vector3T<T_Type>& operator/=(T_Type value);

			inline bool operator==(const Vector3T<T_Type> other) const;
			inline bool operator!=(const Vector3T<T_Type> other) const;

			inline Vector4T<T_Type> HomogenizedVector() const;
			inline Vector4T<T_Type> HomogenizedPoint() const;

			inline T_Type operator[](size_t position) const;
			inline T_Type& operator[](size_t position);

			inline Vector2T<T_Type> XY() const;
			inline Vector2T<T_Type> XZ() const;
			inline Vector2T<T_Type> YZ() const;

		public:

			inline static T_Type Dot(const Vector3T<T_Type>& vector1, const Vector3T<T_Type>& vector2);
			inline static T_Type Dot(const Vector3T<T_Type>&& vector1, const Vector3T<T_Type>&& vector2);
			inline static T_Type Dot(const Vector3T<T_Type>& vector1, const Vector3T<T_Type>&& vector2);
			inline static T_Type Dot(const Vector3T<T_Type>&& vector1, const Vector3T<T_Type>& vector2);

			inline static Vector3T<T_Type> UnitX();
			inline static Vector3T<T_Type> UnitY();
			inline static Vector3T<T_Type> UnitZ();

			static Vector3T<T_Type> Right;
			static Vector3T<T_Type> Left;
			static Vector3T<T_Type> Forward;
			static Vector3T<T_Type> Back;
			static Vector3T<T_Type> Up;
			static Vector3T<T_Type> Down;
			static Vector3T<T_Type> Zero;
			static Vector3T<T_Type> One;

		};

		template <typename T_Type>
		inline Vector3T<T_Type> operator*(Vector3T<T_Type>&& vector, T_Type value);

		//== Implementations ====================================// 

		template<typename T_Type>
		inline Vector3T<T_Type>::Vector3T():
			Vector3T(0.0f)
		{

		}

		template<typename T_Type>
		inline Vector3T<T_Type>::Vector3T(T_Type value):
			X(value),
			Y(value),
			Z(value)
		{

		}

		template<typename T_Type>
		inline Vector3T<T_Type>::Vector3T(const Vector3T<T_Type>& other):
			X(other.X),
			Y(other.Y),
			Z(other.Z)
		{
		}

		template<typename T_Type>
		inline Vector3T<T_Type>::Vector3T(const Vector3T<T_Type>&& other) noexcept:
			X(other.X),
			Y(other.Y),
			Z(other.Z)
		{
		}

		template<typename T_Type>
		inline Vector3T<T_Type>::Vector3T(T_Type x, T_Type y, T_Type z):
			X(x),
			Y(y),
			Z(z)
		{

		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Magnitude() const
		{
			return Functions::Sqrt((X * X) + (Y * Y) + (Z * Z));
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::MagnitudeSquared() const
		{
			return (X * X) + (Y * Y) + (Z * Z);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Normalized() const
		{
			T_Type magnitude = Magnitude();
			return Vector3T(X / magnitude, Y / magnitude, Z / magnitude);
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::Normalize()
		{
			T_Type magnitude = Magnitude();

			X = X / magnitude;
			Y = Y / magnitude;
			Z = Z / magnitude;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator=(const Vector3T<T_Type>& other)
		{
			X = other.X;
			Y = other.Y;
			Z = other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator=(const Vector3T<T_Type>&& other) noexcept
		{
			X = other.X;
			Y = other.Y;
			Z = other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator-() const
		{
			return Vector3T<T_Type>(-X, -Y, -Z);
		}


		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator+(const Vector3T<T_Type>& other) const
		{
			return Vector3T(X + other.X, Y + other.Y, Z + other.Z);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator-(const Vector3T<T_Type>& other) const
		{
			return Vector3T(X - other.X, Y - other.Y, Z - other.Z);
		}


		template<typename T_Type>
		inline float Vector3T<T_Type>::operator||(const Vector3T<T_Type>& other) const
		{
			return (*this - other).Magnitude();
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator^(const Vector3T<T_Type>& other) const
		{
			return Cross(other);
		}


		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator*(const Vector3T<T_Type>& other) const
		{
			return Vector3T(X * other.X, Y * other.Y, Z * other.Z);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator/(const Vector3T<T_Type>& other) const
		{
			return Vector3T(X / other.X, Y / other.Y, Z / other.Z);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator+(T_Type value) const
		{
			return Vector3T(X + value, Y + value, Z + value);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator-(T_Type value) const
		{
			return Vector3T(X - value, Y - value, Z - value);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator*(T_Type value) const
		{
			return Vector3T(X * value, Y * value, Z * value);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::operator/(T_Type value) const
		{
			return Vector3T(X / value, Y / value, Z / value);
		}



		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator+=(const Vector3T<T_Type>& other)
		{
			X += other.X;
			Y += other.Y;
			Z += other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator-=(const Vector3T<T_Type>& other)
		{
			X -= other.X;
			Y -= other.Y;
			Z -= other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator*=(const Vector3T<T_Type>& other)
		{
			X *= other.X;
			Y *= other.Y;
			Z *= other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator/=(const Vector3T<T_Type>& other)
		{
			X /= other.X;
			Y /= other.Y;
			Z /= other.Z;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator+=(T_Type value)
		{
			X += value;
			Y += value;
			Z += value;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator-=(T_Type value)
		{
			X -= value;
			Y -= value;
			Z -= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator*=(T_Type value)
		{
			X *= value;
			Y *= value;
			Z *= value;

			return *this;
		}

		template<typename T_Type>
		inline Vector3T<T_Type>& Vector3T<T_Type>::operator/=(T_Type value)
		{
			X /= value;
			Y /= value;
			Z /= value;

			return *this;
		}

		template<typename T_Type>
		inline bool Vector3T<T_Type>::operator==(const Vector3T<T_Type> other) const
		{
			return (*this - other).MagnitudeSquared() < Math::Constants::Epsilon;
		}

		template<typename T_Type>
		inline bool Vector3T<T_Type>::operator!=(const Vector3T<T_Type> other) const
		{
			return (*this - other).MagnitudeSquared() >= Math::Constants::Epsilon;
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector3T<T_Type>::HomogenizedVector() const
		{
			return Vector4T<T_Type>(X, Y, Z, 0.0f);
		}

		template<typename T_Type>
		inline Vector4T<T_Type> Vector3T<T_Type>::HomogenizedPoint() const
		{
			return Vector4T<T_Type>(X, Y, Z, 1.0f);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::operator[](size_t position) const
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 2, "Out of bounds index provided for Vector3T! ({})", position);
			return GetData()[position];
		}

		template<typename T_Type>
		inline T_Type& Vector3T<T_Type>::operator[](size_t position)
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 2, "Out of bounds index provided for Vector3T! ({})", position);
			return GetData()[position];
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector3T<T_Type>::XY() const
		{
			return Vector2T<T_Type>(X, Y);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector3T<T_Type>::XZ() const
		{
			return Vector2T<T_Type>(X, Z);
		}

		template<typename T_Type>
		inline Vector2T<T_Type> Vector3T<T_Type>::YZ() const
		{
			return Vector2T<T_Type>(Y, Z);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>& other) const
		{
			return (X * other.X) + (Y * other.Y) + (Z * other.Z);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>&& other) const
		{
			return (X * other.X) + (Y * other.Y) + (Z * other.Z);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Cross(const Vector3T<T_Type>& other) const
		{
			return Vector3T<T_Type>(
				Y * other.Z - Z * other.Y,
				Z * other.X - X * other.Z,
				X * other.Y - Y * other.X
				);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>&& vector1, const Vector3T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>& vector1, const Vector3T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>&& vector1, const Vector3T<T_Type>& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline T_Type Vector3T<T_Type>::Dot(const Vector3T<T_Type>& vector1, const Vector3T<T_Type>&& vector2)
		{
			return  vector1.Dot(vector2);
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::UnitX()
		{
			return { 1.0f, 0.0f, 0.0f };
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::UnitY()
		{
			return { 0.0f, 1.0f, 0.0f };
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::UnitZ()
		{
			return { 0.0f, 0.0f, 1.0f };
		}

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Right = Vector3T<T_Type>(1, 0, 0);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Left = Vector3T<T_Type>(-1, 0, 0);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Forward = Vector3T<T_Type>(0, 1, 0);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Back = Vector3T<T_Type>(0, -1, 0);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Up = Vector3T<T_Type>(0, 0, 1);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Down = Vector3T<T_Type>(0, 0, -1);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::Zero = Vector3T<T_Type>(0, 0, 0);

		template<typename T_Type>
		inline Vector3T<T_Type> Vector3T<T_Type>::One = Vector3T<T_Type>(1, 1, 1);


		template<typename T_Type>
		inline Vector3T<T_Type> operator*(Vector3T<T_Type>&& vector, T_Type value)
		{
			return Vector3T<T_Type>(
				vector.X * value,
				vector.Y * value,
				vector.Z * value
				);
		}

		typedef Vector3T<float> Vector3;
	}
}


#pragma once

#include "Vector3.h"
#include "Vector4.h"

#include "Quaternion.h"

#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
	namespace Math
	{
		class Matrix4
		{
		public:

			Vector4 Column0;
			Vector4 Column1;
			Vector4 Column2;
			Vector4 Column3;

			Matrix4();
			Matrix4(const Matrix4& other);
			Matrix4(float diagonal);
			Matrix4(float diagonal0, float diagonal1, float diagonal2, float diagonal3);
			Matrix4(const Vector4& diagonal);
			Matrix4(const Vector4& column0, const Vector4& column1, const Vector4& column2, const Vector4& column3);
			Matrix4(
				float a00, float a01, float a02, float a03,
				float a10, float a11, float a12, float a13,
				float a20, float a21, float a22, float a23,
				float a30, float a31, float a32, float a33
			);

			inline float* GetData() { return Column0.GetData(); };
			inline const float* GetData() const { return Column0.GetData(); };

			Matrix4 T() const;
			Matrix4 Inverse() const;
			Matrix4 Adjungate() const;
			float Determinant() const;

			inline Vector4 operator[] (size_t position) const;
			inline Vector4& operator[] (size_t position);

			inline Matrix4 operator+(const Matrix4& other) const;
			inline Matrix4 operator-(const Matrix4& other) const;
			inline Matrix4 operator*(const Matrix4& other) const;

			inline Matrix4& operator+=(const Matrix4& other);
			inline Matrix4& operator-=(const Matrix4& other);
			inline Matrix4& operator*=(const Matrix4& other);

			inline Matrix4 operator*(float value) const;
			inline Matrix4& operator*=(float value);
			inline Matrix4 operator/(float value) const;
			inline Matrix4& operator/=(float value);

			inline Vector4 operator*(const Vector4& other) const;

		public:

			static Matrix4 Rotation(const Quaternion& quaternion);
			static Matrix4 Rotation(Vector3 axis, float angle);
			static Matrix4 Rotation(const Vector3& eulerAngles);
			static Matrix4 Rotation(float eulerAngleX, float eulerAngleY, float eulerAngleZ);

			inline static Matrix4 Identity();

			inline static Matrix4 Scale(float scaleX, float scaleY, float scaleZ);
			inline static Matrix4 Scale(const Vector3& scale);
			inline static Matrix4 Translation(float translationX, float translationY, float translationZ);
			inline static Matrix4 Translation(const Vector3& translation);

			inline static Matrix4 RotationTranslation(const Quaternion& quaternion, float translationX, float translationY, float translationZ);
			inline static Matrix4 RotationTranslation(const Vector3& axis, float angle, float translationX, float translationY, float translationZ);
			inline static Matrix4 RotationTranslation(const Vector3& eulerAngles, float translationX, float translationY, float translationZ);
			inline static Matrix4 RotationTranslation(const Quaternion& quaternion, const Vector3& translation);
			inline static Matrix4 RotationTranslation(const Vector3& axis, float angle, const Vector3& translation);
			inline static Matrix4 RotationTranslation(const Vector3& eulerAngles, const Vector3& translation);

			inline static Matrix4 ScaleTranslation(const Vector3& scale, const Vector3& translation);

			inline static Matrix4 ScaleRotationTranslation(const Vector3& scale, const Vector3& axis, float angle, const Vector3& translation);
			inline static Matrix4 ScaleRotationTranslation(const Vector3& scale, const Quaternion& quaternion, const Vector3& translation);
			inline static Matrix4 ScaleRotationTranslation(const Vector3& scale, const Vector3& eulerAngles, const Vector3& translation);
			inline static Matrix4 ScaleRotationTranslation(const Vector3& scale, float eulerAngleX, float eulerAngleY, float eulerAngleZ, const Vector3& translation);

			inline static Matrix4 OrthographicProjection();
			inline static Matrix4 OrthographicProjection(float left, float right, float bottom, float top);
			inline static Matrix4 OrthographicProjection(float left, float right, float bottom, float top, float near, float far);

			inline static Matrix4 PerspectiveProjection(float left, float right, float bottom, float top, float near, float far);
			inline static Matrix4 PerspectiveProjection(float width, float height, float fieldOfViewDegrees, float near, float far);

			inline static Matrix4 LookAt(const Vector3& eye, const Vector3& center, Vector3 up);



		};

		//== implementation =============//


		Vector4 Matrix4::operator[] (size_t position) const
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 3, "Out of bounds index provided for Matrix4! ({})", position);

			return (&Column0)[position];
		}

		Vector4& Matrix4::operator[] (size_t position)
		{
			TESSERACT_CORE_ASSERT(0 <= position && position <= 3, "Out of bounds index provided for Matrix4! ({})", position);

			return (&Column0)[position];
		}



		Matrix4 Matrix4::operator+(const Matrix4& other) const
		{
			return Matrix4(
				Column0 + other.Column0,
				Column1 + other.Column1,
				Column2 + other.Column2,
				Column3 + other.Column3
			);
		}

		Matrix4 Matrix4::operator-(const Matrix4& other) const
		{
			return Matrix4(
				Column0 - other.Column0,
				Column1 - other.Column1,
				Column2 - other.Column2,
				Column3 - other.Column3
			);
		}

		Matrix4 Matrix4::operator*(const Matrix4& other) const
		{
			Matrix4 result;

			const Vector4& otherColumn0 = other.Column0;
			const Vector4& otherColumn1 = other.Column1;
			const Vector4& otherColumn2 = other.Column2;
			const Vector4& otherColumn3 = other.Column3;

			result.Column0 = Column0 * otherColumn0.X + Column1 * otherColumn0.Y + Column2 * otherColumn0.Z + Column3 * otherColumn0.W;
			result.Column1 = Column0 * otherColumn1.X + Column1 * otherColumn1.Y + Column2 * otherColumn1.Z + Column3 * otherColumn1.W;
			result.Column2 = Column0 * otherColumn2.X + Column1 * otherColumn2.Y + Column2 * otherColumn2.Z + Column3 * otherColumn2.W;
			result.Column3 = Column0 * otherColumn3.X + Column1 * otherColumn3.Y + Column2 * otherColumn3.Z + Column3 * otherColumn3.W;

			return result;
		}

		Matrix4& Matrix4::operator+=(const Matrix4& other)
		{
			Column0 += other.Column0;
			Column1 += other.Column1;
			Column2 += other.Column2;
			Column3 += other.Column3;

			return *this;
		}

		Matrix4& Matrix4::operator-=(const Matrix4& other)
		{
			Column0 -= other.Column0;
			Column1 -= other.Column1;
			Column2 -= other.Column2;
			Column3 -= other.Column3;

			return *this;
		}

		Matrix4& Matrix4::operator*=(const Matrix4& other)
		{
			*this = *this * other;

			return *this;
		}

		Matrix4 Matrix4::operator*(float value) const
		{
			return Matrix4(
				Column0 * value,
				Column1 * value,
				Column2 * value,
				Column3 * value
			);
		}

		Matrix4& Matrix4::operator*=(float value)
		{
			Column0 *= value;
			Column1 *= value;
			Column2 *= value;
			Column3 *= value;

			return *this;
		}

		Matrix4 Matrix4::operator/(float value) const
		{
			return Matrix4(
				Column0 / value,
				Column1 / value,
				Column2 / value,
				Column3 / value
			);
		}

		Matrix4& Matrix4::operator/=(float value)
		{
			Column0 /= value;
			Column1 /= value;
			Column2 /= value;
			Column3 /= value;

			return *this;
		}

		Vector4 Matrix4::operator*(const Vector4& other) const
		{
			Vector4 result;

			result = Column0 * other[0] + Column1 * other[1] + Column2 * other[2] + Column3 * other[3];

			return result;
		}

		inline Matrix4 Matrix4::Identity()
		{
			return Matrix4(1.0f);
		}


		inline Matrix4 Matrix4::Scale(float scaleX, float scaleY, float scaleZ)
		{
			return Matrix4(scaleX, scaleY, scaleZ, 1.0f);
		}

		inline Matrix4 Matrix4::Scale(const Vector3& scale)
		{
			return Matrix4(scale.X, scale.Y, scale.Z, 1.0f);
		}

		inline Matrix4 Matrix4::Translation(float translationX, float translationY, float translationZ)
		{
			return Matrix4(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				translationX, translationY, translationZ, 1.0f
			);
		}

		inline Matrix4 Matrix4::Translation(const Vector3& translation)
		{
			return Matrix4(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				translation.X, translation.Y, translation.Z, 1.0f
			);
		}

		inline Matrix4 Matrix4::RotationTranslation(const Quaternion& quaternion, float translationX, float translationY, float translationZ)
		{
			Matrix4 result = Matrix4::Rotation(quaternion);

			result[3][0] = translationX;
			result[3][1] = translationY;
			result[3][2] = translationZ;

			return result;
		}

		inline Matrix4 Matrix4::RotationTranslation(const Vector3& axis, float angle, float translationX, float translationY, float translationZ)
		{
			Matrix4 result = Matrix4::Rotation(axis, angle);

			result[3][0] = translationX;
			result[3][1] = translationY;
			result[3][2] = translationZ;

			return result;
		}

		inline Matrix4 Matrix4::RotationTranslation(const Vector3& eulerAngles, float translationX, float translationY, float translationZ)
		{
			Matrix4 result = Matrix4::Rotation(eulerAngles);

			result[3][0] = translationX;
			result[3][1] = translationY;
			result[3][2] = translationZ;

			return result;
		}

		inline Matrix4 Matrix4::RotationTranslation(const Quaternion& quaternion, const Vector3& translation)
		{
			Matrix4 result = Matrix4::Rotation(quaternion);

			result[3][0] = translation.X;
			result[3][1] = translation.Y;
			result[3][2] = translation.Z;

			return result;
		}

		inline Matrix4 Matrix4::RotationTranslation(const Vector3& axis, float angle, const Vector3& translation)
		{
			Matrix4 result = Matrix4::Rotation(axis, angle);

			result[3][0] = translation.X;
			result[3][1] = translation.Y;
			result[3][2] = translation.Z;

			return result;
		}

		inline Matrix4 Matrix4::RotationTranslation(const Vector3& eulerAngles, const Vector3& translation)
		{
			Matrix4 result = Matrix4::Rotation(eulerAngles);

			result[3][0] = translation.X;
			result[3][1] = translation.Y;
			result[3][2] = translation.Z;

			return result;
		}

		inline Matrix4 Matrix4::ScaleTranslation(const Vector3& scale, const Vector3& translation)
		{
			Matrix4 result = Matrix4::Scale(scale);

			result[3][0] = translation.X;
			result[3][1] = translation.Y;
			result[3][2] = translation.Z;

			return result;
		}

		inline Matrix4 Matrix4::ScaleRotationTranslation(const Vector3& scale, const Vector3& axis, float angle, const Vector3& translation)
		{
			Matrix4 result = Rotation(axis, angle) * Scale(scale);
			result.Column3.X = translation.X;
			result.Column3.Y = translation.Y;
			result.Column3.Z = translation.Z;
			return result;
		}

		inline Matrix4 Matrix4::ScaleRotationTranslation(const Vector3& scale, const Quaternion& quaternion, const Vector3& translation)
		{
			Matrix4 result = Rotation(quaternion) * Scale(scale);
			result.Column3.X = translation.X;
			result.Column3.Y = translation.Y;
			result.Column3.Z = translation.Z;
			return result;
		}


		inline Matrix4 Matrix4::ScaleRotationTranslation(const Vector3& scale, const Vector3& eulerAngles, const Vector3& translation)
		{
			Matrix4 result = Rotation(eulerAngles) * Scale(scale);
			result.Column3.X = translation.X;
			result.Column3.Y = translation.Y;
			result.Column3.Z = translation.Z;
			return result;
		}

		inline Matrix4 Matrix4::ScaleRotationTranslation(const Vector3& scale, float eulerAngleX, float eulerAngleY, float eulerAngleZ, const Vector3& translation)
		{
			Matrix4 result = Rotation(eulerAngleX, eulerAngleY, eulerAngleZ) * Scale(scale);
			result.Column3.X = translation.X;
			result.Column3.Y = translation.Y;
			result.Column3.Z = translation.Z;
			return result;
		}



		inline Matrix4 Matrix4::OrthographicProjection()
		{
			return Matrix4::OrthographicProjection(-1.0f, 1.0f, -1.0f, 1.0f);
		}

		inline Matrix4 Matrix4::OrthographicProjection(float left, float right, float bottom, float top)
		{
			return Matrix4::OrthographicProjection(left, right, bottom, top, -1.0f, 1.0f);
		}

		inline Matrix4 Matrix4::OrthographicProjection(float left, float right, float bottom, float top, float near, float far)
		{
			return Matrix4(
				2.0f / (right - left), 0.0f, 0.0f, 0.0f,
				0.0f, 2.0f / (top - bottom), 0.0f, 0.0f,
				0.0f, 0.0f, -2.0f / (far - near), 0.0f,
				-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(far + near) / (far - near), 1.0f
			);
		}

		inline Matrix4 Matrix4::PerspectiveProjection(float left, float right, float bottom, float top, float near, float far)
		{
			return Matrix4(
				2.0f * near / (right - left), 0.0f, 0.0f, 0.0f,
				0.0f, 2.0f * near / (top - bottom), 0.0f, 0.0f,
				(right + left) / (right - left), (top + bottom) / (top - bottom), -(far + near) / (far - near), -1.0f,
				0.0f, 0.0f, -2.0f * far * near / (far - near), 0.0f
			);
		}

		inline Matrix4 Matrix4::PerspectiveProjection(float width, float height, float fieldOfViewDegrees, float near, float far)
		{
			float aspectRatio = width / height;
			float top = Math::Functions::Tan((fieldOfViewDegrees / 180.0f * Math::Constants::Pi) / 2.0f) * near;
			float bottom = -top;
			float right = top * aspectRatio;
			float left = -right;

			return PerspectiveProjection(left, right, bottom, top, near, far);
		}

		inline Matrix4 Matrix4::LookAt(const Vector3& eye, const Vector3& center, Vector3 up) //Up should not be reference
		{
			Vector3 zAxis = center - eye;
			zAxis.Normalize(); 
			up.Normalize();

			Vector3 xAxis = zAxis ^ up;
			Vector3 yAxis = xAxis.Normalized() ^ zAxis;

			return Matrix4(
				xAxis.X, yAxis.X, -zAxis.X, 0.0f, 
				xAxis.Y, yAxis.Y, -zAxis.Y, 0.0f, 
				xAxis.Z, yAxis.Z, -zAxis.Z, 0.0f, 
				xAxis.Dot(-eye), yAxis.Dot(-eye), (-zAxis).Dot(-eye), 1.0f
			);
		}

	}
}


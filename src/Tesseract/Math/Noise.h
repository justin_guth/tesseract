#pragma once

#include "Constants.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

#include "Tesseract/Core/Macros.h"
#include "Random.h"

#include <vector>
#include <functional>

namespace Tesseract::Math
{
    /*
    class Noise
    {
    public:
        virtual static float GetNoise(float p) = 0;
        virtual static float GetNoise(Vector2 p) = 0;
        virtual static float GetNoise(Vector3 p) = 0;
    }*/

    class PerlinNoise/*: public Noise*/
    {
    private:
        static float GetGradientVector(int32_t latticePoint);
        static Vector2 GetGradientVector(Vector2T<int32_t> latticePoint);
        static Vector3 GetGradientVector(Vector3T<int32_t> latticePoint);
        inline static float PerlInterpolate(float a, float b, float t) { return t * t * (3.0 - 2.0 * t) * (b - a) + a; };
        static float InterpolateGradientProducts(std::vector<float> products, std::vector<float> weights);

    public:
        static float GetNoise(float p);
        static float GetNoise(Vector2 p);
        static float GetNoise(Vector3 p);
    };

    template<typename T_Type>
    class PerlinTurbulence
    {
    public:
        struct TurbulenceParameters
        {
            std::function<float(float)> amplitude;
            T_Type frequency;
            float amplitudeFalloff;
            float frequencyIncrease;
            uint8_t octaves;
        };

        static float GetNoise(T_Type p, TurbulenceParameters params);
    };

    static const PerlinTurbulence<Vector3>::TurbulenceParameters testParams = {
        [](float altitude) -> float { return 2; },
        {4,4,4},
        2,
        2,
        4
    };

} // namespace Tesseract::Math

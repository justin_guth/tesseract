#include "Noise.h"

namespace Tesseract::Math
{
    float PerlinNoise::GetGradientVector(int32_t latticePoint)
    {
        return Random::RandomFloat(latticePoint);
    }

    Vector2 PerlinNoise::GetGradientVector(Vector2T<int32_t> latticePoint)
    {
        float phi = 2.0 * Constants::Pi * Random::RandomFloat(latticePoint);
        return { cos(phi), sin(phi) };
    }

    Vector3 PerlinNoise::GetGradientVector(Vector3T<int32_t> latticePoint)
    {
        float phi = 2.0 * Constants::Pi * Random::RandomFloat(latticePoint.XY());
        float theta = 2.0 * Constants::Pi * Random::RandomFloat(latticePoint.XZ());
        Vector3 gradient(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
        TESSERACT_CORE_ASSERT(gradient.MagnitudeSquared() <= 1 + 10 * Constants::Epsilon, "Gradient vector is too long");
        return gradient;
    }

    float PerlinNoise::GetNoise(float p)
    {
        int latticeCoord = floor(p);

        return InterpolateGradientProducts(
            { GetGradientVector(latticeCoord), GetGradientVector(latticeCoord + 1) },
            { p - latticeCoord });
    }

    float PerlinNoise::GetNoise(Vector2 p)
    {
        Vector2T<int32_t> latticeCoord = { (int32_t)floor(p.X), (int32_t)floor(p.Y) };
        std::vector<float> gradientProducts;

        for (int i = 0; i < 4; i++)
        {
            Vector2T<int32_t> latticePoint = latticeCoord + Vector2T<int32_t>(i & 1, i & 2);
            Vector2 offset = { p.X - latticePoint.X,p.Y - latticePoint.Y };
            gradientProducts.push_back(GetGradientVector(latticePoint).Dot(offset));
        }

        return InterpolateGradientProducts(gradientProducts, { p.X - latticeCoord.X, p.Y - latticeCoord.Y });
    }

    float PerlinNoise::GetNoise(Vector3 p)
    {
        Vector3T<int32_t> latticeCoord = { (int32_t)floor(p.X), (int32_t)floor(p.Y), (int32_t)floor(p.Z) };
        std::vector<float> gradientProducts;

        for (int i = 0; i < 8; i++)
        {
            Vector3T<int32_t> latticePoint = latticeCoord + Vector3T<int32_t>((i & 1) ? 1 : 0, (i & 2) ? 1 : 0, (i & 4) ? 1 : 0);
            Vector3 offset = { p.X - latticePoint.X, p.Y - latticePoint.Y, p.Z - latticePoint.Z };
            if (offset.MagnitudeSquared()) { offset.Normalize(); }
            gradientProducts.push_back(GetGradientVector(latticePoint).Dot(offset));
        }

        return InterpolateGradientProducts(gradientProducts, { p.X - latticeCoord.X, p.Y - latticeCoord.Y, p.Z - latticeCoord.Z });
    }

    float PerlinNoise::InterpolateGradientProducts(std::vector<float> products, std::vector<float> weights)
    {
        TESSERACT_CORE_ASSERT(1 << weights.size() == products.size(), "Faulty number of products or weights in perlin noise call");
        std::vector<std::vector<float>> intermediates = { products };
        for (float w : weights)
        {
            std::vector<float> nextReduction;
            for (int i = 0; i < intermediates.back().size(); i += 2)
            {
                nextReduction.push_back(PerlInterpolate(intermediates.back()[i], intermediates.back()[i + 1], w));
            }
            intermediates.push_back(nextReduction);
        }
        TESSERACT_CORE_ASSERT(intermediates.back().size() == 1, "Reducing gradient products went wrong");
        if (abs(intermediates.back().back()) > 1)
        {
            TESSERACT_CORE_ERROR("sth went wrong");
        }
        TESSERACT_CORE_ASSERT(abs(intermediates.back().back()) <= 1, "Noise value of " + std::to_string(intermediates.back().back()) + ", should be in [-1, 1]");
        return intermediates.back().back();
    }

    template<typename T_Type>
    float PerlinTurbulence<T_Type>::GetNoise(T_Type p, TurbulenceParameters params)
    {
        float noise = 1;

        float a = 1;
        float f = 1;

        for (int i = 0; i < params.octaves; i++)
        {
            noise += a * params.amplitude(noise) * PerlinNoise::GetNoise(p * params.frequency * f);
            a /= params.amplitudeFalloff;
            f *= params.frequencyIncrease;
        }
        return 0.0f;

    }

} // namespace Tesseract::Math

#include "Constants.h"

namespace Tesseract
{
    namespace Math
    {
        const float Constants::Pi = 3.14159265358979323846f;
        const float Constants::Epsilon = 0.0000001f;
    } 
}

#pragma once

#include <math.h>
#include <stdint.h>

#include "Constants.h"

#include "Tesseract/Core/Macros.h"

namespace Tesseract
{
	namespace Math
	{
		class Functions
		{
		public:

			static void Init();

			inline static float Sqrt(float value);
			inline static float Sin(float value);
			inline static float Cos(float value);
			inline static float Tan(float value);
			static float QuickSin(float value);
			static float QuickCos(float value);
			inline static float Abs(float value);
			inline static float Min(float value1, float value2);
			inline static float Max(float value1, float value2);
			inline static int32_t Min(int32_t value1, int32_t value2);
			inline static int32_t Max(int32_t value1, int32_t value2);
			inline static float Lerp(float value1, float value2, float alpha);
			inline static float Smoothstep(float value1, float value2, float alpha);
			inline static float Smootherstep(float value1, float value2, float alpha);
			inline static float Floor(float value);
			inline static float Ceil(float value);
			inline static float Fmod(float value, float mod);
			inline static float Clamp(float value, float min, float max);

			inline static float Random();
			inline static int RandomIntRange(int low, int high);


		private:

			const static uint32_t SineCosineBufferResolution = 8192;
			static float s_SineBuffer[SineCosineBufferResolution];
			static float s_CosineBuffer[SineCosineBufferResolution];

			static bool s_Initialized;
		};


		//== implementation =============//

		float Functions::Sqrt(float value)
		{
			return sqrt(value);
		}

		float Functions::Sin(float value)
		{
			return sin(value);
		}

		float Functions::Cos(float value)
		{
			return cos(value);
		}

		float Functions::Tan(float value)
		{
			return tan(value);
		}

		float Functions::Abs(float value)
		{
			return fabs(value);
		}

		float Functions::Min(float value1, float value2)
		{
			return fmin(value1, value2);
		}

		float Functions::Max(float value1, float value2)
		{
			return fmax(value1, value2);
		}

		int32_t Functions::Min(int32_t value1, int32_t value2)
		{
			return value1 < value2 ? value1 : value2;
		}

		int32_t Functions::Max(int32_t value1, int32_t value2)
		{
			return value1 > value2 ? value1 : value2;

		}

		inline float Functions::Lerp(float value1, float value2, float alpha)
		{
			return (value2 - value1) * alpha + value1;
		}

		inline float Functions::Smoothstep(float value1, float value2, float alpha)
		{
			return (value2 - value1) * (3.0 - alpha * 2.0) * alpha * alpha + value1;
		}
		inline float Functions::Smootherstep(float value1, float value2, float alpha)
		{
			return (value2 - value1) * ((alpha * (alpha * 6.0 - 15.0) + 10.0) * alpha * alpha * alpha) + value1;
		}
		inline float Functions::Floor(float value)
		{
			return floor(value);
		}
		inline float Functions::Ceil(float value)
		{
			return ceil(value);
		}

		inline float Functions::Fmod(float value, float mod)
		{
			return fmod(value, mod);
		}

		inline float Functions::Random()
		{
			return (float)std::rand() / RAND_MAX;
		}

		inline int Functions::RandomIntRange(int low, int high)
		{
			return Floor(Random() * (high - low)) - low;
		}


		inline float Functions::Clamp(float value, float min, float max)
		{
			if (value < min) return min;
			if (value > max) return max;
			return value;
		}


	}
}
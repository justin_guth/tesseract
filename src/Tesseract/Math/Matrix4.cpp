#include "Vector4.h"
#include "Matrix4.h"
#include "Functions.h"
#include "Constants.h"

namespace Tesseract
{
	namespace Math
	{
		Matrix4::Matrix4() :
			Column0(),
			Column1(),
			Column2(),
			Column3()
		{
		}

		Matrix4::Matrix4(const Matrix4& other) :
			Column0(other.Column0),
			Column1(other.Column1),
			Column2(other.Column2),
			Column3(other.Column3)
		{
		}

		Matrix4::Matrix4(float diagonal) :
			Column0(diagonal, 0, 0, 0),
			Column1(0, diagonal, 0, 0),
			Column2(0, 0, diagonal, 0),
			Column3(0, 0, 0, diagonal)
		{
		}

		Matrix4::Matrix4(float diagonal0, float diagonal1, float diagonal2, float diagonal3) :
			Column0(diagonal0, 0, 0, 0),
			Column1(0, diagonal1, 0, 0),
			Column2(0, 0, diagonal2, 0),
			Column3(0, 0, 0, diagonal3)
		{

		}

		Matrix4::Matrix4(const Vector4& diagonal) :
			Column0(diagonal.X, 0, 0, 0),
			Column1(0, diagonal.Y, 0, 0),
			Column2(0, 0, diagonal.Z, 0),
			Column3(0, 0, 0, diagonal.W)
		{
		}

		Matrix4::Matrix4(const Vector4& column0, const Vector4& column1, const Vector4& column2, const Vector4& column3) :
			Column0(column0),
			Column1(column1),
			Column2(column2),
			Column3(column3)
		{
		}

		Matrix4::Matrix4(float a00, float a01, float a02, float a03, float a10, float a11, float a12, float a13, float a20, float a21, float a22, float a23, float a30, float a31, float a32, float a33) :
			Column0(a00, a01, a02, a03),
			Column1(a10, a11, a12, a13),
			Column2(a20, a21, a22, a23),
			Column3(a30, a31, a32, a33)
		{
		}

		Matrix4 Matrix4::T() const
		{
			Matrix4 result;

			for (size_t resultColumn = 0; resultColumn < 4; resultColumn++)
			{
				for (size_t resultRow = 0; resultRow < 4; resultRow++)
				{
					result[resultColumn][resultRow] = (*this)[resultRow][resultColumn];
				}
			}

			return result;
		}

		Matrix4 Matrix4::Inverse() const
		{
			return Adjungate() / Determinant();
		}

		Matrix4 Matrix4::Adjungate() const
		{
			Matrix4 result;

			const Matrix4& m = *this;

			result[0][0] = (m[1][1] * m[2][2] * m[3][3]
							+ m[2][1] * m[3][2] * m[1][3]
							+ m[3][1] * m[1][2] * m[2][3]
							- m[3][1] * m[2][2] * m[1][3]
							- m[2][1] * m[1][2] * m[3][3]
							- m[1][1] * m[3][2] * m[2][3]);

			result[1][0] = -(m[1][0] * m[2][2] * m[3][3]
							 + m[2][0] * m[3][2] * m[1][3]
							 + m[3][0] * m[1][2] * m[2][3]
							 - m[3][0] * m[2][2] * m[1][3]
							 - m[2][0] * m[1][2] * m[3][3]
							 - m[1][0] * m[3][2] * m[2][3]);

			result[2][0] = (m[1][0] * m[2][1] * m[3][3]
							+ m[2][0] * m[3][1] * m[1][3]
							+ m[3][0] * m[1][1] * m[2][3]
							- m[3][0] * m[2][1] * m[1][3]
							- m[2][0] * m[1][1] * m[3][3]
							- m[1][0] * m[3][1] * m[2][3]);

			result[3][0] = -(m[1][0] * m[2][1] * m[3][2]
							 + m[2][0] * m[3][1] * m[1][2]
							 + m[3][0] * m[1][1] * m[2][2]
							 - m[1][2] * m[2][1] * m[3][0]
							 - m[2][2] * m[3][1] * m[1][0]
							 - m[3][2] * m[1][1] * m[2][0]);

			result[0][1] = -(m[0][1] * m[2][2] * m[3][3]
							 + m[2][1] * m[3][2] * m[0][3]
							 + m[3][1] * m[0][2] * m[2][3]
							 - m[0][3] * m[2][2] * m[3][1]
							 - m[2][3] * m[3][2] * m[0][1]
							 - m[3][3] * m[0][2] * m[2][1]);

			result[1][1] = (m[0][0] * m[2][2] * m[3][3]
							+ m[2][0] * m[3][2] * m[0][3]
							+ m[3][0] * m[0][2] * m[2][3]
							- m[0][3] * m[2][2] * m[3][0]
							- m[2][3] * m[3][2] * m[0][0]
							- m[3][3] * m[0][2] * m[2][0]);

			result[2][1] = -(m[0][0] * m[2][1] * m[3][3]
							 + m[2][0] * m[3][1] * m[0][3]
							 + m[3][0] * m[0][1] * m[2][3]
							 - m[0][3] * m[2][1] * m[3][0]
							 - m[2][3] * m[3][1] * m[0][0]
							 - m[3][3] * m[0][1] * m[2][0]);

			result[3][1] = (m[0][0] * m[2][1] * m[3][2]
							+ m[2][0] * m[3][1] * m[0][2]
							+ m[3][0] * m[0][1] * m[2][2]
							- m[0][2] * m[2][1] * m[3][0]
							- m[2][2] * m[3][1] * m[0][0]
							- m[3][2] * m[0][1] * m[2][0]);


			result[0][2] = (m[0][1] * m[1][2] * m[3][3]
							+ m[1][1] * m[3][2] * m[0][3]
							+ m[3][1] * m[0][2] * m[1][3]
							- m[0][3] * m[1][2] * m[3][1]
							- m[1][3] * m[3][2] * m[0][1]
							- m[3][3] * m[0][2] * m[1][1]);

			result[1][2] = -(m[0][0] * m[1][2] * m[3][3]
							 + m[1][0] * m[3][2] * m[0][3]
							 + m[3][0] * m[0][2] * m[1][3]
							 - m[0][3] * m[1][2] * m[3][0]
							 - m[1][3] * m[3][2] * m[0][0]
							 - m[3][3] * m[0][2] * m[1][0]);

			result[2][2] = (m[0][0] * m[1][1] * m[3][3]
							+ m[1][0] * m[3][1] * m[0][3]
							+ m[3][0] * m[0][1] * m[1][3]
							- m[0][3] * m[1][1] * m[3][0]
							- m[1][3] * m[3][1] * m[0][0]
							- m[3][3] * m[0][1] * m[1][0]);

			result[3][2] = -(m[0][0] * m[1][1] * m[3][2]
							 + m[1][0] * m[3][1] * m[0][2]
							 + m[3][0] * m[0][1] * m[1][2]
							 - m[0][2] * m[1][1] * m[3][0]
							 - m[1][2] * m[3][1] * m[0][0]
							 - m[3][2] * m[0][1] * m[1][0]);


			result[0][3] = -(m[0][1] * m[1][2] * m[2][3]
							 + m[1][1] * m[2][2] * m[0][3]
							 + m[2][1] * m[0][2] * m[1][3]
							 - m[0][3] * m[1][2] * m[2][1]
							 - m[1][3] * m[2][2] * m[0][1]
							 - m[2][3] * m[0][2] * m[1][1]);

			result[1][3] = (m[0][0] * m[1][2] * m[2][3]
							+ m[1][0] * m[2][2] * m[0][3]
							+ m[2][0] * m[0][2] * m[1][3]
							- m[0][3] * m[1][2] * m[2][0]
							- m[1][3] * m[2][2] * m[0][0]
							- m[2][3] * m[0][2] * m[1][0]);

			result[2][3] = -(m[0][0] * m[1][1] * m[2][3]
							 + m[1][0] * m[2][1] * m[0][3]
							 + m[2][0] * m[0][1] * m[1][3]
							 - m[0][3] * m[1][1] * m[2][0]
							 - m[1][3] * m[2][1] * m[0][0]
							 - m[2][3] * m[0][1] * m[1][0]);

			result[3][3] = (m[0][0] * m[1][1] * m[2][2]
							+ m[1][0] * m[2][1] * m[0][2]
							+ m[2][0] * m[0][1] * m[1][2]
							- m[0][2] * m[1][1] * m[2][0]
							- m[1][2] * m[2][1] * m[0][0]
							- m[2][2] * m[0][1] * m[1][0]);

			return result;
		}

		float Matrix4::Determinant() const
		{
			float result = 0.0f;

			// Assume Matrix most likely has the following structure:
			// x x x x
			// x x x x
			// x x x x
			// 0 0 0 1
			// Therefore, perform laplace expansion along last row.

			const Matrix4& m = (*this);

			if (Functions::Abs(m[0][3]) > Constants::Epsilon)
			{
				result -= m[0][3] * (
					m[1][0] * m[2][1] * m[3][2]
					+ m[2][0] * m[3][1] * m[1][2]
					+ m[3][0] * m[1][1] * m[2][2]
					- m[1][2] * m[2][1] * m[3][0]
					- m[2][2] * m[3][1] * m[1][0]
					- m[3][2] * m[1][1] * m[2][0]
					);
			}

			if (Functions::Abs(m[1][3]) > Constants::Epsilon)
			{
				result += m[1][3] * (
					m[0][0] * m[2][1] * m[3][2]
					+ m[2][0] * m[3][1] * m[0][2]
					+ m[3][0] * m[0][1] * m[2][2]
					- m[0][2] * m[2][1] * m[3][0]
					- m[2][2] * m[3][1] * m[0][0]
					- m[3][2] * m[0][1] * m[2][0]
					);

			}

			if (Functions::Abs(m[2][3]) > Constants::Epsilon)
			{
				result -= m[2][3] * (
					m[0][0] * m[1][1] * m[3][2]
					+ m[1][0] * m[3][1] * m[0][2]
					+ m[3][0] * m[0][1] * m[1][2]
					- m[0][2] * m[1][1] * m[3][0]
					- m[1][2] * m[3][1] * m[0][0]
					- m[3][2] * m[0][1] * m[1][0]
					);
			}

			if (Functions::Abs(m[3][3]) > Constants::Epsilon)
			{
				result += m[3][3] * (
					m[0][0] * m[1][1] * m[2][2]
					+ m[1][0] * m[2][1] * m[0][2]
					+ m[2][0] * m[0][1] * m[1][2]
					- m[0][2] * m[1][1] * m[2][0]
					- m[1][2] * m[2][1] * m[0][0]
					- m[2][2] * m[0][1] * m[1][0]
					);
			}

			return result;
		}



		Matrix4 Matrix4::Rotation(const Quaternion& quaternion)
		{
			float aA = quaternion.A * quaternion.A;
			float aB = quaternion.A * quaternion.B;
			float aC = quaternion.A * quaternion.C;
			float aD = quaternion.A * quaternion.D;

			float bB = quaternion.B * quaternion.B;
			float bC = quaternion.B * quaternion.C;
			float bD = quaternion.B * quaternion.D;

			float cC = quaternion.C * quaternion.C;
			float cD = quaternion.C * quaternion.D;

			float dD = quaternion.D * quaternion.D;

			return Matrix4(
				2 * (aA + bB) - 1.0f,
				2 * (bC + aD),
				2 * (bD - aC),
				0.0f,

				2 * (bC - aD),
				2 * (aA + cC) - 1.0f,
				2 * (cD + aB),
				0.0f,

				2 * (bD + aC),
				2 * (cD - aB),
				2 * (aA + dD) - 1.0f,
				0.0f,

				0.0f,
				0.0f,
				0.0f,
				1.0f
			);
		}

		Matrix4 Matrix4::Rotation(Vector3 axis, float angle)
		{
			Matrix4 result;
			axis.Normalize();

			float cosTheta = Functions::QuickCos(angle);
			float sinTheta = Functions::QuickSin(angle);
			float sinThetaZ = sinTheta * axis.Z;
			float sinThetaY = sinTheta * axis.Y;
			float sinThetaX = sinTheta * axis.X;

			float oneMinusCosTheta = 1 - cosTheta;
			float uxUy = axis.X * axis.Y;
			float uxUz = axis.X * axis.Z;
			float uyUz = axis.Y * axis.Z;

			float oneMinusCosThetaYZ = uyUz * oneMinusCosTheta;
			float oneMinusCosThetaXY = uxUy * oneMinusCosTheta;
			float oneMinusCosThetaXZ = uxUz * oneMinusCosTheta;

			return Matrix4(
				cosTheta + (axis.X * axis.X) * oneMinusCosTheta,
				oneMinusCosThetaXY + sinThetaZ,
				oneMinusCosThetaXZ - sinThetaY,
				0.0f,

				oneMinusCosThetaXY - sinThetaZ,
				cosTheta + axis.Y * axis.Y * oneMinusCosTheta,
				oneMinusCosThetaYZ + sinThetaX,
				0.0f,

				oneMinusCosThetaXZ + sinThetaY,
				oneMinusCosThetaYZ - sinThetaX,
				cosTheta + axis.Z * axis.Z * oneMinusCosTheta,
				0.0f,

				0.0f,
				0.0f,
				0.0f,
				1.0f
			);
		}

		Matrix4 Matrix4::Rotation(const Vector3& eulerAngles)
		{
			return Rotation(eulerAngles.X, eulerAngles.Y, eulerAngles.Z);
		}


		Matrix4 Matrix4::Rotation(float eulerAngleX, float eulerAngleY, float eulerAngleZ)
		{
			// Rotate around x-axis:
			Matrix4 result = Matrix4::Rotation(Vector3::UnitX(), eulerAngleX);

			// Rotate around y-axis:
			Vector3 axisY = (result * Vector3::UnitY().HomogenizedVector()).XYZ();
			result = Matrix4::Rotation(axisY, eulerAngleY) * result;

			// Rotate around z-axis:
			Vector3 axisZ = (result * Vector3::UnitZ().HomogenizedVector()).XYZ();
			result = Matrix4::Rotation(axisZ, eulerAngleZ) * result;

			return result;
		}



	}
}
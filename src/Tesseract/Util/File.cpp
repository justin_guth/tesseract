#include "File.h"

#include "Tesseract/Core/Macros.h"

#include <fstream>
#include <filesystem>

namespace Tesseract
{
	namespace Util
	{
		std::string File::ReadFileContents(const std::string& filePath)
		{
			std::ifstream inputStream(filePath);

			bool exists = std::filesystem::exists(
				std::filesystem::current_path() / filePath
			);

			TESSERACT_CORE_ASSERT(exists, "File {} does not exist!", filePath);

			if (!exists)
			{
				return "";
			}

			std::string contents;
			inputStream.seekg(0, std::ios::end);
			contents.reserve(inputStream.tellg());
			inputStream.seekg(0, std::ios::beg);

			contents.assign((std::istreambuf_iterator<char>(inputStream)),
							std::istreambuf_iterator<char>());

			return contents;
		}

		void File::WriteFile(const std::string& filePath, const std::string& fileContents)
		{
			std::ofstream out(filePath);
			out << fileContents;
			out.close();
		}


		std::string GetCWD()
		{
			return std::filesystem::current_path().string();
		}
	}
}
#pragma once

#include <stdint.h>
#include "Tesseract/Memory/DynamicSlotAllocator.h"

namespace Tesseract
{
	namespace Util
	{
		template<typename T_Type>
		class LinkedList
		{
		public:

			class Node
			{
				friend class Iterator;
				friend class LinkedList;

			public:

				Node(const T_Type& data) : m_Data(data) {}

				inline Node& GetPrevious() { return *m_Previous; }
				inline Node& GetNext() { return *m_Next; }
				inline T_Type& GetData() { return m_Data; }

			private:

				Node* m_Previous = nullptr;
				Node* m_Next = nullptr;
				T_Type m_Data;
			};

			class Iterator
			{
			public:

				Iterator(Node* node) : m_Current(node) {}

				T_Type& operator*();
				T_Type* operator->();

				Iterator& operator++();
				Iterator operator++(int);

				friend bool operator== (const Iterator& a, const Iterator& b) { return a.m_Current == b.m_Current; };
				friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_Current != b.m_Current; };

			private:

				Node* m_Current;
			};

		public:

			LinkedList();
			LinkedList(const LinkedList& other);
			LinkedList(LinkedList&& other) noexcept;
			~LinkedList();

			void operator=(const LinkedList& other);

			void PushFront(const T_Type& value);
			void PushBack(const T_Type& value);

			T_Type PopFront();
			T_Type PopBack();

			inline Iterator begin() const { return Iterator(m_Head); }
			inline Iterator end() const { return Iterator(nullptr); }

			bool Remove(const T_Type& value);
			bool Empty() const;


		private:

			void Cut(Node* node);

		private:

			uint32_t m_Count = 0;
			Node* m_Head = nullptr;
			Node* m_Tail = nullptr;

		private:

			static Memory::DynamicSlotAllocator<Node> s_Allocator;

		};

		template<typename T_Type>
		Memory::DynamicSlotAllocator<typename LinkedList<T_Type>::Node> LinkedList<T_Type>::s_Allocator;

		template<typename T_Type>
		inline LinkedList<T_Type>::LinkedList()
		{
		}

		template<typename T_Type>
		inline LinkedList<T_Type>::LinkedList(const LinkedList<T_Type>& other)
		{
			if (other.m_Head == nullptr)
			{
				return;
			}

			{ // Free current TODO: move since it is the same code as in destructor
				// only if different
				// TODO: Check if this is certain enough
				if (m_Head != other.m_Head)
				{
					Node* current = m_Head;

					while (current != nullptr)
					{
						s_Allocator.FreeSlot(current);
						current = current->m_Next;
					}
				}
			}

			m_Count = other.m_Count;

			Node* current = other.m_Head;
			Node* copy = s_Allocator.Allocate(Node(current->m_Data));
			m_Head = copy;

			current = current->m_Next;

			while (current != nullptr)
			{
				Node* nextCopy = s_Allocator.Allocate(Node(current->m_Data));
				copy->m_Next = nextCopy;
				nextCopy->m_Previous = copy;

				copy = nextCopy;
				current = current->m_Next;
			}

			m_Tail = copy;
		}

		template<typename T_Type>
		inline LinkedList<T_Type>::LinkedList(LinkedList<T_Type>&& other) noexcept :
			m_Head(other.m_Head),
			m_Tail(other.m_Tail)
		{
			other.m_Head = nullptr;
			other.m_Tail = nullptr;
		}



		template<typename T_Type>
		inline LinkedList<T_Type>::~LinkedList()
		{
			Node* current = m_Head;

			while (current != nullptr)
			{
				s_Allocator.FreeSlot(current);
				current = current->m_Next;
			}
		}


		template<typename T_Type>
		void LinkedList<T_Type>::operator=(const LinkedList<T_Type>& other)
		{
			{ // Free current TODO: move since it is the same code as in destructor
				// only if different
				// TODO: Check if this is certain enough
				if (m_Head != other.m_Head)
				{
					Node* current = m_Head;

					while (current != nullptr)
					{
						s_Allocator.FreeSlot(current);
						current = current->m_Next;
					}
				}
			}

			m_Count = other.m_Count;

			{ // Copy TODO: move since it is the same code as in copy constructor
				if (other.m_Head == nullptr)
				{
					m_Head = nullptr;
					m_Tail = nullptr;
					return;
				}

				Node* current = other.m_Head;
				Node* copy = s_Allocator.Allocate(Node(current->m_Data));
				m_Head = copy;

				current = current->m_Next;

				while (current != nullptr)
				{
					Node* nextCopy = s_Allocator.Allocate(Node(current->m_Data));
					copy->m_Next = nextCopy;
					nextCopy->m_Previous = copy;

					copy = nextCopy;
					current = current->m_Next;
				}

				m_Tail = copy;
			}
		}


		template<typename T_Type>
		inline void LinkedList<T_Type>::PushFront(const T_Type& value)
		{
			m_Count++;

			Node* newNode = s_Allocator.Allocate(Node(value));

			if (m_Head == nullptr)
			{
				m_Head = newNode;
				m_Tail = newNode;
			}
			else
			{
				newNode->m_Next = m_Head;
				m_Head->m_Previous = newNode;
				m_Head = newNode;
			}
		}

		template<typename T_Type>
		inline void LinkedList<T_Type>::PushBack(const T_Type& value)
		{
			m_Count++;

			Node* newNode = s_Allocator.Allocate(Node(value));

			if (m_Tail == nullptr)
			{
				m_Head = newNode;
				m_Tail = newNode;
			}
			else
			{
				newNode->m_Previous = m_Tail;
				m_Tail->m_Next = newNode;
				m_Tail = newNode;
			}
		}

		template<typename T_Type>
		inline T_Type LinkedList<T_Type>::PopFront()
		{
			return T_Type();
		}
		template<typename T_Type>
		inline T_Type LinkedList<T_Type>::PopBack()
		{
			return T_Type();
		}

		template<typename T_Type>
		inline bool LinkedList<T_Type>::Remove(const T_Type& value)
		{
			Node* current = m_Head;

			while (current != nullptr)
			{
				if (current->m_Data == value)
				{
					Cut(current);
					return true;
				}

				current = current->m_Next;
			}

			return false;
		}

		template<typename T_Type>
		inline bool LinkedList<T_Type>::Empty() const
		{
			return m_Head == nullptr;
		}


		template<typename T_Type>
		inline void LinkedList<T_Type>::Cut(Node* node)
		{
			if (node == nullptr)
			{
				return;
			}

			if (node == m_Head)
			{
				m_Head = node->m_Next;
			}

			if (node == m_Tail)
			{
				m_Tail = node->m_Previous;
			}

			if (node->m_Previous != nullptr)
			{
				node->m_Previous->m_Next = node->m_Next;
			}

			if (node->m_Next != nullptr)
			{
				node->m_Next->m_Previous = node->m_Previous;
			}

			m_Count--;
			s_Allocator.FreeSlot(node);
		}

		template<typename T_Type>
		inline T_Type* LinkedList<T_Type>::Iterator::operator->()
		{
			return &m_Current->m_Data;
		}

		template<typename T_Type>
		inline T_Type& LinkedList<T_Type>::Iterator::operator*()
		{
			return m_Current->m_Data;
		}

		template<typename T_Type>
		inline LinkedList<T_Type>::Iterator& LinkedList<T_Type>::Iterator::operator++()
		{
			m_Current = m_Current->m_Next;

			return *this;
		}

		template<typename T_Type>
		inline LinkedList<T_Type>::Iterator LinkedList<T_Type>::Iterator::operator++(int)
		{
			Iterator tmp = *this;

			++(*this);

			return tmp;
		}
	}
}
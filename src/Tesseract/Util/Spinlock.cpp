#include "Spinlock.h"

#include <thread>

namespace Tesseract
{
	namespace Util
	{
		Spinlock::Spinlock(Mode mode) :
			m_Flag(),
			m_Mode(mode)
		{
		}

		void Spinlock::Lock()
		{
			while(!AttemptLock())
			{
				if (m_Mode == Mode::YieldOnAcquisitionMiss)
				{
					std::this_thread::yield();
				}
			}
		}

		void Spinlock::Unlock()
		{
			m_Flag.clear(std::memory_order_release);
		}

		bool Spinlock::AttemptLock()
		{
			bool alreadyLocked = m_Flag.test_and_set(std::memory_order_acquire);
			return !alreadyLocked;
		}
	}
}
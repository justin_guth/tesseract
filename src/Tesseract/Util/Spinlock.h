#pragma once

#include <atomic>

namespace Tesseract
{
	namespace Util
	{
		class Spinlock
		{
		public:

			enum class Mode
			{
				YieldOnAcquisitionMiss = 0,
				SpinOnAcquisitionMiss
			};

		public:

			Spinlock(Mode mode = Mode::SpinOnAcquisitionMiss);

			void Lock();
			void Unlock();

		private:

			bool AttemptLock();

		private:

			std::atomic_flag m_Flag;
			const Mode m_Mode;
		};
	}
}

#pragma once

#include <stdint.h>


typedef uint32_t type_hash;

namespace Tesseract
{
	namespace Util
	{
		class TypeHash
		{
		public:

			template <typename T_Type>
			static type_hash Get()
			{
				static type_hash value = s_Count++;
				return value;
			}

		private:

			static type_hash s_Count;

		};
	}
}
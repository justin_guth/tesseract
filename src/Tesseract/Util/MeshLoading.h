#pragma once

#include <tuple>
#include <vector>

#include "Tesseract/Core/Geometry.h"
#include "Tesseract/Graphics/BufferLayout.h"
#include "Tesseract/Graphics/ElementBuffer.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Util
    {

        Ref<Core::Geometry> LoadObjFileData(const std::string& filePath, bool reverse = false);

    } // namespace IO

} // namespace Tesseract


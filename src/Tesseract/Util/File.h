#pragma once

#include <string>

namespace Tesseract
{
	namespace Util
	{
		class File
		{
		public:

			static std::string ReadFileContents(const std::string& filePath);
			static void WriteFile(const std::string& filePath, const std::string& fileContents);

		};

		std::string GetCWD();
	}
}
#pragma once

/**
* @file Format.h
*/

#include <string>
#ifdef __linux__
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#define _TESSERACT_FORMAT_FUNCTION fmt::vformat 
#define _TESSERACT_MAKE_FORMAT_ARGS_FUNCTION fmt::make_format_args 
#else
#include <format>
#define _TESSERACT_FORMAT_FUNCTION std::vformat
#define _TESSERACT_MAKE_FORMAT_ARGS_FUNCTION std::make_format_args 
#endif
namespace Tesseract
{
	namespace Util
	{
		/**
		* Formats a given @p format string with the given parameters @p params.
		*
		* @param[in] format The format
		* @param[in] params The parameters to be filled into the @p format
		*
		* @return The formatted string
		*/
		template <typename ...T_Param>
		std::string Format(const char* format, T_Param&& ... params);



		template<typename ...T_Param>
		std::string Format(const char* format, T_Param && ...params)
		{
			return _TESSERACT_FORMAT_FUNCTION(format, _TESSERACT_MAKE_FORMAT_ARGS_FUNCTION(params...));
		}
	}
}
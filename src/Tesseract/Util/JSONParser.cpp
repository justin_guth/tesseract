#include "JSONParser.h"
#include "DynamicData.h"
#include "File.h"

#include "Tesseract/Core/Macros.h"

#include <string>

namespace Tesseract
{
    namespace Util
    {

        JSONParser::JSONParser(const std::string& string):
            m_String(string),
            m_Position(0)
        {

        }

        DynamicData JSONParser::Parse()
        {
            return ParseValue();
        }


        DynamicData JSONParser::ParseJSONString(const std::string& string)
        {
            return JSONParser(string).Parse();
        }


        DynamicData JSONParser::LoadJSONFile(const std::string& filePath)
        {

            return JSONParser(File::ReadFileContents(filePath)).Parse();
        }

        DynamicData JSONParser::ParseString()
        {
            TESSERACT_CORE_ASSERT(m_String.at(m_Position) == '"', "Unexpected token '{}', expected '\"'", m_String.at(m_Position));

            m_Position++;

            std::string result = "";


            // REVIEW: @Justin also maybe have a mode where newline characters are not allowed
            while (m_String.at(m_Position) != '"')
            {
                bool expectEscape = false;

                if (m_String.at(m_Position) == '\\')
                {
                    expectEscape = true;
                    m_Position++;
                }

                if (expectEscape)
                {
                    switch (m_String.at(m_Position))
                    {
                        case '"': result += '"'; break;
                        case '\\': result += '\\'; break;
                        case '/': result += '/'; break;
                        case 'b': result += '\b'; break;
                        case 'f': result += '\f'; break;
                        case 'n': result += '\n'; break;
                        case 'r': result += '\r'; break;
                        case 't': result += '\t'; break;
                            // TODO: unicode
                        // case 'u': result += '\u'; break;
                        default:  TESSERACT_CORE_ASSERT(false, "Unsupported escape sequence provided!"); break;
                    }
                    m_Position++;
                }
                else
                {
                    result += m_String.at(m_Position);
                    m_Position++;
                }
            }
            m_Position++;

            return result;
        }

        DynamicData JSONParser::ParseNumber()
        {
            std::string stringNumber = "";



            if (m_String.at(m_Position) == '-')
            {
                stringNumber += '-';
                m_Position++;
            }

            bool decimalPointPresent = false;
            int exponent = 0;
            char currentChar = m_String.at(m_Position);

            while (currentChar >= 48 && currentChar <= 57 || currentChar == '.')
            {
                if (currentChar == '.')
                {
                    TESSERACT_CORE_ASSERT(!decimalPointPresent, "Unexpected token: '.' (decimal point already set)!");
                    decimalPointPresent = true;
                }

                stringNumber += currentChar;
                m_Position++;
                currentChar = m_String.at(m_Position);
            }

            if (currentChar == 'e' || currentChar == 'E')
            {
                std::string exponent = "";

                m_Position++;
                currentChar = m_String.at(m_Position);

                int sign = 1;
                if (currentChar == '+' || currentChar == '-')
                {
                    sign = currentChar == '+' ? 1 : -1;
                }

                m_Position++;
                currentChar = m_String.at(m_Position);

                std::string exponentString = "";

                while (currentChar >= 48 && currentChar <= 57)
                {
                    exponentString += currentChar;
                    m_Position++;
                    currentChar = m_String.at(m_Position);
                }

                exponent = sign * std::stoi(exponentString);
            }

            if (decimalPointPresent)
            {
                double doubleValue = std::stod(stringNumber);
                return doubleValue * std::pow(10.0, (double)exponent);
            }
            else
            {
                int64_t intValue = std::stol(stringNumber);

                if (exponent < 0)
                {
                    return (double)intValue * std::pow(10.0, (double)exponent);
                }
                else
                {
                    return intValue * (int64_t)std::pow(10, exponent);
                }
            }


        }

        DynamicData JSONParser::ParseValue()
        {
            SkipWhitespace();

            char currentChar = m_String.at(m_Position);

            DynamicData result;


            if (currentChar == '"')
            {
                result = ParseString();
            }
            else if (currentChar >= 48 && currentChar <= 57 || currentChar == '.' || currentChar == '-')
            {
                result = ParseNumber();
            }
            else if (currentChar == '{')
            {
                result = ParseObject();
            }
            else if (currentChar == '[')
            {
                result = ParseArray();
            }
            else if (currentChar == 'f')
            {
                result = ParseFalse();
            }
            else if (currentChar == 't')
            {
                result = ParseTrue();
            }
            else if (currentChar == 'n')
            {
                result = ParseNull();
            }

            SkipWhitespace();

            return result;

        }


        DynamicData JSONParser::ParseTrue()
        {
            const char* trueLiteral = "true";

            for (int i = 0; i < 4; i++)
            {
                char currentChar = m_String.at(m_Position);

                TESSERACT_CORE_ASSERT(currentChar == trueLiteral[i], "Unexpected token: '{}', expected '{}'", currentChar, trueLiteral[i]);

                m_Position++;
            }

            return true;
        }

        DynamicData JSONParser::ParseFalse()
        {
            const char* falseLiteral = "false";

            for (int i = 0; i < 5; i++)
            {
                char currentChar = m_String.at(m_Position);

                TESSERACT_CORE_ASSERT(currentChar == falseLiteral[i], "Unexpected token: '{}', expected '{}'", currentChar, falseLiteral[i]);

                m_Position++;
            }

            return false;
        }

        DynamicData JSONParser::ParseNull()
        {
            const char* nullLiteral = "null";

            for (int i = 0; i < 4; i++)
            {
                char currentChar = m_String.at(m_Position);

                TESSERACT_CORE_ASSERT(currentChar == nullLiteral[i], "Unexpected token: '{}', expected '{}'", currentChar, nullLiteral[i]);

                m_Position++;
            }

            return DynamicData::Null;
        }

        DynamicData JSONParser::ParseArray()
        {
            char currentChar = m_String.at(m_Position);

            TESSERACT_CORE_ASSERT(currentChar == '[', "Unexpected token: '{}', expected '['!", currentChar);

            m_Position++;

            DynamicData result;
            result.MakeArray();

            SkipWhitespace();

            currentChar = m_String.at(m_Position);

            if (currentChar == ']')
            {
                m_Position++;
                return result;
            }

            while (true)
            {

                result << ParseValue();

                currentChar = m_String.at(m_Position);
                if (currentChar == ']')
                {
                    m_Position++;
                    return result;
                }
                TESSERACT_CORE_ASSERT(m_String.at(m_Position) == ',', "Unexpected token: '{}', expected ','!", currentChar);
                m_Position++;
            }

        }

        DynamicData JSONParser::ParseObject()
        {
            char currentChar = m_String.at(m_Position);

            TESSERACT_CORE_ASSERT(currentChar == '{', "Unexpected token: '{}', expected '{'!", currentChar);

            m_Position++;

            DynamicData result;
            result.MakeDict();


            while (true)
            {
                SkipWhitespace();
                if (m_String.at(m_Position) == '}')
                {
                    return result;
                }
                DynamicData key = ParseString();
                SkipWhitespace();
                TESSERACT_CORE_ASSERT(m_String.at(m_Position) == ':', "Unexpected token: '{}', expected ':'!", currentChar);
                m_Position++;
                result[key.GetString()] = ParseValue();

                if (m_String.at(m_Position) == '}')
                {
                    m_Position++;
                    break;
                }

                TESSERACT_CORE_ASSERT(m_String.at(m_Position) == ',', "Unexpected token: '{}', expected ',' or '}'!", currentChar);
                m_Position++;
            }

            return result;
        }


        void JSONParser::SkipWhitespace()
        {
            if (m_Position >= m_String.size())
            {
                return;
            }

            char currentChar = m_String.at(m_Position);
            while (
                currentChar == ' '
                || currentChar == '\t'
                || currentChar == '\n'
                || currentChar == '\r'
                )
            {
                m_Position++;
                if (m_Position == m_String.size()) { return; }
                currentChar = m_String.at(m_Position);
            }
        }


    } // namespace Util

} // namespace Tesseract

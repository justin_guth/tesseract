#pragma once


#include <string>
#include <vector>
#include <unordered_map>
#include <initializer_list>
#include <tuple>

#include "Tesseract/Math/Vector2.h"
#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Math/Vector4.h"

namespace Tesseract
{
    namespace Util
    {
        class DynamicData
        {
        public:

            struct NullType {};

            enum class Type
            {
                Undefined = 0,
                Null,
                Dict,
                Array,
                String,
                Bool,
                Integer,
                Float
            };

            Type Type;

            union
            {
                void* Pointer;
                std::unordered_map<std::string, DynamicData>* DictPointer;
                std::vector<DynamicData>* ArrayPointer;
                std::string* StringPointer;
                bool* BoolPointer;
                int64_t* IntPointer;
                double* FloatPointer;
            };

            DynamicData();
            DynamicData(const DynamicData& other);
            ~DynamicData();

            DynamicData(int64_t value);
            DynamicData(int value);
            DynamicData(double value);
            DynamicData(bool value);
            DynamicData(std::string value);
            DynamicData(const char* value);
            DynamicData(NullType value);

            DynamicData& operator=(const DynamicData& other);

            DynamicData& operator<<(const DynamicData& other);

            DynamicData& operator[](const std::string& key);
            DynamicData& operator[](const char* key);
            DynamicData& operator[](const int index);

            const DynamicData& operator[](const std::string& key) const;
            const DynamicData& operator[](const char* key) const;
            const DynamicData& operator[](const int index) const;

            DynamicData& MakeDict();
            DynamicData& MakeArray();

            double GetFloat() const;
            std::string GetString() const;
            std::vector<DynamicData>& GetArray() const;
            std::unordered_map<std::string, DynamicData>& GetDict() const;
            bool GetBool() const;
            int64_t GetInt() const;

            Math::Vector2 GetVector2() const;
            Math::Vector3 GetVector3() const;
            Math::Vector4 GetVector4() const;

            std::string ToJSON() const;


            bool operator==(const char* other) const;
            bool operator!=(const char* other) const;

            bool operator==(const NullType& other) const;
            bool operator!=(const NullType& other) const;

        public:

            static NullType Null;

        private:

            void FreeMemoryIfDefined();

        };






    } // namespace Util

} // namespace Tesseract

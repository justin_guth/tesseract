#include "DynamicData.h"


#include "Tesseract/Core/Macros.h"

#include "Tesseract/Logging/Logger.h"

namespace Tesseract
{
    namespace Util
    {

        DynamicData::NullType DynamicData::Null;


        DynamicData::DynamicData() :
            Type(Type::Undefined),
            Pointer(nullptr)
        {

        }

        DynamicData::DynamicData(const DynamicData& other) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = other.Type;

            switch (Type)
            {
                case Type::String: StringPointer = new std::string(*other.StringPointer); break;
                case Type::Array: ArrayPointer = new std::vector(*other.ArrayPointer); break;
                case Type::Dict: DictPointer = new std::unordered_map<std::string, DynamicData>(*other.DictPointer); break;
                case Type::Integer: IntPointer = new int64_t; *IntPointer = *other.IntPointer; break;
                case Type::Bool: BoolPointer = new bool; *BoolPointer = *other.BoolPointer; break;
                case Type::Float: FloatPointer = new double; *FloatPointer = *other.FloatPointer; break;
                default: break;
            }
        }


        DynamicData::~DynamicData()
        {
            FreeMemoryIfDefined();

        }

        void DynamicData::FreeMemoryIfDefined()
        {
            if (Type == Type::Undefined)
            {
                return;
            }

            switch (Type)
            {
                case Type::String: delete StringPointer; break;
                case Type::Array: delete ArrayPointer; break;
                case Type::Dict: delete DictPointer; break;
                case Type::Integer: delete IntPointer; break;
                case Type::Bool: delete BoolPointer; break;
                case Type::Float: delete FloatPointer; break;
                default: break;
            }

        }


        DynamicData& DynamicData::operator=(const DynamicData& other)
        {
            FreeMemoryIfDefined();

            Type = other.Type;

            switch (Type)
            {
                case Type::String: StringPointer = new std::string(*other.StringPointer); break;
                case Type::Array: ArrayPointer = new std::vector(*other.ArrayPointer); break;
                case Type::Dict: DictPointer = new std::unordered_map<std::string, DynamicData>(*other.DictPointer); break;
                case Type::Integer: IntPointer = new int64_t; *IntPointer = *other.IntPointer; break;
                case Type::Bool: BoolPointer = new bool; *BoolPointer = *other.BoolPointer; break;
                case Type::Float: FloatPointer = new double; *FloatPointer = *other.FloatPointer; break;
                default: break;
            }

            return *this;
        }


        /*
            TOMLData& TOMLData::operator=(std::initializer_list<TOMLData>&& value)
            {
                FreeMemoryIfDefined();

                Type = Type::Array;
                ArrayPointer = new std::vector<TOMLData>;


                return *this;
            }
        */

        DynamicData::DynamicData(int64_t value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::Integer;
            IntPointer = new int64_t;
            *IntPointer = value;
        }


        DynamicData::DynamicData(int value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::Integer;
            IntPointer = new int64_t;
            *IntPointer = value;
        }


        DynamicData::DynamicData(double value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::Float;
            FloatPointer = new double;
            *FloatPointer = value;
        }


        DynamicData::DynamicData(bool value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::Bool;
            BoolPointer = new bool;
            *BoolPointer = value;
        }


        DynamicData::DynamicData(std::string value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::String;
            StringPointer = new std::string(value);
        }

        DynamicData::DynamicData(const char* value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::String;
            StringPointer = new std::string(value);
        }

        DynamicData::DynamicData(DynamicData::NullType value) : DynamicData()
        {
            FreeMemoryIfDefined();

            Type = Type::Null;
        }


        DynamicData& DynamicData::operator<<(const DynamicData& other)
        {
            if (Type != Type::Array)
            {
                FreeMemoryIfDefined();
                ArrayPointer = new std::vector<DynamicData>;
                Type = Type::Array;
            }

            ArrayPointer->push_back(other);

            return *this;
        }

        DynamicData& DynamicData::operator[](const std::string& key)
        {
            if (Type != Type::Dict)
            {
                FreeMemoryIfDefined();
                DictPointer = new std::unordered_map<std::string, DynamicData>;
                Type = Type::Dict;
            }

            if (!DictPointer->contains(key))
            {
                DictPointer->operator[](key) = DynamicData();
            }


            return DictPointer->operator[](key);
        }

        DynamicData& DynamicData::operator[](const char* key)
        {
            if (Type != Type::Dict)
            {
                FreeMemoryIfDefined();
                DictPointer = new std::unordered_map<std::string, DynamicData>;
                Type = Type::Dict;
            }

            return DictPointer->operator[](key);
        }

        DynamicData& DynamicData::operator[](const int index)
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Only array type DynamicData may be indexed!");

            return ArrayPointer->operator[](index);
        }

        const DynamicData& DynamicData::operator[](const std::string& key) const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Dict, "Type is not dict!");

            return DictPointer->operator[](key);
        }

        const DynamicData& DynamicData::operator[](const char* key) const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Dict, "Type is not dict!");

            return DictPointer->operator[](key);
        }

        const  DynamicData& DynamicData::operator[](const int index) const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Only array type DynamicData may be indexed!");

            return ArrayPointer->operator[](index);
        }

        DynamicData& DynamicData::MakeDict()
        {
            if (Type != Type::Dict)
            {
                FreeMemoryIfDefined();
                Type = Type::Dict;
                DictPointer = new std::unordered_map<std::string, DynamicData>;
            }

            return *this;
        }


        DynamicData& DynamicData::MakeArray()
        {
            if (Type != Type::Array)
            {
                FreeMemoryIfDefined();
                Type = Type::Array;
                ArrayPointer = new std::vector<DynamicData>;
            }

            return *this;
        }


        double DynamicData::GetFloat() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Float || Type == Type::Integer, "Cannot get float of non numeric type!");

            if (Type == Type::Float)
            {
                return *FloatPointer;
            }
            else
            {
                return (double)(*IntPointer);
            }
        }


        std::string DynamicData::GetString() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::String, "Cannot get String of non string type!");

            return *StringPointer;
        }

        std::vector<DynamicData>& DynamicData::GetArray() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Cannot get array of non array type!");

            return *ArrayPointer;
        }

        std::unordered_map<std::string, DynamicData>& DynamicData::GetDict() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Dict, "Cannot get dict of non dict type!");

            return *DictPointer;
        }

        bool DynamicData::GetBool() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Bool, "Cannot get bool of non bool type!");

            return *BoolPointer;
        }

        int64_t DynamicData::GetInt() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Integer, "Cannot get int of non int type!");

            return *IntPointer;
        }



        std::string DynamicData::ToJSON() const
        {
            if (Type == Type::Bool)
            {
                return *BoolPointer ? "true" : "false";
            }
            else if (Type == Type::Integer)
            {
                return std::to_string(*IntPointer);
            }
            else if (Type == Type::Float)
            {
                return std::to_string(*FloatPointer);
            }
            else if (Type == Type::Null)
            {
                return "null";
            }
            else if (Type == Type::Array)
            {
                std::string result = "[";

                bool processedFirst = false;

                for (const DynamicData& child : *ArrayPointer)
                {
                    if (processedFirst)
                    {
                        result += ",";
                    }
                    else
                    {
                        processedFirst = true;
                    }

                    result += child.ToJSON();
                }

                result += "]";
                return result;
            }
            else if (Type == Type::Dict)
            {
                std::string result = "{";

                bool processedFirst = false;

                for (const auto& [key, value] : *DictPointer)
                {
                    if (processedFirst)
                    {
                        result += ",";
                    }
                    else
                    {
                        processedFirst = true;
                    }

                    result += std::string("\"") + key + "\":" + value.ToJSON();
                }

                result += "}";
                return result;
            }
            else if (Type == Type::String)
            {
                return std::string("\"") + *StringPointer + "\"";
            }
            else
            {
                TESSERACT_CORE_WARN("Undefined type set!");
                return "\"undefined\"";
            }

            return "";
        }


        Math::Vector2 DynamicData::GetVector2() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Cannot get vector of non array type!");
            TESSERACT_CORE_ASSERT(ArrayPointer->size() == 2, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[0].Type == Type::Float || (*ArrayPointer)[0].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[1].Type == Type::Float || (*ArrayPointer)[1].Type == Type::Integer, "Array size invalid!");

            return Math::Vector2(
                (*ArrayPointer)[0].GetFloat(),
                (*ArrayPointer)[1].GetFloat()
            );
        }

        Math::Vector3 DynamicData::GetVector3() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Cannot get vector of non array type!");
            TESSERACT_CORE_ASSERT(ArrayPointer->size() == 3, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[0].Type == Type::Float || (*ArrayPointer)[0].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[1].Type == Type::Float || (*ArrayPointer)[1].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[2].Type == Type::Float || (*ArrayPointer)[2].Type == Type::Integer, "Array size invalid!");

            return Math::Vector3(
                (*ArrayPointer)[0].GetFloat(),
                (*ArrayPointer)[1].GetFloat(),
                (*ArrayPointer)[2].GetFloat()
            );
        }

        Math::Vector4 DynamicData::GetVector4() const
        {
            TESSERACT_CORE_ASSERT(Type == Type::Array, "Cannot get vector of non array type!");
            TESSERACT_CORE_ASSERT(ArrayPointer->size() == 4, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[0].Type == Type::Float || (*ArrayPointer)[0].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[1].Type == Type::Float || (*ArrayPointer)[1].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[2].Type == Type::Float || (*ArrayPointer)[2].Type == Type::Integer, "Array size invalid!");
            TESSERACT_CORE_ASSERT((*ArrayPointer)[3].Type == Type::Float || (*ArrayPointer)[3].Type == Type::Integer, "Array size invalid!");

            return Math::Vector4(
                (*ArrayPointer)[0].GetFloat(),
                (*ArrayPointer)[1].GetFloat(),
                (*ArrayPointer)[2].GetFloat(),
                (*ArrayPointer)[3].GetFloat()
            );
        }



        bool DynamicData::operator==(const char* other) const
        {
            if (Type != Type::String)
            {
                return false;
            }

            return *StringPointer == other;
        }


        bool DynamicData::operator!=(const char* other) const
        {
            return !(*this == other);
        }

        bool DynamicData::operator==(const NullType& other) const
        {
            return this->Type == Type::Null;
        }

        bool DynamicData::operator!=(const NullType& other) const
        {
            return this->Type != Type::Null;
        }


    } // namespace Util

} // namespace Tesseract

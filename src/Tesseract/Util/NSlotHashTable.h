#pragma once

#include "Tesseract/Core/Macros.h"

#include <stdint.h>
#include <memory>

namespace Tesseract
{
	namespace Util
	{
		template<typename T_ValueType, uint32_t T_SlotCount>
		class NSlotHashTable
		{
		private:

			struct Slot
			{
				bool Valid;
				uint64_t Key;
				T_ValueType Value;

				Slot(): Valid(false), Key(), Value() {}
			};

			struct Row
			{
				uint32_t SlotsFilled;
				Slot Slots[T_SlotCount];
			};

		public:

			NSlotHashTable(uint32_t initialSize = 16);
			~NSlotHashTable();
			T_ValueType At(uint64_t key) const;
			void Remove(uint64_t key);
			void Set(uint64_t key, const T_ValueType& value);
			bool Contains(uint64_t key) const; 

		private:

			void Resize();

		private:

			Row* m_Rows;
			uint32_t m_RowCount;
		};

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline NSlotHashTable<T_ValueType, T_SlotCount>::NSlotHashTable(uint32_t initialSize):
			m_RowCount(initialSize),
			m_Rows(nullptr)
		{
			m_Rows = (Row*) std::malloc(sizeof(Row) * m_RowCount);
			
			for (uint32_t row = 0; row < m_RowCount; row++)
			{
				for (uint32_t slot = 0; slot < T_SlotCount; slot++)
				{
					(*(m_Rows + row)).SlotsFilled = 0;
					(*(m_Rows + row)).Slots[slot] = Slot();
				}
			}
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline NSlotHashTable<T_ValueType, T_SlotCount>::~NSlotHashTable()
		{
			std::free(m_Rows);
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline T_ValueType NSlotHashTable<T_ValueType, T_SlotCount>::At(uint64_t key) const
		{
			uint64_t row = key % m_RowCount;

			Row* base = (m_Rows + row);

			for (uint32_t slot = 0; slot < T_SlotCount; slot++)
			{
				if (base->Slots[slot].Valid && base->Slots[slot].Key == key) 
				{
					return base->Slots[slot].Value;
				}
			}

			TESSERACT_CORE_ASSERT(false, "No value found for key {}", key);
			return T_ValueType();
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline void NSlotHashTable<T_ValueType, T_SlotCount>::Remove(uint64_t key)
		{
			uint64_t row = key % m_RowCount;

			Row* base = (m_Rows + row);

			for (uint32_t slot = 0; slot < T_SlotCount; slot++)
			{
				if (!base->Slots[slot].Valid)
				{
					break;
				}

				if (base->Slots[slot].Key == key)
				{
					base->SlotsFilled--;
					base->Slots[slot] = base->Slots[base->SlotsFilled];
					base->Slots[base->SlotsFilled].Valid = false;
					return;
				}
			}

			TESSERACT_CORE_ASSERT(false, "No value found for key {}", key);
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline void NSlotHashTable<T_ValueType, T_SlotCount>::Set(uint64_t key, const T_ValueType& value)
		{
			uint64_t row = key % m_RowCount;

			Row* base = (m_Rows + row);

			for (uint32_t slot = 0; slot < T_SlotCount; slot++)
			{
				if (!base->Slots[slot].Valid)
				{
					base->Slots[slot].Key= key;
					base->Slots[slot].Valid = true;
					base->Slots[slot].Value = value;
					base->SlotsFilled++;
					return;
				}

				if (base->Slots[slot].Valid && base->Slots[slot].Key == key)
				{
					base->Slots[slot].Value = value;
					return;
				}
			}

			Resize();
			Set(key, value);
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline bool NSlotHashTable<T_ValueType, T_SlotCount>::Contains(uint64_t key) const
		{
			uint64_t row = key % m_RowCount;

			Row* base = (m_Rows + row);

			for (uint32_t slot = 0; slot < T_SlotCount; slot++)
			{
				if (base->Slots[slot].Valid && base->Slots[slot].Key == key)
				{
					return true;
				}
			}

			return false;
		}

		template<typename T_ValueType, uint32_t T_SlotCount>
		inline void NSlotHashTable<T_ValueType, T_SlotCount>::Resize()
		{
			uint32_t newRowCount = m_RowCount * 2;
			Row* newRows = (Row*) std::malloc(sizeof(Row) * newRowCount);

			if (newRows == nullptr)
			{
				TESSERACT_CORE_ASSERT(false, "newRows was nullptr!");
				return;
			}

			for (uint32_t row = 0; row < newRowCount; row++)
			{
				for (uint32_t slot = 0; slot < T_SlotCount; slot++)
				{
					(*(newRows + row)).SlotsFilled = 0;
					(*(newRows + row)).Slots[slot] = Slot();
				}
			}

			for (uint32_t row = 0; row < m_RowCount; row++)
			{
				for (uint32_t slot = 0; slot < T_SlotCount; slot++)
				{
					if (!(*(m_Rows + row)).Slots[slot].Valid)
					{
						break;
					}

					uint32_t newRowNumber = (*(m_Rows + row)).Slots[slot].Key % newRowCount;

					uint32_t newRowSlotIndex = (*(newRows + newRowNumber)).SlotsFilled;
					(*(newRows + newRowNumber)).Slots[newRowSlotIndex] = (*(m_Rows + row)).Slots[slot];
					(*(newRows + newRowNumber)).SlotsFilled++;

					TESSERACT_CORE_ASSERT((*(newRows + newRowNumber)).SlotsFilled <= T_SlotCount, "Maximum slot number exceeded in row!");
				}
			}

			std::free(m_Rows);
			m_Rows = newRows;
			m_RowCount = newRowCount;
		}
	}
}
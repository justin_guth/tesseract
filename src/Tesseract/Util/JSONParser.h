#pragma once

#include "DynamicData.h"

#include <string>

namespace Tesseract
{
    namespace Util
    {
        class JSONParser
        {
        public:

            JSONParser(const std::string& string);

            DynamicData Parse();

            static DynamicData ParseJSONString(const std::string& string);
            static DynamicData LoadJSONFile(const std::string& filePath);

        private:

            DynamicData ParseString();
            DynamicData ParseNumber();
            DynamicData ParseValue();
            DynamicData ParseTrue();
            DynamicData ParseFalse();
            DynamicData ParseNull();
            DynamicData ParseArray();
            DynamicData ParseObject();
            void SkipWhitespace();

            uint32_t m_Position;
            std::string m_String;
        };
    } // namespace Util

} // namespace Tesseract

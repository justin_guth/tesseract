#pragma once
#include <queue>

namespace Tesseract
{
	namespace Util
	{
		/**
		* Defines a swappable double queue for a given type.
		* The SwapQueue consists of a front and a back queue. 
		* When pushing elements to the SwapQueue they are enqueued in the front queue.
		* When Popping elements from the SwapQueue they are dequeued from the back queue.
		* Both queues can be swapped.
		* 
		* This class is not thread safe.
		* 
		* @tparam T_Type the type for which the queue is built.
		*/
		template <typename T_Type>
		class SwapQueue
		{
		public:

			/**
			* The default constructor.
			* Initialises the front and back queue.
			*/
			SwapQueue();

			/**
			* Swap the front and back queue.
			*/
			void Swap();

			/**
			* Remove and return the first element in the back queue.
			* 
			* @return The first element in the back queue
			*/
			T_Type Pop();
			
			/**
			* Enqueue a given @element to the front queue.
			*
			* @param[in] The element to be enqueued
			*/
			void Push(const T_Type& element);
			
			/**
			* Returns whether the front queue is empty.
			* 
			* @return Whether the front queue is empty
			*/
			bool IsFrontQueueEmpty();
			
			/**
			* Returns the back queue count.
			*
			* @return The back queue count
			*/
			size_t BackQueueCount();
			
			/**
			* Returns the front queue count.
			*
			* @return The front queue count
			*/
			size_t FrontQueueCount();

		private:

			std::queue<T_Type> m_Queues[2] = {std::queue<T_Type>(), std::queue<T_Type>()};
			int8_t m_CurrentQueue;
		};

		template<typename T_Type>
		inline SwapQueue<T_Type>::SwapQueue() :
			m_CurrentQueue(0)
		{

		}

		template<typename T_Type>
		inline void SwapQueue<T_Type>::Swap()
		{
			m_CurrentQueue += 1;
			m_CurrentQueue %= 2;
		}

		template<typename T_Type>
		inline T_Type SwapQueue<T_Type>::Pop()
		{
			int8_t backQueue = (m_CurrentQueue + 1) % 2;
			T_Type result = m_Queues[backQueue].front();
			m_Queues[backQueue].pop();

			return result;
		}
		template<typename T_Type>
		inline void SwapQueue<T_Type>::Push(const T_Type& element)
		{
			m_Queues[m_CurrentQueue].push(element);
		}

		template<typename T_Type>
		inline bool SwapQueue<T_Type>::IsFrontQueueEmpty()
		{
			return m_Queues[m_CurrentQueue].size() == 0;
		}

		template<typename T_Type>
		inline size_t SwapQueue<T_Type>::BackQueueCount()
		{
			int8_t backQueue = (m_CurrentQueue + 1) % 2;
			return m_Queues[backQueue].size();
		}

		template<typename T_Type>
		inline size_t SwapQueue<T_Type>::FrontQueueCount()
		{
			return m_Queues[m_CurrentQueue].size();
		}
	}
}
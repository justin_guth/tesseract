#define TESSERACT_USE_ALTERNATIVE_MESH_LOADING 1

#include "MeshLoading.h"


#include "File.h"

#include <array>
#include <unordered_map>

#include "Tesseract/Graphics/OldMeshTODORename.h"

namespace Tesseract
{
    namespace Util
    {
#if TESSERACT_USE_ALTERNATIVE_MESH_LOADING
        enum class ObjFileParseState
        {
            ExpectAnything,
            ExpectComment,
            ExpectVertexSpecification,
            ExpectPositionDataStart,
            ExpectTextureDataStart,
            ExpectNormalDataStart,
            ExpectFaceDataStart,
            ExpectPositionData,
            ExpectTextureData,
            ExpectNormalData,
            ExpectFaceData,
            ExpectEndOfLine
        };

        static std::tuple<uint32_t, uint32_t, uint32_t> ExtractIndices(const std::string& faceCorner)
        {
            size_t firstSlashPosition = faceCorner.find_first_of('/');
            size_t secondSlashPosition = faceCorner.find_last_of('/');

            uint32_t positionIndex = std::stoi(faceCorner.substr(0, firstSlashPosition));
            uint32_t textureIndex = secondSlashPosition - firstSlashPosition > 0 ? std::stoi(faceCorner.substr(firstSlashPosition + 1, secondSlashPosition - firstSlashPosition)) : 0;
            uint32_t normalIndex = std::stoi(faceCorner.substr(secondSlashPosition + 1, faceCorner.length() - secondSlashPosition));

            return { positionIndex, textureIndex, normalIndex };
        }

        Ref<Core::Geometry> LoadObjFileData(const std::string& filePath, bool reverse)
        {

            ObjFileParseState currentState = ObjFileParseState::ExpectAnything;

            const std::string fileContents = File::ReadFileContents(filePath);

            size_t currentPosition = 0;

            size_t parameterNumber = 0;

            size_t lastParameterBegin = -1;

            std::vector<Math::Vector3> positionDataList;
            std::vector<Math::Vector2> textureDataList;
            std::vector<Math::Vector3> normalDataList;
            std::vector<std::array<std::string, 3>> faceDataList;


            Math::Vector3 currentPositionData;
            Math::Vector2 currentTextureData;
            Math::Vector3 currentNormalData;
            std::array<std::string, 3> currentFaceData;


            while (currentPosition < fileContents.size())
            {
                char currentSymbol = fileContents.at(currentPosition);

                switch (currentState)
                {
                    case ObjFileParseState::ExpectAnything:

                        if (currentSymbol == '#')
                        {
                            currentState = ObjFileParseState::ExpectComment;
                        }
                        else if (currentSymbol == 'v')
                        {
                            parameterNumber = 0;
                            currentState = ObjFileParseState::ExpectVertexSpecification;
                        }
                        else if (currentSymbol == 'f')
                        {
                            parameterNumber = 0;
                            currentState = ObjFileParseState::ExpectFaceDataStart;
                        }
                        else if (currentSymbol == ' ')
                        {

                        }
                        else
                        {
                            currentState = ObjFileParseState::ExpectEndOfLine;
                                currentPosition--;
                        }

                        break;

                    case ObjFileParseState::ExpectComment:

                        if (currentSymbol == '\n') { currentState = ObjFileParseState::ExpectAnything; }
                        break;

                    case ObjFileParseState::ExpectVertexSpecification:

                        if (currentSymbol == ' ') { currentState = ObjFileParseState::ExpectPositionDataStart; }
                        else if (currentSymbol == 't') { currentState = ObjFileParseState::ExpectTextureDataStart; }
                        else if (currentSymbol == 'n') { currentState = ObjFileParseState::ExpectNormalDataStart; }
                        break;

                    case ObjFileParseState::ExpectFaceDataStart:

                        if (currentSymbol == ' ') { currentState = ObjFileParseState::ExpectFaceDataStart; }
                        else { lastParameterBegin = currentPosition; currentState = ObjFileParseState::ExpectFaceData; }
                        break;

                    case ObjFileParseState::ExpectPositionDataStart:

                        if (currentSymbol == ' ') { currentState = ObjFileParseState::ExpectPositionDataStart; }
                        else { lastParameterBegin = currentPosition; currentState = ObjFileParseState::ExpectPositionData; }
                        break;

                    case ObjFileParseState::ExpectTextureDataStart:

                        if (currentSymbol == ' ') { currentState = ObjFileParseState::ExpectTextureDataStart; }
                        else { lastParameterBegin = currentPosition; currentState = ObjFileParseState::ExpectTextureData; }
                        break;

                    case ObjFileParseState::ExpectNormalDataStart:

                        if (currentSymbol == ' ') { currentState = ObjFileParseState::ExpectNormalDataStart; }
                        else { lastParameterBegin = currentPosition; currentState = ObjFileParseState::ExpectNormalData; }
                        break;

                        // Actual parsing

                    case ObjFileParseState::ExpectPositionData:

                        if (currentSymbol == ' ' || currentSymbol == '\n')
                        {
                            currentPositionData[parameterNumber] = std::stof(fileContents.substr(lastParameterBegin, currentPosition - lastParameterBegin));
                            parameterNumber++;
                            if (parameterNumber >= 3)
                            {
                                positionDataList.push_back(currentPositionData);
                                currentState = ObjFileParseState::ExpectEndOfLine;
                                currentPosition--;
                            }
                            else
                            {
                                currentState = ObjFileParseState::ExpectPositionDataStart;
                            }
                        }
                        else
                        {
                            currentState = ObjFileParseState::ExpectPositionData;
                        }
                        break;

                    case ObjFileParseState::ExpectNormalData:

                        if (currentSymbol == ' ' || currentSymbol == '\n')
                        {
                            currentNormalData[parameterNumber] = std::stof(fileContents.substr(lastParameterBegin, currentPosition - lastParameterBegin));
                            parameterNumber++;
                            if (parameterNumber >= 3)
                            {
                                normalDataList.push_back(currentNormalData);
                                currentState = ObjFileParseState::ExpectEndOfLine;
                                currentPosition--;
                            }
                            else
                            {
                                currentState = ObjFileParseState::ExpectNormalDataStart;
                            }
                        }
                        else
                        {
                            currentState = ObjFileParseState::ExpectNormalData;
                        }
                        break;

                    case ObjFileParseState::ExpectTextureData:

                        if (currentSymbol == ' ' || currentSymbol == '\n')
                        {
                            currentTextureData[parameterNumber] = std::stof(fileContents.substr(lastParameterBegin, currentPosition - lastParameterBegin));
                            parameterNumber++;
                            if (parameterNumber >= 2)
                            {
                                textureDataList.push_back(currentTextureData);
                                currentState = ObjFileParseState::ExpectEndOfLine;
                                currentPosition--;
                            }
                            else
                            {
                                currentState = ObjFileParseState::ExpectTextureDataStart;
                            }
                        }
                        else
                        {
                            currentState = ObjFileParseState::ExpectTextureData;
                        }
                        break;

                    case ObjFileParseState::ExpectFaceData:

                        if (currentSymbol == ' ' || currentSymbol == '\n')
                        {
                            currentFaceData[parameterNumber] = fileContents.substr(lastParameterBegin, currentPosition - lastParameterBegin);
                            parameterNumber++;
                            if (parameterNumber >= 3)
                            {
                                faceDataList.push_back(currentFaceData);
                                currentState = ObjFileParseState::ExpectEndOfLine;
                                currentPosition--;
                            }
                            else
                            {
                                currentState = ObjFileParseState::ExpectFaceDataStart;
                            }
                        }
                        else
                        {
                            currentState = ObjFileParseState::ExpectFaceData;
                        }
                        break;




                    case ObjFileParseState::ExpectEndOfLine:

                        if (currentSymbol == '\n')
                        {
                            currentState = ObjFileParseState::ExpectAnything;
                        }
                        break;

                    default:
                        break;
                }

                currentPosition++;
            }

            std::unordered_map<std::string, uint32_t> faceMap;

            std::vector<std::string> faceIndices;

            for (const auto& [a, b, c] : faceDataList)
            {
                if (reverse)
                {
                    faceIndices.push_back(c);
                    faceIndices.push_back(b);
                    faceIndices.push_back(a);
                }
                else
                {
                    faceIndices.push_back(a);
                    faceIndices.push_back(b);
                    faceIndices.push_back(c);
                }
            }

            std::vector<Core::VertexData> resultVertexData;
            std::vector<uint32_t> resultIndices;

            for (const std::string& faceIndex : faceIndices)
            {
                if (faceMap.contains(faceIndex))
                {
                    resultIndices.push_back(faceMap.at(faceIndex));
                }
                else
                {
                    const auto [indexPosition, indexTexture, indexNormal] = ExtractIndices(faceIndex);

                    resultVertexData.push_back(
                        {
                            positionDataList[indexPosition - 1],
                            indexTexture != 0 ? textureDataList[indexTexture - 1] : Math::Vector2(),
                            normalDataList[indexNormal - 1]
                        }
                    );

                    uint32_t newIndex = resultVertexData.size() - 1;
                    faceMap[faceIndex] = newIndex;
                    resultIndices.push_back(newIndex);

                }
            }

            return Ref<Core::Geometry>(
                new Core::Geometry(
                    resultVertexData,
                    resultIndices,
                    {
                        {"position", DataType::Float3, false},
                        {"textureCoordinate", DataType::Float2, false},
                        {"normal", DataType::Float3, false},
                        {"tangent", DataType::Float3, false},
                        {"bitangent", DataType::Float3, false}
                    },
                    Graphics::ElementBuffer::ElementType::UInt32
                )
            );  
        }

#else 

        typedef const char* tokenStream;

        static tokenStream tok;
        static std::vector<Math::Vector3> vPos;
        static std::vector<Math::Vector2> vTex;
        static std::vector<Math::Vector3> vNor;
        static std::unordered_map<uint64_t, uint16_t> attributeCombinations;

        static uint8_t attributeFlags;

        char ReadToken()
        {
            char t = *tok;
            (tok)++;
            return t;
        }

        uint8_t IntFromAscii(char ascii)
        {
            return ascii - 48;
        }

        template<class T>
        T ParseNum()
        {
            T cur = 0;
            bool reading = true;

            while (reading)
            {
                switch (*tok)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        cur = 10 * cur + IntFromAscii(ReadToken());
                        break;
                    default:
                        reading = false;
                        break;
                        break;
                }
            }
            return cur;
        }

        float ParseObjFloatAfterDecimal()
        {
            float cur = 0;
            float oom = 0.1f;
            bool reading = true;

            while (reading)
            {
                switch (*tok)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        cur += oom * IntFromAscii(ReadToken());
                        oom /= 10;
                        break;
                    case ' ':
                    case '\n':
                    case 0:
                        ReadToken();
                        reading = false;
                        break;
                    default:
                        return 0;
                        break;
                }
            }
            return cur;
        }

        int8_t ParseSign()
        {
            if (*tok == '-')
            {
                ReadToken();
                return -1;
            }
            else if (*tok == '+')
            {
                ReadToken();
                return 1;
            }
            else return 1;
        }

        float ParseObjFloat()
        {
            int sgn = ParseSign();

            float cur = ParseNum<float>();
            switch (*tok)
            {
                case '.':
                    ReadToken();
                    cur += ParseObjFloatAfterDecimal();
                    break;
                case ' ':
                case '\n':
                case 0:
                    ReadToken();
                    break;
                default:
                    return 0;
                    break;
            }
            return sgn * cur;
        }

        Math::Vector2 ParseObjVector2()
        {
            Math::Vector2 res;
            res.X = ParseObjFloat();
            res.Y = ParseObjFloat();
            return res;
        }

        Math::Vector3 ParseObjVector3()
        {
            Math::Vector3 res;
            res.X = ParseObjFloat();
            res.Y = ParseObjFloat();
            res.Z = ParseObjFloat();
            return res;
        }

        void ParseObjVertexPos()
        {
            Math::Vector3 pos = ParseObjVector3();
            //TESSERACT_CORE_TRACE("Read position of vertex " + std::to_string(vPos.size()) + ": (" + std::to_string(pos.X) + ", " + std::to_string(pos.Y) + ", " + std::to_string(pos.Z) + ")");
            vPos.push_back(pos);
        }

        void ParseObjVertexTex()
        {
            Math::Vector2 textureCoord = ParseObjVector2();
            vTex.push_back(textureCoord);
        }

        void ParseObjVertexNormal()
        {
            Math::Vector3 n = ParseObjVector3();
            vNor.push_back(n);
        }

        void ParseObjV()
        {
            switch (*tok)
            {
                case ' ':
                    ReadToken();
                    ParseObjVertexPos();
                    break;
                case 't':
                    ReadToken();
                    ReadToken();
                    ParseObjVertexTex();
                    break;
                case 'n':
                    ReadToken();
                    ReadToken();
                    ParseObjVertexNormal();
                    break;
                default:
                    break;
            }
        }

        uint64_t ParseObjIndex()
        {
            uint64_t p_idx = ParseNum<uint16_t>();
            attributeFlags |= 1 << TESSERACT_VERTEX_POSITION;
            uint64_t t_idx = uint16_t(-1);
            uint64_t n_idx = uint16_t(-1);
            switch (*tok)
            {
                case '/':
                    ReadToken();
                    if (*tok != '/')
                    {
                        t_idx = ParseNum<uint16_t>();
                        attributeFlags |= 1 << TESSERACT_VERTEX_TEXTURE;
                    }
                    if (*tok == '/')
                    {
                        ReadToken();
                        n_idx = ParseNum<uint16_t>();
                        attributeFlags |= 1 << TESSERACT_VERTEX_NORMAL;
                    }
                    if (*tok == ' ' || *tok == '\n' || *tok == 0)
                    {
                        ReadToken();
                        break;
                    }
                    else return 0;
                    break;
                case ' ':
                case '\n':
                case 0:
                    ReadToken();
                    break;
                default:
                    return 0;
                    break;
            }

            //REVIEW: Replace with tuples potentially
            p_idx = p_idx << (TESSERACT_VERTEX_STRIDE * TESSERACT_VERTEX_POSITION);
            t_idx = t_idx << (TESSERACT_VERTEX_STRIDE * TESSERACT_VERTEX_TEXTURE);
            n_idx = n_idx << (TESSERACT_VERTEX_STRIDE * TESSERACT_VERTEX_NORMAL);

            return p_idx + t_idx + n_idx;
        }

        static uint64_t idxmask = (1 << 16) - 1;

        void ParseObjVertex(Graphics::OldMeshTODORename& targetMesh)
        {
            uint64_t idxs = ParseObjIndex();

            auto res = attributeCombinations.find(idxs);
            if (res == attributeCombinations.end())
            {
                std::pair<uint64_t, uint16_t> newIdx(idxs, uint16_t(targetMesh.Verices.size()));
                attributeCombinations.insert(newIdx);
                Graphics::Vertex newVert;

                uint16_t p_idx = (idxs & (idxmask << (TESSERACT_VERTEX_POSITION * TESSERACT_VERTEX_STRIDE))) >> (TESSERACT_VERTEX_POSITION * TESSERACT_VERTEX_STRIDE);
                uint16_t t_idx = (idxs & (idxmask << (TESSERACT_VERTEX_TEXTURE * TESSERACT_VERTEX_STRIDE))) >> (TESSERACT_VERTEX_TEXTURE * TESSERACT_VERTEX_STRIDE);
                uint16_t n_idx = (idxs & (idxmask << (TESSERACT_VERTEX_NORMAL * TESSERACT_VERTEX_STRIDE))) >> (TESSERACT_VERTEX_NORMAL * TESSERACT_VERTEX_STRIDE);

                //TESSERACT_CORE_TRACE("p: " + std::to_string(p_idx) + ", t: " + std::to_string(t_idx) + " n: " + std::to_string(n_idx));

                if (attributeFlags & (1 << TESSERACT_VERTEX_POSITION)) { newVert.m_Position = vPos[p_idx - 1]; }
                if (attributeFlags & (1 << TESSERACT_VERTEX_TEXTURE)) { newVert.m_TextureCoord = vTex[t_idx - 1]; }
                if (attributeFlags & (1 << TESSERACT_VERTEX_NORMAL)) { newVert.m_Normal = vNor[n_idx - 1]; }

                targetMesh.Triangles.push_back(targetMesh.Verices.size());
                targetMesh.Verices.push_back(newVert);
            }
            else
            {
                targetMesh.Triangles.push_back(res->second);
            }
        }

        void ParseObjFace(Graphics::OldMeshTODORename& targetMesh)
        {
            for (int v = 0; v < 3; v++)
            {
                ParseObjVertex(targetMesh);
            }
            /*
            TESSERACT_CORE_TRACE("Face read: " + std::to_string(targetMesh.m_Triangles[targetMesh.m_Triangles.size() - 3]) + " "
                              + std::to_string(targetMesh.m_Triangles[targetMesh.m_Triangles.size() - 2]) + " "
                              + std::to_string(targetMesh.m_Triangles[targetMesh.m_Triangles.size() - 1]));
            */
        }

        void GobbleLine()
        {
            char token;
            do
            {
                token = ReadToken();
            } while (token != '\n' && token != 0);
        }

        void GobbleSpace()
        {
            while (*tok == ' ' || *tok == '\n')
            {
                ReadToken();
            }
        }

        void ParseObjLine(Graphics::OldMeshTODORename& targetMesh)
        {
            switch (*tok)
            {
                case 'v':
                    ReadToken();
                    ParseObjV();
                    break;
                case 'f':
                    ReadToken();
                    ReadToken();
                    ParseObjFace(targetMesh);
                    break;
                case '#':
                    GobbleLine();
                    break;
                case ' ':
                case '\n':
                    GobbleSpace();
                    break;
                default:
                    GobbleLine();
                    break;
            }
        }

        std::tuple<std::vector<Core::VertexData>, std::vector<uint32_t>> LoadObjFileData(const std::string& filePath, bool reverse = false)
        {
            attributeFlags = 0;
            attributeCombinations.clear();
            vPos.clear();
            vTex.clear();
            vNor.clear();
            Graphics::OldMeshTODORename loadedMesh;

            std::string fileContent = Util::File::ReadFileContents(filePath);

            tok = fileContent.c_str();
            const char* oldTokens = tok;

            while (*tok)
            {
                ParseObjLine(loadedMesh);
            }

            loadedMesh.AddAttribute(attributeFlags);

            TESSERACT_CORE_SUCCESS("Loaded mesh from " + filePath);
            TESSERACT_CORE_TRACE(std::to_string(tok - oldTokens) + " tokens read");
            return loadedMesh;
        }
#endif
    } // namespace IO

} // namespace Tesseract



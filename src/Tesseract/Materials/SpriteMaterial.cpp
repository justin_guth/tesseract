#include "SpriteMaterial.h"

namespace Tesseract
{
    namespace Materials
    {
        Ref<Graphics::ShaderProgram> SpriteMaterial::s_ShaderProgram = nullptr;


        SpriteMaterial::SpriteMaterial()
        {
            if (s_ShaderProgram == nullptr)
            {
                s_ShaderProgram = Graphics::ShaderProgram::CreateAndCompileFromSourceFiles(
                    "../../res/shaders/sprite.vert",
                    "../../res/shaders/sprite.frag"
                );
            }

            SetShaderProgram(s_ShaderProgram);
        }

        Ref<Graphics::ShaderProgram> SpriteMaterial::GetShaderProgram()
        {
            return s_ShaderProgram;
        }


        const std::vector<IHasParameters::Parameter> SpriteMaterial::GetParameters()
        {
            return {
                { "Diffuse tint", "uMaterialDiffuseTint", DataType::Color, &DiffuseTint },
                { "Fogginess", "uFogginess", DataType::Float1, &Fogginess }
            };
        }

    } // namespace Materials

} // namespace Tesseract


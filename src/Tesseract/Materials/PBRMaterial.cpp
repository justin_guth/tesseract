#include "PBRMaterial.h"

namespace Tesseract
{
    namespace Materials
    {
        Ref<Graphics::ShaderProgram> PBRMaterial::s_ShaderProgram = nullptr;


        PBRMaterial::PBRMaterial()
        {
            if (s_ShaderProgram == nullptr)
            {
                s_ShaderProgram = Graphics::ShaderProgram::CreateAndCompileFromSourceFiles(
                    "../../res/shaders/PBR.vert",
                    "../../res/shaders/PBR.frag"
                );
            }

            SetShaderProgram(s_ShaderProgram);
        }

        Ref<Graphics::ShaderProgram> PBRMaterial::GetShaderProgram()
        {
            return s_ShaderProgram;
        }


        const std::vector<IHasParameters::Parameter> PBRMaterial::GetParameters()
        {
            return {
                { "Diffuse tint", "uMaterialDiffuseTint", DataType::Color, &DiffuseTint },
                { "Fogginess", "uFogginess", DataType::Float1, &Fogginess }
            };
        }

    } // namespace Materials

} // namespace Tesseract


#pragma once

#include "Tesseract/Graphics/ShaderProgram.h"
#include "Tesseract/Graphics/Material.h"
#include "Tesseract/Math/Vector4.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Materials
    {
        class PhongMaterial : public Graphics::Material
        {
        public:

            PhongMaterial();
            virtual Ref<Graphics::ShaderProgram> GetShaderProgram() override;
            virtual const std::vector<IHasParameters::Parameter> GetParameters() override;

        public:

            Math::Vector4 DiffuseTint;

        private:

            static Ref<Graphics::ShaderProgram> s_ShaderProgram;
        };

    } // namespace Materials

} // namespace Tesseract


#pragma once

#include "Tesseract/Graphics/ShaderProgram.h"
#include "Tesseract/Graphics/Material.h"
#include "Tesseract/Math/Vector4.h"

#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Materials
    {
        class PBRMaterial : public Graphics::Material
        {
        public:

            PBRMaterial();
            virtual Ref<Graphics::ShaderProgram> GetShaderProgram() override;
            virtual const std::vector<IHasParameters::Parameter> GetParameters() override;

        public:

            Math::Vector4 DiffuseTint;
            float Fogginess = 0.0f;

        private:

            static Ref<Graphics::ShaderProgram> s_ShaderProgram;
        };

    } // namespace Materials

} // namespace Tesseract


#include "FlatMaterial.h"

namespace Tesseract
{
    namespace Materials
    {
        Ref<Graphics::ShaderProgram> FlatMaterial::s_ShaderProgram = nullptr;


        FlatMaterial::FlatMaterial()
        {
            if (s_ShaderProgram == nullptr)
            {
                s_ShaderProgram = Graphics::ShaderProgram::CreateAndCompileFromSourceFiles(
                    "../../res/shaders/flat.vert",
                    "../../res/shaders/flat.frag"
                );
            }

            SetShaderProgram(s_ShaderProgram);
        }

        Ref<Graphics::ShaderProgram> FlatMaterial::GetShaderProgram()
        {
            return s_ShaderProgram;
        }


        const std::vector<IHasParameters::Parameter> FlatMaterial::GetParameters()
        {
            return {
                { "Wire Color", "uColorWire", DataType::Color, &ColorWire },
                { "Fill Color", "uColorFill", DataType::Color, &ColorFill }
            };
        }

    } // namespace Materials

} // namespace Tesseract


#include "PhongMaterial.h"

namespace Tesseract
{
    namespace Materials
    {
        Ref<Graphics::ShaderProgram> PhongMaterial::s_ShaderProgram = nullptr;


        PhongMaterial::PhongMaterial()
        {
            if (s_ShaderProgram == nullptr)
            {
                s_ShaderProgram = Graphics::ShaderProgram::CreateAndCompileFromSourceFiles(
                    "../../res/shaders/phong.vert",
                    "../../res/shaders/phong.frag"
                );
            }

            SetShaderProgram(s_ShaderProgram);
        }

        Ref<Graphics::ShaderProgram> PhongMaterial::GetShaderProgram()
        {
            return s_ShaderProgram;
        }


        const std::vector<IHasParameters::Parameter> PhongMaterial::GetParameters()
        {
            return {
                { "Diffuse tint", "uMaterialDiffuseTint", DataType::Color, &DiffuseTint }
            };
        }

    } // namespace Materials

} // namespace Tesseract


#include "Collider.h"
#include "Tesseract/Reference.h"
#include "Ray.h"
#include "Physics.h"

namespace Tesseract
{
    namespace Physics
    {
        //TODO: Transform Scale should also scale up the colliders, refer to other intersect methods

        bool SphereCollider::Intersects(const SphereCollider& collider)
        {
            return false;
            //something like return (this->Radius + collider.Radius > (this->Position - collider.Position).Magnitude());
        }

        bool SphereCollider::Intersects(const PointCollider& collider)
        {
            ECS::Comp<Tesseract::Components::TransformComponent> otherTransform = collider.Transform; //REVIEW Why is this necessary??
            Math::Vector3 pos = Transform->GetGlobalPosition() + Offset;
            Math::Vector3 otherPos = otherTransform->GetGlobalPosition() + collider.Offset;

            return  (otherPos - pos).MagnitudeSquared() <= Radius * Radius;
        }

        bool SphereCollider::Intersects(const AABBCollider& collider)
        {
            return false;
        }

        bool SphereCollider::Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const
        {
            Math::Vector3 diff = Transform->GetGlobalPosition() - r.Position;

            float a = r.Orientation.MagnitudeSquared();
            float b = (r.Orientation * 2).Dot(diff);
            float c = diff.MagnitudeSquared() - Radius * Radius;

            float d = b * b - 4 * a * c;

            if (d < 0) { return false; }

            float t1 = b - d;
            float t2 = b + d;

            if (t1 < 0) { hitTimeNear = t2; }
            else { hitTimeNear = t1; }

            contactPoint = r.Position + r.Orientation * hitTimeNear;
            contactNormal = (contactPoint - Transform->GetGlobalPosition()).Normalized();

            return true;
        }


        bool PointCollider::Intersects(const SphereCollider& collider)
        {
            return false; //refer to SphereCollider.Intersects
        }

        bool PointCollider::Intersects(const PointCollider& collider)
        {
            ECS::Comp<Tesseract::Components::TransformComponent> otherTransform = collider.Transform; //REVIEW Why is this necessary??
            return (otherTransform->GetGlobalPosition() + collider.Offset - Transform->GetGlobalPosition() + Offset).Magnitude() < Math::Constants::Epsilon;
        }

        bool PointCollider::Intersects(const AABBCollider& collider)
        {
            return false; //refer to AABB.Intersects
        }

        bool PointCollider::Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const
        {
            Math::Vector3 t = (Transform->GetGlobalPosition() - r.Position) / r.Orientation;
            hitTimeNear = t.X;

            if (abs(t.X - t.Y) > Math::Constants::Epsilon
                || abs(t.X - t.Z) > Math::Constants::Epsilon
                || abs(t.Z - t.Y) > Math::Constants::Epsilon
                || hitTimeNear < 0)
            {
                return false;
            }

            contactPoint = Transform->GetGlobalPosition();
            contactNormal = r.Orientation * (-1);

            return true;
        }

        bool AABBCollider::Intersects(const SphereCollider& collider)
        {
            return false;
        }

        bool AABBCollider::Intersects(const PointCollider& collider)
        {
            ECS::Comp<Tesseract::Components::TransformComponent> otherTransform = collider.Transform;
            Math::Vector3 p = otherTransform->GetGlobalPosition() + collider.Offset;

            return (p.X >= GetGlobalPosition().X && p.Y >= GetGlobalPosition().Y && p.Z >= GetGlobalPosition().Z
                    && p.X < GetGlobalPosition().X + Size.X && p.Y < GetGlobalPosition().Y + Size.Y && p.Z < GetGlobalPosition().Z + Size.Z);
        }

        bool AABBCollider::Intersects(const AABBCollider& collider)
        {
            ECS::Comp<Tesseract::Components::TransformComponent> otherTransform = collider.Transform;
            Math::Vector3 otherPos = otherTransform->GetGlobalPosition() + collider.Offset;
            Math::Vector3 pos = GetGlobalPosition();

            bool x = pos.X < otherPos.X + collider.Size.X && pos.X + Size.X > otherPos.X;
            bool y = pos.Y < otherPos.Y + collider.Size.Y && pos.Y + Size.Y > otherPos.Y;
            bool z = pos.Z < otherPos.Z + collider.Size.Z && pos.Z + Size.Z > otherPos.Z;

            return x && y && z;
        }

        bool AABBCollider::DynamicIntersects2D(const AABBCollider& collider, const Math::Vector3 velocity, Math::Vector3& contactPoint, Math::Vector2& contactNormal, double& time)
        {
            if (velocity.X == 0 && velocity.Y == 0) { return false; }

            AABBCollider expandedCollider;
            expandedCollider.Offset = collider.Offset - Size / 2;
            expandedCollider.Transform = collider.Transform;
            expandedCollider.Size = collider.Size + Size;

            Ray ray;
            ray.Position = GetGlobalPosition() + Size / 2;
            ray.Orientation = velocity * Physics::Physics::GetInterval(); //TODO: check if this is right

            if (expandedCollider.Intersects2D(ray, contactPoint, contactNormal, time))
            {
                if (time <= 1.f)
                {
                    return true;
                }
            }

            return false;
        }

        bool AABBCollider::Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const
        {
            Tesseract::ECS::Comp<Tesseract::Components::TransformComponent> colTransform = Transform;
            Math::Vector3 colPos = Offset + colTransform->GetGlobalPosition();


            Math::Vector3 tNear = (colPos - r.Position) / r.Orientation;
            Math::Vector3 tFar = (colPos + Size - r.Position) / r.Orientation;

            if (tNear.X > tFar.X) { std::swap(tNear.X, tFar.X); }
            if (tNear.Y > tFar.Y) { std::swap(tNear.Y, tFar.Y); }
            if (tNear.Z > tFar.Z) { std::swap(tNear.Z, tFar.Z); }

            if (tNear.X > std::min(tFar.Y, tFar.Z)
                || tNear.Y > std::min(tFar.X, tFar.Z)
                || tNear.Z > std::min(tFar.X, tFar.Y))
            {
                return false;
            }

            hitTimeNear = std::max({ tNear.X, tNear.Y, tNear.Z });
            double hitTimeFar = std::min({ tFar.X, tFar.Y, tFar.Z });


            if (hitTimeFar < 0) { return false; }

            contactPoint = r.Position + r.Orientation * hitTimeNear;

            if (tNear.X > std::min(tNear.Y, tNear.Z))
            {
                if (r.Orientation.X < 0)
                {
                    contactNormal = { 1, 0, 0 };
                }
                else
                {
                    contactNormal = { -1, 0, 0 };
                }
            }
            else if (tNear.Y > std::min(tNear.X, tNear.Z))
            {
                if (r.Orientation.Y < 0)
                {
                    contactNormal = { 0, 1, 0 };
                }
                else
                {
                    contactNormal = { 0, -1, 0 };
                }
            }
            else
            {
                if (r.Orientation.Z < 0)
                {
                    contactNormal = { 0, 0, 1 };
                }
                else
                {
                    contactNormal = { 0, 0, -1 };
                }
            }

            return true;
        }

        bool AABBCollider::Intersects2D(const Ray ray, Math::Vector3& contactPoint, Math::Vector2& contactNormal, double& hitTimeNear)
        {
            Tesseract::ECS::Comp<Tesseract::Components::TransformComponent> colTransform = Transform;
            Math::Vector3 colPos = Offset + colTransform->GetGlobalPosition();


            Math::Vector3 tNear = (colPos - ray.Position) / ray.Orientation;
            Math::Vector3 tFar = (colPos + Size - ray.Position) / ray.Orientation;

            if (tNear.X > tFar.X) { std::swap(tNear.X, tFar.X); }
            if (tNear.Y > tFar.Y) { std::swap(tNear.Y, tFar.Y); }

            if (tNear.X > tFar.Y || tNear.Y > tFar.X) { return false; }

            hitTimeNear = std::max(tNear.X, tNear.Y);
            double hitTimeFar = std::min(tFar.X, tFar.Y);


            if (hitTimeFar < 0) { return false; }

            contactPoint = ray.Position + tNear * ray.Orientation;

            if (tNear.X > tNear.Y)
            {
                if (ray.Orientation.X < 0)
                {
                    contactNormal = { 1, 0 };
                }
                else
                {
                    contactNormal = { -1, 0 };
                }
            }
            else if (tNear.X < tNear.Y)
            {
                if (ray.Orientation.Y < 0)
                {
                    contactNormal = { 0, 1 };
                }
                else
                {
                    contactNormal = { 0, -1 };
                }
            }

            return true;
        }

    } // namespace Physics

} // namespace Tesseract

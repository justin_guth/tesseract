#pragma once

#include <vector>

#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Util/Index.h"
#include "Ray.h"

namespace Tesseract
{
    namespace Physics
    {

        class SphereCollider;
        class PointCollider;
        class AABBCollider;


        class Collider: public IIntersectable
        {

        public:

            enum class Type { AABB, Sphere, Point };
            enum class Group { Static, Dynamic };

        public:

            virtual bool Intersects(const SphereCollider& collider) = 0;
            virtual bool Intersects(const PointCollider& collider) = 0;
            virtual bool Intersects(const AABBCollider& collider) = 0;
            virtual Type GetType() const = 0;

            inline bool Intersects(const Ref<Tesseract::Physics::Collider>& collider)
            {

                switch ((*collider).GetType())
                {
                    case Type::AABB:
                        return Intersects(*(std::dynamic_pointer_cast<AABBCollider>(collider)));

                    case Type::Sphere:
                        return Intersects(*(std::dynamic_pointer_cast<SphereCollider>(collider)));
                    case Type::Point:
                        return Intersects(*(std::dynamic_pointer_cast<PointCollider>(collider)));
                    default:
                        break;
                }

                return false;
            }

            inline Math::Vector3 GetGlobalPosition()
            {
                return Offset + Transform->GetGlobalPosition();
            }

            ECS::Comp<Tesseract::Components::TransformComponent> Transform;
            Math::Vector3 Offset;
            bool Enabled;
            bool IsActive;
            bool ToResolve;
            Group Group;
            Util::index_t Id;
        };

        class SphereCollider: public Collider
        {

        public:

            virtual bool Intersects(const SphereCollider& collider) override;
            virtual bool Intersects(const PointCollider& collider) override;
            virtual bool Intersects(const AABBCollider& collider) override;
            virtual bool Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const override;

            Type GetType() const override
            {
                return Type::Sphere;
            }

            float Radius;

            inline SphereCollider(float r) { Radius = r; }
            inline SphereCollider() { Radius = 1; }
        };



        class PointCollider: public Collider
        {

        public:

            virtual bool Intersects(const SphereCollider& collider) override;
            virtual bool Intersects(const PointCollider& collider) override;
            virtual bool Intersects(const AABBCollider& collider) override;
            virtual bool Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const override;

            Type GetType() const override
            {
                return Type::Point;
            }

        };

        class AABBCollider: public Collider
        {

        public:

            std::vector<Math::Vector3> GetVertices();
            virtual bool Intersects(const SphereCollider& collider) override;
            virtual bool Intersects(const PointCollider& collider) override;
            virtual bool Intersects(const AABBCollider& collider) override;
            bool DynamicIntersects2D(const AABBCollider& collider, const Math::Vector3 velocity, Math::Vector3& contactPoint, Math::Vector2& contactNormal, double& time);
            virtual bool Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const override;
            bool Intersects2D(const Ray ray, Math::Vector3& contactPoint, Math::Vector2& contactNormal, double& hitTimeNear);


                Type GetType() const override
            {
                return Type::AABB;
            }

            //For AABBColliders, Position is top left corner, Position + Size is bottom right corner
            Math::Vector3 Size;

            inline AABBCollider(Math::Vector3 size): Size(size) {}
            inline AABBCollider() : Size(1) {}

        };

    } // namespace Physics

} // namespace Tesseract

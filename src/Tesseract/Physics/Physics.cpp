#include "Physics.h"

#include "Tesseract/Core/Application.h"
#include "Tesseract/Profiling/ProfilerScopedTimer.h"
#include <chrono>


namespace Tesseract
{
    namespace Physics
    {

        float Physics::s_Frequency = 60.0f;
        bool Physics::s_Running = false;
        Util::Spinlock Physics::s_Spinlock;

        State Physics::ComputeStep(const State& state, float mass, float drag)
        {
            State result = state;

            return ComputeStepInPlace(result, mass, drag);
        }


        State& Physics::ComputeStepInPlace(State& state, float mass, float drag)
        {

            //REVIEW: Drag computation somewhere else like hasGravity?
            float magnitude = state.Velocity.Magnitude();

            state.Position += (state.Velocity * GetInterval());

            Math::Vector3 dragVector;

            if (Math::Functions::Abs(magnitude) > Math::Constants::Epsilon)
            {
                //f = 1/2 * A (approx 1) * rho(air) * cw * v^2 (* -1 since its counterforce)
                // TODO: @Justin @Christian Check if drag force "overshoots"
                // TODO: Find a way to detect this and set velocity to zero if this occurs
                // TODO: Currently, at low framerates any previously moving object will oscillate because of drag
                dragVector = (state.Velocity.Normalized() * magnitude * magnitude * 0.5f * drag * 1.0f * -1.0f) / mass;
            }

            float dragMagnitude = dragVector.Magnitude();
            float velocityMagnitude = state.Velocity.Magnitude();

            if (dragMagnitude > velocityMagnitude)
            {
                dragVector = dragVector * 0.9f * (velocityMagnitude / dragMagnitude);
            }

            state.Velocity += ((state.Acceleration + dragVector) * GetInterval());
            // state.Acceleration = 0.f;

            return state;
        }


        State Physics::AddForce(const State& state, const Math::Vector3& force, float mass)
        {
            State result = state;

            return AddForceInPlace(result, force, mass);
        }


        State& Physics::AddForceInPlace(State& state, const Math::Vector3& force, float mass)
        {
            state.Acceleration += (force / mass);

            return state;
        }


        void Physics::AcquireLock()
        {
            s_Spinlock.Lock();
        }

        void Physics::FreeLock()
        {
            s_Spinlock.Unlock();
        }

        void Physics::Run()
        {

            static std::chrono::time_point<std::chrono::system_clock> last = std::chrono::system_clock::now();

            s_Running = true;

            while (s_Running)
            {
                std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
                std::chrono::duration<double> elapsed_seconds = now - last;
                double secondsInDouble = elapsed_seconds.count();

                if (secondsInDouble >= (1.0 / s_Frequency))
                {
                    AcquireLock();
                    Update();
                    FreeLock();

                    last = now;
                }
            }
        }

        void Physics::Stop()
        {
            s_Running = false;
        }


        void Physics::SetFrequency(float frequency)
        {
            s_Frequency = frequency;
        }

        void Physics::Update()
        {
            TESSERACT_PROFILE_FUNCTION();
            Core::Application::GetApplication()->OnPhysicsUpdate();
        }


        float Physics::GetInterval()
        {
            return 1.0 / s_Frequency;
        }

    } // namespace Physics
} // namespace Tesseract
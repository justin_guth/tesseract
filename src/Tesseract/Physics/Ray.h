#pragma once

#include <algorithm>

#include "Tesseract/Math/Vector3.h"

namespace Tesseract
{
    namespace Physics
    {


        class Ray
        {

        public:

            Math::Vector3 Position;
            Math::Vector3 Orientation;

            /*
            inline bool RayIntersectsAABB2D(const AABBCollider& collider, Math::Vector3& contactPoint, Math::Vector2& contactNormal, double& hitTimeNear)
            {
                Tesseract::ECS::Comp<Tesseract::Components::TransformComponent> colTransform = collider.Transform;
                Math::Vector3 colPos = collider.Offset + colTransform->GetGlobalPosition();


                Math::Vector3 tNear = (colPos - Position) / Orientation;
                Math::Vector3 tFar = (colPos + collider.Size - Position) / Orientation;

                if (tNear.X > tFar.X) { std::swap(tNear.X, tFar.X); }
                if (tNear.Y > tFar.Y) { std::swap(tNear.Y, tFar.Y); }

                if (tNear.X > tFar.Y || tNear.Y > tFar.X) { return false; }

                hitTimeNear = std::max(tNear.X, tNear.Y);
                double hitTimeFar = std::min(tFar.X, tFar.Y);


                if (hitTimeFar < 0) { return false; }

                contactPoint = Position + tNear * Orientation;

                if (tNear.X > tNear.Y)
                {
                    if (Orientation.X < 0)
                    {
                        contactNormal = { 1, 0 };
                    }
                    else
                    {
                        contactNormal = { -1, 0 };
                    }
                }
                else if (tNear.X < tNear.Y)
                {
                    if (Orientation.Y < 0)
                    {
                        contactNormal = { 0, 1 };
                    }
                    else
                    {
                        contactNormal = { 0, -1 };
                    }
                }

                return true;
            }
        */
        };


        class IIntersectable
        {
        public:
            virtual bool Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const = 0;
        };

        class Plane: public IIntersectable
        {
        public:
            Math::Vector3 Support;
            Math::Vector3 Normal;

            inline Plane(Math::Vector3 s, Math::Vector3 n): Support(s), Normal(n) {}

            inline virtual bool Intersects(const Ray r, Math::Vector3& contactPoint, Math::Vector3& contactNormal, double& hitTimeNear) const override
            {
                float d = r.Orientation.Dot(Normal);
                if (d == 0) { return false; }

                float sdotn = Support.Dot(Normal);
                float odotn = r.Position.Dot(Normal);

                hitTimeNear = (Support.Dot(Normal) - r.Position.Dot(Normal)) / d;
                contactPoint = r.Position + r.Orientation * hitTimeNear;
                contactNormal = Normal;
                return hitTimeNear > 0;
            }
        };

    } // namespace Physics
} // namespace Tesseract
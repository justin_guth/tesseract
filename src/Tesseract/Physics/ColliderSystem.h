#pragma once

#include <unordered_map>
#include <unordered_set>

#include "Tesseract/Physics/Collider.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Components/ColliderComponent.h"
#include "Tesseract/Components/TransformComponent.h"

#include "Tesseract/Util/Index.h"

namespace Tesseract
{
    namespace Physics
    {

        struct Collisions
        {
            /*
            //TODO Add logic for Collision leave with second mCollisionmap
            std::unordered_map<Util::index_t, std::unordered_set<Util::index_t>, Util::index_t_hash> m_CollisionMap; doesnt work
            //std::unordered_map<std::pair<Util::index_t, Util::index_t>, bool, Util::index_t_hash> m_NewCollisionMap; //Maybe need to define own hash function

            bool IsNew(Util::index_t id, Util::index_t otherId)
            {
                return !m_CollisionMap.at(id).contains(otherId);
            }

            void Add(Util::index_t id, Util::index_t otherId)
            {
                m_CollisionMap[id].insert(otherId);
            } 
            */       
        };

        class ColliderSystem
        {

        public:

            void AddCollider(Tesseract::Components::ColliderComponent& collider);
            void CheckCollisions();

            std::vector<ECS::Comp<Tesseract::Components::ColliderComponent>> Colliders;

        private:

            Collisions m_Collisions;
            void CheckIntersection(Ref<Collider>& collider, ECS::Comp<Tesseract::Components::ColliderComponent> colliderComp);
        };


    } // namespace Physics
} // namespace Tesseract
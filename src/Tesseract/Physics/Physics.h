#pragma once

#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Util/Spinlock.h"

namespace Tesseract
{
    namespace Physics
    {


        struct State
        {
            Math::Vector3 Position;
            Math::Vector3 Velocity;
            Math::Vector3 Acceleration;

        };

            
        
        class Physics
        {
        public:

            static State ComputeStep(const State& state, float mass, float drag);
            static State& ComputeStepInPlace(State& state, float mass, float drag);
            static State AddForce(const State& state, const Math::Vector3& force, float mass);
            static State& AddForceInPlace(State& state, const Math::Vector3& force, float mass);

            static void AcquireLock();
            static void FreeLock();

            static void Run();
            static void Stop();
            static void Update();
            static void SetFrequency(float frequency);

            static float GetInterval();

        private: 

            static float s_Frequency;
            static bool s_Running;

		    static Util::Spinlock s_Spinlock;



        };

    } // namespace Physics
} // namespace Tesseract

#include "ColliderSystem.h"

#include "Tesseract/Components/ScriptComponent.h"
#include "Tesseract/Components/RigidBodyComponent.h"

namespace Tesseract
{

    namespace Physics
    {


        void ColliderSystem::AddCollider(Tesseract::Components::ColliderComponent& collider)
        {
            ECS::Comp<Tesseract::Components::ColliderComponent> colliderComp = collider;
            Colliders.emplace_back(colliderComp);
        }


        void ColliderSystem::CheckCollisions()
        {

            for (ECS::Comp<Tesseract::Components::ColliderComponent> colliderComp : Colliders)
            {
                for (Ref<Collider> collider : colliderComp->Colliders)
                {

                    if (collider->IsActive && collider->Enabled)
                    {
                        ColliderSystem::CheckIntersection(collider, colliderComp);
                    }

                }
            }
        }

        void ColliderSystem::CheckIntersection(Ref<Collider>& collider, ECS::Comp<Tesseract::Components::ColliderComponent> myColliderComp)
        {
            for (ECS::Comp<Tesseract::Components::ColliderComponent> otherColliderComp : Colliders)
            {

                for (Ref<Collider> otherCollider : otherColliderComp->Colliders)
                {

                    if (!myColliderComp->Entity.HasComponent<Components::ScriptComponent>()) { break; }
                    if (myColliderComp->Entity.GetID() == otherColliderComp->Entity.GetID()) { break; }

                    if (collider->ToResolve && otherCollider->ToResolve
                        && collider->Group == Collider::Group::Dynamic && otherCollider->Group == Collider::Group::Static) //Collision Resolution
                    {
                        AABBCollider aabb = *(std::dynamic_pointer_cast<AABBCollider>(collider));
                        AABBCollider otherAABB = *(std::dynamic_pointer_cast<AABBCollider>(otherCollider));

                        Math::Vector3 velocity = myColliderComp->Entity.GetComponent<Components::RigidBodyComponent>().State.Velocity;
                        Math::Vector3 contactPoint = { 0, 0, 0 };
                        Math::Vector2 contactNormal;
                        double contactTime;

                        if (aabb.DynamicIntersects2D(otherAABB, velocity, contactPoint, contactNormal, contactTime))
                        {
                            Math::Vector3 displacement = { contactNormal.X * std::abs(velocity.X), contactNormal.Y * std::abs(velocity.Y), 0 };
                            myColliderComp->Entity.GetComponent<Components::RigidBodyComponent>().State.Velocity += displacement * (1 - (contactTime * Math::Constants::Epsilon));
                        }


                    }


                    if (!(otherCollider->Enabled && collider->Intersects(otherCollider))) { continue; }


                    if (true) //TODO: Implement different onCollision hooks
                    {
                        for (Ref<Scripting::Script>& script : myColliderComp->Entity.GetComponent<Components::ScriptComponent>().Scripts)
                        {
                            script->OnCollision(collider, otherCollider, *myColliderComp, *otherColliderComp);
                        }
                    }
                    else
                    {
                        for (Ref<Scripting::Script>& script : myColliderComp->Entity.GetComponent<Components::ScriptComponent>().Scripts)
                        {
                            script->OnCollisionEnter(collider, otherCollider, *myColliderComp, *otherColliderComp);
                        }
                    }

                }
            }
        }


    } // namespace Physics

} // namespace Tesseract
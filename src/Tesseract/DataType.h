#pragma once

namespace Tesseract
{
    enum class DataType
    {
        None = 0,
        Float1,
        Float2,
        Float3,
        Float4,
        Color,
        Int1,
        Int2,
        Int3,
        Int4,
        Uint1,
        Uint2,
        Uint3,
        Uint4,
        Mat2,
        Mat3,
        Mat4,
        Bool
    };
} // namespace Tesseract

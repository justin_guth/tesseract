#pragma once

#include "Identifier.h"
#include "Time.h"

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <limits>

namespace Tesseract
{
    namespace Core
    {
        class Ticker
        {
        public:

            struct 	TickerData
            {
                float Period;
                float Offset;
                float LastTick;
                uint32_t TickCount;

                TickerData():
                    TickerData(1.0f, 0.0f)
                {
                }

                TickerData(float period, float offset):
                    Period(period),
                    Offset(offset),
                    LastTick(-std::numeric_limits<float>::max()),
                    TickCount(0)
                {
                }
            };



            inline void Update()
            {
                m_DueTicks.clear();

                float time = Time::TotalGame();

                for (auto& [id, data] : m_Tickers)
                {
                    if (
                        data.LastTick < 0.0f && time >= data.Offset * data.Period
                        || data.LastTick >= 0.0f && time > data.LastTick + data.Offset
                        )
                    {
                        data.LastTick = data.Period * data.TickCount + data.Offset;
                        m_DueTicks.insert(id);
                        data.TickCount++;
                    }
                }
            }

            inline void RegisterTicker(const Identifier& identifier, float period, float offset = 0.0f)
            {
                m_Tickers[identifier] = TickerData(period, offset);
            }

            inline bool Tick(const Identifier& identifier)
            {
                return m_DueTicks.contains(identifier);
            }

        private:

            std::unordered_map<Identifier, TickerData, IdentifierHashFunction> m_Tickers;
            std::unordered_set<Identifier, IdentifierHashFunction> m_DueTicks;
        };
    } // namespace Core

} // namespace Tesseract

#include "Geometry.h"


namespace Tesseract
{
    namespace Core
    {

        void Geometry::RecomputeTangentSpace()
        {
            for (int32_t i = 0; i < Indices.size(); i += 3)
            {
                VertexData& v1 = Vertices[Indices[i + 0]];
                VertexData& v2 = Vertices[Indices[i + 1]];
                VertexData& v3 = Vertices[Indices[i + 2]];

                // Compute edges:

                Math::Vector3 edge12 = v2.Position - v1.Position;
                Math::Vector3 edge13 = v3.Position - v1.Position;

                Math::Vector2 deltaTextureCoordinate12 = v2.TextureCoordinate - v1.TextureCoordinate;
                Math::Vector2 deltaTextureCoordinate13 = v3.TextureCoordinate - v1.TextureCoordinate;

                // Compute Tangent and bitangent vector

                float f = 1.0f / (deltaTextureCoordinate12.X * deltaTextureCoordinate13.Y - deltaTextureCoordinate13.X * deltaTextureCoordinate12.Y);

                Math::Vector3 tangent(
                    f * (deltaTextureCoordinate13.Y * edge12.X - deltaTextureCoordinate12.Y * edge13.X),
                    f * (deltaTextureCoordinate13.Y * edge12.Y - deltaTextureCoordinate12.Y * edge13.Y),
                    f * (deltaTextureCoordinate13.Y * edge12.Z - deltaTextureCoordinate12.Y * edge13.Z)
                );

                Math::Vector3 bitangent(
                    f * (-deltaTextureCoordinate13.X * edge12.X + deltaTextureCoordinate12.X * edge13.X),
                    f * (-deltaTextureCoordinate13.X * edge12.Y + deltaTextureCoordinate12.X * edge13.Y),
                    f * (-deltaTextureCoordinate13.X * edge12.Z + deltaTextureCoordinate12.X * edge13.Z)
                );

                v1.Tangent = tangent;
                v2.Tangent = tangent;
                v3.Tangent = tangent;

                v1.Bitangent = bitangent;
                v2.Bitangent = bitangent;
                v3.Bitangent = bitangent;

                
            }
        }
    } // namespace Core

} // namespace Tesseract

#include "Layer.h"


namespace Tesseract
{
    namespace Core
    {
        void Layer::OnAttach()
        {
        }

        void Layer::OnDetach()
        {
        }

        void Layer::OnUpdate()
        {
        }

        void Layer::OnPhysicsUpdate()
        {
        }

        void Layer::OnImGuiUpdate()
        {
        }

        void Layer::OnEvent(Events::Event& event)
        {
            
        }


    } // namespace Core

} // namespace Tesseract

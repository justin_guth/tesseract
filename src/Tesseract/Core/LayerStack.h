#pragma once

#include <vector>
#include <stdint.h>

#include "Layer.h"

namespace Tesseract
{
    namespace Core
    {
        class LayerStack
        {
        public:

            LayerStack();
            ~LayerStack();

            void PushLayer(Layer* layer);
            void PushOverlay(Layer* overlay);

            void RemoveLayer(Layer* layer);

            inline std::vector<Layer*>::iterator begin() { return m_Layers.begin(); }
            inline std::vector<Layer*>::iterator end() { return m_Layers.end(); }

            inline std::vector<Layer*>::const_iterator cbegin() const { return m_Layers.cbegin(); }
            inline std::vector<Layer*>::const_iterator cend() const { return m_Layers.cend(); }

        private:

            std::vector<Layer*> m_Layers;
            int32_t m_LayerInsertPosition;

        };
    } // namespace Core

} // namespace Tesseract

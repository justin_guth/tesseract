#pragma once

#include "Layer.h"

#include "Scene.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Graphics/Mesh.h"
#include "Tesseract/Core/ResourceManager.h"

namespace Tesseract
{
    namespace Core
    {
        class GUILayer : public Layer
        {
        public:
            GUILayer();
            ~GUILayer();


            void OnAttach() override;
            void OnUpdate() override;
            void OnEvent(Events::Event& event) override;

        private:

            Ref<Scene> m_ActiveScene;

        };

    } // namespace Core

} // namespace Tesseract

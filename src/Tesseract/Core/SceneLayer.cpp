#include "Application.h"
#include "SceneLayer.h"
#include "Tesseract/Math/Matrix4.h"
#include "Tesseract/Components/CameraComponent.h"
#include "Tesseract/Components/MeshComponent.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/RigidBodyComponent.h"
#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/SpriteAnimationComponent.h"
#include "Tesseract/Components/TextComponent.h"
#include "Tesseract/Components/LightSourceComponent.h"
#include "Tesseract/Components/ScriptComponent.h"
#include "Tesseract/Components/AudioSourceComponent.h"
#include "Tesseract/Components/ColliderComponent.h"
#include "Tesseract/Physics/ColliderSystem.h"
#include "Tesseract/Graphics/Renderer.h"
#include "Tesseract/Graphics/Bits.h"
#include "Tesseract/Graphics/FrameBuffer.h"
#include "Tesseract/Util/JSONParser.h"
#include "Tesseract/Reference.h"
#include <algorithm>

#include "Tesseract/Logging/Logger.h"
#include "Tesseract/Profiling/ProfilerScopedTimer.h"

namespace Tesseract
{
    namespace Core
    {
        SceneLayer::SceneLayer():
            m_ActiveScene(Scene::Create()),
            m_RuntimeRunning(false)

        {

        }

        SceneLayer::~SceneLayer()
        {

        }


        void SceneLayer::OnAttach()
        {

        }

        void SceneLayer::OnImGuiUpdate()
        {
            //if (!m_RuntimeRunning)
            //{
            m_ActiveScene->OnImGuiUpdate();
            //}
        }
        void SceneLayer::OnPhysicsUpdate()
        {
            std::vector<ECS::Entity> rigidBodyEntities;
            std::vector<ECS::Entity> colliderEntities;
            std::vector<ECS::Entity> scriptEntities;

            {
                TESSERACT_PROFILE_SCOPE("Physics Entity Grouping");

                for (ECS::Entity entity : m_ActiveScene->GetRegistry().AllEntites())
                {
                    if (entity.HasComponents<Components::TransformComponent, Components::RigidBodyComponent>())
                    {
                        rigidBodyEntities.push_back(entity);
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::ColliderComponent>())
                    {
                        colliderEntities.push_back(entity);
                    }

                    if (entity.HasComponent<Components::ScriptComponent>())
                    {
                        scriptEntities.push_back(entity);
                    }

                }
            }
            std::vector<Ref<Scripting::Script>> scripts;

            {
                TESSERACT_PROFILE_SCOPE("Physics Script entities retrieval");

                for (ECS::Entity entity : scriptEntities)
                {
                    Components::ScriptComponent& script = entity.GetComponent<Components::ScriptComponent>();

                    for (const Ref<Scripting::Script>& script : script.Scripts)
                    {
                        scripts.push_back(script);
                    }
                }
            }
            {
                TESSERACT_PROFILE_SCOPE("Physics Script entities update");

                for (const Ref<Scripting::Script>& script : scripts)
                {
                    script->BeforePhysicsUpdate();
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("RigidBody Updates");
                // Physics computation
                for (ECS::Entity entity : rigidBodyEntities)
                {
                    const auto& [transform, rigidBody] = entity.GetComponents<Components::TransformComponent, Components::RigidBodyComponent>();

                    if (rigidBody.HasGravity)
                    {
                        rigidBody.AddForce(Math::Vector3::UnitZ() * -98.1f * rigidBody.Mass);
                    }

                    //TODO: @Justin Fix the world
                    //rigidBody.State.Position = transform.GetGlobalTransformMatrix().Column3.XYZ();
                    rigidBody.State.Position = transform.Position;
                    Physics::Physics::ComputeStepInPlace(rigidBody.State, rigidBody.Mass, rigidBody.Drag);
                    //transform.Position = (transform.GetGlobalTransformMatrix().Inverse() * rigidBody.State.Position.HomogenizedPoint()).XYZ();
                    transform.Position = rigidBody.State.Position;
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("Physics Script entities update");

                for (const Ref<Scripting::Script>& script : scripts)
                {
                    script->OnPhysicsUpdate();
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("Collision Updates");

                //collision detection
                for (ECS::Entity entity : colliderEntities)
                {
                    Components::ColliderComponent& collider = entity.GetComponent<Components::ColliderComponent>();
                    m_ColliderSystem.AddCollider(collider); //TODO: @Christian Do Octree or something instead of this
                }

                m_ColliderSystem.CheckCollisions();
                m_ColliderSystem.Colliders.clear();
            }




        }

        void SceneLayer::OnUpdate()
        {
            std::vector<ECS::Entity> scriptEntities;
            std::vector<ECS::Entity> spriteAnimationEntities;
            std::vector<ECS::Entity> rigidBodyEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Components::CameraComponent,
                Math::Vector3
                >> cameraEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Components::TextComponent,
                Math::Matrix4
                >> textEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::LightSourceComponent,
                Math::Vector3
                >> lightEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::MeshComponent,
                Math::Matrix4
                >> meshEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::SpriteAnimationComponent,
                Math::Matrix4
                >> spriteEntities;

            Physics::Physics::AcquireLock();

            {
                using namespace std;

                TESSERACT_PROFILE_SCOPE("Entity Grouping");


                for (ECS::Entity entity : m_ActiveScene->GetRegistry().AllEntites())
                {
                    if (entity.HasComponents<Components::TransformComponent, Components::RigidBodyComponent>())
                    {
                        rigidBodyEntities.push_back(entity);
                    }

                    if (entity.HasComponent<Components::ScriptComponent>())
                    {
                        scriptEntities.push_back(entity);
                    }

                    if (entity.HasComponent<Components::SpriteAnimationComponent>())
                    {
                        spriteAnimationEntities.push_back(entity);
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::CameraComponent>())
                    {
                        auto [transform, camera] = entity.GetComponents<Components::TransformComponent, Components::CameraComponent>();

                        cameraEntities.push_back(
                            std::tuple{
                                entity,
                                transform,
                                camera,
                                transform.GetGlobalPosition()
                            }
                        );
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::TextComponent>())
                    {
                        auto [transform, text] = entity.GetComponents<Components::TransformComponent, Components::TextComponent>();

                        textEntities.push_back(
                            std::tuple{
                                entity,
                                transform,
                                text,
                                transform.GetGlobalTransformMatrix()
                            }
                        );
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::LightSourceComponent>())
                    {
                        auto [transform, lightSource] = entity.GetComponents<Components::TransformComponent, Components::LightSourceComponent>();

                        lightEntities.push_back(
                            tuple{
                                entity,
                                lightSource,
                                transform.GetGlobalPosition()
                            }
                        );
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::MeshComponent>())
                    {
                        auto [transform, mesh] = entity.GetComponents<Components::TransformComponent, Components::MeshComponent>();

                        meshEntities.push_back(
                            tuple{
                                entity,
                                mesh,
                                transform.GetGlobalTransformMatrix()
                            }
                        );
                    }


                    if (entity.HasComponents<Components::TransformComponent, Components::SpriteAnimationComponent>())
                    {
                        auto [transform, sprite] = entity.GetComponents<Components::TransformComponent, Components::SpriteAnimationComponent>();

                        spriteEntities.push_back(
                            tuple{
                                entity,
                                sprite,
                                transform.GetGlobalTransformMatrix()
                            }
                        );
                    }
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("RigidBody Resets");
                // Physics computation
                for (ECS::Entity entity : rigidBodyEntities)
                {
                    Components::RigidBodyComponent& rigidBody = entity.GetComponent<Components::RigidBodyComponent>();

                    rigidBody.State.Acceleration = 0.0f;
                }
            }

            Physics::Physics::FreeLock();


            Math::Matrix4 projection;
            Math::Matrix4 view;
            Math::Vector3 viewPosition;

            // Update camera view

            for (auto& [entity, transform, camera, globalPosition] : cameraEntities)
            {
                if (camera.Camera != nullptr && camera.Camera->IsActive())
                {
                    // TODO: Get global position (note: scale cannot be included!)
                    camera.Camera->SetViewParameters(globalPosition, transform.RotationEulerAngles);
                    projection = camera.Camera->GetProjectionMatrix();
                    view = camera.Camera->GetViewMatrix();
                    // TODO: some GetGlobalPosition getter
                    viewPosition = globalPosition;
                }
            }

            Graphics::Renderer::SetProjectionMatrix(projection);
            Graphics::Renderer::SetViewMatrix(view);
            Graphics::Renderer::SetViewPosition(viewPosition);

            std::vector<std::tuple<Ref<Scripting::Script>, std::string>> scripts;

            {
                TESSERACT_PROFILE_SCOPE("Script entities retrieval");

                for (ECS::Entity entity : scriptEntities)
                {
                    Components::ScriptComponent& script = entity.GetComponent<Components::ScriptComponent>();

                    for (const Ref<Scripting::Script>& script : script.Scripts)
                    {
                        scripts.push_back(std::make_tuple(script, entity.GetComponent<Components::TagComponent>().Tag));
                    }
                }
            }


            if (m_RuntimeRunning)
            {
                Time::UpdateGameTime();

                m_ActiveScene->OnRuntimeUpdate();


                {
                    TESSERACT_PROFILE_SCOPE("Script entities update");


                    Physics::Physics::AcquireLock();
                    for (const auto& [ script, tag] : scripts)
                    {
                        TESSERACT_PROFILE_SCOPE("Updating Script on {}", tag);

                        script->OnUpdate();

                    }
                    Physics::Physics::FreeLock();

                }

                {
                    TESSERACT_PROFILE_SCOPE("Sprite animation update");

                    for (ECS::Entity entity : spriteAnimationEntities)
                    {
                        Components::SpriteAnimationComponent& animation = entity.GetComponent<Components::SpriteAnimationComponent>();

                        if (animation.Animation != nullptr)
                        {
                            animation.Animation->Update();
                        }
                    }
                }

                // Here was the physics and rigidbody update

                // Audio computation
                // for (const auto& [entity, transform, audioSource] : m_ActiveScene->GetRegistry().Filter<Components::TransformComponent, Components::AudioSourceComponent>())
                // {
                //     // TODO: Update positions and velocities etc.
                // }

            }

            Math::Vector3 primaryLightPosition;
            {
                TESSERACT_PROFILE_SCOPE("Light Update");
                // Get list of light sources

                Graphics::Renderer::ClearLights();
                Graphics::Renderer::SetCameraPosition(viewPosition);


                for (auto& [entity, lightSource, globalPosition] : lightEntities)
                {
                    if (lightSource.Type == Graphics::LightSourceType::Point)
                    {
                        Graphics::Renderer::AddPointLight(
                            // TODO: Replace with some GetGlobalPosition thing
                            globalPosition,
                            lightSource.Color,
                            lightSource.Intensity
                        );


                    }

                    if (lightSource.IsPrimary)
                    {
                        primaryLightPosition = globalPosition;
                    }
                }
            }

            // Shadow computation:

            // for light in lights:
            Graphics::Renderer::BeginShadowPass(); // TODO: somehow pass a texture or sth.
            Graphics::Renderer::EnableDepthTest();

            Graphics::Renderer::ComputeShadows(primaryLightPosition);

            Graphics::Renderer::SetBackfaceCullingEnabled(true);

            {
                TESSERACT_PROFILE_SCOPE("Drawing mesh shadows");

                // Draw all meshes in scene

                for (auto& [entity, meshComponent, globalTransform] : meshEntities)
                {
                    Graphics::Renderer::DrawMeshShadow(
                        meshComponent.Mesh,
                        globalTransform
                    );
                }
            }

            // TODO: remove, since current setup aready provides copied data
            std::vector<std::tuple<float, Components::SpriteAnimationComponent, Math::Matrix4>> sprites;
            {
                TESSERACT_PROFILE_SCOPE("Retrieving sprites");


                for (auto& [entity, spriteAnimationComponent, globalTransform] : spriteEntities)
                {
                    float distance = Graphics::Renderer::GetSpriteDistanceFunction()(viewPosition, globalTransform.Column3.XYZ());

                    sprites.push_back({ distance, spriteAnimationComponent, globalTransform });
                }
            }

            {
                TESSERACT_PROFILE_SCOPE("Sorting sprites");
                // Sort sprites back to front so there are no trivial z buffer issues
                std::sort(
                    sprites.begin(),
                    sprites.end(),
                    [](auto const& t1, auto const& t2)
                    {
                        return get<0>(t1) > get<0>(t2);
                    }
                );
            }

            Graphics::Renderer::SetBackfaceCullingEnabled(false);



            {
                TESSERACT_PROFILE_SCOPE("Drawing sprite shadows");

                for (auto [_, spriteAnimationComponent, globalTransform] : sprites)
                {
                    if (!spriteAnimationComponent.CastsShadow)
                    {
                        continue;
                    }

                    Graphics::Renderer::DrawAnimatedQuadShadow(
                        globalTransform,
                        spriteAnimationComponent.Animation
                    );
                }
                Graphics::Renderer::FlushSpriteBuffer();
            }



            Graphics::Renderer::BeginRenderPass();




            Graphics::Renderer::SetBackfaceCullingEnabled(true);


            {
                TESSERACT_PROFILE_SCOPE("Drawing meshes");

                for (auto& [entity, meshComponent, globalTransform] : meshEntities)
                {
                    Graphics::Renderer::DrawMesh(
                        meshComponent.Mesh,
                        globalTransform);
                }
            }

            Graphics::Renderer::SetBackfaceCullingEnabled(false);

            // Draw all sprite animations in scene



            {
                TESSERACT_PROFILE_SCOPE("Drawing sprites");

                for (auto [_, spriteAnimationComponent, globalTransform] : sprites)
                {
                    Graphics::Renderer::DrawAnimatedQuad(
                        globalTransform,
                        spriteAnimationComponent.Animation);
                }
                Graphics::Renderer::FlushSpriteBuffer();
            }


            {
                TESSERACT_PROFILE_SCOPE("Text rendering");

                for (auto& [entity, transform, text, globalTransform] : textEntities)
                {
                    if (text.IsOverlay || !text.Visible) {
                        continue;
                    }

                    Graphics::Renderer::DrawText(
                        text.Text, 
                        globalTransform, 
                        text.Face,
                        text.Size, 
                        text.Color
                    );
                }

                Graphics::Renderer::FlushTextBuffer();
            }

            {
                TESSERACT_PROFILE_SCOPE("Script draw update");

                for (const auto& [ script, tag] : scripts)
                {
                    TESSERACT_PROFILE_SCOPE("Calling Script OnDraw on {}", tag);

                    script->OnDraw();

                }
            }
            


            {
                TESSERACT_PROFILE_SCOPE("Destroying queued entities");

                Physics::Physics::AcquireLock();

                // Must happen last
                m_ActiveScene->GetRegistry().DestroyQueuedEnties();

                Physics::Physics::FreeLock();
            }

            
        }

        void SceneLayer::OnEvent(Events::Event& event)
        {
            Events::EventHandler(event).Handle<Tesseract::Events::WindowResizeEvent>(
                [this](Events::WindowResizeEvent& event) -> bool
                {
                    if (m_ActiveScene == nullptr)
                    {
                        return false;
                    }

                    float aspectRatio = (float)event.GetWidth() / (float)event.GetHeight();

                    for (auto [entity, camera] : m_ActiveScene->GetRegistry().Filter<Components::CameraComponent>())
                    {
                        if (camera.Camera == nullptr)
                        {
                            continue;
                        }

                        camera.Camera->SetWindowSize(event.GetWidth(), event.GetHeight());
                    }

                    return false;
                }
            );
        }


        Ref<Scene> SceneLayer::LoadScene(const std::string& filePath)
        {
            // FIXME: If scripts try to spawn entities in OnAttach, they are attached to the wrong scene
            m_ActiveScene = Ref<Scene>(
                Scene::FromSerializationData(
                    Util::JSONParser::LoadJSONFile(std::string("../../res/scenes/") + filePath + ".json")
                )
            );


            Events::SceneLoadedEvent event(m_ActiveScene);
            Core::Application::GetApplication()->OnEvent(
                event
            );

            Time::ResetGameTime();

            return m_ActiveScene;
        }


    } // namespace Core

} // namespace Tesseract

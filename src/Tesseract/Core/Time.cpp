#include "Time.h"
#include "Tesseract/Logging/Logger.h"

namespace Tesseract
{
    namespace Core
    {
        bool Time::m_IsInitialized = false;
        std::chrono::steady_clock::time_point Time::m_Last = std::chrono::steady_clock::now();
        double Time::m_Delta = 1.0;
        double Time::m_Total = 0.0;
        double Time::m_TotalGame = 0.0;

        void Time::Init()
        {
            if (Time::m_IsInitialized)
            {
                return;
            }

            Time::m_Last = std::chrono::steady_clock::now();
            Time::m_Total = 0.0f;
            TESSERACT_CORE_INFO("Initialised time module");

            Time::m_IsInitialized = true;
        }

        void Time::Update()
        {
            std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
            int64_t delta = std::chrono::duration_cast<std::chrono::nanoseconds>(now - Time::m_Last).count();
            Time::m_Delta = (double)delta / 1000000000.0; // cast to seconds
            Time::m_Last = now;
            Time::m_Total += Time::m_Delta;
        }

        void Time::UpdateGameTime()
        {
            Time::m_TotalGame += Time::m_Delta;
        }

        void Time::ResetGameTime()
        {
            Time::m_TotalGame = 0.0f;
        }

        

    } 
} 

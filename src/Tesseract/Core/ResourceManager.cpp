#include "ResourceManager.h"


#include "Tesseract/Util/MeshLoading.h"

#include "Tesseract/Util/DynamicData.h"
#include "Tesseract/Util/JSONParser.h"

#include "Tesseract/Materials/PhongMaterial.h"
#include "Tesseract/Materials/PBRMaterial.h"
#include "Tesseract/Materials/FlatMaterial.h"
#include "Tesseract/Materials/SpriteMaterial.h"


namespace Tesseract
{
    namespace Core
    {

        DEFINE_TABLE(Geometry);
        DEFINE_TABLE(ShaderProgram);
        DEFINE_TABLE(Material);
        DEFINE_TABLE(VertexBuffer);
        DEFINE_TABLE(ElementBuffer);
        DEFINE_TABLE(VertexArrayObject);
        DEFINE_TABLE(Mesh);
        DEFINE_TABLE(Texture2D);
        DEFINE_TABLE(TextureAtlas);
        DEFINE_TABLE(SpriteAnimation);
        DEFINE_TABLE(Prefab);
        DEFINE_TABLE(AudioClip);


        TESSERACT_RESOURCEMANAGER_IMPL(Geometry);
        TESSERACT_RESOURCEMANAGER_IMPL(ShaderProgram);
        TESSERACT_RESOURCEMANAGER_IMPL(Material);
        TESSERACT_RESOURCEMANAGER_IMPL(VertexBuffer);
        TESSERACT_RESOURCEMANAGER_IMPL(ElementBuffer);
        TESSERACT_RESOURCEMANAGER_IMPL(VertexArrayObject);
        TESSERACT_RESOURCEMANAGER_IMPL(Mesh);
        TESSERACT_RESOURCEMANAGER_IMPL(Texture2D);
        TESSERACT_RESOURCEMANAGER_IMPL(TextureAtlas);
        TESSERACT_RESOURCEMANAGER_IMPL(SpriteAnimation);
        TESSERACT_RESOURCEMANAGER_IMPL(Prefab);
        TESSERACT_RESOURCEMANAGER_IMPL(AudioClip);

        FontLibrary ResourceManager::s_FontLibrary;


        template <>
        Ref<Geometry> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_GeometryTable.contains(resourceIdentifier))
            {
                return s_GeometryTable.at(resourceIdentifier).ResourceReference;
            }

            Ref<Geometry> result = Util::LoadObjFileData(std::string("../../res/geometry/") + resourceIdentifier + ".obj", false);

            result->MarkPersistent(resourceIdentifier);

            s_GeometryTable[resourceIdentifier] = { result };

            return result;
        }

        template <>
        Ref<Face> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            Ref<Face> result = s_FontLibrary.LoadFace("../../res/fonts/" + resourceIdentifier + ".ttf", resourceIdentifier);

            return result;
        }


        template <>
        Ref<Graphics::Texture2D> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_Texture2DTable.contains(resourceIdentifier))
            {
                return s_Texture2DTable.at(resourceIdentifier).ResourceReference;
            }

            // TODO: do something about extentions
            Ref<Graphics::Texture2D> result = Graphics::Texture2D::LoadFromFile(std::string("../../res/textures/") + resourceIdentifier + ".png");

            result->MarkPersistent(resourceIdentifier);

            s_Texture2DTable[resourceIdentifier] = { result };

            return result;
        }


        template <>
        Ref<Graphics::Material> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_MaterialTable.contains(resourceIdentifier))
            {
                return s_MaterialTable.at(resourceIdentifier).ResourceReference;
            }

            Util::DynamicData materialData = Util::JSONParser::LoadJSONFile(std::string("../../res/materials/") + resourceIdentifier + ".json");

            Ref<Graphics::Material> result;

            if (materialData["Type"] == "pbr")
            {
                Ref<Materials::PBRMaterial> pbrResult = MakeRef<Materials::PBRMaterial>();
                pbrResult->DiffuseTint = materialData["Parameters"]["DiffuseTint"].GetVector4();
                // TODO: other  parameters
                result = pbrResult;
            }
            else if (materialData["Type"] == "phong")
            {
                Ref<Materials::PhongMaterial> phongResult = MakeRef<Materials::PhongMaterial>();
                phongResult->DiffuseTint = materialData["Parameters"]["DiffuseTint"].GetVector4();
                // TODO: other  parameters
                result = phongResult;
            }
            else if (materialData["Type"] == "sprite")
            {
                Ref<Materials::SpriteMaterial> spriteResult = MakeRef<Materials::SpriteMaterial>();
                spriteResult->DiffuseTint = materialData["Parameters"]["DiffuseTint"].GetVector4();
                // TODO: other  parameters
                result = spriteResult;
            }
            else if (materialData["Type"] == "flat")
            {
                Ref<Materials::FlatMaterial> flatResult = MakeRef<Materials::FlatMaterial>();
                flatResult->ColorFill = materialData["Parameters"]["ColorFill"].GetVector4();
                flatResult->ColorWire = materialData["Parameters"]["ColorWire"].GetVector4();
                // TODO: other  parameters
                result = flatResult;
            }
            else
            {
                TESSERACT_CORE_ERROR("Could not identify material type '{}'!", materialData["Type"].GetString());
                return nullptr;
            }

            result->MarkPersistent(resourceIdentifier);

            s_MaterialTable[resourceIdentifier] = { result };

            return result;
        }


        template <>
        Ref<Graphics::Mesh> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_MeshTable.contains(resourceIdentifier))
            {
                return s_MeshTable.at(resourceIdentifier).ResourceReference;
            }

            Util::DynamicData meshData = Util::JSONParser::LoadJSONFile(std::string("../../res/meshes/") + resourceIdentifier + ".json");

            Ref<Graphics::Mesh> result = MakeRef<Graphics::Mesh>();

            result->MarkPersistent(resourceIdentifier);

            result->SetGeometry(LoadFromFile<Geometry>(meshData["Geometry"].GetString()));
            result->SetMaterial(LoadFromFile<Material>(meshData["Material"].GetString()));

            if (meshData["Textures"] != Util::DynamicData::Null)
            {
                for (const auto& [key, value] : meshData["Textures"].GetDict())
                {
                    result->AddTexture(key, LoadFromFile<Texture2D>(value.GetString()));
                }
            }

            if (meshData.GetDict().contains("TextureScale"))
            {
                result->SetTextureScale(meshData["TextureScale"].GetFloat());
            }

            s_MeshTable[resourceIdentifier] = { result };

            return result;
        }

        template <>
        Ref<TextureAtlas> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_TextureAtlasTable.contains(resourceIdentifier))
            {
                return s_TextureAtlasTable.at(resourceIdentifier).ResourceReference;
            }

            Util::DynamicData atlasData = Util::JSONParser::LoadJSONFile(std::string("../../res/atlases/") + resourceIdentifier + ".json");


            Ref<TextureAtlas> result = TextureAtlas::Create(
                LoadFromFile<Graphics::Texture2D>(atlasData["Texture"].GetString()),
                atlasData["Columns"].GetInt(),
                atlasData["Rows"].GetInt()
            );

            for (const auto& [key, value] : atlasData["Textures"].GetDict())
            {
                result->DefineSubTexture(
                    key,
                    value["Column"].GetInt(),
                    value["Row"].GetInt(),
                    value["Width"].GetInt(),
                    value["Height"].GetInt()
                );
            }

            result->MarkPersistent(resourceIdentifier);


            s_TextureAtlasTable[resourceIdentifier] = { result };

            return result;
        }


        template <>
        Ref<SpriteAnimation> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_SpriteAnimationTable.contains(resourceIdentifier))
            {
                return MakeRef<SpriteAnimation>(*(s_SpriteAnimationTable.at(resourceIdentifier).ResourceReference));
            }

            Util::DynamicData spriteAnimationData = Util::JSONParser::LoadJSONFile(std::string("../../res/animations/") + resourceIdentifier + ".json");

            Ref<TextureAtlas> atlas = LoadFromFile<TextureAtlas>(spriteAnimationData["Atlas"].GetString());
            std::vector<std::string> start;
            std::vector<std::string> frames;
            std::vector<std::string> end;

            for (const Util::DynamicData& frame : spriteAnimationData["Start"].GetArray())
            {
                start.push_back(frame.GetString());
            }

            for (const Util::DynamicData& frame : spriteAnimationData["Frames"].GetArray())
            {
                frames.push_back(frame.GetString());
            }

            for (const Util::DynamicData& frame : spriteAnimationData["End"].GetArray())
            {
                end.push_back(frame.GetString());
            }

            Ref<SpriteAnimation> result = MakeRef<SpriteAnimation>();
            result->LoadAtlas(atlas, start, frames, end);

            result->MarkPersistent(resourceIdentifier);

            s_SpriteAnimationTable[resourceIdentifier] = { result };

            return MakeRef<SpriteAnimation>(*(result));
        }


        template <>
        Ref<Prefab> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_PrefabTable.contains(resourceIdentifier))
            {
                return MakeRef<Prefab>(*(s_PrefabTable.at(resourceIdentifier).ResourceReference));
            }

            Util::DynamicData data = Util::JSONParser::LoadJSONFile(std::string("../../res/prefabs/") + resourceIdentifier + ".json");

            Ref<Prefab> result = MakeRef<Prefab>();
            result->Template = data;

            s_PrefabTable[resourceIdentifier] = { result };

            return result;

        }

        template <>
        Ref<AudioClip> ResourceManager::LoadFromFile(const std::string& resourceIdentifier)
        {
            if (s_AudioClipTable.contains(resourceIdentifier))
            {
                return (s_AudioClipTable.at(resourceIdentifier).ResourceReference);
            }

            Ref<AudioClip> result = AudioClip::LoadFromFile(std::string("../../res/audio/") + resourceIdentifier + ".wav");

            s_AudioClipTable[resourceIdentifier] = { result };

            return result;
        }


    } // namespace Core
} // namespace Tesseract

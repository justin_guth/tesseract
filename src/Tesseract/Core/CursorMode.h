#pragma once

namespace Tesseract
{
    namespace Core
    {
        enum class CursorMode {
            Arrow,
            Pointer
        };
    } // namespace Core
    
} // namespace Tesseract

#include "GLFWWindow.h"

namespace Tesseract
{
    namespace Core
    {

        bool Window::s_ContextInvalid = false;

        Window* Window::Create(Application* application)
        {
            return new GLFWWindow(application);
        }

        void Window::OnImGuiUpdate()
        {

        }

        void Window::OnUpdate()
        {

        }

        void Window::Destroy()
        {

        }


    } // namespace Core

} // namespace Tesseract

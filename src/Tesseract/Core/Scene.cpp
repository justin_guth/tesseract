#include "Scene.h"

#include "Tesseract/Components/ScriptComponent.h"
#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/MeshComponent.h"
#include "Tesseract/Components/CameraComponent.h"
#include "Tesseract/Components/SpriteAnimationComponent.h"
#include "Tesseract/Components/LightSourceComponent.h"
#include "Tesseract/Components/TextComponent.h"
#include "Tesseract/Components/ButtonComponent.h"
#include "Tesseract/Components/ColliderComponent.h"

#include "Tesseract/Graphics/PerspectiveCamera.h"

#include "Tesseract/Scripting/ScriptRegistry.h"
#include "Tesseract/Core/ResourceManager.h"

#include "Tesseract/Util/Index.h"

namespace Tesseract
{
    namespace Core
    {
        Scene::Scene()
        {
        }

        Scene::~Scene()
        {
        }

        void Scene::OnRuntimeUpdate()
        {

        }

        void Scene::OnImGuiUpdate()
        {
            for (const auto& [entity, script] : m_Registry.Filter<Components::ScriptComponent>())
            {
                for (const Ref<Scripting::Script>& script : script.Scripts)
                {
                    script->OnImGuiUpdate();
                }
            }
        }


        ECS::Entity Scene::CreateEntity()
        {
            return m_Registry.CreateEntity();
        }


        Scene* Scene::Create()
        {
            return new Scene;
        }


        Util::DynamicData Scene::GetSerializationData()
        {
            Util::DynamicData result;
            result["Scene"]["Entities"] = Util::DynamicData().MakeArray();

            for (const ECS::Entity& entity : m_Registry.RootEntites())
            {
                Util::DynamicData entityData = SerializeEntity(entity);
                result["Scene"]["Entities"] << entityData;
            }

            return result;
        }

        Scene* Scene::FromSerializationData(const Util::DynamicData& data)
        {
            Scene* result = Scene::Create();

            result->DeserializeSceneData(data);

            return result;
        }


        Util::DynamicData Scene::SerializeEntity(const ECS::Entity& entity)
        {
            Util::DynamicData result;

            result["Components"] = Util::DynamicData().MakeDict();
            result["Children"] = Util::DynamicData().MakeArray();

            // Serialize Components

            if (entity.HasComponent<Components::TagComponent>())
            {
                result["Components"]["Tag"]["Tag"] = entity.GetComponent<Components::TagComponent>().Tag;
            }

            if (entity.HasComponent<Components::TransformComponent>())
            {
                const Components::TransformComponent& transform = entity.GetComponent<Components::TransformComponent>();
                result["Components"]["Transform"]["Position"]
                    << transform.Position.X << transform.Position.Y << transform.Position.Z;
                result["Components"]["Transform"]["RotationEulerAngles"]
                    << transform.RotationEulerAngles.X << transform.RotationEulerAngles.Y << transform.RotationEulerAngles.Z;
                result["Components"]["Transform"]["Scale"]
                    << transform.Scale.X << transform.Scale.Y << transform.Scale.Z;
            }

            if (entity.HasComponent<Components::CameraComponent>())
            {
                const Components::CameraComponent& camera = entity.GetComponent<Components::CameraComponent>();

                if (camera.Camera == nullptr)
                {
                    result["Components"]["Camera"]["Camera"] = Util::DynamicData::Null;
                }
                else
                {
                    result["Components"]["Camera"]["Camera"] = camera.Camera->GetSerializationData();
                }
            }

            if (entity.HasComponent<Components::MeshComponent>())
            {
                const Components::MeshComponent& mesh = entity.GetComponent<Components::MeshComponent>();

                if (mesh.Mesh == nullptr)
                {
                    result["Components"]["Mesh"]["Mesh"] = Util::DynamicData::Null;
                }
                else if (mesh.Mesh->IsPersistent())
                {
                    result["Components"]["Mesh"]["Mesh"] = mesh.Mesh->GetIdentifier();
                }
                else
                {
                    result["Components"]["Mesh"]["Mesh"] = mesh.Mesh->GetSerializationData();
                }
            }

            if (entity.HasComponent<Components::ScriptComponent>())
            {
                const Components::ScriptComponent& script = entity.GetComponent<Components::ScriptComponent>();

                for (const Ref<Scripting::Script>& attachedScript : script.Scripts)
                {
                    result["Components"]["Script"]["Scripts"] << attachedScript->GetIdentifier();
                }
            }

            // Serialize Children

            for (ECS::Entity child : entity.GetChildren())
            {
                result["Children"] << SerializeEntity(child);
            }

            return result;

        }

        void Scene::DeserializeSceneData(const Util::DynamicData& sceneData)
        {
            for (const Util::DynamicData& entityData : sceneData["Scene"]["Entities"].GetArray())
            {
                DeserializeEntityData(entityData, false);
            }

            for (const auto& [entity, scriptComponent] : m_Registry.Filter<Components::ScriptComponent>())
            {
                for (const Ref<Scripting::Script>& script : scriptComponent.Scripts)
                {
                    script->OnSceneLoaded();
                    script->OnAttach();
                }
            }
        }


        ECS::Entity Scene::DeserializeEntityData(const Util::DynamicData& entityData, bool callScriptOnAttach)
        {
            ECS::Entity entity;

            if (entityData.Type == Util::DynamicData::Type::String)
            {
                return LoadPrefab(entityData.GetString(), callScriptOnAttach);
            }
            else
            {
                entity = CreateEntity();
            }

            const std::vector<Util::DynamicData>& entityChildArray = entityData["Children"].GetArray();

            for (const Util::DynamicData& entityData : entityChildArray)
            {
                ECS::Entity child = DeserializeEntityData(entityData);
                entity.AddChild(child);
            }


            const std::unordered_map<std::string, Util::DynamicData>& entityComponentsDict = entityData["Components"].GetDict();

            // Check for components and add accordingly:
            if (entityComponentsDict.contains("Tag"))
            {
                entity.GetComponent<Components::TagComponent>().Tag = entityComponentsDict.at("Tag")["Tag"].GetString();
            }
            if (entityComponentsDict.contains("Text"))
            {
                Components::TextComponent& text = entity.AddComponent<Components::TextComponent>();
                text.Text = entityComponentsDict.at("Text")["Text"].GetString();
                text.Face = ResourceManager::LoadFromFile<Typing::Face>(entityComponentsDict.at("Text")["Face"].GetString());
                text.Size = entityComponentsDict.at("Text")["Size"].GetFloat();
                text.Color = entityComponentsDict.at("Text")["Color"].GetVector4();
                if (entityComponentsDict.at("Text").GetDict().contains("IsOverlay"))
                {
                    text.IsOverlay = entityComponentsDict.at("Text")["IsOverlay"].GetBool();
                }

                if (entityComponentsDict.at("Text").GetDict().contains("Visible"))
                {
                    text.Visible = entityComponentsDict.at("Text")["Visible"].GetBool();
                }
            }
            if (entityComponentsDict.contains("Button"))
            {
                Components::ButtonComponent& button = entity.AddComponent<Components::ButtonComponent>();
                button.Text = entityComponentsDict.at("Button")["Text"].GetString();
                button.TextFace = ResourceManager::LoadFromFile<Typing::Face>(entityComponentsDict.at("Button")["TextFace"].GetString());
                button.TextSize = entityComponentsDict.at("Button")["TextSize"].GetFloat();
                button.TextColor = entityComponentsDict.at("Button")["TextColor"].GetVector4();
                button.Dimensions = entityComponentsDict.at("Button")["Dimensions"].GetVector2();

                if (entityComponentsDict.at("Button").GetDict().contains("Visible"))
                {
                    button.Visible = entityComponentsDict.at("Button")["Visible"].GetBool();
                }
                if (entityComponentsDict.at("Button").GetDict().contains("Color"))
                {
                    button.Color = entityComponentsDict.at("Button")["Color"].GetVector4();
                }
                if (entityComponentsDict.at("Button").GetDict().contains("TextAlignment"))
                {
                    // TODO: Set alignment enum
                }
            }
            if (entityComponentsDict.contains("Transform"))
            {
                Components::TransformComponent& transform = entity.GetComponent<Components::TransformComponent>();

                if (entityComponentsDict.at("Transform").GetDict().contains("Position"))
                {
                    transform.Position = entityComponentsDict.at("Transform")["Position"].GetVector3();
                }
                if (entityComponentsDict.at("Transform").GetDict().contains("RotationEulerAngles"))
                {
                    transform.RotationEulerAngles = entityComponentsDict.at("Transform")["RotationEulerAngles"].GetVector3();
                }
                if (entityComponentsDict.at("Transform").GetDict().contains("Scale"))
                {
                    transform.Scale = entityComponentsDict.at("Transform")["Scale"].GetVector3();
                }
            }

            if (entityComponentsDict.contains("SpriteAnimation"))
            {
                Components::SpriteAnimationComponent& spriteAnimation = entity.AddComponent<Components::SpriteAnimationComponent>();

                spriteAnimation.Animation = ResourceManager::LoadFromFile<Graphics::SpriteAnimation>(
                    entityComponentsDict.at("SpriteAnimation")["Animation"].GetString()
                    );
            }

            if (entityComponentsDict.contains("Light"))
            {
                Components::LightSourceComponent& lightSource = entity.AddComponent<Components::LightSourceComponent>();

                if (entityComponentsDict.at("Light")["Type"] == "point")
                {
                    lightSource.Type = Graphics::LightSourceType::Point;
                }

                if (entityComponentsDict.at("Light").GetDict().contains("Color"))
                {
                    lightSource.Color = entityComponentsDict.at("Light")["Color"].GetVector3();
                }

                if (entityComponentsDict.at("Light").GetDict().contains("IsPrimary"))
                {
                    lightSource.IsPrimary = entityComponentsDict.at("Light")["IsPrimary"].GetBool();
                }

                if (entityComponentsDict.at("Light").GetDict().contains("Intensity"))
                {
                    lightSource.Intensity = entityComponentsDict.at("Light")["Intensity"].GetFloat();
                }
            }

            if (entityComponentsDict.contains("Collider"))
            {
                Components::ColliderComponent& collider = entity.AddComponent<Components::ColliderComponent>();

                if (entityComponentsDict.at("Collider").GetDict().contains("Colliders"))
                {
                    for (const Util::DynamicData& colliderData : entityComponentsDict.at("Collider")["Colliders"].GetArray())
                    {

                        if (colliderData["Type"] == "aabb")
                        {

                            Ref<Physics::AABBCollider> col = MakeRef<Physics::AABBCollider>();
                            col->Enabled = colliderData["Enabled"].GetBool();
                            col->IsActive = colliderData["IsActive"].GetBool();
                            col->Offset = colliderData["Offset"].GetVector3();
                            col->Size = colliderData["Size"].GetVector3();
                            col->ToResolve = colliderData["ToResolve"].GetBool();
                            col->Id = Util::index_t::RequestUnique();
                            col->Transform = entity.GetComponent<Components::TransformComponent>();
                            collider.Colliders.emplace_back(col);

                            if (colliderData["Group"] == "static") { col->Group = Tesseract::Physics::Collider::Group::Static; }
                            if (colliderData["Group"] == "dynamic") { col->Group = Tesseract::Physics::Collider::Group::Dynamic; }
                        }

                        if (colliderData["Type"] == "point")
                        {

                            Ref<Physics::PointCollider> col = MakeRef<Physics::PointCollider>();
                            col->Enabled = colliderData["Enabled"].GetBool();
                            col->IsActive = colliderData["IsActive"].GetBool();
                            col->Offset = colliderData["Offset"].GetVector3();
                            col->ToResolve = colliderData["ToResolve"].GetBool();
                            col->Id = Util::index_t::RequestUnique();
                            col->Transform = entity.GetComponent<Components::TransformComponent>();
                            collider.Colliders.emplace_back(col);

                            if (colliderData["Group"] == "static") { col->Group = Tesseract::Physics::Collider::Group::Static; }
                            if (colliderData["Group"] == "dynamic") { col->Group = Tesseract::Physics::Collider::Group::Dynamic; }
                        }

                        if (colliderData["Type"] == "sphere")
                        {

                            Ref<Physics::SphereCollider> col = MakeRef<Physics::SphereCollider>();
                            col->Enabled = colliderData["Enabled"].GetBool();
                            col->IsActive = colliderData["IsActive"].GetBool();
                            col->Offset = colliderData["Offset"].GetVector3();
                            col->ToResolve = colliderData["ToResolve"].GetBool();
                            col->Radius = colliderData["Radius"].GetFloat();
                            col->Id = Util::index_t::RequestUnique();
                            col->Transform = entity.GetComponent<Components::TransformComponent>();
                            collider.Colliders.emplace_back(col);

                            if (colliderData["Group"] == "static") { col->Group = Tesseract::Physics::Collider::Group::Static; }
                            if (colliderData["Group"] == "dynamic") { col->Group = Tesseract::Physics::Collider::Group::Dynamic; }
                        }


                    }
                }
            }

            if (entityComponentsDict.contains("Script"))
            {
                ECS::Comp<Components::ScriptComponent> script = entity.AddComponent<Components::ScriptComponent>();

                if (entityComponentsDict.at("Script")["Scripts"].Type != Util::DynamicData::Type::Null)
                {
                    for (const Util::DynamicData& scriptIdentifier : entityComponentsDict.at("Script")["Scripts"].GetArray())
                    {
                        script->Attach(
                            Scripting::ScriptRegistry::CreateInstance(scriptIdentifier.GetString()),
                            false
                        );
                    }
                }
            }
            if (entityComponentsDict.contains("Camera"))
            {
                Components::CameraComponent& camera = entity.AddComponent<Components::CameraComponent>();
                if (entityComponentsDict.at("Camera")["Camera"].Type == Util::DynamicData::Type::String)
                {
                    // Set via reference
                    // TODO: load resource from disk
                }
                else
                {
                    // Defined in place
                    const Util::DynamicData& cameraData = entityComponentsDict.at("Camera")["Camera"];
                    if (cameraData["Type"] == "perspective")
                    {
                        Ref<Graphics::PerspectiveCamera> perspectiveCamera = MakeRef<Graphics::PerspectiveCamera>();
                        perspectiveCamera->SetFOV(cameraData["FOV"].GetFloat());
                        perspectiveCamera->SetNear(cameraData["Near"].GetFloat());
                        perspectiveCamera->SetFar(cameraData["Far"].GetFloat());
                        perspectiveCamera->SetAspectRatio(cameraData["AspectRatio"].GetFloat());
                        if (cameraData["Active"].GetBool())
                        {
                            perspectiveCamera->MakeActive();
                        }
                        camera.Camera = perspectiveCamera;
                    }
                    // TODO: Add orthographic
                }
            }
            if (entityComponentsDict.contains("Mesh"))
            {
                Components::MeshComponent& mesh = entity.AddComponent<Components::MeshComponent>();

                if (entityComponentsDict.at("Mesh")["Mesh"].Type == Util::DynamicData::Type::String)
                {
                    // Set via reference
                    mesh.Mesh = ResourceManager::LoadFromFile<Graphics::Mesh>(entityComponentsDict.at("Mesh")["Mesh"].GetString());
                }
                else
                {
                    // Defined in place (not currently supported)
                    TESSERACT_CORE_ASSERT(false, "In place definition of meshes currently not supported!");
                    // TODO: Add orthographic
                }
            }



            return entity;
        }


        ECS::Entity Scene::LoadPrefab(const std::string& identifier, bool callScriptOnAttach)
        {
            Ref<Prefab> prefab = ResourceManager::LoadFromFile<Prefab>(identifier);

            ECS::Entity created = DeserializeEntityData(prefab->Template, callScriptOnAttach);

            std::vector<ECS::Entity> createdEntities;
            createdEntities.push_back(created);

            uint32_t index = 0;

            while (index != createdEntities.size())
            {
                for (ECS::Entity child : createdEntities[index].GetChildren())
                {
                    createdEntities.push_back(child);
                }

                index++;
            }

            if (callScriptOnAttach)
            {
                for (ECS::Entity entity : createdEntities)
                {
                    if (!entity.HasComponent<Components::ScriptComponent>())
                    {
                        continue;
                    }

                    const Components::ScriptComponent& scriptComponent = entity.GetComponent<Components::ScriptComponent>();

                    for (const Ref<Scripting::Script>& script : scriptComponent.Scripts)
                    {
                        script->OnAttach();
                    }
                }
            }

            return created;
        }



    } // namespace Core

} // namespace Tesseract

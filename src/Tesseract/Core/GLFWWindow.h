#pragma once

#include "Window.h"
#include "Tesseract/Events/Events.h"

#include <stdint.h>
#include "CursorMode.h"

#include "GLFW/glfw3.h"

namespace Tesseract
{
    namespace Core
    {
        class GLFWWindow : public Window
        {
        public:

            GLFWWindow(Application* application);
            ~GLFWWindow();

            void SetTitle(const char*) override;
            void SwapBuffers() override;

            bool Closed() override;

            void OnUpdate() override;

            void Destroy() override;

            void* GetWindowHandle() override;

            void SetCursorMode(CursorMode cursorMode) override;

            Dimensions GetDimensions() override;
            float GetAspectRatio() override;


        private:

            GLFWwindow* m_WindowHandle;
            static uint32_t s_WindowCount;
            Application* m_Application;

            uint32_t m_Width = 720;
            uint32_t m_Height = 480;

            GLFWcursor* m_HoverCursor = nullptr;

        private:

            static void InitGLFW();
            static void ShutdownGLFW();

            void EventCallback(Events::Event& event);
            static void  ErrorCallback(int error, const char* description);
            
        };
    } // namespace Core
} // namespace Tesseract

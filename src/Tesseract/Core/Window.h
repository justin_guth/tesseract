#pragma once

#include <stdint.h>

#include "CursorMode.h"

namespace Tesseract
{
    namespace Core
    {
        class Application;

        class Window
        {
        public:

            struct Dimensions {
                uint32_t Width;
                uint32_t Height;
            };

            virtual void SetTitle(const char*) = 0;
            virtual void SwapBuffers() = 0;

            virtual bool Closed() = 0;

            virtual void OnUpdate();
            virtual void OnImGuiUpdate();

            virtual void Destroy();

            virtual void* GetWindowHandle() = 0;

            virtual void SetCursorMode(CursorMode cursorMode) = 0;

            static Window* Create(Application* application);

            virtual Dimensions GetDimensions() = 0;
            virtual float GetAspectRatio() = 0;

            inline static bool ContextInvalid() { return s_ContextInvalid; }

        protected:

            static bool s_ContextInvalid;
            inline static void InvalidateContext() { s_ContextInvalid = true; }
        };
    } // namespace Core
} // namespace Tesseract

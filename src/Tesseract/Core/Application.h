#pragma once

#include "Window.h"
#include "LayerStack.h"

#include "Tesseract/Events/Events.h"
#include "Tesseract/Reference.h"
#include "Ticker.h"

#include "CursorMode.h"

#include <string>
#include <vector>

namespace Tesseract
{
    namespace Core
    {
        class ImGuiLayer;
        class SceneLayer;
        class EditorLayer;
        class GUILayer;
        class DebugLayer;
        class Scene;

        class Application
        {
        public:

            enum class DebugMode
            {
                NoDraw,
                DrawSolidOverlay,
                DrawWireframeOverlay,
                DrawSolid,
                DrawWireframe
            };

        public:

            Application();
            Application(const char* title);
            ~Application();

            void OnEvent(Events::Event& event);

            inline void PushLayer(Layer* layer) { m_LayerStack.PushLayer(layer); }
            inline void PushOverlay(Layer* overlay) { m_LayerStack.PushOverlay(overlay); }
            inline void RemoveLayer(Layer* layer) { m_LayerStack.RemoveLayer(layer); }

            virtual void BeforeInit();
            virtual void OnInit();
            virtual void OnShutdown();

            void OnSceneLoad();

            void Init();
            void BeforeRun();
            void Run();
            void Shutdown();

            void SetCursorMode(CursorMode cursorMode);

            void OnPhysicsUpdate();

            inline std::string GetTitle() const { return m_Title; }
            inline std::string GetWindowTitle() const { return GetTitle(); }  // Can be changed later if that should seem necessary

            inline Window* GetWindow() const { return m_Window; }

            static Application* GetApplication();
            static void SetApplication(Application* application);

            Ref<Scene> GetActiveScene();

            void SetSceneRunning(bool running = true);
            bool IsSceneRunning();
            Ref<Scene> LoadScene(const std::string& filePath);
            void LoadSceneLater(const std::string& filePath);



            inline void RegisterTicker(const Identifier& identifier, float period, float offset = 0.0f)
            {
                m_Ticker.RegisterTicker(identifier, period, offset);
            }

            inline bool Tick(const Identifier& identifier)
            {
                return m_Ticker.Tick(identifier);
            }

            inline void SetDebugModeColliders(DebugMode mode) { m_DebugModeColliders = mode; }
            inline DebugMode GetDebugModeColliders() { return m_DebugModeColliders; }

        protected:

            Ticker m_Ticker;

        private:

            bool m_Running;
            std::string m_Title;
            Window* m_Window;
            LayerStack m_LayerStack;

            ImGuiLayer* m_ImGuiLayer;
            EditorLayer* m_EditorLayer;
            SceneLayer* m_SceneLayer;
            DebugLayer* m_DebugLayer;
            GUILayer* m_GUILayer;

            static Application* s_Application;

            DebugMode m_DebugModeColliders;

            std::vector<std::string> m_SceneQueue;

        };
    } // namespace Core
} // namespace Tesseract

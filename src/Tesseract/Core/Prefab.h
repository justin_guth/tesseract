#pragma once

#include "Tesseract/Util/DynamicData.h"

namespace Tesseract
{
    namespace Core
    {
        class Prefab
        {
        public:

            Util::DynamicData Template;

        };
    } // namespace Core

} // namespace Tesseract

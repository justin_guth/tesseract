#pragma once

#include <stdint.h>
#include <string>
#include <cstring>
#include "xxh64.h"

#define _TESSERACT_XXH64_SEED 100120231337101

namespace Tesseract
{
	class Identifier
	{
	private:

		const uint64_t m_Hash;

	public:

		constexpr inline Identifier();
		constexpr inline Identifier(const Identifier& other);
		constexpr inline Identifier(const char* string);
		inline Identifier(const std::string& string);
		constexpr inline bool operator==(const Identifier& other) const;
		constexpr inline uint64_t Get() const;
	};

	class IdentifierHashFunction
	{
	public:

		inline uint64_t operator()(const Identifier& identifier) const
		{
			return identifier.Get();
		}
	};

	constexpr inline Identifier::Identifier() : Identifier::Identifier("")
	{
	}

	constexpr inline Identifier::Identifier(const Identifier& other) : m_Hash(other.m_Hash)
	{
	}

	constexpr inline Identifier::Identifier(const char* string) :
		m_Hash(xxh64::hash(string, std::strlen(string), _TESSERACT_XXH64_SEED))
	{
	}

	inline Identifier::Identifier(const std::string& string):Identifier(string.c_str())
	{
	}

	constexpr inline bool Identifier::operator==(const Identifier& other) const
	{
		return m_Hash == other.m_Hash;
	}

	constexpr inline uint64_t Identifier::Get() const
	{
		return m_Hash;
	}
}

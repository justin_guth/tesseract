#pragma once

#include <unordered_map>

#include "Tesseract/Memory/DynamicSlotAllocator.h"
#include "Tesseract/Core/Macros.h"
#include "Tesseract/Reference.h"
#include "Geometry.h"
#include "Tesseract/Graphics/ShaderProgram.h"
#include "Tesseract/Graphics/Material.h"
#include "Tesseract/Graphics/Mesh.h"
#include "Tesseract/Graphics/VertexBuffer.h"
#include "Tesseract/Graphics/ElementBuffer.h"
#include "Tesseract/Graphics/VertexArrayObject.h"
#include "Tesseract/Graphics/Texture2D.h"
#include "Tesseract/Graphics/TextureAtlas.h"
#include "Tesseract/Graphics/SpriteAnimation.h"
#include "Tesseract/Core/Prefab.h"
#include "Tesseract/Audio/AudioClip.h"
#include "Tesseract/Typing/Typing.h"


#define TESSERACT_RESOURCEMANAGER_REGISTER_IMPL(X) \
\
template <> \
void ResourceManager::Register<X>(const std::string& identifier, const Ref<X>& reference) \
{ \
    TESSERACT_CORE_ASSERT(!s_##X##Table.contains(identifier), "identifier already in " #X " table!"); \
    s_##X##Table[identifier] = {reference};\
}


#define TESSERACT_RESOURCEMANAGER_UNREGISTER_IMPL(X) \
\
template <> \
Ref<X> ResourceManager::Unregister(const std::string& identifier) \
{ \
  TESSERACT_CORE_ASSERT(s_##X##Table.contains(identifier), "identifier not in " #X " table!"); \
  Ref<X> result = s_##X##Table.at(identifier).ResourceReference; \
  s_##X##Table.erase(identifier); \
  return result; \
}

#define TESSERACT_RESOURCEMANAGER_GET_IMPL(X) \
\
template <> \
Ref<X> ResourceManager::Get(const std::string& identifier) \
{ \
    TESSERACT_CORE_ASSERT(s_##X##Table.contains(identifier), "identifier not in " #X " table!"); \
    return s_##X##Table.at(identifier).ResourceReference; \
}

#define TESSERACT_RESOURCEMANAGER_IMPL(X) \
TESSERACT_RESOURCEMANAGER_GET_IMPL(X) \
TESSERACT_RESOURCEMANAGER_UNREGISTER_IMPL(X) \
TESSERACT_RESOURCEMANAGER_REGISTER_IMPL (X)


#define DECLARE_TABLE(X) static ResourceTable<X> s_##X##Table
#define DEFINE_TABLE(X) ResourceManager::ResourceTable<X> ResourceManager::s_##X##Table

namespace Tesseract
{
    namespace Core
    {

        using namespace Tesseract::Graphics;
        using namespace Tesseract::Audio;
        using namespace Tesseract::Typing;

        class ResourceManager
        {

        public:


            template <typename T_Type>
            struct ResourceTableEntry
            {

                Ref<T_Type> ResourceReference;

            };

            template <typename T_Type>
            using ResourceTable = std::unordered_map < std::string, ResourceTableEntry<T_Type>>;

        public:

            template <typename T_Type>
            static void Register(const std::string& identifier, const Ref<T_Type>& reference);

            template <typename T_Type>
            static Ref<T_Type> Unregister(const std::string& identifier);

            template <typename T_Type>
            static Ref<T_Type> Get(const std::string& identifier);

            template <typename T_Type>
            static Ref<T_Type> LoadFromFile(const std::string& resourceIdentifier);

        private:

            //REVIEW: Do we want to be able to use the same identifier between different tables for different object types?
            //IF NO: Keep unordered set of all used ids.

            DECLARE_TABLE(Geometry);
            DECLARE_TABLE(ShaderProgram);
            DECLARE_TABLE(Material);
            DECLARE_TABLE(VertexBuffer);
            DECLARE_TABLE(ElementBuffer);
            DECLARE_TABLE(VertexArrayObject);
            DECLARE_TABLE(Mesh);
            DECLARE_TABLE(Texture2D);
            DECLARE_TABLE(TextureAtlas);
            DECLARE_TABLE(SpriteAnimation);
            DECLARE_TABLE(Prefab);
            DECLARE_TABLE(AudioClip);

            static FontLibrary s_FontLibrary;
        };


    } // namespace Core
} // namespace Tesseract

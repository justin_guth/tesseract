#pragma once

#include "Layer.h"
#include "Scene.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Physics/ColliderSystem.h"
#include "Tesseract/Graphics/FrameBuffer.h"
#include "Tesseract/Graphics/CubeMap.h"

namespace Tesseract
{
    namespace Core
    {
        class SceneLayer : public Layer
        {
        public:

            SceneLayer();
            ~SceneLayer();

            void OnAttach() override;
            void OnUpdate() override;
            void OnImGuiUpdate() override;
            void OnPhysicsUpdate() override;

            inline Ref<Scene> GetActiveScene() { return m_ActiveScene; }
            Ref<Scene> LoadScene(const std::string& filePath) ;

            void OnEvent(Events::Event& event) override;

            inline void SetRunning(bool running = true) { m_RuntimeRunning = running; }
            inline bool IsRunning() { return m_RuntimeRunning; }

        private:

            Ref<Scene> m_ActiveScene;
            bool m_RuntimeRunning;
            Physics::ColliderSystem m_ColliderSystem;
            
            
        };

    } // namespace Core

} // namespace Tesseract

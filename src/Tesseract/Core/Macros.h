#pragma once
#include <type_traits>

#include "Tesseract/Logging/Logger.h"
#include "debugbreak.h"


// TODO: Flush command for log queue
#ifdef TESSERACT_BUILD_DEBUG
#define TESSERACT_CORE_ASSERT(condition, format, ...) if (!(condition)) { TESSERACT_CORE_CRITICAL(format, __VA_ARGS__); debug_break(); }
#define TESSERACT_ASSERT(condition, format, ...)      if (!(condition)) { TESSERACT_CLIENT_CRITICAL(format, __VA_ARGS__); debug_break(); }
#else
#define TESSERACT_CORE_ASSERT(condition, format, ...) 
#define TESSERACT_ASSERT(condition, format, ...)      
#endif

template <typename T_Type>
struct false_type : public std::false_type {};


#define TESSERACT_BIT(X) (1 << X)


#define COMMA ,

#ifdef TESSERACT_BUILD_DEBUG
#define TESSERACT_DEBUG_ONLY(X) X
#else 
#define TESSERACT_DEBUG_ONLY(X)
#endif // TESSERACT_BUILD_DEBUG

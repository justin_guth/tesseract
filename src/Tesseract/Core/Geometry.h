#pragma once

#include <vector>

#include "Tesseract/Graphics/BufferLayout.h"
#include "Tesseract/Graphics/ElementBuffer.h"
#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Math/Vector2.h"

#include "Tesseract/Core/Resource.h"


namespace Tesseract
{
    namespace Core
    {

        //TODO: VertexData has empty fields which are unused, specified in Layout. Make iterator?
        struct VertexData
        {
            Math::Vector3 Position;
            Math::Vector2 TextureCoordinate;
            Math::Vector3 Normal;
            Math::Vector3 Tangent;
            Math::Vector3 Bitangent;
        };

        class Geometry : public Resource
        {
        public:

            inline Geometry(
                const std::vector<VertexData>& vertices,
                const std::vector<uint32_t>& indices,
                const Graphics::BufferLayout& layout,
                const Graphics::ElementBuffer::ElementType& elementType
            ) :
                Resource(),
                Vertices(vertices),
                Indices(indices),
                BufferLayout(layout),
                ElementType(elementType)
            {

            }

            // Create implicitly empty geometry
            inline Geometry(
            ) :
                Resource(),
                Vertices(),
                Indices(),
                BufferLayout(),
                ElementType()
            {

            }

            std::vector<VertexData> Vertices;
            std::vector<uint32_t> Indices;
            Graphics::BufferLayout BufferLayout;
            Graphics::ElementBuffer::ElementType ElementType;

            void RecomputeTangentSpace();
        };
    } // namespace Core

} // namespace Tesseract

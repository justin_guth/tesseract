#include "LayerStack.h"


#include "Tesseract/Logging/Logger.h"

namespace Tesseract
{
    namespace Core
    {
        LayerStack::LayerStack() :
            m_Layers(),
            m_LayerInsertPosition(0)
        {
        }

        LayerStack::~LayerStack()
        {
            for (Layer* layer : m_Layers)
            {
                layer->OnDetach();
                delete layer;
            }
        }



        void LayerStack::PushLayer(Layer* layer)
        {
            std::vector<Layer*>::const_iterator iterator = m_Layers.begin() + m_LayerInsertPosition;
            m_Layers.emplace(iterator, layer);
            m_LayerInsertPosition++;
            layer->OnAttach();
        }

        void LayerStack::PushOverlay(Layer* overlay)
        {
            m_Layers.emplace_back(overlay);
            overlay->OnAttach();
        }

        void LayerStack::RemoveLayer(Layer* layer)
        {
            std::vector<Layer*>::iterator iterator = std::find(m_Layers.begin(), m_Layers.end(), layer);

            if (iterator == m_Layers.end())
            {
                TESSERACT_CORE_ERROR("Layer not found in layer stack!");
                return;
            }

            m_Layers.erase(iterator);
            layer->OnDetach();
        }

    } // namespace Core

} // namespace Tesseract

#pragma once

#include "Layer.h"

#include "Tesseract/DebugPanels/SceneHierarchyPanel.h"
#include "Tesseract/DebugPanels/PropertiesPanel.h"
#include "Tesseract/DebugPanels/RuntimeControlsPanel.h"
#include "Tesseract/DebugPanels/SceneManagerPanel.h"



namespace Tesseract
{
    namespace Core
    {
        class EditorLayer : public Layer
        {
        public:
            EditorLayer();
            ~EditorLayer();

            void OnAttach() override;
            void OnImGuiUpdate() override;
            void OnEvent(Events::Event& event) override;

        private:

            DebugPanels::SceneHierarchyPanel* m_SceneHierarchyPanel;
            DebugPanels::PropertiesPanel* m_PropertiesPanel;
            DebugPanels::RuntimeControlsPanel* m_RuntimeControlsPanel;
            DebugPanels::SceneManagerPanel* m_SceneManagerPanel;
        };

    } // namespace Core

} // namespace Tesseract

#pragma once

#include "Layer.h"

#include "Scene.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Graphics/Mesh.h"
#include "Tesseract/Core/ResourceManager.h"

namespace Tesseract
{
    namespace Core
    {
        class DebugLayer : public Layer
        {
        public:
            DebugLayer();
            ~DebugLayer();


            void DrawColliders(bool wireframe, bool overlay);
            void OnAttach() override;
            void OnUpdate() override;
            void OnEvent(Events::Event& event) override;

        private:

            Ref<Scene> m_ActiveScene;
            Ref<Graphics::Mesh> m_Cube;
            Ref<Graphics::Mesh> m_Sphere;

        };

    } // namespace Core

} // namespace Tesseract

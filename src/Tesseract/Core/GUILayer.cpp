#include "GUILayer.h"

#include "Application.h"

#include "Tesseract/Graphics/Renderer.h"
#include "Tesseract/Graphics/Bits.h"
#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/TextComponent.h"
#include "Tesseract/Components/ButtonComponent.h"
#include "Tesseract/Components/CameraComponent.h"
#include "Tesseract/Physics/Physics.h"
#include "Tesseract/Profiling/ProfilerScopedTimer.h"
#include "Tesseract/IO/Input.h"
#include "Tesseract/Components/ScriptComponent.h"
#include <tuple>

namespace Tesseract
{
    namespace Core
    {
        GUILayer::GUILayer() :
            m_ActiveScene(nullptr)
        {
        }

        GUILayer::~GUILayer()
        {

        }

        void GUILayer::OnAttach()
        {
        }


        void GUILayer::OnUpdate()
        {
            TESSERACT_PROFILE_FUNCTION();

            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Components::TextComponent,
                Math::Matrix4
                >> textEntities;


            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Components::ButtonComponent,
                Math::Matrix4
                >> buttonEntities;


            Physics::Physics::AcquireLock();

            {
                using namespace std;

                TESSERACT_PROFILE_SCOPE("Entity Grouping");

                if (m_ActiveScene != nullptr)
                {
                    for (ECS::Entity entity : m_ActiveScene->GetRegistry().AllEntites())
                    {
                        if (entity.HasComponents<Components::TransformComponent, Components::TextComponent>())
                        {
                            auto [transform, text] = entity.GetComponents<Components::TransformComponent, Components::TextComponent>();

                            textEntities.push_back(
                                std::tuple{
                                entity,
                                    transform,
                                    text,
                                    transform.GetGlobalTransformMatrix()
                            }
                            );
                        }
                    }

                    for (ECS::Entity entity : m_ActiveScene->GetRegistry().AllEntites())
                    {
                        if (entity.HasComponents<Components::TransformComponent, Components::ButtonComponent>())
                        {
                            auto [transform, button] = entity.GetComponents<Components::TransformComponent, Components::ButtonComponent>();

                            buttonEntities.push_back(
                                std::tuple{
                                entity,
                                    transform,
                                    button,
                                    transform.GetGlobalTransformMatrix()
                            }
                            );
                        }
                    }
                }
            }

            Physics::Physics::FreeLock();


            const auto [width, height] = Application::GetApplication()->GetWindow()->GetDimensions();
            Math::Matrix4 projection = Math::Matrix4::OrthographicProjection(
                0.0f,
                width,
                0.0f,
                height
            );
            Math::Matrix4 view = Math::Matrix4::Identity();

            Renderer::SetProjectionMatrix(projection);
            Renderer::SetViewMatrix(view);

            // TODO: Perform z index sorting across all gui elements

            Graphics::Renderer::EnableDepthTest(false);
            {
                TESSERACT_PROFILE_SCOPE("Text rendering");

                for (auto& [entity, transform, text, globalTransform] : textEntities)
                {
                    if (!text.IsOverlay || !text.Visible)
                    {
                        continue;
                    }

                    Graphics::Renderer::DrawText(
                        text.Text,
                        globalTransform,
                        text.Face,
                        text.Size,
                        text.Color
                    );
                }

                Graphics::Renderer::FlushTextBuffer();
            }

            {
                TESSERACT_PROFILE_SCOPE("Button rendering");

                for (auto& [entity, transform, button, globalTransform] : buttonEntities)
                {
                    Math::Vector3 globalPosition = transform.GetGlobalPosition();

                    Graphics::Renderer::DrawRectangle(
                        globalPosition,
                        button.Dimensions,
                        button.Color
                    );

                    // Check button click
                    IO::MousePosition mousePosition = IO::Input::GetMousePosition();
                    const auto& [windowWidth, windowHeight] = Core::Application::GetApplication()->GetWindow()->GetDimensions();
                    if (
                        globalPosition.X <= mousePosition.X && mousePosition.X <= globalPosition.X + button.Dimensions.X
                        && globalPosition.Y <= windowHeight - mousePosition.Y && windowHeight - mousePosition.Y <= globalPosition.Y + button.Dimensions.Y
                        )
                    {
                        // Core::Application::GetApplication()->SetCursorMode(Core::CursorMode::Pointer);

                        if (IO::Input::MouseButtonPressed(IO::MouseButton::_Left))
                        {
                            if (entity.HasComponent<Components::ScriptComponent>())
                            {
                                Physics::Physics::AcquireLock();
                                for (const Ref<Scripting::Script>& script: entity.GetComponent<Components::ScriptComponent>().Scripts)
                                {
                                    script->OnButtonPressed();
                                }
                                Physics::Physics::FreeLock();
                            }
                        }
                    }
                }
                Graphics::Renderer::FlushRectangleBuffer();

                for (auto& [entity, transform, button, globalTransform] : buttonEntities)
                {
                    Math::Matrix4 globalTransformCorrected = globalTransform;
                    globalTransformCorrected.Column3 += {button.Dimensions.X / 2.0f, button.Dimensions.Y / 2.0f, 0.0f, 0.0f};

                    Graphics::Renderer::DrawText(
                        button.Text,
                        globalTransformCorrected,
                        button.TextFace,
                        button.TextSize,
                        button.TextColor
                    );
                }

                Graphics::Renderer::FlushTextBuffer();
            }

        }


        void GUILayer::OnEvent(Events::Event& event)
        {
            Events::EventHandler handler(event);
            handler.Handle<Events::SceneLoadedEvent>(
                [this](Events::SceneLoadedEvent& sceneLoadedEvent) -> bool
                {
                    m_ActiveScene = sceneLoadedEvent.GetScene();
                    return false;
                }
            );
        }

    } // namespace Core

} // namespace Tesseract

#include "glad/gl.h"
#include <GLFW/glfw3.h>

// Include GLFWWindow.h after this line only

#include "GLFWWindow.h"
#include "Application.h"
#include "Tesseract/Events/Events.h"
#include "Tesseract/Logging/Logger.h"
#include "Tesseract/Graphics/Renderer.h"
#include "Tesseract/Profiling/ProfilerScopedTimer.h"

namespace Tesseract
{
    namespace Core
    {
        uint32_t GLFWWindow::s_WindowCount = 0;

        GLFWWindow::GLFWWindow(Application* application)
        {
            if (s_WindowCount == 0)
            {
                GLFWWindow::InitGLFW();
            }


            m_WindowHandle = glfwCreateWindow(m_Width, m_Height, application->GetWindowTitle().c_str(), NULL, NULL);
            glfwSetWindowUserPointer(m_WindowHandle, (void*)this);

            if (!m_WindowHandle)
            {
                TESSERACT_CORE_CRITICAL("Failed to create window!");

                if (s_WindowCount == 0)
                {
                    GLFWWindow::ShutdownGLFW();
                }

                return;
            }

            m_Application = application;

            glfwMakeContextCurrent(m_WindowHandle);

            if (s_WindowCount == 0)
            {
                if (!gladLoadGL((GLADloadfunc)glfwGetProcAddress))
                {
                    TESSERACT_CORE_CRITICAL("Failed to initialize GLAD!");
                    // TODO: Throw meaningful exception
                    throw - 1;
                }
                else
                {
                    TESSERACT_CORE_SUCCESS("Initialized GLAD");
                }
            }

            // TODO:  check if glfwSetFramebufferSizeCallback is better for this purpose or at least for viewport resizings
            glfwSetWindowSizeCallback(
                m_WindowHandle,
                [](GLFWwindow* window, int width, int height)
                {
                    GLFWWindow* user = (GLFWWindow*)glfwGetWindowUserPointer(window);
                    Events::WindowResizeEvent windowResizeEvent((uint32_t)width, (uint32_t)height);
                    user->EventCallback(windowResizeEvent);
                }
            );

            glfwSetKeyCallback(
                m_WindowHandle,
                [](GLFWwindow* window, int key, int scancode, int action, int mods)
                {
                    GLFWWindow* user = (GLFWWindow*)glfwGetWindowUserPointer(window);

                    if (action == GLFW_PRESS)
                    {
                        Events::KeyPressedEvent keyPressedEvent(key, 1);
                        user->EventCallback(keyPressedEvent);
                    }

                    else if (action == GLFW_REPEAT)
                    {
                        Events::KeyPressedEvent keyPressedEvent(key, 1); // TODO: check how to handle repeat count
                        user->EventCallback(keyPressedEvent);
                    }

                    else if (action == GLFW_RELEASE)
                    {
                        Events::KeyReleasedEvent keyReleasedEvent(key);
                        user->EventCallback(keyReleasedEvent);
                    }
                }
            );

            glfwSetCursorPosCallback(
                m_WindowHandle,
                [](GLFWwindow* window, double x, double y)
                {
                    GLFWWindow* user = (GLFWWindow*)glfwGetWindowUserPointer(window);

                    Events::MouseMovedEvent mouseMovedEvent(x, y);
                    user->EventCallback(mouseMovedEvent);
                }
            );

            glfwSetMouseButtonCallback(
                m_WindowHandle,
                [](GLFWwindow* window, int button, int action, int mods)
                {
                    GLFWWindow* user = (GLFWWindow*)glfwGetWindowUserPointer(window);

                    if (action == GLFW_PRESS)
                    {
                        Events::MouseButtonPressedEvent mouseButtonPressedEvent(button);
                        user->EventCallback(mouseButtonPressedEvent);
                    }

                    else if (action == GLFW_REPEAT)
                    {
                        Events::MouseButtonPressedEvent mouseButtonPressedEvent(button); // TODO: check how to handle repeat count
                        user->EventCallback(mouseButtonPressedEvent);
                    }

                    else if (action == GLFW_RELEASE)
                    {
                        Events::MouseButtonReleasedEvent mouseButtonReleasedEvent(button);
                        user->EventCallback(mouseButtonReleasedEvent);
                    }
                }
            );

            m_HoverCursor = glfwCreateStandardCursor(GLFW_HAND_CURSOR);

            s_WindowCount++;
        }

        GLFWWindow::~GLFWWindow()
        {
        }

        void GLFWWindow::SetTitle(const char*)
        {
            // TODO:
        }

        void GLFWWindow::SwapBuffers()
        {
            TESSERACT_PROFILE_FUNCTION();

            glfwSwapBuffers(m_WindowHandle);
        }

        bool GLFWWindow::Closed()
        {
            return glfwWindowShouldClose(m_WindowHandle);
        }

        void GLFWWindow::OnUpdate()
        {
            //glfwMakeContextCurrent(m_WindowHandle);
            glfwPollEvents();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }

        void GLFWWindow::Destroy()
        {
            glfwDestroyCursor(m_HoverCursor);

            glfwDestroyWindow(m_WindowHandle);

            s_WindowCount--;

            if (s_WindowCount == 0)
            {
                GLFWWindow::ShutdownGLFW();
            }
        }

        Window::Dimensions GLFWWindow::GetDimensions()
        {
            return { m_Width, m_Height };
        }


        float GLFWWindow::GetAspectRatio()
        {
            return (float)m_Width / (float)m_Height;
        }

        void GLFWWindow::InitGLFW()
        {
            if (!glfwInit())
            {
                TESSERACT_CORE_CRITICAL("GLFW initialization failed!");
                return;
            }
            else
            {
                TESSERACT_CORE_SUCCESS("GLFW initialized");
            }

            glfwSetErrorCallback(&GLFWWindow::ErrorCallback);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        }

        void GLFWWindow::ShutdownGLFW()
        {
            glfwTerminate();
            Window::InvalidateContext();
            TESSERACT_CORE_SUCCESS("GLFW terminated");
        }

        void GLFWWindow::EventCallback(Events::Event& event)
        {
            Events::EventHandler(event).Handle<Events::WindowResizeEvent>(
                [this](Events::WindowResizeEvent& event)
                {
                    m_Width = event.GetWidth();
                    m_Height = event.GetHeight();

                    Graphics::Renderer::SetDefaultViewport(0, 0, m_Width, m_Height);

                    return false;
                }
            );

            m_Application->OnEvent(event);
        }

        void GLFWWindow::ErrorCallback(int error, const char* description)
        {
            TESSERACT_CORE_ERROR("Glfw Error {}: {}\n", error, description);
        }

        void* GLFWWindow::GetWindowHandle()
        {
            return (void*)m_WindowHandle;
        }


        void GLFWWindow::SetCursorMode(CursorMode cursorMode)
        {
            switch (cursorMode)
            {
                case CursorMode::Pointer:
                    glfwSetCursor(m_WindowHandle, m_HoverCursor);
                    break;

                case CursorMode::Arrow:
                default:
                    glfwSetCursor(m_WindowHandle, nullptr);
                    break;
            }
        }

    } // namespace Core
} // namespace Tesseract

#pragma once


#include <string>

namespace Tesseract
{
    namespace Core
    {
        class Resource
        {
        public:

            inline Resource() :
                m_ResourceIdentifier(),
                m_IsPersistent(false)
            {

            }

            inline bool IsPersistent() { return m_IsPersistent; }
            inline const std::string& GetIdentifier() { return m_ResourceIdentifier; }
            inline void MarkPersistent(const std::string& identifier) {
                m_IsPersistent = true;
                m_ResourceIdentifier = identifier;
            }


        private:

            bool m_IsPersistent;
            std::string m_ResourceIdentifier;
        };
    } // namespace Core

} // namespace Tesseract

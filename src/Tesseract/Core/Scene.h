#pragma once

#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Graphics/Camera.h"
#include "Tesseract/Reference.h"


#include "Tesseract/Util/DynamicData.h"

namespace Tesseract
{
    namespace Core
    {
        class Scene
        {
        public:
            Scene();
            ~Scene();

            ECS::Entity CreateEntity();

            void OnRuntimeUpdate();
            void OnImGuiUpdate();

            inline ECS::Registry& GetRegistry() { return m_Registry; }

            Util::DynamicData GetSerializationData();
            static Scene* FromSerializationData(const Util::DynamicData& data);

            static Scene* Create();
            ECS::Entity LoadPrefab(const std::string& identifier, bool callScriptOnAttach = true);

        private:

            void DeserializeSceneData(const Util::DynamicData& sceneData);
            ECS::Entity DeserializeEntityData(const Util::DynamicData& entity, bool callScriptOnAttach = true);
            Util::DynamicData SerializeEntity(const ECS::Entity& entity);



        private:

            ECS::Registry m_Registry;
        };

    } // namespace Core

} // namespace Tesseract

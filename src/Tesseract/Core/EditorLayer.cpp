#include "EditorLayer.h"

#include "Application.h"

namespace Tesseract
{
    namespace Core
    {
        EditorLayer::EditorLayer() :
            m_SceneHierarchyPanel(new DebugPanels::SceneHierarchyPanel),
            m_PropertiesPanel(new DebugPanels::PropertiesPanel),
            m_RuntimeControlsPanel(new DebugPanels::RuntimeControlsPanel),
            m_SceneManagerPanel(new DebugPanels::SceneManagerPanel)
        {
            m_PropertiesPanel->SetSceneHierarchyPanel(m_SceneHierarchyPanel);
        }

        EditorLayer::~EditorLayer()
        {
            delete m_SceneHierarchyPanel;
            delete m_PropertiesPanel;
            delete m_RuntimeControlsPanel;
            delete m_SceneManagerPanel;
        }

        void EditorLayer::OnAttach()
        {
        }

        void EditorLayer::OnImGuiUpdate()
        {
            m_SceneHierarchyPanel->Draw();
            m_PropertiesPanel->Draw();
            m_RuntimeControlsPanel->Draw();
            m_SceneManagerPanel->Draw();
        }


        void EditorLayer::OnEvent(Events::Event& event)
        {
            Events::EventHandler handler(event);
            handler.Handle<Events::SceneLoadedEvent>(
                [this](Events::SceneLoadedEvent& sceneLoadedEvent) -> bool
                {
                    m_SceneHierarchyPanel->ClearSelectedEntity();
                    return false;
                }
            );
        }

    } // namespace Core

} // namespace Tesseract

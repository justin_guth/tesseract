#pragma once

#include "Layer.h"

namespace Tesseract
{
    namespace Core
    {
        class ImGuiLayer: public Layer
        {
        public:
            ImGuiLayer();
            ~ImGuiLayer();

            void OnAttach() override;
            void OnImGuiUpdate() override;

            void BeginFrame();
            void EndFrame();
        };
        
    } // namespace Core
    
} // namespace Tesseract

#include "ImGuiLayer.h"

#include "Tesseract/Logging/Logger.h"

#include "vendor/imgui-docking/imgui.h"
#include "vendor/imgui-docking/backends/imgui_impl_opengl3.h"
#include "vendor/imgui-docking/backends/imgui_impl_glfw.h"

#include "Application.h"

#include "Tesseract/Core/Macros.h"

#include "Tesseract/Profiling/ProfilerScopedTimer.h"

#include "GLFW/glfw3.h"

namespace Tesseract
{
    namespace Core
    {
        ImGuiLayer::ImGuiLayer()
        {

        }

        ImGuiLayer::~ImGuiLayer()
        {
            ImGui_ImplOpenGL3_Shutdown();
            ImGui_ImplGlfw_Shutdown();
            ImGui::DestroyContext();
        }

        void ImGuiLayer::OnAttach()
        {
            TESSERACT_CORE_INFO("ImGuiLayer attached");

            IMGUI_CHECKVERSION();
            ImGui::CreateContext();
            ImGuiIO& io = ImGui::GetIO();
            io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
            io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
            io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

            io.BackendFlags |= ImGuiBackendFlags_PlatformHasViewports;

            void* windowHandle = Core::Application::GetApplication()->GetWindow()->GetWindowHandle();
            TESSERACT_CORE_ASSERT(windowHandle != nullptr, "Received window handle was nullptr!");

            ImGui_ImplGlfw_InitForOpenGL(
                (GLFWwindow*)windowHandle,
                true
            );
            ImGui_ImplOpenGL3_Init("#version 450");
            ImGui::StyleColorsDark();
        }

        void ImGuiLayer::OnImGuiUpdate()
        {
        }

        void ImGuiLayer::BeginFrame()
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
        }


        void ImGuiLayer::EndFrame()
        {
            TESSERACT_PROFILE_FUNCTION();

            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
            GLFWwindow* backupCurrentContext = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backupCurrentContext);
        }


    } // namespace Core

} // namespace Tesseract

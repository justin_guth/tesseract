#pragma once

#include <chrono>

namespace Tesseract
{
    namespace Core
    {
        /**
        * Provides an interface for timing related functionality such as getting the time delta between frames.
        */
        class Time
        {
        public:

            /**
            * Initializes the Time interface.
            */
            static void Init();

            /**
            * Updates the Time interface and recomputes the time delta from the last time it was called.
            */
            static void Update();
            
            /**
            * Updates the Time interface for the running game time.
            */
            static void UpdateGameTime();
            
            /**
            * Resets the Time interface for the running game time.
            */
            static void ResetGameTime();
            
            /**
            * Returns the time delta between the last two updates.
            * 
            * @return The time passed since the last update
            */
            inline static double Delta() { return m_Delta; }

            /**
            * Returns the total running time for the application.
            * 
            * @return The time passed since app start
            */
            inline static double Total() { return m_Total; }

            /**
            * Returns the running time for the game.
            * 
            * @return The time for which a scene has been running
            */
            inline static double TotalGame() { return m_TotalGame; }

        private:

            static bool m_IsInitialized;
            static std::chrono::steady_clock::time_point m_Last;
            static double m_Delta;
            static double m_Total;
            static double m_TotalGame;
        };
    } 
} 

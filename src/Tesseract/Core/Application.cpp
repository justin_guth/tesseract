#include "Application.h"

#include "Tesseract/Logging/Logger.h"
#include "Tesseract/IO/Input.h"
#include "Time.h"
#include "Tesseract/Audio/Audio.h"

#include "Tesseract/Core/ImGuiLayer.h"
#include "Tesseract/Core/SceneLayer.h"
#include "Tesseract/Core/DebugLayer.h"
#include "Tesseract/Core/GUILayer.h"
#include "Tesseract/Core/EditorLayer.h"
#include "Tesseract/Graphics/Renderer.h"

#include "Tesseract/Scripting/ScriptRegistry.h"
#include "Tesseract/Scripting/AudioSourceHandlerScript.h"

#include "Tesseract/Profiling/ProfilerScopedTimer.h"

namespace Tesseract
{
    namespace Core
    {

        Application* Application::s_Application = nullptr;

        Application::Application() :
            Application("Game Tesseract Application")
        {

        }

        Application::Application(const char* title) :
            m_Running(true),
            m_Title(title),
            m_Window(nullptr),
            m_LayerStack(),
            m_ImGuiLayer(new Tesseract::Core::ImGuiLayer),
            m_EditorLayer(new Tesseract::Core::EditorLayer),
            m_SceneLayer(nullptr),
            m_DebugModeColliders(DebugMode::NoDraw),
            m_DebugLayer(nullptr),
            m_GUILayer(nullptr),
            m_SceneQueue()
        {
        }

        Application::~Application()
        {

        }

        void Application::OnPhysicsUpdate()
        {
            for (Layer* layer : m_LayerStack)
            {
                layer->OnPhysicsUpdate();
            }
        }

        void Application::Run()
        {
            TESSERACT_CORE_INFO("Running Application: {}", m_Title);
            TESSERACT_PROFILE_FUNCTION();

            while (!m_Window->Closed())
            {
                TESSERACT_PROFILE_SCOPE("Tesseract Main Loop Iteration");

                {
                    TESSERACT_PROFILE_SCOPE("Tesseract Early Updates");
                    Core::Time::Update();
                    IO::Input::Update();
                    Tesseract::Audio::Audio::Update();

                    m_Ticker.Update();

                    // Update:
                    m_Window->OnUpdate();
                }

                {
                    TESSERACT_PROFILE_SCOPE("Layer Updates");
                    for (Layer* layer : m_LayerStack)
                    {
                        layer->OnUpdate();
                    }
                }

                m_ImGuiLayer->BeginFrame();

                m_Window->OnImGuiUpdate();

                {
                    TESSERACT_PROFILE_SCOPE("Layer ImGui Updates");

                    for (Layer* layer : m_LayerStack)
                    {
                        layer->OnImGuiUpdate();
                    }
                }

                m_ImGuiLayer->EndFrame();

                m_Window->SwapBuffers();

                if (!m_SceneQueue.empty())
                {
                    for (const std::string& identifier : m_SceneQueue)
                    {
                        LoadScene(identifier);
                    }

                    m_SceneQueue.clear();
                }
            }
        }

        void Application::Init()
        {
            // FIXME: Provide interface
            srand(time(0));

            m_Window = Window::Create(this);
            m_SceneLayer = new Tesseract::Core::SceneLayer;
            m_DebugLayer = new Tesseract::Core::DebugLayer;
            m_GUILayer = new Tesseract::Core::GUILayer;

            Scripting::ScriptRegistry::Register<Scripts::AudioSourceHandlerScript>("::audio_source_handler");

            PushOverlay(m_GUILayer);

            #ifdef TESSERACT_BUILD_DEBUG
            PushOverlay(m_DebugLayer);
            #endif

            PushOverlay(m_ImGuiLayer);

            #ifdef TESSERACT_BUILD_DEBUG
            PushOverlay(m_EditorLayer);
            #endif

            PushLayer(m_SceneLayer);
        }

        void Application::BeforeRun()
        {
            // REVIEW: Check if this is necessary, feels hacky
            // Goal initialize any event dependent code at app start
            auto [width, height] = m_Window->GetDimensions();
            Events::WindowResizeEvent event(width, height);

            OnEvent(event);
            Graphics::Renderer::SetDefaultViewport(0, 0, width, height);
        }


        void Application::Shutdown()
        {

            m_Window->Destroy();
            delete m_Window;
        }

        void Application::SetCursorMode(CursorMode cursorMode)
        {
            m_Window->SetCursorMode(cursorMode);
        }



        void Application::OnInit()
        {

        }

        void Application::BeforeInit()
        {

        }

        void Application::OnShutdown()
        {

        }


        void Application::OnEvent(Events::Event& event)
        {
            IO::Input::OnEvent(event);

            std::vector<Layer*>::iterator it = m_LayerStack.end();
            while (it != m_LayerStack.begin())
            {
                --it;
                (*it)->OnEvent(event);

                if (event.Handled())
                {
                    break;
                }
            }
        }

        Application* Application::GetApplication()
        {
            return s_Application;
        }

        void Application::SetApplication(Application* application)
        {
            s_Application = application;
        }

        Ref<Scene> Application::GetActiveScene()
        {
            return m_SceneLayer->GetActiveScene();
        }

        void Application::SetSceneRunning(bool running)
        {
            m_SceneLayer->SetRunning(running);
        }


        bool Application::IsSceneRunning()
        {
            return m_SceneLayer->IsRunning();
        }

        Ref<Scene> Application::LoadScene(const std::string& filePath)
        {
            return m_SceneLayer->LoadScene(filePath);
        }

        void Application::LoadSceneLater(const std::string& filePath)
        {
            m_SceneQueue.push_back(filePath);
        }



    } // namespace Core
} // namespace Tesseract
#include "DebugLayer.h"

#include "Application.h"

#include "Tesseract/Graphics/Renderer.h"
#include "Tesseract/Graphics/Bits.h"
#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/ColliderComponent.h"
#include "Tesseract/Components/CameraComponent.h"
#include "Tesseract/Physics/Physics.h"
#include "Tesseract/Profiling/ProfilerScopedTimer.h"
#include <tuple>

namespace Tesseract
{
    namespace Core
    {
        DebugLayer::DebugLayer():
            m_Cube(ResourceManager::LoadFromFile<Graphics::Mesh>("debug_cube")),
            m_Sphere(ResourceManager::LoadFromFile<Graphics::Mesh>("debug_sphere"))
        {
        }

        DebugLayer::~DebugLayer()
        {

        }

        void DebugLayer::OnAttach()
        {
        }


        void DebugLayer::DrawColliders(bool wireframe, bool overlay)
        {
            TESSERACT_PROFILE_FUNCTION();

            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Components::CameraComponent,
                Math::Vector3
                >> cameraEntities;

            std::vector<std::tuple<
                ECS::Entity,
                Components::TransformComponent,
                Ref<Physics::Collider>,
                Math::Matrix4
                >> colliderEntities;


            Physics::Physics::AcquireLock();

            {
                using namespace std;

                TESSERACT_PROFILE_SCOPE("Entity Grouping");


                for (ECS::Entity entity : m_ActiveScene->GetRegistry().AllEntites())
                {
                    if (entity.HasComponents<Components::TransformComponent, Components::ColliderComponent>())
                    {
                        auto [transform, colliderComponent] = entity.GetComponents<Components::TransformComponent, Components::ColliderComponent>();

                        for (Ref<Physics::Collider> collider : colliderComponent.Colliders)
                        {

                            Ref<Physics::AABBCollider> aabbCollider = std::dynamic_pointer_cast<Physics::AABBCollider>(collider);
                            if (aabbCollider != nullptr)
                            {
                                Math::Matrix4 globalTransform = Math::Matrix4::Translation(transform.GetGlobalPosition() + aabbCollider->Offset);
                                globalTransform *= Math::Matrix4::Scale(aabbCollider->Size / 2);
                                globalTransform *= Math::Matrix4::Translation(1.0f);

                                colliderEntities.push_back(std::tuple{
                                    entity,
                                    transform,
                                    aabbCollider,
                                    globalTransform
                                                           });
                                continue;
                            }

                            Ref<Physics::SphereCollider> sphereCollider = std::dynamic_pointer_cast<Physics::SphereCollider>(collider);
                            if (sphereCollider != nullptr)
                            {
                                Math::Matrix4 globalTransform = Math::Matrix4::Translation(transform.GetGlobalPosition() + sphereCollider->Offset);
                                globalTransform *= Math::Matrix4::Scale(sphereCollider->Radius);

                                colliderEntities.push_back(std::tuple{
                                    entity,
                                    transform,
                                    sphereCollider,
                                    globalTransform
                                                           });
                                continue;
                            }

                            Ref<Physics::PointCollider> pointCollider = std::dynamic_pointer_cast<Physics::PointCollider>(collider);
                            if (pointCollider != nullptr)
                            {
                                Math::Matrix4 globalTransform = Math::Matrix4::Translation(transform.GetGlobalPosition() + pointCollider->Offset);

                                colliderEntities.push_back(std::tuple{
                                    entity,
                                    transform,
                                    pointCollider,
                                    globalTransform
                                                           });
                                continue;
                            }

                        }
                    }

                    if (entity.HasComponents<Components::TransformComponent, Components::CameraComponent>())
                    {
                        auto [transform, camera] = entity.GetComponents<Components::TransformComponent, Components::CameraComponent>();

                        cameraEntities.push_back(
                            std::tuple{
                                entity,
                                transform,
                                camera,
                                transform.GetGlobalPosition()
                            }
                        );
                    }

                }
            }

            Physics::Physics::FreeLock();


            Math::Matrix4 projection;
            Math::Matrix4 view;
            Math::Vector3 viewPosition;

            // Update camera view

            for (auto& [entity, transform, camera, globalPosition] : cameraEntities)
            {
                if (camera.Camera != nullptr && camera.Camera->IsActive())
                {
                    // TODO: Get global position (note: scale cannot be included!)
                    camera.Camera->SetViewParameters(globalPosition, transform.RotationEulerAngles);
                    projection = camera.Camera->GetProjectionMatrix();
                    view = camera.Camera->GetViewMatrix();
                    // TODO: some GetGlobalPosition getter
                    viewPosition = globalPosition;
                }
            }

            if (overlay)
            {
                Graphics::Renderer::Clear(Graphics::Bits::DepthBufferBit);
            }
            Graphics::Renderer::EnableDepthTest();

            Graphics::Renderer::SetBackfaceCullingEnabled();

            Ref<Graphics::ShaderProgram> shaderProgram = m_Cube->GetMaterial()->GetShaderProgram();
            shaderProgram->Use();

            if (!wireframe)
            {
                Graphics::Renderer::SetWireframe(false);

                shaderProgram->SetUniformBool("uFilled", true);

                TESSERACT_PROFILE_SCOPE("Collider Solid draws");

                Ref<Graphics::Mesh> mesh = nullptr;


                //collision detection
                for (auto& [entity, transform, collider, globalTransform] : colliderEntities)
                {
                    if (collider->GetType() == Physics::Collider::Type::AABB)
                    {
                        mesh = m_Cube;
                    }
                    else if (collider->GetType() == Physics::Collider::Type::Sphere)
                    {
                        mesh = m_Sphere;
                    }
                    else if (collider->GetType() == Physics::Collider::Type::Point)
                    {
                        mesh = m_Sphere;
                        globalTransform[0][0] = 0.1f;
                        globalTransform[1][1] = 0.1f;
                        globalTransform[2][2] = 0.1f;
                    }


                    Renderer::DrawMesh(
                        mesh,
                        globalTransform
                    );
                }
            }

            shaderProgram->SetUniformBool("uFilled", false);
            Graphics::Renderer::SetWireframe();

            Graphics::Renderer::SetBackfaceCullingEnabled(false);


            {
                TESSERACT_PROFILE_SCOPE("Collider Wireframe draws");

                Ref<Graphics::Mesh> mesh = nullptr;

                for (auto& [entity, transform, collider, globalTransform] : colliderEntities)
                {
                    if (collider->GetType() == Physics::Collider::Type::AABB)
                    {
                        mesh = m_Cube;
                    }
                    else if (collider->GetType() == Physics::Collider::Type::Sphere)
                    {
                        mesh = m_Sphere;
                    }
                    else if (collider->GetType() == Physics::Collider::Type::Point)
                    {
                        mesh = m_Sphere;
                        globalTransform[0][0] = 0.1f;
                        globalTransform[1][1] = 0.1f;
                        globalTransform[2][2] = 0.1f;
                    }

                    Renderer::DrawMesh(
                        mesh,
                        globalTransform
                    );
                }
            }


            Graphics::Renderer::SetWireframe(false);
        }

        void DebugLayer::OnUpdate()
        {
            if (m_ActiveScene == nullptr)
            {
                return;
            }

            switch (Application::GetApplication()->GetDebugModeColliders())
            {
                case Application::DebugMode::DrawSolid: DrawColliders(false, false); break;
                case Application::DebugMode::DrawWireframe: DrawColliders(true, false); break;
                case Application::DebugMode::DrawSolidOverlay: DrawColliders(false, true); break;
                case Application::DebugMode::DrawWireframeOverlay: DrawColliders(true, true); break;
                case Application::DebugMode::NoDraw: break;
                default: break;
            }
        }


        void DebugLayer::OnEvent(Events::Event& event)
        {
            Events::EventHandler handler(event);
            handler.Handle<Events::SceneLoadedEvent>(
                [this](Events::SceneLoadedEvent& sceneLoadedEvent) -> bool
                {
                    m_ActiveScene = sceneLoadedEvent.GetScene();
            return false;
                }
            );
        }

    } // namespace Core

} // namespace Tesseract

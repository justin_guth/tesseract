#pragma once

#include "Tesseract/Events/Events.h"
#include "Scene.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{
    namespace Core
    {
        class Layer
        {
        public:

            virtual void OnAttach();
            virtual void OnDetach();

            virtual void OnEvent(Events::Event& event);

            virtual void OnUpdate();
            virtual void OnImGuiUpdate();
            virtual void OnPhysicsUpdate();

        };
    } // namespace Core

} // namespace Tesseract

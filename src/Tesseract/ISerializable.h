#pragma once

#include "Tesseract/Util/DynamicData.h"

#include <vector>

namespace Tesseract
{
    class ISerializable
    {
    public:
    
        virtual Util::DynamicData GetSerializationData() const = 0;

    };

} // namespace Tesseract

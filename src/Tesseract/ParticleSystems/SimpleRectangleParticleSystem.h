#pragma once

#include "Tesseract/Graphics/ParticleSystem.h"

#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Memory/SlotAllocator.h"

namespace Tesseract
{
    namespace ParticleSystems
    {
        class SimpleRectangleParticleSystem : public Graphics::ParticleSystem
        {
        public:

            virtual void Update() override;
            virtual WeakRef<Graphics::ShaderProgram> GetShaderProgram() override;

        private:

            struct ParticleData
            {
                Math::Vector3 Position;
                Math::Vector3 Velocity;
                Math::Vector4 Color;
                float Rotation;
                float Age;
                float MaxAge;
            };

            Memory::SlotAllocator<ParticleData, 65536> m_ParticleAllocator;
            Ref<Graphics::ShaderProgram> m_ShaderProgram;
        };
    } // namespace ParticleSystems

} // namespace Tesseract

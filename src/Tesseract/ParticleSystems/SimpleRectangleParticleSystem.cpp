#include "SimpleRectangleParticleSystem.h"

namespace Tesseract
{
    namespace ParticleSystems
    {
        void SimpleRectangleParticleSystem::Update()
        {

        }

        WeakRef<Graphics::ShaderProgram> SimpleRectangleParticleSystem::GetShaderProgram()
        {
            return m_ShaderProgram;
        }

    } // namespace ParticleSystems

} // namespace Tesseract

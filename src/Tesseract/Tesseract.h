#pragma once

// REVIEW: should this be in the Tesseract header for the client app?: find out how to fix
#include "glad/gl.h"
#include "GLFW/glfw3.h"


#include "Core/Application.h"
#include "Core/LayerStack.h"
#include "Core/Layer.h"
#include "Core/Window.h"
#include "Core/GLFWWindow.h"
#include "Core/Time.h"
#include "Core/Scene.h"
#include "Core/Geometry.h"
#include "Core/ResourceManager.h"

#include "Physics/Physics.h"
#include "Physics/Ray.h"

#include "ECS/ECS.h"
#include "Components/TagComponent.h"
#include "Components/TransformComponent.h"
#include "Components/MeshComponent.h"
#include "Components/CameraComponent.h"
#include "Components/SpriteAnimationComponent.h"
#include "Components/ScriptComponent.h"
#include "Components/AudioSourceComponent.h"
#include "Components/RigidBodyComponent.h"
#include "Components/LightSourceComponent.h"
#include "Components/TextComponent.h"
#include "Components/ColliderComponent.h"


#include "Logging/Logger.h"

#include "IO/Input.h"

#include "Graphics/ShaderProgram.h"
#include "Graphics/Material.h"


#include "Audio/Audio.h"
#include "Audio/AudioClip.h"
#include "Audio/AudioDevice.h"
#include "Audio/AudioBuffer.h"
#include "Audio/AudioSource.h"


#include "Math/Matrix4.h"
#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Math/Quaternion.h"

#include "Graphics/VertexBuffer.h"
#include "Graphics/ElementBuffer.h"
#include "Graphics/BufferLayout.h"
#include "Graphics/VertexArrayObject.h"
#include "Graphics/Renderer.h"
#include "Graphics/Camera.h"
#include "Graphics/SpriteAnimation.h"
#include "Graphics/PerspectiveCamera.h"
#include "Graphics/OrthographicCamera.h"

#include "Graphics/Mesh.h"

#include "Materials/PhongMaterial.h"

#include "Core/Time.h"

#include "Scripting/Script.h"
#include "Scripting/ScriptRegistry.h"

#include "Util/MeshLoading.h"
#include "Util/DynamicData.h"
#include "Util/JSONParser.h"
#include "Reference.h"

#include "EntryPoint.h"

// ============ DEBUG STUFF =============

#include "DebugPanels/SceneHierarchyPanel.h"
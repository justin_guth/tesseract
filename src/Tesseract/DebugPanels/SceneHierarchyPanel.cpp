#include "SceneHierarchyPanel.h"
#include "Tesseract/Components/TagComponent.h"
#include "vendor/imgui-docking/imgui.h"

#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/MeshComponent.h"
#include "Tesseract/Components/CameraComponent.h"

#include "Tesseract/Core/Application.h"

#include "Utils.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        SceneHierarchyPanel::SceneHierarchyPanel() :
            m_SelectedEntity(ECS::Entity::Null)
        {

        }


        void SceneHierarchyPanel::DrawEntityInfo(ECS::Entity& entity)
        {
            ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_SpanAvailWidth
                | ImGuiTreeNodeFlags_FramePadding
                | ImGuiTreeNodeFlags_OpenOnArrow;

            if (m_SelectedEntity == entity)
            {
                flags |= ImGuiTreeNodeFlags_Selected;
            }

            if (!entity.HasChildren())
            {
                flags |= ImGuiTreeNodeFlags_Leaf;
            }

            ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

            bool nodeOpened = ImGui::TreeNodeEx((void*)entity.GetID(), flags, "%s", entity.GetComponent<Components::TagComponent>().Tag.c_str());

            // Checks iof last element on stack is clicked
            if (ImGui::IsItemClicked())
            {
                m_SelectedEntity = entity;
            }

            if (ImGui::BeginDragDropTarget())
            {
                const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("id");

                if (payload != nullptr)
                {
                    entity_id id = *((entity_id*)(payload->Data));
                    entity.AddChild(id);
                }
                ImGui::EndDragDropTarget();
            }

            if (ImGui::BeginDragDropSource())
            {
                ImGui::Text("%s", entity.GetComponent<Components::TagComponent>().Tag.c_str());

                entity_id id = entity.GetID();

                ImGui::SetDragDropPayload("id", (void*)(&id), sizeof(entity_id));
                ImGui::EndDragDropSource();
            }

            if (ImGui::BeginPopupContextItem(nullptr, popupFlags))
            {
                if (ImGui::MenuItem("Delete entity"))
                {
                    entity.DestroyLater();
                }

                if (ImGui::MenuItem("Add child entity"))
                {
                    // FIXME: Dangerous to modify container while iterating
                    // Maybe add CreateChildLater functionality
                    entity.CreateChild();
                }

                DrawAddComponentMenu(entity);

                ImGui::EndPopup();
            }


            if (nodeOpened)
            {

                for (ECS::Entity& childEntity : entity.GetChildren())
                {
                    DrawEntityInfo(childEntity);
                }

                ImGui::TreePop();
            }
        }

        void SceneHierarchyPanel::Draw()
        {
            ECS::Registry& registry = Core::Application::GetApplication()->GetActiveScene()->GetRegistry();

            ImGui::Begin("Scene Hierarchy");

            DrawCreateEntityPopup(registry);

            for (auto [entity, tag] : registry.Filter<Components::TagComponent>())
            {
                if (!entity.HasParent())
                {
                    DrawEntityInfo(entity);
                }
            }

            registry.DestroyQueuedEnties();

            ImGui::End();
        }

        void SceneHierarchyPanel::DrawCreateEntityPopup(ECS::Registry& registry)
        {
            ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

            if (ImGui::BeginPopupContextItem(nullptr, popupFlags))
            {
                if (ImGui::BeginMenu("Create entity"))
                {
                    if (ImGui::MenuItem("Empty"))
                    {
                        ECS::Entity entity = registry.CreateEntity();
                        entity.GetComponent<Components::TagComponent>().Tag = "Empty";
                    }

                    // TODO: remove false when implemented
                    if (false) // ImGui::BeginMenu("With geometry")) {
                    {
                        if (ImGui::MenuItem("Cube"))
                        {
                            // TODO:
                            registry.CreateEntity();
                        }

                        if (ImGui::MenuItem("Sphere"))
                        {
                            // TODO:
                            registry.CreateEntity();
                        }

                        if (ImGui::MenuItem("Monkey"))
                        {
                            // TODO:
                            registry.CreateEntity();
                        }

                        ImGui::EndMenu();
                    }

                    ImGui::EndMenu();
                }

                ImGui::EndPopup();
            }
        }

    } // namespace DebugPanels

} // namespace Tesseract

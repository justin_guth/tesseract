#pragma once


#include "Tesseract/Util/TypeHash.h"
#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Reference.h"

#include "Tesseract/IHasParameters.h"

#include "vendor/imgui-docking/imgui.h"

#include <functional>

namespace Tesseract
{
    namespace DebugPanels
    {
        class SceneHierarchyPanel;

        class PropertiesPanel
        {
        public:

            PropertiesPanel();
            PropertiesPanel(SceneHierarchyPanel* sceneHierarchyPanel);
            ~PropertiesPanel();

            inline void SetSceneHierarchyPanel(SceneHierarchyPanel* sceneHierarchyPanel) { m_SceneHierarchyPanel = sceneHierarchyPanel; }
            void Draw();

            template<typename T_Type>
            inline void DrawComponentEditor(const std::string& title, ECS::Entity entity, std::function<void(ECS::Entity&, T_Type&)> function);

        private:

            static void DrawParameterEditor(IHasParameters& hasParameters);

        private:

            SceneHierarchyPanel* m_SceneHierarchyPanel;

        };

        template<typename T_Type>
        inline void PropertiesPanel::DrawComponentEditor(const std::string& title, ECS::Entity entity, std::function<void(ECS::Entity&, T_Type&)> function)
        {
            if (!entity.HasComponent<T_Type>())
            {
                return;
            }

            ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_DefaultOpen;
            flags |= ImGuiTreeNodeFlags_SpanAvailWidth;

            bool nodeOpened = ImGui::TreeNodeEx((void*) (uint64_t) Util::TypeHash::Get<T_Type>(), flags, "%s", title.c_str());

            ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;


            bool componentRemoved = false;

            if (ImGui::BeginPopupContextItem(nullptr, popupFlags))
            {
                if (ImGui::MenuItem("Remove component"))
                {
                    entity.RemoveComponent<T_Type>();
                    componentRemoved = true;
                }

                ImGui::EndPopup();
            }


            if (nodeOpened)
            {
                if (!componentRemoved)
                {
                    function(entity, entity.GetComponent<T_Type>());
                }

                ImGui::TreePop();
            }

        }

    } // namespace DebugPanels
} // namespace Tesseract


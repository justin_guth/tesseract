#pragma once


namespace Tesseract
{
    namespace DebugPanels
    {
        class RuntimeControlsPanel
        {
        public:

            inline RuntimeControlsPanel()
            {
                for (int i = 0; i < 256; i++)
                {
                    m_Deltas[i] = 0.0f;
                }
            }

            ~RuntimeControlsPanel() = default;

            void Draw();
            
        private:

            float m_Deltas[256];
            int m_CurrentDeltaIndex = 0;
        };

    } // namespace DebugPanels
} // namespace Tesseract


#include "PropertiesPanel.h"

#include "vendor/imgui-docking/imgui.h"

#include "SceneHierarchyPanel.h"

#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/TextComponent.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/MeshComponent.h"
#include "Tesseract/Components/CameraComponent.h"
#include "Tesseract/Components/SpriteAnimationComponent.h"
#include "Tesseract/Components/RigidBodyComponent.h"
#include "Tesseract/Components/LightSourceComponent.h"

#include "Tesseract/Graphics/OrthographicCamera.h"
#include "Tesseract/Graphics/PerspectiveCamera.h"

#include "Tesseract/Materials/PhongMaterial.h"

#include <cstring>

#include "vendor/imgui-docking/imgui.h"

#include "Tesseract/Math/Vector2.h"
#include "Tesseract/Math/Vector3.h"
#include "Tesseract/Math/Vector4.h"

#include "Tesseract/Core/ResourceManager.h"

#include "Tesseract/Core/Application.h"

#include "Utils.h"

#define TESSERACT_PROPERTIES_WARN_COLOR ImVec4(0.8f, 0.8f, 0.0f, 1.0f)

namespace Tesseract
{
    namespace DebugPanels
    {
        PropertiesPanel::PropertiesPanel() :
            m_SceneHierarchyPanel(nullptr)
        {
        }

        PropertiesPanel::PropertiesPanel(SceneHierarchyPanel* sceneHierarchyPanel) :
            m_SceneHierarchyPanel(sceneHierarchyPanel)
        {
        }

        PropertiesPanel::~PropertiesPanel()
        {
        }

        void PropertiesPanel::Draw()
        {
            ECS::Entity entity = m_SceneHierarchyPanel->GetSelectedEntity();

            ImGui::Begin("Entity Properties");


            if (entity == ECS::Entity::Null)
            {
                ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "No entity selected");
                ImGui::End();
                return;
            }

            ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

            if (ImGui::BeginPopupContextItem(nullptr, popupFlags))
            {
                DrawAddComponentMenu(entity);
                ImGui::EndPopup();
            }

            if (!entity.HasComponent<Components::TagComponent>())
            {
                ImGui::Text("Untagged Entity");
            }
            else
            {
                Components::TagComponent& tag = entity.GetComponent<Components::TagComponent>();
                ImGui::Text("%s", tag.Tag.c_str());
            }

            DrawComponentEditor<Components::TagComponent>(
                "Tag",
                entity,
                [](ECS::Entity& entity, Components::TagComponent& tag)
                {
                    char tagBuffer[128] = { 0 };
                    std::memcpy(tagBuffer, tag.Tag.c_str(), Math::Functions::Min(tag.Tag.length(), 127));

                    if (ImGui::InputText("Tag", tagBuffer, 128))
                    {
                        tag.Tag = tagBuffer;
                    }
                }
            );

            DrawComponentEditor<Components::TextComponent>(
                "Text",
                entity,
                [](ECS::Entity& entity, Components::TextComponent& text)
                {
                    char textBuffer[128] = { 0 };
                    std::memcpy(textBuffer, text.Text.c_str(), Math::Functions::Min(text.Text.length(), 127));

                    if (ImGui::InputText("Text", textBuffer, 128))
                    {
                        text.Text = textBuffer;
                    }
                    
                    ImGui::DragFloat("Font Size", & text.Size, 0.1f);


                }
            );


            DrawComponentEditor<Components::TransformComponent>(
                "Transform",
                entity,
                [](ECS::Entity& entity, Components::TransformComponent& transform)
                {
                    ImGui::DragFloat3("Position", transform.Position.GetData(), 0.1f);
                    Math::Vector3 rotation = transform.RotationEulerAngles;
                    rotation *= 180.0f / (Math::Constants::Pi);

                    if (ImGui::DragFloat3("Rotation", rotation.GetData()))
                    {
                        transform.RotationEulerAngles = rotation * Math::Constants::Pi / 180.0f;
                    }

                    ImGui::DragFloat3("Scale", transform.Scale.GetData(), 0.1f);
                    ImGui::Columns(1);
                }
            );

            DrawComponentEditor<Components::RigidBodyComponent>(
                "Rigid Body",
                entity,
                [](ECS::Entity& entity, Components::RigidBodyComponent& rigidBody)
                {

                    ImGui::DragFloat("Mass", &rigidBody.Mass, 0.1f, 0.0001f);
                    ImGui::DragFloat("Drag", &rigidBody.Drag, 0.1f, 0.0001f);
                    ImGui::Text("State");
                    ImGui::DragFloat3("Position##RigidBodyPosition", rigidBody.State.Position.GetData(), 0.1f);
                    ImGui::DragFloat3("Velocity##RigidBodyVelocity", rigidBody.State.Velocity.GetData(), 0.1f);
                    ImGui::DragFloat3("Acceleration##RigidBodyAcceleration", rigidBody.State.Acceleration.GetData(), 0.1f);
                     
                }
            );


            DrawComponentEditor<Components::LightSourceComponent>(
                "Light Source",
                entity,
                [](ECS::Entity& entity, Components::LightSourceComponent& lightSource)
                {
                    ImGui::ColorEdit3("Color##LightSourceColor", lightSource.Color.GetData());
                    ImGui::DragFloat("Intensity##LightSourceIntensity", &lightSource.Intensity);
                }
            );

            DrawComponentEditor<Components::CameraComponent>(
                "Camera",
                entity,
                [this](ECS::Entity& entity, Components::CameraComponent& camera)
                {
                    if (camera.Camera == nullptr)
                    {
                        ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "<unset>");

                        ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

                        if (ImGui::BeginPopupContextItem("UnsetCameraPopup", popupFlags))
                        {
                            if (ImGui::BeginMenu("Create Camera"))
                            {

                                if (ImGui::MenuItem("Perspective"))
                                {
                                    // TODO: Register with / Create through resourcemanager facade

                                    camera.Camera = MakeRef<Graphics::PerspectiveCamera>();
                                    camera.Camera->SetAspectRatio(
                                        Core::Application::GetApplication()->GetWindow()->GetAspectRatio()
                                    );
                                }

                                if (ImGui::MenuItem("Orthographic"))
                                {
                                    // TODO: Register with / Create through resourcemanager facade

                                    // camera.Camera = new Graphics::OrthographicCamera;
                                }
                                ImGui::EndMenu();
                            }

                            ImGui::EndPopup();
                        }

                        return;
                    }

                    // Camera is set, display parameters

                    DrawParameterEditor(*(camera.Camera));
                    if (ImGui::Button("Make Active"))
                    {
                        camera.Camera->MakeActive();
                    }
                }
            );

            DrawComponentEditor<Components::MeshComponent>(
                "Mesh",
                entity,
                [](ECS::Entity& entity, Components::MeshComponent& mesh)
                {
                    if (mesh.Mesh == nullptr)
                    {
                        ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "<unset>");

                        ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

                        if (ImGui::BeginPopupContextItem("UnsetMeshPopup", popupFlags))
                        {
                            if (ImGui::BeginMenu("Create Mesh"))
                            {

                                if (ImGui::MenuItem("Default Cube"))
                                {
                                    // TODO: Register with / Create through resourcemanager facade

                                    mesh.Mesh = MakeRef<Graphics::Mesh>();
                                    // mesh.Mesh->SetVertexArrayObject();
                                    // mesh.Mesh->SetMaterial();
                                }
                                ImGui::EndMenu();
                            }

                            ImGui::EndPopup();
                        }

                        return;
                    }

                    // Mesh set

                    const Ref<Graphics::Material>& material = mesh.Mesh->GetMaterial();

                    ImGui::Text("Material");
                    if (material != nullptr)
                    {
                        DrawParameterEditor(*(material));
                        // TODO: remove button
                    }
                    else
                    {
                        ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "Material not set");
                        // TODO: add create button
                    }


                    /* const Ref<Core::Geometry>& material = mesh.Mesh->GetMaterial();

                     ImGui::Text("Material");
                     if (material != nullptr)
                     {
                         DrawParameterEditor(*(material));
                         // TODO: remove button
                     }
                     else
                     {
                         ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "Material not set");
                         // TODO: add create button
                     }
                    */
                }
            );

            DrawComponentEditor<Components::SpriteAnimationComponent>(
                "Animation",
                entity,
                [](ECS::Entity& entity, Components::SpriteAnimationComponent& animation)
                {
                    if (animation.Animation == nullptr)
                    {
                        ImGui::TextColored(TESSERACT_PROPERTIES_WARN_COLOR, "<unset>");

                        ImGuiPopupFlags popupFlags = ImGuiPopupFlags_MouseButtonRight;

                        if (ImGui::BeginPopupContextItem("UnsetAnimationPopup", popupFlags))
                        {
                            if (ImGui::MenuItem("Create Animation"))
                            {
                                // TODO: implement
                            }

                            ImGui::EndPopup();
                        }

                        return;
                    }

                    // Animation set

                    ImGui::Text("Animation time: %.2fs", animation.Animation->GetTime());
                    float speed = animation.Animation->GetAnimationSpeed();
                    if (ImGui::DragFloat("Animation speed", &speed, 0.01f))
                    {
                        animation.Animation->SetAnimationSpeed(Math::Functions::Max(speed, 0.01f));
                    }
                    if (!animation.Animation->IsPlaying())
                    {
                        if (ImGui::Button("Play##playanimation"))
                        {
                            animation.Animation->Play();
                        }
                    }
                    else
                    {
                        if (ImGui::Button("Pause##pauseanimation"))
                        {
                            animation.Animation->Pause();
                        }
                    }

                    if (ImGui::Button("Stop##stopanimation"))
                    {
                        animation.Animation->Stop();
                    }
                }
            );

            ImGui::End();
        }

        void PropertiesPanel::DrawParameterEditor(IHasParameters& hasParameters)
        {
            const std::vector<IHasParameters::Parameter>& parameters = hasParameters.GetParameters();

            for (IHasParameters::Parameter parameter : parameters)
            {
                using namespace Graphics;

                auto [name, identifier, type, address] = parameter;

                std::string label = name + "##" + identifier;
                const char* labelPointer = label.c_str();

                bool modified = false;

                switch (type)
                {
                    case DataType::Float1: modified |= ImGui::DragFloat(labelPointer, (float*)address, 0.1f); break;
                    case DataType::Float2: modified |= ImGui::DragFloat2(labelPointer, ((Math::Vector2*)address)->GetData(), 0.1f); break;
                    case DataType::Float3: modified |= ImGui::DragFloat3(labelPointer, ((Math::Vector3*)address)->GetData(), 0.1f); break;
                    case DataType::Float4: modified |= ImGui::DragFloat4(labelPointer, ((Math::Vector4*)address)->GetData(), 0.1f); break;
                    case DataType::Color: modified |= ImGui::ColorEdit4(labelPointer, ((Math::Vector3*)address)->GetData(), 0.1f); break;
                    case DataType::Int1: break; // TODO
                    case DataType::Int2: break; // TODO
                    case DataType::Int3: break; // TODO
                    case DataType::Int4: break; // TODO
                    case DataType::Mat2: break; // TODO
                    case DataType::Mat3: break; // TODO
                    case DataType::Mat4: break; // TODO
                    case DataType::Bool: break; // TODO
                    case DataType::None: TESSERACT_CORE_ASSERT(false, "None provided as data type!"); break;
                    default: break;
                }

                if (modified)
                {
                    hasParameters.OnParameterChange(parameter);
                }
            }
        }

    } // namespace DebugPanels

} // namespace Tesseract

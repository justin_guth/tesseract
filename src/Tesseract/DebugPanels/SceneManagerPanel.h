#pragma once


namespace Tesseract
{
    namespace DebugPanels
    {
        class SceneManagerPanel
        {
        public:

            SceneManagerPanel() = default;
            ~SceneManagerPanel() = default;

            void Draw();

        private: 

            char m_SaveFilePathBuffer[256] { };
            char m_LoadFilePathBuffer[256] { };

        };

    } // namespace DebugPanels
} // namespace Tesseract


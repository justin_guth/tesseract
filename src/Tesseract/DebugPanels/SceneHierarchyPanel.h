#pragma once

#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Core/Scene.h"
#include "Tesseract/Reference.h"

#include "vendor/imgui-docking/imgui.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        class SceneHierarchyPanel
        {
        public:

            SceneHierarchyPanel();

            void Draw();
            inline ECS::Entity& GetSelectedEntity() { return m_SelectedEntity; }
            inline void ClearSelectedEntity() { m_SelectedEntity = ECS::Entity::Null; }
        private:

            void DrawEntityInfo(ECS::Entity& entity);
            void DrawCreateEntityPopup(ECS::Registry& registry);
            
            
        private:

            ECS::Entity m_SelectedEntity;
        };

    } // namespace DebugPanels

} // namespace Tesseract

#include "SceneManagerPanel.h"


#include "Tesseract/Logging/Logger.h"
#include "Tesseract/Util/DynamicData.h"
#include "Tesseract/Util/File.h"
#include "Tesseract/Core/Application.h"
#include "Tesseract/Core/Scene.h"
#include "vendor/imgui-docking/imgui.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        void SceneManagerPanel::Draw()
        {
            ImGui::Begin("Scene management");


            ImGui::InputText("Name##SaveScene", m_SaveFilePathBuffer, 256);

            if (ImGui::Button("Save scene as JSON"))
            {
                TESSERACT_CORE_INFO("Saving scene to {}", m_SaveFilePathBuffer);
                Util::DynamicData sceneData = Core::Application::GetApplication()->GetActiveScene()->GetSerializationData();
                std::string jsonData = sceneData.ToJSON();
                Util::File::WriteFile(std::string("../../res/scenes/") + m_SaveFilePathBuffer + ".json", jsonData);

            }

            ImGui::InputText("Name##LoadScene", m_LoadFilePathBuffer, 256);

            if (ImGui::Button("Load scene from JSON"))
            {
                TESSERACT_CORE_INFO("Loading scene from {}", m_LoadFilePathBuffer);
                Util::File::WriteFile("../../res/other/last_open.sc", m_LoadFilePathBuffer);
                Core::Application::GetApplication()->LoadScene(
                    m_LoadFilePathBuffer
                );

            }

            ImGui::End();
        }

    } // namespace DebugPanels
} // namespace Tesseract


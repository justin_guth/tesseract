#include "RuntimeControlsPanel.h"


#include "Tesseract/Core/Application.h"
#include "vendor/imgui-docking/imgui.h"

#include "Tesseract/Core/Time.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        void RuntimeControlsPanel::Draw()
        {
            ImGui::Begin("Runtime Controls");

            bool running = Core::Application::GetApplication()->IsSceneRunning();

            float sum = 0.0f;

            m_Deltas[m_CurrentDeltaIndex] = Tesseract::Core::Time::Delta();
            m_CurrentDeltaIndex = (m_CurrentDeltaIndex + 1) % 256;

            for (int i = 0; i < 256; i++)
            {
                sum += m_Deltas[i];
            }

            sum /= 256.0f;

            if (!running)
            {
                if (ImGui::Button("Play"))
                {
                    Core::Application::GetApplication()->SetSceneRunning(true);
                }
            }
            else
            {
                if (ImGui::Button("Pause"))
                {
                    Core::Application::GetApplication()->SetSceneRunning(false);
                }
            }

            ImGui::Text("Frame Time: %.2fms", Tesseract::Core::Time::Delta() * 1000.f);
            ImGui::Text("Avg. Frame Time: %.2fms", sum * 1000.f);
            ImGui::Text("FPS: %.1f", 1.0 / Tesseract::Core::Time::Delta());
            ImGui::Text("Avg. FPS: %.1f", 1.0 / sum);
            // TODO add again  ImGui::Text("Active Particles: %lu", Graphics::ParticleSystem::s_ActiveParticles);

            const char* const items[] = {
                "Filled",
                "Filled Overlay",
                "Wireframe",
                "Wireframe Overlay",
                "None"
            };

            static const char* currentItem = items[4];

            if (ImGui::BeginCombo("Collider debug view", currentItem)) // The second parameter is the label previewed before opening the combo.
            {
                for (int n = 0; n < 5; n++)
                {
                    bool isSelected = (currentItem == items[n]); // You can store your selection however you want, outside or inside your objects
                    if (ImGui::Selectable(items[n], isSelected))
                    {
                        currentItem = items[n];
                    }
                    if (isSelected)
                    {
                        ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
                    }
                }
                ImGui::EndCombo();
            }

            if (currentItem == items[0])
            {
                Core::Application::GetApplication()->SetDebugModeColliders(Core::Application::DebugMode::DrawSolid);
            }
            else if (currentItem == items[1])
            {
                Core::Application::GetApplication()->SetDebugModeColliders(Core::Application::DebugMode::DrawSolidOverlay);
            }
            else if (currentItem == items[2])
            {
                Core::Application::GetApplication()->SetDebugModeColliders(Core::Application::DebugMode::DrawWireframe);
            }
            else if (currentItem == items[3])
            {
                Core::Application::GetApplication()->SetDebugModeColliders(Core::Application::DebugMode::DrawWireframeOverlay);
            }
            else if (currentItem == items[4])
            {
                Core::Application::GetApplication()->SetDebugModeColliders(Core::Application::DebugMode::NoDraw);
            }

            ImGui::End();
        }

    } // namespace DebugPanels
} // namespace Tesseract


#include "Utils.h"


#include "Tesseract/Components/TagComponent.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/MeshComponent.h"
#include "Tesseract/Components/CameraComponent.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        void DrawAddComponentMenu(ECS::Entity& entity)
        {
            if (!entity.HasComponents<
                Components::TagComponent,
                Components::TransformComponent,
                Components::MeshComponent,
                Components::CameraComponent
            >())
            {
                if (ImGui::BeginMenu("Add component"))
                {
                    DrawAddComponentOption<Components::TagComponent>("Tag", entity);
                    DrawAddComponentOption<Components::TransformComponent>("Transform", entity);
                    DrawAddComponentOption<Components::MeshComponent>("Mesh", entity);
                    DrawAddComponentOption<Components::CameraComponent>("Camera", entity);

                    ImGui::EndMenu();
                }
            }
        }

        template<typename T_Type>
        inline void DrawAddComponentOption(const std::string& title, ECS::Entity& entity);

    } // namespace DebugPanels

} // namespace Tesseract

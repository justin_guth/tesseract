#pragma once

#include "Tesseract/ECS/ECS.h"

#include "vendor/imgui-docking/imgui.h"

namespace Tesseract
{
    namespace DebugPanels
    {
        void DrawAddComponentMenu(ECS::Entity& entity);

        template<typename T_Type>
        inline void DrawAddComponentOption(const std::string& title, ECS::Entity& entity);


        template<typename T_Type>
        inline void DrawAddComponentOption(const std::string& title, ECS::Entity& entity)
        {
            if (entity.HasComponent<T_Type>())
            {
                return;
            }

            if (ImGui::MenuItem(title.c_str()))
            {
                entity.AddComponent<T_Type>();
            }
        }


    } // namespace DebugPanels

} // namespace Tesseract

#include "ScriptRegistry.h"

namespace Tesseract
{
    namespace Scripting
    {
        std::unordered_map<std::string, Ref<ScriptFactory>> ScriptRegistry::s_Scripts;
    } // namespace Scripting

} // namespace Tesseract

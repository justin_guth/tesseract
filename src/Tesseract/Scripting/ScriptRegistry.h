#pragma once

#include "ScriptFactory.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Core/Macros.h"

#include <unordered_map>
#include <string>

namespace Tesseract
{
    namespace Scripting
    {
        class ScriptRegistry
        {
        public:

            template<typename T_Type>
            inline static void Register(const std::string& identifier);

            inline static Ref<Script> CreateInstance(const std::string& identifier)
            {
                TESSERACT_CORE_ASSERT(s_Scripts.contains(identifier), "Identifier '{}' not registered in ScriptRegistry!");
                Ref<Script> result = s_Scripts.at(identifier)->CreateInstance();
                result->MarkPersistent(identifier);
                return result;
            }

        private:

            static std::unordered_map<std::string, Ref<ScriptFactory>> s_Scripts;

        };

        template<typename T_Type>
        inline void ScriptRegistry::Register(const std::string& identifier)
        {
            TESSERACT_CORE_ASSERT(!s_Scripts.contains(identifier), "Identifier '{}' already registered in ScriptRegistry!");
        
            s_Scripts[identifier] = MakeRef<SpecificScriptFactory<T_Type>>();
        }

    } // namespace Scripting

} // namespace Tesseract

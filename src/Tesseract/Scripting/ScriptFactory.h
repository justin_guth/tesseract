#pragma once

#include "Script.h"
#include "Tesseract/Reference.h"

namespace Tesseract
{

    namespace Scripting
    {
        class ScriptFactory
        {
        public:

            virtual Ref<Script> CreateInstance() = 0;
        };

        template<typename T_Type>
        class SpecificScriptFactory : public ScriptFactory
        {
            inline virtual Ref<Script> CreateInstance() override
            {
                return MakeRef<T_Type>();
            }
        };
    } // namespace Scripting

} // namespace Tesseract

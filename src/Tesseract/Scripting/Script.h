#pragma once

#include "Tesseract/ECS/ECS.h"
#include "Tesseract/Components/TransformComponent.h"
#include "Tesseract/Components/ColliderComponent.h"
#include "Tesseract/Core/Resource.h"
#include "Tesseract/Core/Application.h"

namespace Tesseract
{
    namespace Scripting
    {
        class Script: public Core::Resource
        {
        public:

            virtual void OnAttach() {}
            virtual void OnUpdate() {}
            virtual void OnDraw() {}
            virtual void BeforePhysicsUpdate() {}
            virtual void OnPhysicsUpdate() {}
            virtual void OnSceneLoaded() {}
            virtual void OnImGuiUpdate() {}
            virtual void OnEntityDestroyed() {}
            virtual void OnButtonPressed() {}
            virtual void OnCollision(const Ref<Tesseract::Physics::Collider>& col, const Ref<Tesseract::Physics::Collider>& otherCol, Components::ColliderComponent& thisCollider, Components::ColliderComponent& otherCollider) {}
            virtual void OnCollisionEnter(const Ref<Tesseract::Physics::Collider>& col, const Ref<Tesseract::Physics::Collider>& otherCol, Components::ColliderComponent& thisCollider, Components::ColliderComponent& otherCollider) {}
            virtual void OnCollisionLeave(const Ref<Tesseract::Physics::Collider>& col, const Ref<Tesseract::Physics::Collider>& otherCol, Components::ColliderComponent& thisCollider, Components::ColliderComponent& otherCollider) {}

            inline Components::TransformComponent& Transform() { return Entity.GetComponent<Components::TransformComponent>(); }

            inline ECS::Entity CreateEntity() { return Entity.GetRegistry()->CreateEntity(); }
            
            // TODO: move prefab loading into custom utility function instead of scene
            inline ECS::Entity LoadPrefab(const std::string& identifier) { return Core::Application::GetApplication()->GetActiveScene()->LoadPrefab(identifier); }


            // TODO: do something to prevent script creation outside of registry
            // Maybe make constructor private and add ScriptFactory as friend class or something

            inline bool Tick(const Identifier& identifier)
            {
                return Core::Application::GetApplication()->Tick(identifier);
            }

        public:

            ECS::Entity Entity;

        };

    } // namespace Scripting

} // namespace Tesseract

#pragma once

#include "Tesseract/Components/AudioSourceComponent.h"

namespace Tesseract
{
    namespace Scripts
    {
        class AudioSourceHandlerScript: public Scripting::Script
        {
        public:

            inline void OnAttach() override
            {

            }

            inline void OnUpdate() override
            {
                Components::AudioSourceComponent& audioSource = Entity.GetComponent<Components::AudioSourceComponent>();

                if (!audioSource.AudioSource->IsPlaying() && !audioSource.AudioSource->IsPaused())
                {
                    Entity.DestroyLater();
                }
            }

        };
    } // namespace Scripts
} // namespace Game

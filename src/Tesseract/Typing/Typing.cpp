
#include "Typing.h"
#include "Tesseract/Logging/Logger.h"

namespace Tesseract
{
    namespace Typing
    {
        Face::Face(const FT_Library& library, const std::string& path) :
            m_Library(library),
            m_Face()
        {
            if (FT_New_Face(library, path.c_str(), 0, &m_Face))
            {
                TESSERACT_CORE_ERROR("Failed to load font {}", path);
            }

            SetPixels(128);
            Reload();
        }

        Face::~Face()
        {
            if (m_Invalid)
            {
                return;
            }

            FT_Done_Face(m_Face);
        }

        void Face::Reload()
        {
            for (unsigned char c = 0; c < 128; c++)
            {
                LoadCharacter(c);
            }
        }

        void Face::SetPixels(uint32_t pixels)
        {
            FT_Set_Pixel_Sizes(m_Face, 0, pixels);
        }

        Character Face::GetCharacter(char character)
        {
            if (m_Characters.contains(character))
            {
                return m_Characters.at(character);
            }

            return LoadCharacter(character);
        }

        Character Face::LoadCharacter(char character)
        {

            if (FT_Load_Char(m_Face, character, FT_LOAD_RENDER))
            {
                TESSERACT_CORE_ERROR("Failed to load Glyph");
                return {};
            }

            Ref<Graphics::GlyphTexture> texture = Graphics::GlyphTexture::Create(
                m_Face->glyph->bitmap.width,
                m_Face->glyph->bitmap.rows,
                m_Face->glyph->bitmap.buffer
            );
            // now store character for later use
            Character characterStruct;

            characterStruct.Texture = texture;
            characterStruct.Size = Math::Vector2T<uint32_t>(m_Face->glyph->bitmap.width, m_Face->glyph->bitmap.rows);
            characterStruct.Bearing = Math::Vector2T<int32_t>(m_Face->glyph->bitmap_left, m_Face->glyph->bitmap_top);
            characterStruct.Advance = m_Face->glyph->advance.x;

            m_Characters[character] = characterStruct;
            return characterStruct;
        }


        FontLibrary::FontLibrary()
        {
            if (FT_Init_FreeType(&m_FreetypeLibrary))
            {
                TESSERACT_CORE_ERROR("Could not initialize FreeType Library");
                return;
            }
        }

        FontLibrary::~FontLibrary()
        {
            for (const auto& [key, face] : m_Faces)
            {
                face->Invalidate();
            }

            FT_Done_FreeType(m_FreetypeLibrary);
        }

        Ref<Face> FontLibrary::LoadFace(const std::string& path, const Identifier& identifier)
        {
            if (m_Faces.contains(identifier))
            {
                return m_Faces.at(identifier);
            }

            m_Faces[identifier] = MakeRef<Face>(m_FreetypeLibrary, path);
            return m_Faces.at(identifier);
        }


    } // namespace Typing

} // namespace Tesseract


void BLA()
{

}
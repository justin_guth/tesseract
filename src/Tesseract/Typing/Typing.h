#pragma once

#include "freetype/freetype.h"


#include "Tesseract/Core/Identifier.h"
#include "Tesseract/Reference.h"
#include "Tesseract/Graphics/GlyphTexture.h"
#include "Tesseract/Math/Vector2.h"

#include <string>
#include <unordered_map>

namespace Tesseract
{
    namespace Typing
    {
        struct Character {

            Ref<Graphics::GlyphTexture> Texture;
            Math::Vector2T<uint32_t> Size;
            Math::Vector2T<int32_t> Bearing;
            uint32_t Advance;
        };

        class FontLibrary;

        class Face
        {
        public:

        friend class FontLibrary;

            Face(const FT_Library& library, const std::string& path);
            ~Face();
            void SetPixels(uint32_t pixels);
            Character LoadCharacter(char character);
            Character GetCharacter(char character);
            void Reload();

        private:

            FT_Library m_Library;
            FT_Face m_Face;
            bool m_Invalid = false;

            std::unordered_map<char, Character> m_Characters;

            inline void Invalidate() {m_Invalid = true; }
        };

        class FontLibrary
        {
        public:

            FontLibrary();
            ~FontLibrary();

            Ref<Face> LoadFace(const std::string& path, const Identifier& identifier);

        private:

            FT_Library m_FreetypeLibrary;
            std::unordered_map<Identifier, Ref<Face>, IdentifierHashFunction> m_Faces;

        };
    } // namespace Typing

} // namespace Tesseract



void BLA();
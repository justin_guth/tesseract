#pragma once

#include "Tesseract/Tesseract.h"

namespace Game
{
    class GameApplication : public Tesseract::Core::Application
    {
    public:
    
        GameApplication();

        void OnInit() override;
        void BeforeInit() override;
    };
} // namespace Game

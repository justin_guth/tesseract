#pragma once

#include "Tesseract/Scripting/Script.h"
#include "Tesseract/Core/Application.h"

class StartGameButtonHandler : public Tesseract::Scripting::Script
{
    virtual void OnButtonPressed() override
    {
        Tesseract::Core::Application::GetApplication()->LoadSceneLater("pong/game");
    }
};
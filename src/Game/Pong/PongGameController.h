#pragma once

#include "Tesseract/Scripting/Script.h"
#include "Tesseract/Core/Application.h"
#include "Tesseract/Tesseract.h"

using namespace Tesseract;

class PongGameController : public Tesseract::Scripting::Script
{
    float lPaddleHeight = 0.0f;
    float rPaddleHeight = 0.0f;

    float paddleSpeed = 0.5f;
    float paddleLength = 0.3f;
    float paddleWidth = 0.05f;
    float tolerance = 0.015f;
    float ballWidth = 0.05f;

    Math::Vector2 BallPosition = { 0.5,0.5 };
    Math::Vector2 BallDirection = Math::Vector2(1.0, 0.0);
    float BallVelocity = 0.25;
    float BallBaseVelocity = 0.25;
    float BallSpeedIncrement = 0.02f;

    float aspectRatio = 2.0f; // with:height

    Core::Window* GameWindow;

    float fieldWidth;
    float fieldHeight;

    bool Interrupted = true;

    int lastScored = 0;
    bool Scored = false;

    int p1score = 0;
    int p2score = 0;

    float elapsedTime = 0.0f;

    inline void OnAttach() override
    {
        lPaddleHeight = 0.5 - paddleLength / 2;
        rPaddleHeight = 0.5 - paddleLength / 2;

        GameWindow = Core::Application::GetApplication()->GetWindow();
        GameWindow->SetTitle("Pong! on Tesseract");
    }

    inline void OnUpdate() override
    {
        if (IO::Input::KeyPressed(IO::KeyCode::_Space))
        {
            Interrupted = !Interrupted;

            if (Scored)
            {
                BallPosition = { 0.5, 0.5 };
            }

            Scored = false;
        }

        if (Interrupted)
        {
            return;
        }

        elapsedTime += Core::Time::Delta();

        if (IO::Input::KeyDown(IO::KeyCode::_W))
        {
            lPaddleHeight += Core::Time::Delta() * paddleSpeed;
        }
        if (IO::Input::KeyDown(IO::KeyCode::_A))
        {
            lPaddleHeight -= Core::Time::Delta() * paddleSpeed;
        }

        if (IO::Input::KeyDown(IO::KeyCode::_P))
        {
            rPaddleHeight += Core::Time::Delta() * paddleSpeed;
        }
        if (IO::Input::KeyDown(IO::KeyCode::_L))
        {
            rPaddleHeight -= Core::Time::Delta() * paddleSpeed;
        }

        BallPosition += BallDirection * BallVelocity * Core::Time::Delta();

        if (BallPosition.Y <= 0)
        {
            BallPosition.Y = 0.0f + Math::Constants::Epsilon;
            BallDirection.Y = -BallDirection.Y;
        }
        else  if (BallPosition.Y >= 1.0 - ballWidth * aspectRatio)
        {
            BallPosition.Y = 1.0 - ballWidth * aspectRatio - Math::Constants::Epsilon;
            BallDirection.Y = -BallDirection.Y;
        }

        // Check left paddle intersection
        if (
            lPaddleHeight <= BallPosition.Y + ballWidth * aspectRatio
            && BallPosition.Y <= lPaddleHeight + paddleLength
            && paddleWidth - tolerance <= BallPosition.X
            && BallPosition.X <= paddleWidth
            )
        {
            BallVelocity += BallSpeedIncrement;
            BallPosition.X = paddleWidth;

            float paddleCenter = lPaddleHeight + paddleLength / 2;
            float ballcontact = BallPosition.Y + ballWidth; // Not times 2 because it must be halved
            float factor = (ballcontact - paddleCenter) / (paddleLength / 2 + ballWidth);
            BallDirection = { Math::Functions::Cos(factor * 1.5), Math::Functions::Sin(factor * 1.5) * aspectRatio};
        }

        // Check right paddle intersection
        if (
            rPaddleHeight <= BallPosition.Y + ballWidth * aspectRatio
            && BallPosition.Y <= rPaddleHeight + paddleLength
            && 1.0f - paddleWidth - ballWidth <= BallPosition.X
            && BallPosition.X <= 1.0f - paddleWidth - ballWidth + tolerance
            )
        {

            BallPosition.X = 1.0f - paddleWidth - ballWidth;
            BallVelocity += BallSpeedIncrement;

            float paddleCenter = rPaddleHeight + paddleLength / 2;
            float ballcontact = BallPosition.Y + ballWidth; // Not times 2 because it must be halved
            float factor = (ballcontact - paddleCenter) / (paddleLength / 2 + ballWidth);
            BallDirection = { -Math::Functions::Cos(factor * 1.5), Math::Functions::Sin(factor * 1.5) * aspectRatio};
        }


        lPaddleHeight = Math::Functions::Clamp(lPaddleHeight, 0.0f, 1.0f - paddleLength);
        rPaddleHeight = Math::Functions::Clamp(rPaddleHeight, 0.0f, 1.0f - paddleLength);

        // Check goal:

        if (BallPosition.X <= 0.0)
        {
            // P2 wins
            p2score++;
            Interrupted = true;
            Scored = true;
            lastScored = 2;
            BallDirection = -Math::Vector2::UnitX();
            BallVelocity = BallBaseVelocity;
        }
        else if (BallPosition.X + ballWidth >= 1.0)
        {
            // P1 Wins
            p1score++;
            Interrupted = true;
            Scored = true;
            lastScored = 1;
            BallDirection = Math::Vector2::UnitX();
            BallVelocity = BallBaseVelocity;
        }
    }

    void OnImGuiUpdate() override
    {
        ImGui::Begin("Game Debug Info");
     
        ImGui::Text("%f, %f, %f", BallDirection.X, BallDirection.Y, BallDirection.Magnitude());

        ImGui::End();
    }

    void OnDraw() override
    {

        const auto& [windowWidth, windowHeight] = GameWindow->GetDimensions();

        Graphics::Renderer::SetProjectionMatrix(Math::Matrix4::OrthographicProjection(0, windowWidth, 0, windowHeight));
        Graphics::Renderer::SetViewMatrix(Math::Matrix4::Identity());


        if (windowWidth >= aspectRatio * windowHeight) // window is limited by height
        {
            fieldWidth = windowHeight * aspectRatio;
            fieldHeight = windowHeight;
        }
        else // limited by width
        {
            fieldWidth = windowWidth;
            fieldHeight = windowWidth / aspectRatio;
        }

        Graphics::Renderer::DrawRectangle(
            { (windowWidth - fieldWidth) / 2.f, (windowHeight - fieldHeight) / 2.0f, -0.1f },
            { fieldWidth, fieldHeight },
            { 0.3, 0.1, 0.3, 1.0 }
        );

        Graphics::Renderer::DrawRectangle(
            { (windowWidth - fieldWidth) / 2.f, (windowHeight - fieldHeight) / 2.0f + lPaddleHeight * fieldHeight, 0.0f },
            { paddleWidth * fieldWidth, paddleLength * fieldHeight },
            { 0.15, 0.4, 0.8, 1.0 }
        );

        Graphics::Renderer::DrawRectangle(
            { windowWidth - paddleWidth * fieldWidth - (windowWidth - fieldWidth) / 2.f, (windowHeight - fieldHeight) / 2.0f + rPaddleHeight * fieldHeight, 0.0f },
            { paddleWidth * fieldWidth, paddleLength * fieldHeight },
            { 0.15, 0.4, 0.8, 1.0 }
        );

        Graphics::Renderer::DrawRectangle(
            { (windowWidth - fieldWidth) / 2.f + BallPosition.X * fieldWidth, (windowHeight - fieldHeight) / 2.0f + BallPosition.Y * fieldHeight, 0.0f },
            { ballWidth * fieldWidth, ballWidth * fieldWidth },
            { 0.7, 0.4, 0.8, 1.0 }
        );


        Graphics::Renderer::FlushRectangleBuffer(); // Needs to be here becuase draw text flushes (9 characters > 8)

        if (Interrupted && !Scored)
        {
            Graphics::Renderer::DrawText(
                "Game Paused",
                Math::Matrix4::Translation(windowWidth / 2, windowHeight / 2, 0.5f),
                Core::ResourceManager::LoadFromFile<Typing::Face>("arial"),
                500,
                { 1.0,1.0,1.0,1.0 }
            );
        }
        else if (Scored)
        {
            Graphics::Renderer::DrawText(
                "Player " + std::to_string(lastScored) + " scored!",
                Math::Matrix4::Translation(windowWidth / 2, windowHeight / 2, 0.5f),
                Core::ResourceManager::LoadFromFile<Typing::Face>("arial"),
                300,
                { 1.0,1.0,1.0,1.0 }
            );
        }

        Graphics::Renderer::DrawText(
            "P1: " + std::to_string(p1score),
            Math::Matrix4::Translation(windowWidth / 2 - 80, windowHeight - 30, 0.5f),
            Core::ResourceManager::LoadFromFile<Typing::Face>("arial"),
            100,
            { 1.0,1.0,1.0,1.0 }
        );

        Graphics::Renderer::DrawText(
            "P2: " + std::to_string(p2score),
            Math::Matrix4::Translation(windowWidth / 2 + 80, windowHeight - 30, 0.5f),
            Core::ResourceManager::LoadFromFile<Typing::Face>("arial"),
            100,
            { 1.0,1.0,1.0,1.0 }
        );

        Graphics::Renderer::FlushTextBuffer();

    }


};
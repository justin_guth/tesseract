#include "GameApplication.h"

#include "Tesseract/Scripting/ScriptRegistry.h"

#include "Tron/TitleTextController.h"
#include "Tron/StartGameTextController.h"
#include "Tron/GameController.h"

namespace Game
{
    GameApplication::GameApplication(): Tesseract::Core::Application(/* Custom title here later*/)
    {
    }

    void GameApplication::OnInit()
    {
        using namespace Tesseract;

        LoadScene("tron/menu");
        glClearColor(0.0, 0.0, 0.0, 1.0);
        Core::Application::GetApplication()->SetSceneRunning();
        
        Graphics::Renderer::SetSpriteDistanceFunction(
            [](const Math::Vector3& viewPosition, const Math::Vector3& spritePosition)
            {
                return (spritePosition.Y - viewPosition.Y);
            }
        );

    }

    void GameApplication::BeforeInit()
    {
        Tesseract::Scripting::ScriptRegistry::Register<Tron::TitleTextController>("tron_title_text_controller");
        Tesseract::Scripting::ScriptRegistry::Register<Tron::StartGameTextController>("tron_start_game_text_controller");
        Tesseract::Scripting::ScriptRegistry::Register<Tron::GameController>("tron_game_controller");
    }


} // namespace Game

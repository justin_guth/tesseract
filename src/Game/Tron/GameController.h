#pragma once

#include "Tesseract/Tesseract.h"

#include <vector>

using namespace Tesseract;

namespace Tron
{

    class GameController : public Tesseract::Scripting::Script
    {
        const int GridWidth = 40;
        const int GridHeight = 30;

        const float AspectRatio = (float)GridWidth / (float)GridHeight;
        const float PaddingTop = 60.0f;
        const float PaddingBottom = 35.0f;

        Math::Vector2I P1StartPos = { 5, 5 };
        Math::Vector2I P2StartPos = { GridWidth - 5 - 1, GridHeight - 5 - 1 };

        struct Pickup
        {
            enum class Type
            {
                Increment,
                Ghost,
                Trail,
                Speedup
            };

            inline bool operator==(const Pickup& other) const
            {
                return AssociatedType == other.AssociatedType && Position == other.Position;
            }

            Type AssociatedType;
            Math::Vector2I Position;
        };

        std::vector<Pickup> Pickups;

        Core::Window* Window;

        enum class GameState
        {
            Running,
            PendingStart,
            Paused,
            P1Won,
            P2Won,
            Draw
        };

        enum class Direction
        {
            Up,
            Down,
            Left,
            Right
        };

        struct Player
        {
            std::vector<Math::Vector2I> Trace;
            Direction LastDirection;
            int Score = 0;
            float Slow = 0.0f;
            bool SkipFrame = false;
        };

        Player P1;
        Player P2;

        Direction P1Intent = Direction::Up;
        Direction P2Intent = Direction::Down;

        GameState CurrentGameState = GameState::PendingStart;

        ECS::Entity GameInterruptionTextEntity;
        ECS::Entity P1ScoreEntity;
        ECS::Entity P2ScoreEntity;
        ECS::Entity TimeEntity;

        float TimeStep = 0.4f;
        float Timer = 0.7;

        float GameTime = 0.0f;

        int ActiveIncrementPickups = 0;

        inline void OnAttach() override
        {
            Window = Core::Application::GetApplication()->GetWindow();
            Window->SetTitle("Pong! on Tesseract");

            GameInterruptionTextEntity = Entity.Find("Game Interruption Text");
            P1ScoreEntity = Entity.Find("P1 Score");
            P2ScoreEntity = Entity.Find("P2 Score");
            TimeEntity = Entity.Find("Time");
            GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Visible = true;
            ResetBoard();

            Core::Application::GetApplication()->RegisterTicker("tron_event_tick", 3.0f);
        }

        inline void OnUpdate() override
        {
            const auto& [windowWidth, windowHeight] = Window->GetDimensions();

            Components::TransformComponent& p1Transform = P1ScoreEntity.GetComponent<Components::TransformComponent>();
            Components::TransformComponent& p2Transform = P2ScoreEntity.GetComponent<Components::TransformComponent>();
            Components::TransformComponent& timeTransform = TimeEntity.GetComponent<Components::TransformComponent>();

            p1Transform.Position.X = windowWidth / 2.0 - 130.0f;
            p2Transform.Position.X = windowWidth / 2.0 + 130.0f;
            timeTransform.Position.X = windowWidth / 2.0f;

            p1Transform.Position.Y = windowHeight - 30.0f;
            p2Transform.Position.Y = windowHeight - 30.0f;
            timeTransform.Position.Y = windowHeight - 30.0f;

            GameInterruptionTextEntity.GetComponent<Components::TransformComponent>().Position = Math::Vector3((float)windowWidth / 2.0f, (float)windowHeight / 2.0f, 0.3f);

            P1ScoreEntity.GetComponent<Components::TextComponent>().Text = "P1: " + std::to_string(P1.Score);
            P2ScoreEntity.GetComponent<Components::TextComponent>().Text = "P2: " + std::to_string(P2.Score);

            int totalSeconds = (int)GameTime;
            int seconds = totalSeconds % 60;
            int minutes = totalSeconds / 60;

            std::string padMin = minutes < 10 ? "0" : "";
            std::string padSec = seconds < 10 ? "0" : "";

            TimeEntity.GetComponent<Components::TextComponent>().Text = padMin + std::to_string(minutes) + ":" + padSec + std::to_string(seconds);


            
            switch (CurrentGameState)
            {
                case GameState::PendingStart: HandlePendingStart(); break;
                case GameState::Paused: HandlePaused(); break;
                case GameState::Running: HandleRunning(); break;
                case GameState::P1Won: HandleP1Won(); break;
                case GameState::P2Won: HandleP2Won(); break;
                case GameState::Draw: HandleDraw(); break;
                default: break;
            }
        }

        void HandleRandomTickEvent()
        {
            if (ActiveIncrementPickups == 0)
            {
                SpawnIncrementPickup();
            }
            else if (Math::Functions::Random() <= 0.1f)
            {
                // SpawnSpeedupPickup();
            }
        }

        Math::Vector2I GetFreeSpot()
        {
            Math::Vector2I pos(
                Math::Functions::RandomIntRange(0, GridWidth),
                Math::Functions::RandomIntRange(0, GridHeight)
            );

            while (Collides(pos) || CollidesPickup(pos))
            {
                pos = Math::Vector2I(
                    Math::Functions::RandomIntRange(0, GridWidth),
                    Math::Functions::RandomIntRange(0, GridHeight)
                );
            }

            return pos;
        }

        void SpawnIncrementPickup()
        {
            Math::Vector2I pos = GetFreeSpot();

            Pickups.push_back({ Pickup::Type::Increment, pos });
            ActiveIncrementPickups++;
        }

        void SpawnSpeedupPickup()
        {
            Math::Vector2I pos = GetFreeSpot();

            Pickups.push_back({ Pickup::Type::Speedup, pos });
            ActiveIncrementPickups++;
        }

        void HandlePendingStart()
        {
            Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
            text.Text = "GET READY";
            text.Visible = Math::Functions::Fmod(Core::Time::TotalGame(), 1.0) <= 0.5;

            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::Running;
                GameTime = 0.0f;
                // TODO: reset points, etc.
                text.Visible = false;
                ResetBoard();
            }
        }

        void HandlePaused()
        {
            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::Running;
                Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
                text.Visible = false;
            }
        }

        void HandleRunning()
        {
            Timer -= Core::Time::Delta();
            GameTime += Core::Time::Delta();

            TimeStep -= (Core::Time::Delta() / 240) * 0.4;

            bool tick = false;

            if (Tick("tron_event_tick"))
            {
                HandleRandomTickEvent();
            }

            if (Timer < 0.0f)
            {
                Timer = TimeStep;
                tick = true;
            }

            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::Paused;
                Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
                text.Visible = true;
                text.Text = "GAME PAUSED";
                Timer = TimeStep;
                return;
            }


            if (IO::Input::KeyDown(IO::KeyCode::_W) && P1.LastDirection != Direction::Down) { P1Intent = Direction::Up; }
            else if (IO::Input::KeyDown(IO::KeyCode::_A) && P1.LastDirection != Direction::Right) { P1Intent = Direction::Left; }
            else if (IO::Input::KeyDown(IO::KeyCode::_S) && P1.LastDirection != Direction::Up) { P1Intent = Direction::Down; }
            else if (IO::Input::KeyDown(IO::KeyCode::_D) && P1.LastDirection != Direction::Left) { P1Intent = Direction::Right; }

            if (IO::Input::KeyDown(IO::KeyCode::_I) && P2.LastDirection != Direction::Down) { P2Intent = Direction::Up; }
            else if (IO::Input::KeyDown(IO::KeyCode::_J) && P2.LastDirection != Direction::Right) { P2Intent = Direction::Left; }
            else if (IO::Input::KeyDown(IO::KeyCode::_K) && P2.LastDirection != Direction::Up) { P2Intent = Direction::Down; }
            else if (IO::Input::KeyDown(IO::KeyCode::_L) && P2.LastDirection != Direction::Left) { P2Intent = Direction::Right; }


            if (!tick)
            {
                return;
            }

            // Check collisions:

            Math::Vector2I newHeadP1;
            Math::Vector2I newHeadP2;

            switch (P1Intent)
            {
                case Direction::Up: newHeadP1 = P1.Trace[0] + Math::Vector2I {0, 1}; break;
                case Direction::Left: newHeadP1 = P1.Trace[0] + Math::Vector2I {-1, 0}; break;
                case Direction::Down: newHeadP1 = P1.Trace[0] + Math::Vector2I {0, -1}; break;
                case Direction::Right: newHeadP1 = P1.Trace[0] + Math::Vector2I {1, 0}; break;
                default: break;
            }

            switch (P2Intent)
            {
                case Direction::Up: newHeadP2 = P2.Trace[0] + Math::Vector2I {0, 1}; break;
                case Direction::Left: newHeadP2 = P2.Trace[0] + Math::Vector2I {-1, 0}; break;
                case Direction::Down: newHeadP2 = P2.Trace[0] + Math::Vector2I {0, -1}; break;
                case Direction::Right: newHeadP2 = P2.Trace[0] + Math::Vector2I {1, 0}; break;
                default: break;
            }

            P1.LastDirection = P1Intent;
            P2.LastDirection = P2Intent;

            bool p1Collides = Collides(newHeadP1);
            bool p2Collides = Collides(newHeadP2);

            if (p1Collides && p2Collides || newHeadP1 == newHeadP2)
            {
                CurrentGameState = GameState::Draw;
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Text = "DRAW";
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Visible = true;

            }
            else if (p1Collides)
            {
                CurrentGameState = GameState::P2Won;
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Text = "PLAYER TWO WINS";
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Visible = true;
            }
            else if (p2Collides)
            {
                CurrentGameState = GameState::P1Won;
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Text = "PLAYER ONE WINS";
                GameInterruptionTextEntity.GetComponent<Components::TextComponent>().Visible = true;

            }

            bool incrementP1 = false;
            bool incrementP2 = false;

            std::vector<Pickup> tbd;

            for (const auto& pickup : Pickups)
            {
                if (pickup.AssociatedType == Pickup::Type::Increment)
                {
                    if (pickup.Position == newHeadP1)
                    {
                        tbd.push_back(pickup);
                        incrementP1 = true;
                        P1.Score++;
                    }
                    if (pickup.Position == newHeadP2)
                    {
                        tbd.push_back(pickup);
                        incrementP2 = true;
                        P2.Score++;
                    }
                }

                if (pickup.AssociatedType == Pickup::Type::Speedup)
                {
                    if (pickup.Position == newHeadP1)
                    {
                        tbd.push_back(pickup);
                        P2.Slow = 3.0f;
                    }
                    if (pickup.Position == newHeadP2)
                    {
                        tbd.push_back(pickup);
                        P1.Slow = 3.0f;
                    }
                }
            }

            std::vector<Pickup> newPickups;

            for (const auto& pickup : Pickups)
            {
                bool inTBD = false;

                for (const auto& tbdCandidate : tbd)
                {
                    if (pickup == tbdCandidate)
                    {
                        inTBD = true;
                    }
                }

                if (!inTBD)
                {
                    newPickups.push_back(pickup);
                }
                else if (pickup.AssociatedType == Pickup::Type::Increment)
                {
                    ActiveIncrementPickups--;
                }
            }

            Pickups = newPickups;

            // Update traces

            if (incrementP1)
            {
                P1.Trace.push_back(P1.Trace[P1.Trace.size() - 1]);
            }

            if (incrementP2)
            {
                P2.Trace.push_back(P2.Trace[P2.Trace.size() - 1]);
            }

            for (int i = P1.Trace.size() - 1; i > 0; i--)
            {
                P1.Trace[i] = P1.Trace[i - 1];
            }
            P1.Trace[0] = newHeadP1;

            for (int i = P2.Trace.size() - 1; i > 0; i--)
            {
                P2.Trace[i] = P2.Trace[i - 1];
            }
            P2.Trace[0] = newHeadP2;

        }

        bool Collides(Math::Vector2I newPosition)
        {
            if (
                newPosition.X < 0
                || newPosition.X >= GridWidth
                || newPosition.Y < 0
                || newPosition.Y >= GridHeight
                )
            {
                return true;
            }

            for (const auto& segment : P1.Trace)
            {
                if (segment == newPosition) { return true; }
            }

            for (const auto& segment : P2.Trace)
            {
                if (segment == newPosition) { return true; }
            }

            return false;
        }

        bool CollidesPickup(Math::Vector2I position)
        {
            for (const auto& pickup : Pickups)
            {
                if (pickup.Position == position) { return true; }
            }

            return false;
        }

        void HandleP1Won()
        {

            Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
            text.Visible = Math::Functions::Fmod(Core::Time::TotalGame(), 1.0) <= 0.5;

            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::PendingStart;
            }
        }

        void HandleP2Won()
        {
            Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
            text.Visible = Math::Functions::Fmod(Core::Time::TotalGame(), 1.0) <= 0.5;
            
            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::PendingStart;
            }
        }

        void HandleDraw()
        {
            Components::TextComponent& text = GameInterruptionTextEntity.GetComponent<Components::TextComponent>();
            text.Visible = Math::Functions::Fmod(Core::Time::TotalGame(), 1.0) <= 0.5;
            
            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                CurrentGameState = GameState::PendingStart;
            }
        }

        void ResetBoard()
        {

            P1 = Player();
            P2 = Player();

            P1.Trace.push_back(P1StartPos);
            P2.Trace.push_back(P2StartPos);

            Pickups.clear();
            ActiveIncrementPickups = 0;

            P1Intent = Direction::Up;
            P2Intent = Direction::Down;


        }


        void OnImGuiUpdate() override
        {
        }

        void OnDraw() override
        {
            const auto& [windowWidth, windowHeight] = Window->GetDimensions();

            Graphics::Renderer::SetProjectionMatrix(Math::Matrix4::OrthographicProjection(0, windowWidth, 0, windowHeight));
            Graphics::Renderer::SetViewMatrix(Math::Matrix4::Identity());

            float fieldWidth;
            float fieldHeight;

            if (windowWidth >= AspectRatio * Math::Functions::Max(0.0f, (windowHeight - PaddingTop - PaddingBottom))) // window is limited by height
            {
                fieldHeight = windowHeight - PaddingTop - PaddingBottom;
                fieldWidth = fieldHeight * AspectRatio;
            }
            else // limited by width
            {
                fieldWidth = windowWidth;
                fieldHeight = windowWidth / AspectRatio;
            }

            const float fieldOffsetLeft = (windowWidth - fieldWidth) / 2.0f;
            const float fieldOffsetBottom = PaddingBottom;

            const float cellWidth = fieldWidth / (float)GridWidth;

            // Draw Field
            Graphics::Renderer::DrawRectangle(
                { fieldOffsetLeft, fieldOffsetBottom, -0.1f },
                { fieldWidth, fieldHeight },
                { 0.05, 0.1, 0.15, 1.0 }
            );

            // Draw Pickups

            for (const auto pickup : Pickups)
            {
                Math::Vector4 color(0.0, 0.0, 0.0, 1.0);

                if (pickup.AssociatedType == Pickup::Type::Increment)
                {
                    color = { 0.9, 0.9, 1.0, 1.0 };
                }
                else if (pickup.AssociatedType == Pickup::Type::Speedup)
                {
                    color = { 0.9, 0.2, 0.2, 1.0 };
                }

                color *= (0.8 + Math::Functions::Sin(Core::Time::TotalGame() * 4) * 0.2);
                color.A = 1.0f;

                Graphics::Renderer::DrawRectangle(
                    { fieldOffsetLeft + cellWidth * pickup.Position.X, fieldOffsetBottom + cellWidth * pickup.Position.Y, 0.0f },
                    { cellWidth, cellWidth },
                    color
                );
            }

            // Draw Players

            bool head = true;
            for (const auto segment : P1.Trace)
            {
                Math::Vector4 color(0.1, 0.680, 0.783, 1.0);

                if (head)
                {
                    head = false;
                    color = { 0.2, 0.9, 1.0, 1.0 };
                }

                Graphics::Renderer::DrawRectangle(
                    { fieldOffsetLeft + cellWidth * segment.X, fieldOffsetBottom + cellWidth * segment.Y, 0.0f },
                    { cellWidth, cellWidth },
                    color
                );
            }

            head = true;

            for (const auto segment : P2.Trace)
            {
                Math::Vector4 color(0.783, 0.680, 0.1, 1.0);

                if (head)
                {
                    head = false;
                    color = { 1.0, 0.9, 0.2, 1.0 };
                }

                Graphics::Renderer::DrawRectangle(
                    { fieldOffsetLeft + cellWidth * segment.X, fieldOffsetBottom + cellWidth * segment.Y, 0.0f },
                    { cellWidth, cellWidth },
                    color
                );
            }


            Graphics::Renderer::FlushRectangleBuffer(); // Needs to be here becuase draw text flushes (9 characters > 8)
        }
    };
} // namespace Tron
#pragma once

#include "Tesseract/Tesseract.h"

using namespace Tesseract;

namespace Tron
{
    class StartGameTextController : public Tesseract::Scripting::Script
    {
        Core::Window* Window;

        void OnAttach() override
        {
            Window = Core::Application::GetApplication()->GetWindow();
            Window->SetTitle("Pong! Menu on Tesseract");
        }

        void OnUpdate() override
        {
            const auto& [windowWidth, windowHeight] = Window->GetDimensions();
            Transform().Position.X = windowWidth / 2;
            Transform().Position.Y = windowHeight / 2;

            Entity.GetComponent<Components::TextComponent>().Visible = Math::Functions::Fmod(Core::Time::TotalGame(), 1.0f) < 0.5;

            if (IO::Input::KeyPressed(IO::KeyCode::_Space))
            {
                Core::Application::GetApplication()->LoadSceneLater("tron/game");
            }
        }
    };
} // namespace Tron
#pragma once

#include "Tesseract/Tesseract.h"

using namespace Tesseract;

namespace Tron
{
    class TitleTextController : public Tesseract::Scripting::Script
    {
        Core::Window* Window;

        void OnAttach() override
        {
            Window = Core::Application::GetApplication()->GetWindow();
        }

        void OnUpdate() override
        {
            const auto&[windowWidth, windowHeight] = Window->GetDimensions();
            Transform().Position.X = windowWidth / 2 ;
            Transform().Position.Y = windowHeight * 3 / 4 + (Math::Functions::Sin(Core::Time::TotalGame() / 4) * windowHeight / 30);
        }
    };
} // namespace Tron
#define TESSERACT_ENTRY_POINT
#include "Tesseract/Tesseract.h"

#include "GameApplication.h"

using namespace Tesseract;

Core::Application* GetApplication()
{
    return new Game::GameApplication;
}
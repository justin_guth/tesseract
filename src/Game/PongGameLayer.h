#include "Tesseract/Tesseract.h"

namespace Game
{
    class PongGameLayer : public Tesseract::Core::Layer
    {
        inline void OnAttach() override
        {
            TESSERACT_CLIENT_INFO("Pushed Pong Game Layer onto the layer stack!");

        }
    };
} // namespace Game
